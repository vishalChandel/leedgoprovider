package com.leedgoprovider.app.retrofit;


import com.google.gson.JsonObject;
import com.leedgoprovider.app.models.AddLocationModel;
import com.leedgoprovider.app.models.AddModel;
import com.leedgoprovider.app.models.AddProviderLocationModel;
import com.leedgoprovider.app.models.AddScreenCheckModel;
import com.leedgoprovider.app.models.BidModel;
import com.leedgoprovider.app.models.ChangePwdModel;
import com.leedgoprovider.app.models.ChatMessageModel;
import com.leedgoprovider.app.models.ChatModel;
import com.leedgoprovider.app.models.CheckModel;
import com.leedgoprovider.app.models.CompleteTaskModel;
import com.leedgoprovider.app.models.DeleteLocationModel;
import com.leedgoprovider.app.models.EditLocationModel;
import com.leedgoprovider.app.models.EditServiceDetailModel;
import com.leedgoprovider.app.models.ForgotPwdModel;
import com.leedgoprovider.app.models.InRequestModel;
import com.leedgoprovider.app.models.NotificationModel;
import com.leedgoprovider.app.models.ProviderAccountDetailModel;
import com.leedgoprovider.app.models.ProviderAccountModel;
import com.leedgoprovider.app.models.ProviderCompleteTaskModel;
import com.leedgoprovider.app.models.RequestModel;
import com.leedgoprovider.app.models.RequestTaskModel;
import com.leedgoprovider.app.models.RoomCreateModel;
import com.leedgoprovider.app.models.ServicesModel;
import com.leedgoprovider.app.models.SignInModel;
import com.leedgoprovider.app.models.SignUpModel;
import com.leedgoprovider.app.models.SignupCompleteModel;
import com.leedgoprovider.app.models.StatesModel;
import com.leedgoprovider.app.models.StripeModel;
import com.leedgoprovider.app.models.TaskDetailsModel;
import com.leedgoprovider.app.models.TicketListModel;
import com.leedgoprovider.app.models.TransactionHistoryModel;
import com.leedgoprovider.app.models.TransactionModel;
import com.leedgoprovider.app.models.UpcomingTaskModel;
import com.leedgoprovider.app.models.UploadDocumentModel;
import com.leedgoprovider.app.models.locationlatlong.PlaceLatLongModel;
import com.leedgoprovider.app.models.locations.SearchLocationModel;
import com.leedgoprovider.app.models.profile.ProfileModel;

import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Url;

public interface ApiInterface {


    @POST("Provider_SignUp.php")
    Call<SignUpModel> signUpRequest(@Body Map<String, String> mParams);

    @POST("ProviderLogin.php")
    Call<SignInModel> signInRequest(@Body Map<String, String> mParams);

    @POST("GetProviderCompleteTaskList.php")
    Call<ProviderCompleteTaskModel> providerCompleteTask(@Body Map<String, String> mParams);


    @POST("ForgetProviderPassword.php")
    Call<ForgotPwdModel> forgotPasswordRequest(@Body Map<String, String> mParams);

    @GET("Getstatename.php")
    Call<StatesModel> statesRequest();

    @GET
    Call<SearchLocationModel> getSearchPlacesRequest(@Url String mUrl);

    @GET
    Call<PlaceLatLongModel> getLatLongFromPlaceId(@Url String mUrl);

    @GET("GetCategory.php")
    Call<ServicesModel> getServicesRequest();

    @POST("AddServiceDetails.php")
    Call<SignupCompleteModel> submitSignUpDetailsRequest(@Body Map<String, String> mParams);


    @POST("UpdateProviderPassword.php")
    Call<ChangePwdModel> changePasswordRequest(@Body Map<String, String> mParams);

    @POST("GetProviderProfileDetails.php")
    Call<ProfileModel> getProviderDataRequest(@Body Map<String, String> mParams);

    @POST("EditProviderProfile.php")
    Call<ProfileModel> updateProviderProfileRequest(@Body Map<String, String> mParams);


    @POST("CustomerSupport.php")
    Call<ProfileModel> customerSupportRequest(@Body Map<String, String> mParams);

    @POST("GetCategory.php")
    Call<AddModel> executeAddData(@Body Map<String, String> mUpdateProfileParam);

    @POST("GetProviderBusinessDetails.php")
    Call<AddScreenCheckModel> executeCheckAddData(@Body Map<String, String> mUpdateProfileParam);

    @POST("GetProviderLocations.php")
    Call<AddLocationModel> executeLocationData(@Body Map<String, String> mParam);

    @POST("EditServiceDetails.php")
    Call<EditServiceDetailModel> executeEditServices(@Body Map<String, String> mParam);

    @POST("DeleteProviderLocation.php")
    Call<DeleteLocationModel> executeDeleteLocation(@Body Map<String, String> mParam);

    @POST("AddProviderLocation.php")
    Call<AddProviderLocationModel> executeAddLocationData(@Body Map<String, String> mParam);

    @POST("EditProviderLocation.php")
    Call<EditLocationModel> executeEditLocationData(@Body Map<String, String> mParamEdit);

    @POST("Provider_Google_SignUp.php")
    Call<SignInModel> signInGoogleRequest(@Body Map<String, String> mParams2);

    @POST("Provider_FB_SignUp.php")
    Call<SignInModel> signInFbRequest(@Body Map<String, String> mParams3);

    @POST("EditProviderBankDetails.php")
    Call<EditServiceDetailModel> executeUpdateBankdetails(@Body Map<String, String> mParam);

    @POST("NotificationActivity.php")
    Call<NotificationModel> executeNotificationList(@Body Map<String, String> mParam);

    @POST("GetProviderInprogressTaskList.php")
    Call<JsonObject> executeProgressData(@Body Map<String, String> mParams);

    @POST("GetRecentRequests.php")
    Call<InRequestModel> executeRequestsData(@Body Map<String, String> mUpdateProfileParam);

    @POST("AddBidByProvider.php")
    Call<BidModel> executeBid(@Body Map<String, String> mUpdateProfileParam);

    @POST("GetProviderUpcomingTaskList.php")
    Call<UpcomingTaskModel> executeUpcomingData(@Body Map<String, String> mUpdateProfileParam);

    @POST("UpdateProviderTaskDate.php")
    Call<RequestModel> executeUpdateTaskDate(@Body Map<String, String> mParam);

    @Multipart
    @POST("Chat/AddMessage.php")
    Call<JsonObject> addUpdateChatMessage(
            @Part("room_id") RequestBody client_id,
            @Part("sender_type") RequestBody category_id,
            @Part("message") RequestBody subcategory_id,
            @Part MultipartBody.Part image
    );

    @Multipart
    @POST("stripe/UploadDocument.php")
    Call<UploadDocumentModel> uploadDocument(
            @Part("image_type") RequestBody imageType,
            @Part("provider_id") RequestBody providerId,
            @Part MultipartBody.Part image
    );

    @Multipart
    @POST("stripe/CreateStripeAccount.php")
    Call<StripeModel> stripeAccount(
            @Part("provider_id") RequestBody imageType,
            @Part("first_name") RequestBody firstName,
            @Part("last_name") RequestBody lastName,
            @Part("city") RequestBody city,
            @Part("country") RequestBody country,
            @Part("line1") RequestBody line1,
            @Part("line2") RequestBody line2,
            @Part("postal_code") RequestBody postalCode,
            @Part("state") RequestBody state,
            @Part("dob") RequestBody dob,
            @Part("id_number") RequestBody idNumber,
            @Part("phone") RequestBody phone,
            @Part("ssn_last_4") RequestBody ssnLast,
            @Part("back") RequestBody back,
            @Part("front") RequestBody front,
            @Part("account_holder_name") RequestBody holderName,
            @Part("routing_number") RequestBody routingNumber,
            @Part("account_number") RequestBody accountNumber
    );


    @POST("Chat/GetChatListing.php")
    Call<ChatMessageModel> chatGroupListRequest(@Body Map<String, String> mParam);

    @POST("Chat/GetChatMembersList.php")
    Call<ChatModel> getAllChats(@Body Map<String, String> mParams);

    @POST("Chat/RequestChat.php")
    Call<RoomCreateModel> createRoom(@Body Map<String, String> mParam);

    @POST("CustomerSupportList.php")
    Call<TicketListModel> getTicketList(@Body Map<String, String> mParams);

    @POST("GetProvidersTaskList.php")
    Call<RequestTaskModel> getAllTasks(@Body Map<String, String> mParam);

    @POST("AddSupportMessage.php")
    Call<JsonObject> chatSupportSendMessage(@Body Map<String, String> mParamt);

    @POST("GetSupportMessageList.php")
    Call<JsonObject> chatGroupSupport(@Body Map<String, String> mParam);

    @POST("EnableDisableNotification.php")
    Call<CheckModel> getNotification(@Body Map<String, String> mParam2);

    @POST("GetTaskDetailByTaskId.php")
    Call<TaskDetailsModel> getRequesDetails(@Body Map<String, String> mParam);

    @POST("UpdateTaskStatusByProvider.php")
    Call<CheckModel> getUpdateStatus(@Body Map<String, String> mParam2);

    @POST("stripe/GetProviderAccountDetail.php")
    Call<ProviderAccountDetailModel> getProviderAccountDetail(@Body Map<String, String> mParam3);

    @POST("ProviderTransactionHistory.php")
    Call<TransactionHistoryModel> getTransactionHistory(@Body Map<String, String> mParam3);

    @POST("stripe/GetProviderAccountDetail.php")
    Call<ProviderAccountModel> getProviderDetail(@Body Map<String, String> mParam3);

    @POST("ClientCompletetask.php")
    Call<CompleteTaskModel> completeTask(@Body Map<String, String> mParam3);

    @POST("logOut.php")
    Call<JsonObject> logoutRequest(@Body Map<String, String> mParam);

    @POST("UpdateNotificationRead.php")
    Call<NotificationModel> readNotificationRequest(@Body Map<String, String> mParam2);
}
