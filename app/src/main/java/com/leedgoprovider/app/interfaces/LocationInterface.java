package com.leedgoprovider.app.interfaces;


import com.leedgoprovider.app.models.locations.PredictionsItem;

public interface LocationInterface {
    void getSelectedLocation(PredictionsItem mModel);
}
