package com.leedgoprovider.app.interfaces;

import com.leedgoprovider.app.models.AddScreenCheckModel;
import com.leedgoprovider.app.models.ServicesSubCatData;

import java.util.ArrayList;

public interface EditServiceInterface {
    public void getSelectedServices(ArrayList<AddScreenCheckModel.Service_detail> mArrayList);
}
