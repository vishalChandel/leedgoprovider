package com.leedgoprovider.app.interfaces;

import com.leedgoprovider.app.models.ServicesSubCatData;

import java.util.ArrayList;

public interface ServicesSubCatInterface {
    public void getSubCategoryItem(ServicesSubCatData mModel,boolean b);
}
