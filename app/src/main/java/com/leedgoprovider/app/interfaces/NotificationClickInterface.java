package com.leedgoprovider.app.interfaces;

import com.leedgoprovider.app.models.NotificationModel;

public interface NotificationClickInterface {
    void getNotification(NotificationModel.Data mModel);
}
