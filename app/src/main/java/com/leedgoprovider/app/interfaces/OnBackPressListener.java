package com.leedgoprovider.app.interfaces;

public interface OnBackPressListener {
    public boolean onBackPressed();
}

