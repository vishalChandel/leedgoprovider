package com.leedgoprovider.app.interfaces;

import com.leedgoprovider.app.models.ServicesSubCatData;

import java.util.ArrayList;

public interface ServicesInterface {
    public void getSelectedServices(ArrayList<ServicesSubCatData> mArrayList);
}
