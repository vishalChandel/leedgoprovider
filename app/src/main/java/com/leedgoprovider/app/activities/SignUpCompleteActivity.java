package com.leedgoprovider.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import com.leedgoprovider.app.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpCompleteActivity extends BaseActivity {

    /**
     * Getting the Current Class Name
     */
    String TAG = SignUpCompleteActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = SignUpCompleteActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.btnContinueB)
    Button btnContinueB;



    /*
     * Initialize...
     * */


    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_complete);
        ButterKnife.bind(this);
    }


    /*
     * Widgets Click Listener
     * */
    @OnClick({R.id.btnContinueB})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnContinueB:
                performContinueClick();
                break;

        }
    }

    private void performContinueClick() {
        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
        btnContinueB.startAnimation(myAnim);
        Intent intent = new Intent(mActivity, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {

    }
}