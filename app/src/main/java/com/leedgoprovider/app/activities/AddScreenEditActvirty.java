package com.leedgoprovider.app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.leedgoprovider.app.R;
import com.leedgoprovider.app.adapters.AddScreenAdapter;
import com.leedgoprovider.app.adapters.AddScreenEditAdapter;
import com.leedgoprovider.app.adapters.ServicesAdapter;
import com.leedgoprovider.app.interfaces.ServicesInterface;
import com.leedgoprovider.app.models.AddScreenCheckModel;
import com.leedgoprovider.app.models.EditServiceDetailModel;
import com.leedgoprovider.app.models.ServicesDataModel;
import com.leedgoprovider.app.models.ServicesModel;
import com.leedgoprovider.app.models.ServicesSubCatData;
import com.leedgoprovider.app.models.SignUpStepsModel;
import com.leedgoprovider.app.retrofit.ApiClient;
import com.leedgoprovider.app.retrofit.ApiInterface;
import com.leedgoprovider.app.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddScreenEditActvirty extends BaseActivity {

    /**
     * Getting the Current Class Name
     */
    String TAG = AddScreenEditActvirty.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = AddScreenEditActvirty.this;
    /*
     * Widgets
     * */
    @BindView(R.id.tab1LL)
    LinearLayout tab1LL;
    @BindView(R.id.tab1IV)
    ImageView tab1IV;
    @BindView(R.id.tab2LL)
    LinearLayout tab2LL;
    @BindView(R.id.tab2IV)
    ImageView tab2IV;
    @BindView(R.id.tab3LL)
    LinearLayout tab3LL;
    @BindView(R.id.tab3IV)
    ImageView tab3IV;
    @BindView(R.id.tab4LL)
    LinearLayout tab4LL;
    @BindView(R.id.tab4IV)
    ImageView tab4IV;
    @BindView(R.id.txtHeaderTV)
    TextView txtHeaderTV;
    @BindView(R.id.add_screensave)
    TextView tx_save;
    @BindView(R.id.add_recyclerview_edit)
    RecyclerView recyclerView;
    AddScreenAdapter adapter;
    AddScreenCheckModel model;
    List<AddScreenCheckModel.Service_detail> mArrayList2 = new ArrayList<AddScreenCheckModel.Service_detail>();
  //  SignUpStepsModel mSignUpStepsModel;

    ArrayList<ServicesDataModel> mServicesArrayList = new ArrayList<>();
    ArrayList<ServicesSubCatData> mSelectedServicesAL = new ArrayList<>();

    ArrayList<ServicesSubCatData> servicesSubCatDataAL=new ArrayList<>();
    JSONArray mCatSubCatArray = new JSONArray();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_screen_edit_actvirty);
        ButterKnife.bind(this);
        txtHeaderTV.setText(R.string.edit_service_details);
        performTab2Click();
        exexuteApi();
    }

    private void exexuteApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getServicesRequest().enqueue(new Callback<ServicesModel>() {
            @Override
            public void onResponse(Call<ServicesModel> call, Response<ServicesModel> response) {
                ServicesModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    mServicesArrayList.addAll(response.body().getData());
                    executeCheckapi();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ServicesModel> call, Throwable t) {
               // dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }
    ServicesInterface mServicesInterface = new ServicesInterface() {
        @Override
        public void getSelectedServices(ArrayList<ServicesSubCatData> mArrayList) {
            Log.e("Datam" ,"**Selected SubCategories**"+mArrayList.size());
            mSelectedServicesAL = mArrayList;
            Log.e("Datam" ,"**Selected SubCategories**"+mSelectedServicesAL);
        }
    };
    private void setAdapter() {
        AddScreenEditAdapter mSearchLocationAdapter = new AddScreenEditAdapter(mActivity, mServicesArrayList,(ArrayList<AddScreenCheckModel.Service_detail>) mArrayList2, model,mServicesInterface);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mSearchLocationAdapter);
    }
    /*
     * Execute api update profile
     * */
    private Map<String, String> mUpdateProfileParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("provider_id", getProviderID());
        return mMap;
    }
    private void executeCheckapi() {

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.executeCheckAddData(mUpdateProfileParam()).enqueue(new Callback<AddScreenCheckModel>() {
            @Override
            public void onResponse(Call<AddScreenCheckModel> call, Response<AddScreenCheckModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                Log.e("Dataq","2"+response.body());
                AddScreenCheckModel mModel = response.body();

                if (mModel.getStatus() == 1) {
                    model=response.body().getData();
                    mArrayList2= response.body().getData().getService_detail();
                    Log.e("Dataq","21"+mArrayList2);
                    Log.e("Dataq",""+getProviderID());
                    //   mModel = (AddModel) response.body().getData();
                    // if (mArrayList2!=null){
                    setAdapter();


                    //}

                } else {
                    Log.e("Data","error fetch");
                }
            }

            @Override
            public void onFailure(Call<AddScreenCheckModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("Dataq","3");
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }



    /*
     * Widgets Click Listener
     * */
    @OnClick({R.id.add_screensave, R.id.tab1LL, R.id.tab2LL, R.id.tab3LL, R.id.tab4LL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.add_screensave:
                performSave();
                break;
            case R.id.tab1LL:
                performTab1Click();
                break;
            case R.id.tab2LL:
                performTab2Click();
                break;
            case R.id.tab3LL:
                performTab3Click();
                break;
            case R.id.tab4LL:
                performTab4Click();
                break;
        }
    }

    private void performSave() {
        for (int i=0;i<mServicesArrayList.size();i++)
        {
            Log.e("Datawr","Peoro:::::"+mServicesArrayList.get(i));
        }
     servicesSubCatDataAL.addAll(mSelectedServicesAL);
      //      mSignUpStepsModel.setServicesSubCatDataAL(mSelectedServicesAL);

      //  Toast.makeText(mActivity, ""+mServicesArrayList.size(), Toast.LENGTH_SHORT).show();
    //    //finish();


        //Categories

        try {
            //Getting Categories List Data
            ArrayList<String> mCategoriesAL = new ArrayList<>();
            for (int i = 0; i < servicesSubCatDataAL.size(); i++) {
                mCategoriesAL.add(""+servicesSubCatDataAL.get(i).getScatId());
            }

            Set<String> set = new HashSet<>(mCategoriesAL);
            mCategoriesAL.clear();
            mCategoriesAL.addAll(set);

            for (int k = 0; k < mCategoriesAL.size(); k++){
                JSONObject mObject = new JSONObject();
                mObject.put("category_id", mCategoriesAL.get(k));
                JSONArray mSubCategoriesArray = new JSONArray();
                for (int i = 0; i < servicesSubCatDataAL.size(); i++) {
                    if (mCategoriesAL.get(k).equals(""+servicesSubCatDataAL.get(i).getScatId())){
                        mSubCategoriesArray.put(servicesSubCatDataAL.get(i).getId());
                    }
                }
                mObject.put("sub_cat_ids",mSubCategoriesArray);
                mCatSubCatArray.put(mObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.e("Dataq",""+mCatSubCatArray.toString());
executeEditServiceApi();
//        Intent mIntent = new Intent(mActivity, AddScreenActivity.class);
//        mIntent.putExtra(Constants.MODEL,mSignUpStepsModel);
//        startActivity(mIntent);
    }
    /*
     * Execute api update profile
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("provider_id", getProviderID());
        mMap.put("services",mCatSubCatArray.toString());
        return mMap;
    }
    private void executeEditServiceApi() {
        showProgressDialog(mActivity);
        Log.e("Dataq","eeee::::"+mParam());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.executeEditServices(mParam()).enqueue(new Callback<EditServiceDetailModel>() {
            @Override
            public void onResponse(Call<EditServiceDetailModel> call, Response<EditServiceDetailModel> response) {
                dismissProgressDialog();
                Log.e("Dataq", "WW:::" + response.body().toString());
                Log.e("Dataq","2"+response.body());
                EditServiceDetailModel mModel = response.body();

                if (mModel.getStatus() == 1) {
                   // Toast.makeText(mActivity, "success", Toast.LENGTH_SHORT).show();
               finish();

                    //}

                } else {
                    finish();
                   // showAlertDialog(mActivity,""+response.body().getMessage());
                  //  Toast.makeText(mActivity, "fail"+response.body(), Toast.LENGTH_SHORT).show();
                    Log.e("Data","error fetch");
                }
                Toast.makeText(mActivity, "Services saved successfully", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<EditServiceDetailModel> call, Throwable t) {
                dismissProgressDialog();
                showAlertDialog(mActivity,t.getMessage());
                Log.e("Dataq","3");
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void performTab1Click() {
        tab1IV.setImageResource(R.drawable.ic_home_select);
        tab2IV.setImageResource(R.drawable.ic_add_unselect);
        tab3IV.setImageResource(R.drawable.ic_message_unselect);
        tab4IV.setImageResource(R.drawable.ic_location_unselect);
        switchActivity1(HomeActivity.class);

    }

    private void performTab2Click() {
        tab1IV.setImageResource(R.drawable.ic_home_unselect);
        tab2IV.setImageResource(R.drawable.ic_add_select);
        tab3IV.setImageResource(R.drawable.ic_message_unselect);
        tab4IV.setImageResource(R.drawable.ic_location_unselect);
    }

    private void performTab3Click() {
        tab1IV.setImageResource(R.drawable.ic_home_unselect);
        tab2IV.setImageResource(R.drawable.ic_add_unselect);
        tab3IV.setImageResource(R.drawable.ic_message_select);
        tab4IV.setImageResource(R.drawable.ic_location_unselect);
        switchActivity20(ChatMainActivity.class);
    }

    private void performTab4Click() {
        tab1IV.setImageResource(R.drawable.ic_home_unselect);
        tab2IV.setImageResource(R.drawable.ic_add_unselect);
        tab3IV.setImageResource(R.drawable.ic_message_unselect);
        tab4IV.setImageResource(R.drawable.ic_location_select);
        switchActivity3(AddLocation.class);
    }

    private void switchActivity3(Class<AddLocation> addLocationClass) {
        Intent intent=new Intent(mActivity,addLocationClass);
        startActivity(intent);
    }
    /**Activity Switch
     * **/
    private void switchActivity1(Class<HomeActivity> homeActivityClass) {
        Intent intent=new Intent(mActivity,homeActivityClass);
        startActivity(intent);
    }

    private void switchActivity20(Class<ChatMainActivity> chatMainActivityClass) {
        Intent intent=new Intent(mActivity,chatMainActivityClass);
        startActivity(intent);
    }
}