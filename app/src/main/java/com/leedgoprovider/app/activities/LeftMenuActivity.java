package com.leedgoprovider.app.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.gson.JsonObject;
import com.leedgoprovider.app.BuildConfig;
import com.leedgoprovider.app.R;
import com.leedgoprovider.app.models.CheckModel;
import com.leedgoprovider.app.models.ProviderAccountDetailModel;
import com.leedgoprovider.app.models.profile.ProfileModel;
import com.leedgoprovider.app.retrofit.ApiClient;
import com.leedgoprovider.app.retrofit.ApiInterface;
import com.leedgoprovider.app.utils.Constants;
import com.leedgoprovider.app.utils.LeedgoProviderPrefrences;
import com.suke.widget.SwitchButton;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LeftMenuActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = LeftMenuActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = LeftMenuActivity.this;
    /*
     * Widgets
     * */
    @BindView(R.id.backRL)
    RelativeLayout backRL;
    @BindView(R.id.imgProfileCIV)
    CircleImageView imgProfileCIV;
    @BindView(R.id.txtUserNameTV)
    TextView txtUserNameTV;
    @BindView(R.id.txtUserEmailTV)
    TextView txtUserEmailTV;
    @BindView(R.id.editProfileLL)
    LinearLayout editProfileLL;
    @BindView(R.id.notificationsLL)
    LinearLayout notificationsLL;
    @BindView(R.id.notificationsSB)
    SwitchButton notificationsSB;
    @BindView(R.id.editPaymentLL)
    LinearLayout editPaymentLL;
    @BindView(R.id.customerSupportLL)
    LinearLayout customerSupportLL;
    @BindView(R.id.aboutLeedGoLL)
    LinearLayout aboutLeedGoLL;
    @BindView(R.id.termServicesLL)
    LinearLayout termServicesLL;
    @BindView(R.id.privacyPolicyLL)
    LinearLayout privacyPolicyLL;
    @BindView(R.id.rateUsLL)
    LinearLayout rateUsLL;
    @BindView(R.id.logoutLL)
    LinearLayout logoutLL;

    @BindView(R.id.transactionHistoryLL)
    LinearLayout transactionHistoryLL;

    String notificationVal;
    /*
     * Initialize...
     *
     *
     *
     * */

    /*
     * Activity Override method
     * #onActivityCreated
     * */

    String providerName = "", providerAddress = "", providerAccountNumber = "", providerSSNNumber = "", providerPhone = "", providerEnable = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_left_menu);
        ButterKnife.bind(this);

        setSavedDataOnWidgets();

        executeProfileDataApi();

        notificationsSB.setOnCheckedChangeListener(new SwitchButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SwitchButton view, boolean isChecked) {
                if (view.isChecked()) {
                    notificationVal = "0";
                    executeNotificationApi();
                } else {
                    notificationVal = "1";
                    executeNotificationApi();
                }
            }
        });
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        executeProfileDataApi();
        //setSavedDataOnWidgets();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void setSavedDataOnWidgets() {

        if (getProviderPicture() != null && getProviderPicture().contains("http")) {
            Glide.with(mActivity)
                    .load(getProviderPicture())
                    .apply(new RequestOptions().placeholder(R.drawable.ic_person).error(R.drawable.ic_person))
                    .into(imgProfileCIV);
        } else {
            imgProfileCIV.setImageResource(R.drawable.ic_person);
        }

        if (getProviderEmail() != null) {
            txtUserEmailTV.setText(getProviderEmail());
        }
        if (getProviderName() != null) {
            txtUserNameTV.setText(getProviderName());
        }

        if (getNotificationVal() != null) {
            if (getNotificationVal().equals("0")) {
                notificationsSB.setChecked(true);
            } else {
                notificationsSB.setChecked(false);
            }
        }
    }

    /*
     * Widgets Click Listener
     * */
    @OnClick({R.id.backRL, R.id.transactionHistoryLL, R.id.editProfileLL, R.id.notificationsLL, R.id.editPaymentLL, R.id.customerSupportLL,
            R.id.aboutLeedGoLL, R.id.termServicesLL, R.id.privacyPolicyLL, R.id.shareAppLL, R.id.rateUsLL, R.id.logoutLL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backRL:
                performCloseClick();
                break;
            case R.id.editProfileLL:
                performEditProfileClick();
                break;
            case R.id.notificationsLL:
                performNotificationsClick();
                break;
            case R.id.editPaymentLL:
                performEditPaymentClick();
                break;
            case R.id.customerSupportLL:
                performCustomerSupportClick();
                break;
            case R.id.transactionHistoryLL:
                performTransactionHistoryClick();
                break;

            case R.id.aboutLeedGoLL:
                performAboutLeedgoClick();
                break;
            case R.id.termServicesLL:
                performTermServicesClick();
                break;
            case R.id.privacyPolicyLL:
                performPrivacyPolicyClick();
                break;
            case R.id.shareAppLL:
                performShareAppClick();
                break;
            case R.id.rateUsLL:
                performRateUsClick();
                break;
            case R.id.logoutLL:
                performLogoutClick();
                break;
        }
    }

    private void performTransactionHistoryClick() {
        //showToast(mActivity,"Coming soon");
        Intent intent = new Intent(mActivity, TransactionHistoryActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();

    }


    private void performCloseClick() {
        finish();
    }

    private void performEditProfileClick() {
        startActivity(new Intent(mActivity, EditProfileActivity.class));
    }

    private void performNotificationsClick() {
        startActivity(new Intent(mActivity, NotificationsActivity.class));
    }

    private void performEditPaymentClick() {

        executeCheckProviderDetailApi();
        // startActivity(new Intent(mActivity, EditPaymentActivity.class));
    }

    private void performCustomerSupportClick() {
        startActivity(new Intent(mActivity, CustomerTicketListMain.class));

    }

    private void performAboutLeedgoClick() {
        Intent mIntent = new Intent(mActivity, OpenLinksActivity.class);
        mIntent.putExtra(Constants.LINK_TYPE, Constants.ABOUT_US);
        startActivity(mIntent);
    }

    private void performTermServicesClick() {
        Intent mIntent = new Intent(mActivity, OpenLinksActivity.class);
        mIntent.putExtra(Constants.LINK_TYPE, Constants.TERMS_SERVICES);
        startActivity(mIntent);
    }

    private void performPrivacyPolicyClick() {
        Intent mIntent = new Intent(mActivity, OpenLinksActivity.class);
        mIntent.putExtra(Constants.LINK_TYPE, Constants.PRIVACY_POLICY);
        startActivity(mIntent);
    }

    private void performShareAppClick() {
        /*Create an ACTION_SEND Intent*/
        Intent intent = new Intent(android.content.Intent.ACTION_SEND);
        /*This will be the actual content you wish you share.*/
        String shareBody = "Hey check out my app at: https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID;
        /*The type of the content is text, obviously.*/
        intent.setType("text/plain");
        /*Applying information Subject and Body.*/
        intent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
        /*Fire!*/
        startActivity(Intent.createChooser(intent, getString(R.string.app_name)));
    }

    private void performRateUsClick() {
        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
        }
    }


    private void performLogoutClick() {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_logout);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView btnOkB = alertDialog.findViewById(R.id.btnOkB);
        TextView btnCancelB = alertDialog.findViewById(R.id.btnCancelB);

        btnCancelB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        btnOkB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                executeLogoutApi();
            }
        });

        alertDialog.show();
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("provider_id", getProviderID());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeProfileDataApi() {
        //  showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getProviderDataRequest(mParam()).enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                try {
                    dismissProgressDialog();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                ProfileModel mProfileModel = response.body();
                if (mProfileModel.getStatus() == 1) {
                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.PROVIDER_ID, mProfileModel.getData().getProviderId());
                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.PROVIDER_NAME, mProfileModel.getData().getProviderName());
                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.BUSINESS_EMAIL, mProfileModel.getData().getBusinessEmail());
                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.PROVIDER_IMAGE, mProfileModel.getData().getProfileImage());
                    setSavedDataOnWidgets();

                } else {

                }
            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam2() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getProviderID());
        mMap.put("user_type", "provider");
        mMap.put("notification_status", notificationVal);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeNotificationApi() {
        backRL.setEnabled(false);
//          showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getNotification(mParam2()).enqueue(new Callback<CheckModel>() {
            @Override
            public void onResponse(Call<CheckModel> call, Response<CheckModel> response) {
//                dismissProgressDialog();
                backRL.setEnabled(true);
                CheckModel model;
                model = response.body();
                if (model.getStatus() == 1) {
                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.ENABLE_PUSH, notificationVal);

                }
            }

            @Override
            public void onFailure(Call<CheckModel> call, Throwable t) {
                backRL.setEnabled(true);
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private Map<String, String> mParam3() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("provider_id", getProviderID());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    /*
     * Execute Forgot Password Api
     * */
    private void executeCheckProviderDetailApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getProviderAccountDetail(mParam3()).enqueue(new Callback<ProviderAccountDetailModel>() {
            @Override
            public void onResponse(Call<ProviderAccountDetailModel> call, Response<ProviderAccountDetailModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                ProviderAccountDetailModel mModel = response.body();
                if (mModel.getStatus() == 0) {
                    startActivity(new Intent(mActivity, EditPaymentActivityN.class));
                } else if (mModel.getStatus() == 1) {

                    if (mModel.getData() != null) {
                        providerAddress = mModel.getData().getLine1() + " " + mModel.getData().getLine2() + " " + mModel.getData().getCity() + " " + mModel.getData().getState() + " " + mModel.getData().getCountry();

                        if (mModel.getData().getBankAccHolderName() != null && !mModel.getData().getBankAccHolderName().equals("")) {
                            providerName = mModel.getData().getBankAccHolderName();
                        }
                        if (mModel.getData().getBankAccNumber() != null && !mModel.getData().getBankAccNumber().equals("")) {
                            providerAccountNumber = mModel.getData().getBankAccNumber();
                        }
                        if (mModel.getData().getSsnLast4() != null && !mModel.getData().getSsnLast4().equals("")) {
                            providerSSNNumber = mModel.getData().getSsnLast4();
                        }
                        if (mModel.getData().getPhone() != null && !mModel.getData().getPhone().equals("")) {
                            providerPhone = mModel.getData().getPhone();
                        }
                        if (mModel.getData().getEnabled() != null && !mModel.getData().getEnabled().equals("")) {
                            providerEnable = mModel.getData().getEnabled();
                        }

                        showDialog();
                    }

                } else if (mModel.getStatus() == 2) {
                    showAlertDialog(mActivity, mModel.getMessage());


                } else if (mModel.getStatus() == 3) {
                    if (mModel.getMessage() != null && !mModel.getMessage().equals("")) {
                        showNextAlertDialog(mActivity, mModel.getMessage());

                    } else {
                        showNextAlertDialog(mActivity, getString(R.string.verification_text));
                    }


                } else if (mModel.getStatus() == 4) {
                    if (mModel.getMessage() != null && !mModel.getMessage().equals("")) {
                        showNextAlertDialog(mActivity, mModel.getMessage());
                    } else {
                        showNextAlertDialog(mActivity, getString(R.string.verification_text));
                    }

                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ProviderAccountDetailModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void showDialog() {

        Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_bank_details);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCancelable(true);
        alertDialog.setCanceledOnTouchOutside(true);
        // set the custom dialog components - text, image and button
        TextView nameTV = alertDialog.findViewById(R.id.nameTV);
        TextView accountNumberTV = alertDialog.findViewById(R.id.accountNumberTV);
        TextView ssnTV = alertDialog.findViewById(R.id.ssnTV);
        TextView addressTV = alertDialog.findViewById(R.id.addressTV);
        TextView phoneTV = alertDialog.findViewById(R.id.phoneTV);
        TextView paymentStatusTV = alertDialog.findViewById(R.id.paymentStatusTV);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        Button btnEditDetails = alertDialog.findViewById(R.id.btnEditDetails);
        nameTV.setText(providerName);
        String lastFourDigits = providerAccountNumber.substring(providerAccountNumber.length() - 4);
        accountNumberTV.setText("XXXXXXXX" + lastFourDigits);
        ssnTV.setText("XXXXX" + providerSSNNumber);
        addressTV.setText(providerAddress);
        phoneTV.setText(providerPhone);
        if (providerEnable.equals("1")) {
            paymentStatusTV.setText(getString(R.string.enabled));
        }
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        btnEditDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                startActivity(new Intent(mActivity, EditPaymentActivityN.class));
            }
        });
        alertDialog.show();
    }

    /*
     *
     * Error Alert Dialog
     * */
    public void showNextAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                startActivity(new Intent(mActivity, EditPaymentActivityN.class));
            }
        });
        alertDialog.show();
    }

    /*
     * Execute logout api for provider
     *
     * type 1 for user
     * type 2 for provider
     *
     * */
    private Map<String, String> mLogoutParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_provider_id", getProviderID());
        mMap.put("type", "2");
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeLogoutApi() {
        backRL.setEnabled(false);
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.logoutRequest(mLogoutParams()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                dismissProgressDialog();
                if (response.body() != null) {
                    try {
                        JSONObject jsonObjectMM = new JSONObject(response.body().toString());

                        if (jsonObjectMM.getString("status").equals("1")) {

                            String reEmail = getRemeberEmail();
                            String rePassword = getRemeberPassword();
                            boolean reRememberCre = IsRememberMe();
                            SharedPreferences preferences = LeedgoProviderPrefrences.getPreferences(Objects.requireNonNull(mActivity));
                            SharedPreferences.Editor editor = preferences.edit();
                            editor.clear();
                            editor.apply();
                            editor.commit();

                            LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.REMEMBER_EMAIL, reEmail);
                            LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.REMEMBER_PASSWORD, rePassword);
                            LeedgoProviderPrefrences.writeBoolean(mActivity, LeedgoProviderPrefrences.REMEMBER_CREDENTIALS, reRememberCre);

                            Intent mIntent = new Intent(mActivity, SignInActivity.class);
                            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(mIntent);
                            finish();

                            LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.REMEMBER_EMAIL, reEmail);
                            LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.REMEMBER_PASSWORD, rePassword);
                            GoogleSignIn.getClient(mActivity,
                                    new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).build()
                            ).signOut();

                        } else {
                            showAlertDialog(mActivity, jsonObjectMM.getString("message"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }
}
