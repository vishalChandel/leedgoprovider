package com.leedgoprovider.app.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.android.gms.common.api.Status;
import com.google.gson.JsonObject;
import com.leedgoprovider.app.R;
import com.leedgoprovider.app.fonts.TextViewRegular;
import com.leedgoprovider.app.models.ChatMessageModel;
import com.leedgoprovider.app.models.SignUpStepsModel;
import com.leedgoprovider.app.models.SignupCompleteModel;
import com.leedgoprovider.app.models.StateData;
import com.leedgoprovider.app.models.UploadDocumentModel;
import com.leedgoprovider.app.retrofit.ApiClient;
import com.leedgoprovider.app.retrofit.ApiInterface;
import com.leedgoprovider.app.utils.Constants;
import com.leedgoprovider.app.utils.LeedgoProviderPrefrences;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.yalantis.ucrop.UCrop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

public class SignUpStep4Activity extends BaseActivity {
    /*
     * Initialize Menifest Permissions:
     * & Camera Gallery Request @params
     * */
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    public String writeCamera = Manifest.permission.CAMERA;
    /**
     * Getting the Current Class Name
     */
    String TAG = SignUpStep4Activity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = SignUpStep4Activity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.editFirstNameET)
    EditText editFirstNameET;
    @BindView(R.id.editLastNameET)
    EditText editLastNameET;
    @BindView(R.id.editDateBirthTV)
    TextView editDateBirthTV;
    @BindView(R.id.editphoneNoET)
    EditText editphoneNoET;
    @BindView(R.id.editstreetET)
    EditText editstreetET;
    @BindView(R.id.editAptNumerET)
    EditText editAptNumerET;
    @BindView(R.id.editcityET)
    EditText editcityET;
    @BindView(R.id.editStateET)
    EditText editStateET;
    @BindView(R.id.editZipCodeET)
    EditText editZipCodeET;
    @BindView(R.id.editIdentificationNumberET)
    EditText editIdentificationNumberET;
    @BindView(R.id.editAccountHolderET)
    EditText editAccountHolderET;
    @BindView(R.id.editRoutingET)
    EditText editRoutingET;
    @BindView(R.id.editaccountNumberET)
    EditText editaccountNumberET;
    @BindView(R.id.editaccountNumbConfirmET)
    EditText editaccountNumbConfirmET;
    @BindView(R.id.uploadFrontImageTV)
    TextView uploadFrontImageTV;
    @BindView(R.id.fileFrontImageTV)
    TextView fileFrontImageTV;
    @BindView(R.id.uploadBackImageTV)
    TextView uploadBackImageTV;
    @BindView(R.id.fileBackImageTV)
    TextView fileBackImageTV;
    @BindView(R.id.btnContinueB)
    Button btnContinueB;
    int front_status = 0, back_status = 0;
    Handler handler = new Handler();
    String StrImageType = "", StrFrontFileId = "", StrBackFileId = "";
    String selectedState = "", ssn_last4 = "";
    String isChecked = "true";
    String isChecked2 = "true";
    String filenEW = "";
    /*
     * Initialize...
     * */
    SignUpStepsModel mSignUpStepsModel;
    ArrayList<StateData> mStatesArrayList = new ArrayList<>();
    boolean isSameBusinessAddress = false;
    boolean isDiffrentBusinessAddress = false;
    boolean isSavePaymentOption = false;
    String mStateName = "";
    String mStateID = "";
    Bitmap mBitmapFrontImage, mBitmapLastImage;
    String currentPhotoPath = "";
    Bitmap selectedImage;

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_step4);
        ButterKnife.bind(this);
           getIntentData();
        uploadFrontImageTV.setEnabled(true);
        uploadBackImageTV.setEnabled(true);
    }


    private void getIntentData() {
        if (getIntent() != null) {
            mSignUpStepsModel = (SignUpStepsModel) getIntent().getSerializableExtra(Constants.MODEL);
            mStatesArrayList = (ArrayList<StateData>) getIntent().getSerializableExtra(Constants.LIST);
            editstreetET.setText(mSignUpStepsModel.getAddressLine1());
            editAptNumerET.setText(mSignUpStepsModel.getAddressLine2());
            editcityET.setText(mSignUpStepsModel.getCity());
            editStateET.setText(mSignUpStepsModel.getState());
            editZipCodeET.setText(mSignUpStepsModel.getZipcode());
            editFirstNameET.setText(mSignUpStepsModel.getBusinessName());
            editLastNameET.setText(mSignUpStepsModel.getBusinessName());
            editphoneNoET.setText(mSignUpStepsModel.getBusinessPhone());
        }
    }


    /*
     * Widgets Click Listener
     * */
    @OnClick({R.id.btnContinueB, R.id.editDateBirthTV, R.id.uploadFrontImageTV, R.id.uploadBackImageTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnContinueB:
                performContinueClick();
                break;
            case R.id.editDateBirthTV:
                showDatePickerDialog(mActivity, editDateBirthTV);
                break;
            case R.id.uploadFrontImageTV:
                performFrontUploadImageClick();
                break;

            case R.id.uploadBackImageTV:
                performBackUploadImageClick();
                break;
        }
    }

    private void performBackUploadImageClick() {
        StrImageType = "back";
        if (checkPermission()) {
            openCameraGallery();
        } else {
            requestPermission();
        }
    }

    private void performFrontUploadImageClick() {
        StrImageType = "front";
        if (checkPermission()) {
            openCameraGallery();
        } else {
            requestPermission();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            //val fileUri = data?.data
            if (data != null) {

                Uri mFileUri = data.getData();

                String path = data.getData().getPath();


                showIImage(mFileUri);

            } else if (resultCode == ImagePicker.RESULT_ERROR) {
                Toast.makeText(mActivity, ("Image Not Valid"), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mActivity, "Task Cancelled", Toast.LENGTH_SHORT).show();
            }


        }
    }

    private void openCameraGallery() {
        ImagePicker.Companion.with(this)
                .crop()                    //Crop image(Optional), Check Customization for more option
//                .compress(400)            //Final image size will be less than 1 MB(Optional)
                .maxResultSize(400, 400)    //Final image resolution will be less than 1080 x 1080(Optional)
                .cropSquare()
                .start();
    }


    private void showIImage(Uri imageUri) {
        filenEW = getRealPathFromURI_API19(mActivity, imageUri);
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(filenEW);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        selectedImage = BitmapFactory.decodeStream(inputStream);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        selectedImage.compress(Bitmap.CompressFormat.PNG, 100, out);
        Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
        if (decoded != null) {


            // disable all buttons
            uploadFrontImageTV.setEnabled(false);
            uploadBackImageTV.setEnabled(false);
            //final InputStream imageStream = getContentResolver().openInputStream(decoded);

            if (StrImageType.equals("front")) {
                mBitmapFrontImage = selectedImage;

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while (front_status < 100) {

                            front_status += 5;

                            try {
                                Thread.sleep(300);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {

                                    fileFrontImageTV.setText("Uploading " + String.valueOf(front_status) + ".00%");

                                    if (front_status == 100) {
                                        if (isChecked.equals("true")) {
                                            isChecked = "false";
                                            executeUploadDocumentApi("front", mBitmapFrontImage);
                                        }

                                    }
                                }
                            });
                        }
                    }
                }).start();

            } else if (StrImageType.equals("back")) {
                mBitmapLastImage = selectedImage;

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while (back_status < 100) {

                            back_status += 5;

                            try {
                                Thread.sleep(300);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {

                                    fileBackImageTV.setText("Uploading " + String.valueOf(back_status) + ".00%");

                                    if (back_status == 100) {
                                        if (isChecked2.equals("true")) {
                                            isChecked2 = "false";
                                            executeUploadDocumentApi("back", mBitmapLastImage);
                                        }
                                    }
                                }
                            });
                        }
                    }
                }).start();
            }


        }
    }


    /*********
     * Support for Marshmallows Version
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) CAMERA PERMISSION
     * 2) WRITE_EXTERNAL_STORAGE PERMISSION
     * 3) READ_EXTERNAL_STORAGE PERMISSION
     **********/
    private boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
        int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
        int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
    }


    private void requestPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{writeExternalStorage, writeReadStorage, writeCamera}, 369);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 369:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    openCameraGallery();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }
                break;
        }
    }


    /**
     * Start pick image activity with chooser.
     */
    public void
    onSelectImageClick() {
        CropImage.startPickImageActivity(this);
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
//                .setAspectRatio(200, 100)
                .setMultiTouchEnabled(false)
                .start(this);
    }

//
//    @RequiresApi(api = Build.VERSION_CODES.M)
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        // handle result of pick image chooser
//        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
//            Uri imageUri = CropImage.getPickImageResultUri(this, data);
//
//            // For API >= 23 we need to check specifically that we have permissions to read external storage.
//            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
//                // request permissions and handle the result in onRequestPermissionsResult()
//
//            } else {
//                // no permissions required or already grunted, can start crop image activity
//                startCropImageActivity(imageUri);
//            }
//        }
//
//        // handle result of CropImageActivity
//        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
//            CropImage.ActivityResult result = CropImage.getActivityResult(data);
//            if (resultCode == RESULT_OK) {
//
//                try {
//
//                    // disable all buttons
//                    uploadFrontImageTV.setEnabled(false);
//                    uploadBackImageTV.setEnabled(false);
//                    final InputStream imageStream = getContentResolver().openInputStream(result.getUri());
//
//                    if (StrImageType.equals("front")) {
//                        mBitmapFrontImage = BitmapFactory.decodeStream(imageStream);
//
//                        new Thread(new Runnable() {
//                            @Override
//                            public void run() {
//                                while (front_status < 100) {
//
//                                    front_status += 5;
//
//                                    try {
//                                        Thread.sleep(300);
//                                    } catch (InterruptedException e) {
//                                        e.printStackTrace();
//                                    }
//
//                                    handler.post(new Runnable() {
//                                        @Override
//                                        public void run() {
//
//                                            fileFrontImageTV.setText("Uploading " + String.valueOf(front_status) + ".00%");
//
//                                            if (front_status == 100) {
//                                                if (isChecked.equals("true")) {
//                                                    isChecked = "false";
//                                                    executeUploadDocumentApi("front", mBitmapFrontImage);
//                                                }
//
//                                            }
//                                        }
//                                    });
//                                }
//                            }
//                        }).start();
//
//                    } else if (StrImageType.equals("back")) {
//                        mBitmapLastImage = BitmapFactory.decodeStream(imageStream);
//
//                        new Thread(new Runnable() {
//                            @Override
//                            public void run() {
//                                while (back_status < 100) {
//
//                                    back_status += 5;
//
//                                    try {
//                                        Thread.sleep(300);
//                                    } catch (InterruptedException e) {
//                                        e.printStackTrace();
//                                    }
//
//                                    handler.post(new Runnable() {
//                                        @Override
//                                        public void run() {
//
//                                            fileBackImageTV.setText("Uploading " + String.valueOf(back_status) + ".00%");
//
//                                            if (back_status == 100) {
//                                                if (isChecked2.equals("true")) {
//                                                    isChecked2 = "false";
//                                                    executeUploadDocumentApi("back", mBitmapLastImage);
//                                                }
//                                            }
//                                        }
//                                    });
//                                }
//                            }
//                        }).start();
//                    }
//
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
//            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
//                showToast(mActivity, "Cropping failed: " + result.getError());
//            }
//        }
//
//
//    }

    private void performOpenLinkClick() {
        Intent mIntent = new Intent(mActivity, OpenLinksActivity.class);
        mIntent.putExtra(Constants.LINK_TYPE, Constants.PRIVACY_POLICY);
        startActivity(mIntent);
    }

    private void performContinueClick() {
        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
        btnContinueB.startAnimation(myAnim);
        if (isValidate()) {
            //Execute Submit All SignUp Steps
            if (isNetworkAvailable(mActivity)) {
                executeSubmitApi();
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error));
            }
        }
    }

    private void performSignInClick() {
        Intent intent = new Intent(mActivity, SignInActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    /*
     * Set up validations for fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editFirstNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_first_name));
            flag = false;
        } else if (editLastNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_last_name));
            flag = false;
        } else if (editDateBirthTV.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_dateof_birth));
            flag = false;
        } else if (editphoneNoET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_phone_number));
            flag = false;
        } else if (editstreetET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_street_address));
            flag = false;
        } else if (editAptNumerET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_apt_number));
            flag = false;
        } else if (editcityET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_city_name));
            flag = false;
        } else if (editStateET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_state_name));
            flag = false;
        } else if (editZipCodeET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_zip_code));
            flag = false;
        } else if (editIdentificationNumberET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_identification_number));
            flag = false;
        } else if (editAccountHolderET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_bank_account_holder_name));
            flag = false;
        } else if (editRoutingET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_routing_number));
            flag = false;
        } else if (editaccountNumberET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_account_number));
            flag = false;
        } else if (editaccountNumbConfirmET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_account_number_confirm));
            flag = false;
        } else if (StrFrontFileId.equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_add_front_image));
            flag = false;
        } else if (StrBackFileId.equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_add_back_image));
            flag = false;
        }

        return flag;
    }


//    /*
//     *  States Spinners.....
//     * */
//    private void chooseStateSpinner() {
//        ArrayList<String> mArrayList = new ArrayList<>();
//        for (int i = 0; i < mStatesArrayList.size(); i++) {
//            mArrayList.add(mStatesArrayList.get(i).getAbbr());
//        }
//
//        Typeface font = Typeface.createFromAsset(mActivity.getAssets(), "AvenirMedium.otf");
//        // Spinner click listener
//        statesSpinnerSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
//                // On selecting a spinner item
//                Log.e(TAG, "onItemSelected: " + mArrayList.get(position));
//                Log.e(TAG, "onItemSelected: " + mStatesArrayList.get(position).getAbbr());
//                Log.e(TAG, "onItemSelected: " + mStatesArrayList.get(position).getId());
//                mStateName = mStatesArrayList.get(position).getAbbr();
//                mStateID = mStatesArrayList.get(position).getId();
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parent) {
//            }
//        });
//        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mArrayList) {
//            public View getView(int position, View convertView, android.view.ViewGroup parent) {
//                TextView v = (TextView) super.getView(position, convertView, parent);
//                v.setTypeface(font);
//                v.setTextColor(getResources().getColor(R.color.colorBlack));
//                v.setTextSize(16);
//                return v;
//            }
//
//            public View getDropDownView(final int position, View convertView, android.view.ViewGroup parent) {
//                TextView v = (TextView) super.getView(position, convertView, parent);
//                v.setTypeface(font);
//                v.setTextColor(getResources().getColor(R.color.colorBlack));
//                v.setTextSize(16);
//
//                return v;
//            }
//        };
//
//        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        statesSpinnerSP.setAdapter(adapter1);
//    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        //Services Area
        JSONArray mServiceAreaArray = new JSONArray();
        try {
            for (int i = 0; i < mSignUpStepsModel.getAddServiceArea().size(); i++) {
                JSONObject mObject = new JSONObject();
                mObject.put("city", mSignUpStepsModel.getAddServiceArea().get(i).getCity());
                mObject.put("latitude", mSignUpStepsModel.getAddServiceArea().get(i).getLatitude());
                mObject.put("longitude", mSignUpStepsModel.getAddServiceArea().get(i).getLongitude());
                mServiceAreaArray.put(mObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Business Hours
        JSONArray mBusinessHours = new JSONArray();
        try {
            for (int i = 0; i < 5; i++) {
                JSONObject mObject = new JSONObject();
                mObject.put("day", getDayFormat(i));
                mObject.put("start_time", "9:00 AM");
                mObject.put("end_time", "5:00 PM");
                mBusinessHours.put(mObject);
            }
            if (mSignUpStepsModel.getSaturdayHours() != null && mSignUpStepsModel.getSaturdayHours().length() > 0) {
                String[] mStartEndTime = mSignUpStepsModel.getSaturdayHours().split("-");
                String mStart = mStartEndTime[0];
                String mEnd = mStartEndTime[1];
                JSONObject mObject = new JSONObject();
                mObject.put("day", getDayFormat(5));
                mObject.put("start_time", mStart);
                mObject.put("end_time", mEnd);
                mBusinessHours.put(mObject);
            }
            if (mSignUpStepsModel.getSundayHours() != null && mSignUpStepsModel.getSundayHours().length() > 0) {
                String[] mStartEndTime = mSignUpStepsModel.getSundayHours().split("-");
                String mStart = mStartEndTime[0];
                String mEnd = mStartEndTime[1];
                JSONObject mObject = new JSONObject();
                mObject.put("day", getDayFormat(6));
                mObject.put("start_time", mStart);
                mObject.put("end_time", mEnd);
                mBusinessHours.put(mObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Categories
        JSONArray mCatSubCatArray = new JSONArray();
        try {
            //Getting Categories List Data
            ArrayList<String> mCategoriesAL = new ArrayList<>();
            for (int i = 0; i < mSignUpStepsModel.getServicesSubCatDataAL().size(); i++) {
                mCategoriesAL.add("" + mSignUpStepsModel.getServicesSubCatDataAL().get(i).getScatId());
            }

            Set<String> set = new HashSet<>(mCategoriesAL);
            mCategoriesAL.clear();
            mCategoriesAL.addAll(set);

            for (int k = 0; k < mCategoriesAL.size(); k++) {
                JSONObject mObject = new JSONObject();
                mObject.put("category_id", mCategoriesAL.get(k));
                JSONArray mSubCategoriesArray = new JSONArray();
                for (int i = 0; i < mSignUpStepsModel.getServicesSubCatDataAL().size(); i++) {
                    if (mCategoriesAL.get(k).equals("" + mSignUpStepsModel.getServicesSubCatDataAL().get(i).getScatId())) {
                        mSubCategoriesArray.put(mSignUpStepsModel.getServicesSubCatDataAL().get(i).getId());
                    }
                }
                mObject.put("subcategories", mSubCategoriesArray);
                mCatSubCatArray.put(mObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Map<String, String> mMap = new HashMap<>();
        mMap.put("business_name", String.valueOf(mSignUpStepsModel.getBusinessName()));
        mMap.put("provider_id", String.valueOf(getProviderID()));
        mMap.put("business_phone", String.valueOf(mSignUpStepsModel.getBusinessPhone()));
        mMap.put("licence_number", String.valueOf(mSignUpStepsModel.getBusinessLicenceNo()));
        mMap.put("licence_state", String.valueOf(mSignUpStepsModel.getBusinessLicenceState()));
        mMap.put("work_experince", String.valueOf(mSignUpStepsModel.getBusinessSince()));
        mMap.put("website", String.valueOf(mSignUpStepsModel.getBusinessWebSite()));
        mMap.put("country", String.valueOf(mSignUpStepsModel.getCountry()));
        mMap.put("address1", String.valueOf(mSignUpStepsModel.getAddressLine1()));
        mMap.put("address2", String.valueOf(mSignUpStepsModel.getAddressLine2()));
        mMap.put("city", String.valueOf(mSignUpStepsModel.getCity()));
        mMap.put("state", String.valueOf(mSignUpStepsModel.getState()));
        mMap.put("zip", String.valueOf(mSignUpStepsModel.getZipcode()));
        mMap.put("service_area", String.valueOf(mServiceAreaArray.toString()));
        mMap.put("business_hours", mBusinessHours.toString());
        mMap.put("selected_categories", mCatSubCatArray.toString());
        mMap.put("emergency_service", "" + String.valueOf(mSignUpStepsModel.getEmergencyService()));

        if (editIdentificationNumberET.getText().toString().trim().length() == 4) {
            ssn_last4 = editIdentificationNumberET.getText().toString().trim();
        } else if (editIdentificationNumberET.getText().toString().trim().length() > 4) {
            ssn_last4 = editIdentificationNumberET.getText().toString().trim()
                    .substring(editIdentificationNumberET.getText().toString().trim().length() - 4);
        } else {
            // whatever is appropriate in this case
            throw new IllegalArgumentException("word has less than 4 characters!");
        }
        Log.e(TAG, "**Api**" + ssn_last4);
        mMap.put("first_name", "" + editFirstNameET.getText().toString().trim());
        mMap.put("last_name", "" + editLastNameET.getText().toString().trim());
        mMap.put("dob", "" + editDateBirthTV.getText().toString().trim());
        mMap.put("stripe_city", editcityET.getText().toString().trim());
        mMap.put("stripe_country", "US");
        mMap.put("line1", editstreetET.getText().toString().trim());
        mMap.put("line2", editAptNumerET.getText().toString().trim());
        mMap.put("postal_code", "" + editZipCodeET.getText().toString().trim());
        mMap.put("stripe_state", editStateET.getText().toString().trim());
        mMap.put("stripe_phone", editphoneNoET.getText().toString().trim());
        mMap.put("id_number", "" + editIdentificationNumberET.getText().toString().trim());
        mMap.put("ssn_last_4", "" + ssn_last4);
        mMap.put("back_document", StrBackFileId);
        mMap.put("front_document", StrFrontFileId);
        mMap.put("account_holder_name", "" + editAccountHolderET.getText().toString().trim());
        mMap.put("routing_number", String.valueOf(editRoutingET.getText().toString()));
        mMap.put("account_number", String.valueOf(editaccountNumberET.getText().toString()));

//        if (isSameBusinessAddress == true) {
//            mMap.put("same_as_business_address",String.valueOf(isSameBusinessAddress));
//            mMap.put("billing_country",String.valueOf(mSignUpStepsModel.getCountry()));
//            mMap.put("billing_address1",String.valueOf(mSignUpStepsModel.getAddressLine1()));
//            mMap.put("billing_address2",String.valueOf(mSignUpStepsModel.getAddressLine2()));
//            mMap.put("billing_city",String.valueOf(mSignUpStepsModel.getCity()));
//            mMap.put("billing_state",String.valueOf(mSignUpStepsModel.getState()));
//            mMap.put("billing_zip",String.valueOf(mSignUpStepsModel.getZipcode()));
//        } else {
//            mMap.put("same_as_business_address",String.valueOf(isSameBusinessAddress));
//            mMap.put("billing_country",String.valueOf(txtChooseCountryTV.getText().toString().trim()));
//            mMap.put("billing_address1",String.valueOf(editAddress1ET.getText().toString().trim()));
//            mMap.put("billing_address2",String.valueOf(editAddress2ET.getText().toString().trim()));
//            mMap.put("billing_city",String.valueOf(editCityNameET.getText().toString().trim()));
//            mMap.put("billing_state",String.valueOf(editStateET.getText().toString().trim()));
//            mMap.put("billing_zip",String.valueOf(editZipCodeET.getText().toString().trim()));
//        }


        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }


    private void executeSubmitApi() {
        showProgressDialog(mActivity);
        Log.e("Check", "" + mParam());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.submitSignUpDetailsRequest(mParam()).enqueue(new Callback<SignupCompleteModel>() {
            @Override
            public void onResponse(Call<SignupCompleteModel> call, Response<SignupCompleteModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                SignupCompleteModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showToast(mActivity, mModel.getMessage());
                    Log.e("Check", "qqqqqqqqq" + mModel.getProviderId());
                    LeedgoProviderPrefrences.writeBoolean(mActivity, LeedgoProviderPrefrences.IS_SIGNUP_COMPLETE, true);
                    Intent mIntent = new Intent(mActivity, SignUpCompleteActivity.class);
                    mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(mIntent);
                    finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SignupCompleteModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });

    }


    private void executeUploadDocumentApi(String imgType, Bitmap mBitmapFrontImage) {
        showProgressDialog(mActivity);
        MultipartBody.Part mMultipartBody1 = null;
        if (selectedImage != null) {
            File propertyImageFile = new File(filenEW);
            RequestBody requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
            mMultipartBody1 = MultipartBody.Part.createFormData("image", getAlphaNumericString() + ".jpg", requestFile1);
        }


//        MultipartBody.Part image = null;
//        File imageFile1 = null;
//        if (mBitmapFrontImage != null) {
//            Date date32 = new Date();
//            long time3 = date32.getTime();
//            imageFile1 = getBitmapFile(mBitmapFrontImage, "reduceimage1" + time3);
//            Log.e("Checkio", imageFile1.getPath());
//            RequestBody ppostImage = RequestBody.create(MediaType.parse("multipart/form-data"), imageFile1);
//            image = MultipartBody.Part.createFormData("image", imageFile1.getName(), ppostImage);
//        } else {
//            RequestBody ppostImage = RequestBody.create(MediaType.parse("multipart/form-data"), "");
//            image = MultipartBody.Part.createFormData("image", "", ppostImage);
//        }

        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);

        RequestBody imageTypee = RequestBody.create(MediaType.parse("multipart/form-data"), imgType);
        RequestBody providerIdd = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(getProviderID()));

        Call<UploadDocumentModel> call1 = mApiInterface1.uploadDocument(imageTypee, providerIdd, mMultipartBody1);

        call1.enqueue(new Callback<UploadDocumentModel>() {
            @Override
            public void onResponse(Call<UploadDocumentModel> call, Response<UploadDocumentModel> response) {
                dismissProgressDialog();
                if (response.body() != null) {
                    UploadDocumentModel mModel = response.body();
                    if (StrImageType.equals("front")) {
                        StrFrontFileId = mModel.getFileId();
                        fileFrontImageTV.setText(StrFrontFileId);
                    } else if (StrImageType.equals("back")) {
                        StrBackFileId = mModel.getFileId();
                        fileBackImageTV.setText(StrBackFileId);
                    }
                    uploadFrontImageTV.setEnabled(true);
                    uploadBackImageTV.setEnabled(true);

                    back_status = 0;
                    front_status = 0;
                }

            }

            @Override
            public void onFailure(Call<UploadDocumentModel> call, Throwable t) {
                dismissProgressDialog();
            }
        });

    }

    private File getBitmapFile(Bitmap bmThumbnail_resized, String reduceimage1) {
        File file = new File(Environment.getExternalStorageDirectory() + File.separator + reduceimage1);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bmThumbnail_resized.compress(Bitmap.CompressFormat.JPEG, 100, bos);
        byte[] bitmapdata = bos.toByteArray();
        OutputStream os;
        try {
            file.createNewFile();
            FileOutputStream osw = new FileOutputStream(file);
            osw.write(bitmapdata);
            osw.flush();
            osw.close();
            return file;
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }
        return file;
    }


}