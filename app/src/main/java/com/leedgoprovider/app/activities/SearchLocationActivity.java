package com.leedgoprovider.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.leedgoprovider.app.R;
import com.leedgoprovider.app.adapters.SearchLocationAdapter;
import com.leedgoprovider.app.interfaces.LocationInterface;
import com.leedgoprovider.app.models.ServiceAreaModel;
import com.leedgoprovider.app.models.locationlatlong.Location;
import com.leedgoprovider.app.models.locationlatlong.PlaceLatLongModel;
import com.leedgoprovider.app.models.locations.PredictionsItem;
import com.leedgoprovider.app.models.locations.SearchLocationModel;
import com.leedgoprovider.app.retrofit.ApiClient;
import com.leedgoprovider.app.retrofit.ApiInterface;
import com.leedgoprovider.app.utils.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchLocationActivity extends BaseActivity {

    /**
     * Getting the Current Class Name
     */
    String TAG = SearchLocationActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = SearchLocationActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.editSearchET)
    EditText editSearchET;
    @BindView(R.id.mRecyclerViewRV)
    RecyclerView mRecyclerViewRV;
    @BindView(R.id.backRL)
    RelativeLayout backRL;
    @BindView(R.id.closeRL)
    RelativeLayout closeRL;

    /*
     * Initialize...
     * */
    ArrayList<PredictionsItem> mLocationArrayLists = new ArrayList<>();



    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_location);
        ButterKnife.bind(this);
        searchWatchListner();
    }

    private void searchWatchListner() {
        editSearchET.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.length() > 0) {
                    closeRL.setVisibility(View.VISIBLE);
                    getSearchData();
                } else {
                   try
                   {
                       mLocationArrayLists.clear();
                       setAdapter();
                   }
                   catch (Exception e)
                   {
                       e.printStackTrace();
                   }
                    closeRL.setVisibility(View.GONE);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });


        editSearchET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    getSearchData();
                    return true;
                }
                return false;
            }
        });

    }

    /*
     * Widgets Click Listener
     * */
    @OnClick({R.id.backRL, R.id.closeRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backRL:
                onBackPressed();
                break;
            case R.id.closeRL:
                performCloseClick();
                break;
        }
    }

    private void performCloseClick() {
        editSearchET.setText("");
        mLocationArrayLists.clear();
        setAdapter();
    }

    private void getSearchData() {
        if (isNetworkAvailable(mActivity))
            executeSearchApi();
        else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }

    private void executeSearchApi() {
        mLocationArrayLists.clear();
      //  showProgressDialog(mActivity);
        String mSearhText = editSearchET.getText().toString().trim();
        String mApiUrl = Constants.GOOGLE_PLACES_SEARCH + "input=" + mSearhText  + "&radius=15000" + "&key=" + getString(R.string.google_places_key);

      //  String mApiUrl = Constants.GOOGLE_PLACES_SEARCH + "input=" + mSearhText + "&components=country:us" + "&radius=15000" + "&key=" + getString(R.string.google_places_key);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getSearchPlacesRequest(mApiUrl).enqueue(new Callback<SearchLocationModel>() {
            @Override
            public void onResponse(Call<SearchLocationModel> call, Response<SearchLocationModel> response) {
                dismissProgressDialog();
                if (response.code() == 200) {
                    if (response.body().getPredictions().size() > 0) {
                        mLocationArrayLists.addAll(response.body().getPredictions());
                        setAdapter();
                    } else {
                        showToast(mActivity, response.body().toString());
                    }
                } else if (response.code() == 500) {
                    showToast(mActivity, getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<SearchLocationModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    public void setAdapter() {
        SearchLocationAdapter mSearchLocationAdapter = new SearchLocationAdapter(mActivity, mLocationArrayLists, mLocationInterface);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        mRecyclerViewRV.setLayoutManager(mLayoutManager);
        mRecyclerViewRV.setAdapter(mSearchLocationAdapter);
    }




    LocationInterface mLocationInterface = new LocationInterface() {
        @Override
        public void getSelectedLocation(PredictionsItem mModel) {

            executePlaceLatLongApi(mModel.getPlaceId(), mModel.getDescription(),mModel.getStructuredFormatting().getMainText());
        }
    };
    private void executePlaceLatLongApi(String placeId, String address, String city) {
        showProgressDialog(mActivity);
        String mApiUrl = Constants.GOOGLE_PLACES_LAT_LONG + placeId + "&key=" + getString(R.string.google_places_key);
        Log.e(TAG, "**Google Places Api**" + mApiUrl);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getLatLongFromPlaceId(mApiUrl).enqueue(new Callback<PlaceLatLongModel>() {
            @Override
            public void onResponse(Call<PlaceLatLongModel> call, Response<PlaceLatLongModel> response) {
                dismissProgressDialog();
                if (response.code() == 200) {
                    if (response.body().getResult().getGeometry() != null) {
                        if (response.body().getResult().getGeometry().getLocation() != null) {
                            Location mModel = response.body().getResult().getGeometry().getLocation();
                            ServiceAreaModel mServiceAreaModel = new ServiceAreaModel();
                            mServiceAreaModel.setAddress(address);
                            mServiceAreaModel.setCity(city);
                            mServiceAreaModel.setLatitude("" + mModel.getLat());
                            mServiceAreaModel.setLongitude("" + mModel.getLng());
                            Intent mIntent = new Intent();
                            mIntent.putExtra(Constants.MODEL, mServiceAreaModel);
                            setResult(Constants.REQUEST_CODE, mIntent);
                            finish();
                        }
                    }
                } else if (response.code() == 500) {
                    dismissProgressDialog();
                    showToast(mActivity, getString(R.string.server_error));
                }
            }

            @Override
            public void onFailure(Call<PlaceLatLongModel> call, Throwable t) {
                dismissProgressDialog();
                showToast(mActivity, getString(R.string.some_thing_went_wrong));
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

}