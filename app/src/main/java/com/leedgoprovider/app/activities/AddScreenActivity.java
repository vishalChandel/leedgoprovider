package com.leedgoprovider.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.leedgoprovider.app.R;
import com.leedgoprovider.app.adapters.AddScreenAdapter;
import com.leedgoprovider.app.models.AddModel;
import com.leedgoprovider.app.models.AddScreenCheckModel;
import com.leedgoprovider.app.models.SignUpStepsModel;
import com.leedgoprovider.app.retrofit.ApiClient;
import com.leedgoprovider.app.retrofit.ApiInterface;
import com.leedgoprovider.app.utils.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddScreenActivity extends BaseActivity {

    /**
     * Getting the Current Class Name
     */
    String TAG = AddScreenActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = AddScreenActivity.this;
    /*
     * Widgets
     * */
    @BindView(R.id.tab1LL)
    LinearLayout tab1LL;
    @BindView(R.id.tab1IV)
    ImageView tab1IV;
    @BindView(R.id.tab2LL)
    LinearLayout tab2LL;
    @BindView(R.id.tab2IV)
    ImageView tab2IV;
    @BindView(R.id.tab3LL)
    LinearLayout tab3LL;
    @BindView(R.id.tab3IV)
    ImageView tab3IV;
    @BindView(R.id.tab4LL)
    LinearLayout tab4LL;
    @BindView(R.id.tab4IV)
    ImageView tab4IV;
    @BindView(R.id.txtHeaderTV)
    TextView txtHeaderTV;
    @BindView(R.id.add_screenedit)
    TextView tx_edit;
    @BindView(R.id.add_recyclerview)
    RecyclerView recyclerView;
    AddScreenAdapter adapter;
    SignUpStepsModel mSignUpStepsModel;
    List<AddModel.Data> mArrayList = new ArrayList<AddModel.Data>();
    AddScreenCheckModel model;
    List<AddScreenCheckModel.Service_detail> mArrayList2 = new ArrayList<AddScreenCheckModel.Service_detail>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_screen);
        ButterKnife.bind(this);
        txtHeaderTV.setText(R.string.edit_service_details);
        performTab2Click();
        if (getIntent() != null) {
            mSignUpStepsModel = (SignUpStepsModel) getIntent().getSerializableExtra(Constants.MODEL);
        }
    }
    /*
     * Widgets Click Listener
     * */
    @OnClick({R.id.add_screenedit, R.id.tab1LL, R.id.tab2LL, R.id.tab3LL, R.id.tab4LL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.add_screenedit:
                performClick();
                break;
            case R.id.tab1LL:
                performTab1Click();
                break;
            case R.id.tab2LL:
                performTab2Click();
                break;
            case R.id.tab3LL:
                performTab3Click();
                break;
            case R.id.tab4LL:
                performTab4Click();
                break;

        }
    }

    private void performClick() {
        Intent intent = new Intent(this, AddScreenEditActvirty.class);
        startActivity(intent);

    }

    @Override
    protected void onResume() {
        super.onResume();
        exexuteApi();
    }

    /*
     * Execute api update profile
     * */
    private Map<String, String> mUpdateProfileParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("provider_id", getProviderID());
        return mMap;
    }

    private void exexuteApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.executeAddData(mUpdateProfileParam()).enqueue(new Callback<AddModel>() {
            @Override
            public void onResponse(Call<AddModel> call, Response<AddModel> response) {

                Log.e(TAG, "RESPONSE" + response.body().toString());
                AddModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    mArrayList = response.body().getData();
                    if (mArrayList != null) {
                        executeCheckapi();
                    }
                } else {
                    Log.e("Data", "error fetch");
                }
            }

            @Override
            public void onFailure(Call<AddModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void executeCheckapi() {

        Log.e("Dataq", "1");
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.executeCheckAddData(mUpdateProfileParam()).enqueue(new Callback<AddScreenCheckModel>() {
            @Override
            public void onResponse(Call<AddScreenCheckModel> call, Response<AddScreenCheckModel> response) {

                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                Log.e("Dataq", "2" + response.body());
                AddScreenCheckModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    model = response.body().getData();
                    mArrayList2 = response.body().getData().getService_detail();
                    Log.e("Dataq", "21" + mArrayList2);
                    Log.e("Dataq", "" + getProviderID());
                    setAdapter();

                } else {
                    Log.e("Data", "error fetch");
                }

            }

            @Override
            public void onFailure(Call<AddScreenCheckModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("Dataq", "3");
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    //    private void setAdapter() {
//        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
//        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//        recyclerView.setLayoutManager(layoutManager);
//        Log.e("Data"+mArrayList,"Data2:"+mArrayList2);
//        adapter = new AddScreenAdapter(mActivity, (ArrayList<AddModel.Data>) mArrayList, (ArrayList<AddScreenCheckModel.Service_detail>) mArrayList2,model);
//        recyclerView.setAdapter(adapter);
//    }
    private void setAdapter() {
        adapter = new AddScreenAdapter(mActivity, (ArrayList<AddModel.Data>) mArrayList, (ArrayList<AddScreenCheckModel.Service_detail>) mArrayList2, model);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        dismissProgressDialog();

    }

    private void performTab1Click() {
        tab1IV.setImageResource(R.drawable.ic_home_select);
        tab2IV.setImageResource(R.drawable.ic_add_unselect);
        tab3IV.setImageResource(R.drawable.ic_message_unselect);
        tab4IV.setImageResource(R.drawable.ic_location_unselect);
        switchActivity1(HomeActivity.class);
        finish();
    }

    private void performTab2Click() {
        tab1IV.setImageResource(R.drawable.ic_home_unselect);
        tab2IV.setImageResource(R.drawable.ic_add_select);
        tab3IV.setImageResource(R.drawable.ic_message_unselect);
        tab4IV.setImageResource(R.drawable.ic_location_unselect);
    }

    private void performTab3Click() {
        tab1IV.setImageResource(R.drawable.ic_home_unselect);
        tab2IV.setImageResource(R.drawable.ic_add_unselect);
        tab3IV.setImageResource(R.drawable.ic_message_select);
        tab4IV.setImageResource(R.drawable.ic_location_unselect);
        switchActivity20(ChatMainActivity.class);
        finish();
    }

    private void performTab4Click() {
        tab1IV.setImageResource(R.drawable.ic_home_unselect);
        tab2IV.setImageResource(R.drawable.ic_add_unselect);
        tab3IV.setImageResource(R.drawable.ic_message_unselect);
        tab4IV.setImageResource(R.drawable.ic_location_select);
        switchActivity3(AddLocation.class);
        finish();
    }

    /**
     * Activity Switch
     **/
    private void switchActivity3(Class<AddLocation> addLocationClass) {
        Intent intent = new Intent(mActivity, addLocationClass);
        startActivity(intent);
    }

    private void switchActivity1(Class<HomeActivity> homeActivityClass) {
        Intent intent = new Intent(mActivity, homeActivityClass);
        startActivity(intent);
    }

    private void switchActivity20(Class<ChatMainActivity> chatMainActivityClass) {
        Intent intent = new Intent(mActivity, chatMainActivityClass);
        startActivity(intent);
    }
}