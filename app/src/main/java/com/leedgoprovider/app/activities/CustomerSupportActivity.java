package com.leedgoprovider.app.activities;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.leedgoprovider.app.R;
import com.leedgoprovider.app.adapters.TicketListAdapter;
import com.leedgoprovider.app.models.ForgotPwdModel;
import com.leedgoprovider.app.models.RequestTaskModel;
import com.leedgoprovider.app.models.ServicesDataModel;
import com.leedgoprovider.app.models.ServicesModel;
import com.leedgoprovider.app.models.profile.ProfileModel;
import com.leedgoprovider.app.retrofit.ApiClient;
import com.leedgoprovider.app.retrofit.ApiInterface;
import com.leedgoprovider.app.utils.ImageUtil;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerSupportActivity extends BaseActivity {
    /*
     * Initialize Menifest Permissions:
     * & Camera Gallery Request @params
     * */
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    public String writeCamera = Manifest.permission.CAMERA;
    /**
     * Getting the Current Class Name
     */
    String TAG = CustomerSupportActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = CustomerSupportActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.backRL)
    RelativeLayout backRL;
    @BindView(R.id.txtHeaderTV)
    TextView txtHeaderTV;
    @BindView(R.id.spinnerServicesSP)
    Spinner spinnerServicesSP;
    @BindView(R.id.editSubjectET)
    EditText editSubjectET;
    @BindView(R.id.editDescriptionET)
    EditText editDescriptionET;
    @BindView(R.id.btnChooseFile1B)
    Button btnChooseFile1B;
    @BindView(R.id.txtChooseFile1TV)
    TextView txtChooseFile1TV;
    @BindView(R.id.btnChooseFile2B)
    Button btnChooseFile2B;
    @BindView(R.id.txtChooseFile2TV)
    TextView txtChooseFile2TV;
    @BindView(R.id.btnSupportTicketB)
    Button btnSupportTicketB;
    @BindView(R.id.llyServiceRepairs)
    LinearLayout llyServiceRepairs;
    @BindView(R.id.txServiceName)
    TextView txServiceName;

    Dialog alertDialog;
    ArrayList<RequestTaskModel.Data> mArrayList2;
    RecyclerView recyclerView;
    TextView txNoTickets,txHeading;

    /*
     * Initialize...Objects...
     * */
    ArrayList<ServicesDataModel> mServicesArrayList = new ArrayList<>();
    String mServiceName = "";
    String mServiceID = "";
    Bitmap mBitmap1 = null;
    Bitmap mBitmap2 = null;
    boolean isChoosefile1 = false;
    boolean isChoosefile2 = false;
    String serviceValue;
    String taskId;

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_support);
        ButterKnife.bind(this);
        setToolbarText(txtHeaderTV, getString(R.string.customer_support));

        setEditTextScrollView();
        if (getIntent() != null) {
            if (getIntent().getStringExtra("servicename") != null) {
                serviceValue = getIntent().getStringExtra("servicename");
                txServiceName.setText(serviceValue);
                taskId = getIntent().getStringExtra("task_id");

            }

        }

    }

    private void setEditTextScrollView() {
        editDescriptionET.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                if (editDescriptionET.hasFocus()) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_SCROLL:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            return true;
                    }
                }
                return false;
            }
        });
    }

    /*
     * Widgets Click Listener
     * */
    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.backRL, R.id.btnChooseFile1B, R.id.btnChooseFile2B, R.id.btnSupportTicketB, R.id.llyServiceRepairs})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backRL:
                onBackPressed();
                break;
            case R.id.btnChooseFile1B:
                performChooseFile1Click();
                break;
            case R.id.btnChooseFile2B:
                performChooseFile2Click();
                break;
            case R.id.btnSupportTicketB:
                performSupportTicketClick();
                break;
            case R.id.llyServiceRepairs:
                performIntent();
                break;
        }
    }

    private void performIntent() {
        showAlertPDialog(mActivity);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void performChooseFile1Click() {
        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
        btnChooseFile1B.startAnimation(myAnim);
        isChoosefile1 = true;
        if (checkPermission()) {
            onSelectImageClick();
        } else {
            requestPermission();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void performChooseFile2Click() {
        isChoosefile2 = true;
        if (checkPermission()) {
            onSelectImageClick();
        } else {
            requestPermission();
        }
    }

    private void performSupportTicketClick() {
        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
       btnSupportTicketB.startAnimation(myAnim);
        if (isValidate()) {
            if (isNetworkAvailable(mActivity)) {
                if (!CustomerTicketListMain.mData.contains(taskId)) {
                    executeSupportTicketApi();
                } else {
                    showAlertDialog(mActivity, "Can't add query to same Ticket");
                }
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error));
            }
        }
    }










    /*
     * Set up validations for fields
     * */
    private boolean isValidate() {
        boolean flag = true;

        if (txServiceName.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity,"Please select task");
            flag = false;
        } else if (editDescriptionET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity,getString(R.string.please_enter_descriptions));
            flag = false;
        }
        return flag;
    }


    /*
     * Execute api @param
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getProviderID());
        mMap.put("task_id", taskId);
        mMap.put("role", "provider");

        mMap.put("description", editDescriptionET.getText().toString().trim());
        mMap.put("fileattachment1", ImageUtil.convert(mBitmap1));

        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    /*
     * Execute Forgot Password Api
     * */
    private void executeSupportTicketApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.customerSupportRequest(mParam()).enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                dismissProgressDialog();
                ProfileModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    startActivity(new Intent(mActivity,TicketSubmitted2.class));
                } else{
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {
                dismissProgressDialog();
            }
        });
    }





    /*
     * Update Profile Picture
     * */
    private boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
        int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
        int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void requestPermission() {
        requestPermissions(new String[]{writeExternalStorage, writeReadStorage, writeCamera}, 369);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 369:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    onSelectImageClick();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }

                break;
        }
    }

    /**
     * Start pick image activity with chooser.
     */
    private void onSelectImageClick() {
        CropImage.startPickImageActivity(mActivity);
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.OFF)
                .setAspectRatio(50, 50)
                .setRequestedSize(120, 120)
                .setMultiTouchEnabled(false)
                .start(mActivity);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(mActivity, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(mActivity, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()

            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                try {
                    final InputStream imageStream = getContentResolver().openInputStream(result.getUri());
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.PNG, 40, out);
                    if (isChoosefile1 == true){
                        mBitmap1 = selectedImage;
                        txtChooseFile1TV.setText(result.getUri().toString());
                        isChoosefile1 = false;
                    }else if (isChoosefile2 == true){
                        mBitmap2 = selectedImage;
                        txtChooseFile2TV.setText(result.getUri().toString());
                        isChoosefile2 = false;
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                showToast(mActivity, "  failed: " + result.getError());
            }
        }
    }


    /*
     *
     * Error Alert Dialog
     * */
    @SuppressLint("SetTextI18n")
    public void showAlertPDialog(Activity mActivity) {
        alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_ticket_list);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setGravity(Gravity.TOP);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ImageView imClose = alertDialog.findViewById(R.id.imClose);
        recyclerView = alertDialog.findViewById(R.id.recyclerViewRv);
        txNoTickets = alertDialog.findViewById(R.id.txNoTickets);
        txHeading = alertDialog.findViewById(R.id.textHeading);
        executeCompletedApi();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                txHeading.setVisibility(View.VISIBLE);
                imClose.setVisibility(View.VISIBLE);
            }
        }, 1500);
        // set the custom dialog components - text, image and button


        imClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParamq() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("provider_id", getProviderID());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeCompletedApi() {

        // showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getAllTasks(mParamq()).enqueue(new Callback<RequestTaskModel>() {
            @Override
            public void onResponse(Call<RequestTaskModel> call, Response<RequestTaskModel> response) {
                // txHeading.setVisibility(View.VISIBLE);
                //dismissProgressDialog();
                try {
                    Log.e(TAG, "**RESPONSE**" + mParam().toString() + ":::::" + response.body().toString());
                    RequestTaskModel mModel = null;
                    mModel = response.body();
                    if (mModel.getStatus() == 1) {

                        if (mArrayList2 != null) {
                            mArrayList2.clear();
                        }
                        mArrayList2 = (ArrayList<RequestTaskModel.Data>) mModel.getData();

                        setAdapter2(mArrayList2);

                        try {
                            txNoTickets.setVisibility(View.GONE);
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }

                    }
                    else {
                        try {
                            txNoTickets.setVisibility(View.VISIBLE);
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }


                    }

                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<RequestTaskModel> call, Throwable t) {
                //  dismissProgressDialog();
                showToast(mActivity, t.getMessage());
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void setAdapter2(ArrayList<RequestTaskModel.Data> data) {
        try {


            LinearLayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
            TicketListAdapter mAdapter = new TicketListAdapter(mActivity, (ArrayList<RequestTaskModel.Data>) data);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setAdapter(mAdapter);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


    }



}