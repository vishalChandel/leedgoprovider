package com.leedgoprovider.app.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.leedgoprovider.app.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TicketSubmitted2 extends AppCompatActivity {

    @BindView(R.id.btnContinueB)
    Button btContinue;
    @BindView(R.id.order_text)
    TextView txOrder;

    String serviceType="";


    Activity mActivity=TicketSubmitted2.this;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_submitted2);
        ButterKnife.bind(this);
    }
    @OnClick({R.id.btnContinueB})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnContinueB:
                Goto();
                break;
            default:
                break;
        }
    }
    private void Goto() {
        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
        btContinue.startAnimation(myAnim);

        Intent intent=new Intent(mActivity,CustomerTicketListMain.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }
}