package com.leedgoprovider.app.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.leedgoprovider.app.R;
import com.leedgoprovider.app.models.AddScreenCheckModel;
import com.leedgoprovider.app.models.EditServiceDetailModel;
import com.leedgoprovider.app.retrofit.ApiClient;
import com.leedgoprovider.app.retrofit.ApiInterface;
import com.leedgoprovider.app.utils.Constants;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditPaymentActivity extends BaseActivity {

    /**
     * Current Activity Instance
     */
    Activity mActivity = EditPaymentActivity.this;

    @BindView(R.id.txtHeaderTV)
    TextView txtHeaderTV;
    @BindView(R.id.editBankNameET)
    EditText ed_bank_name;
    @BindView(R.id.editRoutingNumET)
    EditText ed_routing_name;
    @BindView(R.id.editAccountNoET)
    EditText ed_accountno;
    @BindView(R.id.editReAccountNoET)
    EditText ed_reaccountno;
    @BindView(R.id.txtChooseCountryTV)
    EditText ed_billing_country;
    @BindView(R.id.editAddress1ET)
    EditText ed_billing_address1;
    @BindView(R.id.editAddress2ET)
    EditText ed_billing_address2;
    @BindView(R.id.editCityNameET)
    EditText ed_billing_city;
    @BindView(R.id.editStateEdT)
    EditText ed_billing_state;
    @BindView(R.id.editZipCodeET)
    EditText ed_billing_zipcode;
    @BindView(R.id.btnSubmitB)
    Button bt_submit;
    @BindView(R.id.cbSavePaymentCB)
    CheckBox cbSavePaymentCB;
    @BindView(R.id.txtTermsAndPPTV)
    TextView txtTermsAndPPTV;
    @BindView(R.id.backRL)
    RelativeLayout backRL;


    boolean isSavePaymentOption = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_payment);

        ButterKnife.bind(this);
        txtHeaderTV.setText(R.string.edit_payment);

        String text = "<font color=#989898>" + getString(R.string.step_4_1) + " " + "</font> <font color=#069E7C>" + getString(R.string.step_4_2) + " " + "</font><font color=#989898>" + getString(R.string.step_4_3) + "</font>";
        txtTermsAndPPTV.setText(Html.fromHtml(text));

        executeDataApi();
        setUpClicks();

    }

    private void setUpClicks() {

        bt_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
                bt_submit.startAnimation(myAnim);
                if (isValidate()) {
                    //Execute Submit All SignUp Steps
                    if (isNetworkAvailable(mActivity)) {
                        submitApi();
                    } else {
                        showToast(mActivity, getString(R.string.internet_connection_error));
                    }
                }

            }
        });

        backRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        txtTermsAndPPTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtTermsAndPPTV.setClickable(false);
                Intent mIntent = new Intent(mActivity, OpenLinksActivity.class);
                mIntent.putExtra(Constants.LINK_TYPE, Constants.PRIVACY_POLICY);
                startActivity(mIntent);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        txtTermsAndPPTV.setClickable(true);
                    }
                },1500);
            }
        });


        cbSavePaymentCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    isSavePaymentOption = true;
                }else{
                    isSavePaymentOption = false;
                }
            }
        });
    }

    private boolean isValidate() {

            boolean flag = true;
            if (ed_bank_name.getText().toString().trim().equals("")) {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_bank_name));
                flag = false;
            } else if (ed_routing_name.getText().toString().trim().equals("")) {
                showAlertDialog(mActivity, getString(R.string.please_enter_routing_number));
                flag = false;
            } else if (ed_accountno.getText().toString().trim().equals("")) {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_account_number));
                flag = false;
            } else if (ed_reaccountno.getText().toString().trim().equals("")) {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_account_number_confirm));
                flag = false;
            } else if (!ed_accountno.getText().toString().trim().equals(ed_reaccountno.getText().toString().trim())) {
                showAlertDialog(mActivity, getString(R.string.account_number_should_not_matched));
                flag = false;
            } else if (ed_billing_country.getText().toString().trim().equals("")) {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_country));
                flag = false;
            }
            else if (ed_billing_country.getText().toString().trim().equals("")) {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_country));
                flag = false;
            }
            else if (ed_billing_address1.getText().toString().trim().equals("")) {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_street_address));
                flag = false;
            } else if (ed_billing_address2.getText().toString().trim().equals("")) {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_suite_number));
                flag = false;
            } else if (ed_billing_city.getText().toString().trim().equals("")) {
                showAlertDialog(mActivity, getString(R.string.please_enter_city_name));
                flag = false;
            }
            else if (ed_billing_state.getText().toString().trim().equals("")) {
                showAlertDialog(mActivity, getString(R.string.please_enter_state_name));
                flag = false;
            } else if (ed_billing_zipcode.getText().toString().trim().equals("")) {
                showAlertDialog(mActivity, getString(R.string.please_enter_zip_code));
                flag = false;
            } else if (ed_billing_zipcode.getText().toString().trim().length()< 5) {
                showAlertDialog(mActivity, getString(R.string.please_enter_zip_code_valid));
                flag = false;
            }
            else if (!isSavePaymentOption) {
                showAlertDialog(mActivity, getString(R.string.please_checked_paymeny_options));
                flag = false;
            }

            return  flag;
    }


    /*
     * Execute api data
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("provider_id", getProviderID());
        return mMap;
    }
    private void executeDataApi() {
        showProgressDialog(mActivity);

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.executeCheckAddData(mParam()).enqueue(new Callback<AddScreenCheckModel>() {
            @Override
            public void onResponse(Call<AddScreenCheckModel> call, Response<AddScreenCheckModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                Log.e("Dataq","2"+response.body());
                AddScreenCheckModel mModel = response.body();

                if (mModel.getStatus() == 1) {

                  ed_bank_name.setText(mModel.getData().getBank_details().getBank_name());
                  ed_routing_name.setText(mModel.getData().getBank_details().getRouting_number());
                    ed_accountno.setText(mModel.getData().getBank_details().getAccount_number());
                    ed_reaccountno.setText(mModel.getData().getBank_details().getAccount_number());
                    ed_billing_country.setText(mModel.getData().getBank_details().getBilling_country());
                    ed_billing_address1.setText(mModel.getData().getBank_details().getBilling_address1());
                    ed_billing_address2.setText(mModel.getData().getBank_details().getBilling_address2());
                    ed_billing_city.setText(mModel.getData().getBank_details().getBilling_city());
                    ed_billing_state.setText(mModel.getData().getBank_details().getBilling_state());
                    ed_billing_zipcode.setText(mModel.getData().getBank_details().getBilling_zip());




                } else {
                    Log.e("Data","error fetch");
                }
            }

            @Override
            public void onFailure(Call<AddScreenCheckModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("Dataq","3");
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    /*
     * Execute submit data
     * */
    private Map<String, String> mParam2() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("bank_name", ed_bank_name.getText().toString().trim());
        mMap.put("routing_number", ed_routing_name.getText().toString().trim());
        mMap.put("account_number", ed_accountno.getText().toString().trim());
        mMap.put("provider_id", getProviderID());
        mMap.put("billing_country", ed_billing_country.getText().toString().trim());
        mMap.put("billing_state", ed_billing_state.getText().toString().trim());
        mMap.put("billing_address1", ed_billing_address1.getText().toString().trim());
        mMap.put("billing_address2", ed_billing_address2.getText().toString().trim());
        mMap.put("billing_city", ed_billing_city.getText().toString().trim());
        mMap.put("billing_zip", ed_billing_zipcode.getText().toString().trim());
        return mMap;
    }

    private void submitApi() {

        showProgressDialog(mActivity);
        Log.e("Dataq","1");
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.executeUpdateBankdetails(mParam2()).enqueue(new Callback<EditServiceDetailModel>() {
            @Override
            public void onResponse(Call<EditServiceDetailModel> call, Response<EditServiceDetailModel> response) {
                dismissProgressDialog();

                EditServiceDetailModel mModel = response.body();

                if (mModel.getStatus() == 1) {
                    finish();
                    Toast.makeText(mActivity, ""+mModel.getMessage(), Toast.LENGTH_SHORT).show();

                } else {
                    Log.e("Data","error fetch");
                    Toast.makeText(mActivity, ""+mModel.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<EditServiceDetailModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("Dataq","3");
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }
}