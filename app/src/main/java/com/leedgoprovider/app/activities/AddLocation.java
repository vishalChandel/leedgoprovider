package com.leedgoprovider.app.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.leedgoprovider.app.R;
import com.leedgoprovider.app.adapters.AddLocationAdapter;
import com.leedgoprovider.app.adapters.AddScreenEditAdapter;
import com.leedgoprovider.app.models.AddLocationModel;
import com.leedgoprovider.app.models.AddProviderLocationModel;
import com.leedgoprovider.app.models.AddScreenCheckModel;
import com.leedgoprovider.app.models.DeleteLocationModel;
import com.leedgoprovider.app.models.EditLocationModel;
import com.leedgoprovider.app.models.ServiceAreaModel;
import com.leedgoprovider.app.models.SignUpStepsModel;
import com.leedgoprovider.app.retrofit.ApiClient;
import com.leedgoprovider.app.retrofit.ApiInterface;
import com.leedgoprovider.app.utils.Constants;
import com.ligl.android.widget.iosdialog.IOSDialog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddLocation extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = AddLocation.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = AddLocation.this;
    @BindView(R.id.tab1LL)
    LinearLayout tab1LL;
    @BindView(R.id.tab1IV)
    ImageView tab1IV;
    @BindView(R.id.tab2LL)
    LinearLayout tab2LL;
    @BindView(R.id.tab2IV)
    ImageView tab2IV;
    @BindView(R.id.tab3LL)
    LinearLayout tab3LL;
    @BindView(R.id.tab3IV)
    ImageView tab3IV;
    @BindView(R.id.tab4LL)
    LinearLayout tab4LL;
    @BindView(R.id.tab4IV)
    ImageView tab4IV;
    @BindView(R.id.txtHeaderTV)
    TextView txtHeaderTV;
    @BindView(R.id.tx_locationadd)
    TextView tx_location;
    @BindView(R.id.location_no_areas)
    TextView tx_no_areas;
    @BindView(R.id.location_areas_found)
    TextView tx_areas_found;
    @BindView(R.id.llyLocationAdd)
    LinearLayout llyLocationAdd;

    String provider_location_id;
    String provider_location_city,provider_location_longitude,provider_location_lattitude;
    @BindView(R.id.location_recyclerview)
    RecyclerView recyclerView;
    List<AddLocationModel.Data> mArrayList = new ArrayList<AddLocationModel.Data>();
    LinearLayoutManager linearLayoutManager;
    ServiceAreaModel mServiceAreaModel;
    SignUpStepsModel mSignUpStepsModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_location);

        ButterKnife.bind(this);
        txtHeaderTV.setText(R.string.location);
        tab4IV.setImageResource(R.drawable.ic_location_select);
        tab1IV.setImageResource(R.drawable.ic_home_unselect);
        executeApi();
    }
    /*
     * Execute api update profile
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("provider_id", getProviderID());
        return mMap;
    }
    private void executeApi() {
        showProgressDialog(mActivity);
        Log.e("Dataq","1"+mParam());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.executeLocationData(mParam()).enqueue(new Callback<AddLocationModel>() {
            @Override
            public void onResponse(Call<AddLocationModel> call, Response<AddLocationModel> response) {
                dismissProgressDialog();
                Log.e("Dataq", "**RESPONSE**" + response.body().toString());
                mArrayList= response.body().getData();
                AddLocationModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    tx_no_areas.setVisibility(View.GONE);

                    recyclerView.setVisibility(View.VISIBLE);
                    setAdapter();
                    tx_areas_found.setVisibility(View.VISIBLE);


                    //}

                } else {

                    recyclerView.setVisibility(View.GONE);
                    tx_no_areas.setVisibility(View.VISIBLE);
                    tx_areas_found.setVisibility(View.GONE);
                    Log.e("Data","error fetch");
                }
            }

            @Override
            public void onFailure(Call<AddLocationModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("Dataq","3");
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getIntent() != null) {
            mSignUpStepsModel = (SignUpStepsModel) getIntent().getSerializableExtra(Constants.MODEL);
            provider_location_id="";
        }
    }

    private void setAdapter() {
        AddLocationAdapter adapter = new AddLocationAdapter(mActivity, mArrayList, new AddLocationAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int positon, AddLocationModel.Data item, View view) {
                switch (view.getId()) {
                    case R.id.im_location_delete:
                        View layout=linearLayoutManager.findViewByPosition(positon);
                        DeleteLocation(item,positon);
                        layout.findViewById(R.id.im_location_delete).setClickable(false);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                            layout.findViewById(R.id.im_location_delete).setClickable(true);
                            }
                        },1500);

                    break;
                    case R.id.im_location_edit:
                        View layout2=linearLayoutManager.findViewByPosition(positon);
                        EditLocation(item,positon);
                        layout2.findViewById(R.id.im_location_edit).setClickable(false);
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                layout2.findViewById(R.id.im_location_edit).setClickable(true);
                            }
                        },1500);

                        break;

                }
            }
        });
        linearLayoutManager = new LinearLayoutManager(mActivity);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void EditLocation(AddLocationModel.Data item, int positon) {
        String message="edit location";
        String type="edit";
        provider_location_id=item.getProvider_location_id();
        funDelalertDialog(message,type,positon,item);

    }

    private void DeleteLocation(AddLocationModel.Data item, int positon) {
        String message="delete location";
        String type="delete";
        provider_location_id=item.getProvider_location_id();
        funDelalertDialog(message,type,positon,item);


    }


    void  funDelalertDialog(String message, String type, int positon, AddLocationModel.Data item)
    {
        View layout=linearLayoutManager.findViewByPosition(positon);

        new IOSDialog.Builder(this)
                .setMessage("Are you sure you want to "+message+" ?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                    if (type.equals("delete"))
                    {
                        executeDeleteapi();

                    }
                    else {
                      performClick();

                    }
                    }
                })

                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();
                    }
                }).show();
    }


    private Map<String, String> mParamDel() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("provider_location_id", provider_location_id);
        return mMap;
    }
    private void executeDeleteapi() {
        showProgressDialog(mActivity);
        Log.e("Dataq","1"+mParam());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.executeDeleteLocation(mParamDel()).enqueue(new Callback<DeleteLocationModel>() {
            @Override
            public void onResponse(Call<DeleteLocationModel> call, Response<DeleteLocationModel> response) {
                dismissProgressDialog();
                Log.e("Dataq", "**RESPONSE**" + response.body().toString());
                provider_location_id="";
                DeleteLocationModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                  executeApi();

                    //}

                } else {
                  showAlertDialog(mActivity,response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<DeleteLocationModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("Dataq","3");
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }



    /*
     * Widgets Click Listener
     * */
    @OnClick({ R.id.llyLocationAdd,R.id.tab1LL, R.id.tab2LL, R.id.tab3LL, R.id.tab4LL})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.llyLocationAdd:
              //  provider_location_id="";
                performClick();
                break;
            case R.id.tab1LL:
                performTab1Click();
                break;
            case R.id.tab2LL:
                performTab2Click();
                break;
            case R.id.tab3LL:
                performTab3Click();
                break;
            case R.id.tab4LL:
                performTab4Click();
                break;
        }}

    private void performClick() {
//        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
//        llyLocationAdd.startAnimation(myAnim);
        Intent mIntent = new Intent(mActivity,SearchLocationActivity.class);
        startActivityForResult(mIntent, Constants.REQUEST_CODE);
    }
    private void performTab1Click() {
        tab1IV.setImageResource(R.drawable.ic_home_select);
        tab2IV.setImageResource(R.drawable.ic_add_unselect);
        tab3IV.setImageResource(R.drawable.ic_message_unselect);
        tab4IV.setImageResource(R.drawable.ic_location_unselect);
        switchActivity1(HomeActivity.class);
        finish();
    }

    private void performTab2Click() {
        tab1IV.setImageResource(R.drawable.ic_home_unselect);
        tab2IV.setImageResource(R.drawable.ic_add_select);
        tab3IV.setImageResource(R.drawable.ic_message_unselect);
        tab4IV.setImageResource(R.drawable.ic_location_unselect);
        switchActivity2(AddScreenActivity.class);
        finish();
    }

    private void switchActivity2(Class<AddScreenActivity> addScreenActivityClass) {
        Intent intent=new Intent(mActivity,addScreenActivityClass);
        startActivity(intent);
    }

    private void performTab3Click() {
        tab1IV.setImageResource(R.drawable.ic_home_unselect);
        tab2IV.setImageResource(R.drawable.ic_add_unselect);
        tab3IV.setImageResource(R.drawable.ic_message_select);
        tab4IV.setImageResource(R.drawable.ic_location_unselect);
        switchActivity20(ChatMainActivity.class);
        finish();
    }
    private void switchActivity20(Class<ChatMainActivity> chatMainActivityClass) {
        Intent intent=new Intent(mActivity,chatMainActivityClass);
        startActivity(intent);
    }

    private void performTab4Click() {
        tab1IV.setImageResource(R.drawable.ic_home_unselect);
        tab2IV.setImageResource(R.drawable.ic_add_unselect);
        tab3IV.setImageResource(R.drawable.ic_message_unselect);
        tab4IV.setImageResource(R.drawable.ic_location_select);
    }
    /**Activity Switch
     * **/
    private void switchActivity1(Class<HomeActivity> homeActivityClass) {
        Intent intent=new Intent(mActivity,homeActivityClass);
        startActivity(intent);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null){
            if (requestCode == Constants.REQUEST_CODE){
                mServiceAreaModel = (ServiceAreaModel)data.getSerializableExtra(Constants.MODEL);
                Log.e("Getg",mServiceAreaModel.getAddress());
                Log.e("Getg",mServiceAreaModel.getLatitude());
                Log.e("Getg",provider_location_id);
                provider_location_city=mServiceAreaModel.getCity();
                provider_location_lattitude=mServiceAreaModel.getLatitude();
                provider_location_longitude=mServiceAreaModel.getLongitude();
              try {

                  if (provider_location_id==null||provider_location_id.equals("")){
                      executeAddLocApi();
                  }
                 else if (provider_location_id!=null||!provider_location_id.equals(""))
                  {
                      executeEditLocApi();
                  }
              }
              catch (Exception e)
              {
                  e.printStackTrace();
              }

               // txtServiceET.setText(mServiceAreaModel.getAddress());
            }
        }
    }

    private Map<String, String> mParamEdit() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("provider_location_id", provider_location_id);
        mMap.put("city", provider_location_city);
        mMap.put("latitude", provider_location_lattitude);
        mMap.put("longitude", provider_location_longitude);
        return mMap;
    }

    private void executeEditLocApi() {
        showProgressDialog(mActivity);

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.executeEditLocationData(mParamEdit()).enqueue(new Callback<EditLocationModel>() {
            @Override
            public void onResponse(Call<EditLocationModel> call, Response<EditLocationModel> response) {
                dismissProgressDialog();
                Log.e("Dataq", "**RESPONSE**" + response.body().toString());

                EditLocationModel mModel = response.body();
                if (mModel.getStatus() == 1) {

                    executeApi();
                    Log.e("Test","Success");

                    //}

                } else {
                    showAlertDialog(mActivity,response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<EditLocationModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("Dataq","3");
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });

    }

    private Map<String, String> mParamw() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("provider_id", getProviderID());
        mMap.put("city", provider_location_city);
        mMap.put("latitude", provider_location_lattitude);
        mMap.put("longitude", provider_location_longitude);

        return mMap;
    }
    private void executeAddLocApi() {
        showProgressDialog(mActivity);

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.executeAddLocationData(mParamw()).enqueue(new Callback<AddProviderLocationModel>() {
            @Override
            public void onResponse(Call<AddProviderLocationModel> call, Response<AddProviderLocationModel> response) {
                dismissProgressDialog();
                Log.e("Dataq", "**RESPONSE**" + response.body().toString());

                AddProviderLocationModel mModel = response.body();
                if (mModel.getStatus() == 1) {

                     executeApi();
                    Log.e("Test","Success");

                    //}

                } else {
                showAlertDialog(mActivity,response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<AddProviderLocationModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("Dataq","3");
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


}