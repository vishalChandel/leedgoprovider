package com.leedgoprovider.app.activities;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.dhaval2404.imagepicker.ImagePicker;
import com.leedgoprovider.app.R;
import com.leedgoprovider.app.models.SignUpStepsModel;
import com.leedgoprovider.app.models.SignupCompleteModel;
import com.leedgoprovider.app.models.StateData;
import com.leedgoprovider.app.models.StripeModel;
import com.leedgoprovider.app.models.UploadDocumentModel;
import com.leedgoprovider.app.retrofit.ApiClient;
import com.leedgoprovider.app.retrofit.ApiInterface;
import com.leedgoprovider.app.utils.Constants;
import com.leedgoprovider.app.utils.LeedgoProviderPrefrences;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.multibindings.ElementsIntoSet;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditPaymentActivityN extends BaseActivity {
    /*
     * Initialize Menifest Permissions:
     * & Camera Gallery Request @params
     * */
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    public String writeCamera = Manifest.permission.CAMERA;
    /**
     * Getting the Current Class Name
     */
    String TAG = EditPaymentActivityN.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = EditPaymentActivityN.this;

    /*
     * Widgets
     * */
    @BindView(R.id.editFirstNameET)
    EditText editFirstNameET;
    @BindView(R.id.editLastNameET)
    EditText editLastNameET;
    @BindView(R.id.editDateBirthTV)
    TextView editDateBirthTV;
    @BindView(R.id.editphoneNoET)
    EditText editphoneNoET;
    @BindView(R.id.editstreetET)
    EditText editstreetET;
    @BindView(R.id.editAptNumerET)
    EditText editAptNumerET;
    @BindView(R.id.editcityET)
    EditText editcityET;
    @BindView(R.id.editStateET)
    EditText editStateET;
    @BindView(R.id.editZipCodeET)
    EditText editZipCodeET;
    @BindView(R.id.editIdentificationNumberET)
    EditText editIdentificationNumberET;
    @BindView(R.id.editAccountHolderET)
    EditText editAccountHolderET;
    @BindView(R.id.editRoutingET)
    EditText editRoutingET;
    @BindView(R.id.editaccountNumberET)
    EditText editaccountNumberET;
    @BindView(R.id.editaccountNumbConfirmET)
    EditText editaccountNumbConfirmET;
    @BindView(R.id.uploadFrontImageTV)
    TextView uploadFrontImageTV;
    @BindView(R.id.fileFrontImageTV)
    TextView fileFrontImageTV;
    @BindView(R.id.uploadBackImageTV)
    TextView uploadBackImageTV;
    @BindView(R.id.fileBackImageTV)
    TextView fileBackImageTV;
    @BindView(R.id.btnContinueB)
    Button btnContinueB;
    @BindView(R.id.txtHeaderTV)
    TextView txtHeaderTV;
    @BindView(R.id.backRL)
    RelativeLayout backRL;
    int front_status = 0, back_status = 0;
    Handler handler = new Handler();
    String StrImageType = "", StrFrontFileId = "", StrBackFileId = "";
    String selectedState = "", ssn_last4 = "0000";
    String filenEW = "";
    Bitmap selectedImage;
    String isChecked = "true";
    String isChecked2 = "true";
    /*
     * Initialize...
     * */
    SignUpStepsModel mSignUpStepsModel;
    ArrayList<StateData> mStatesArrayList = new ArrayList<>();
    boolean isSameBusinessAddress = false;
    boolean isDiffrentBusinessAddress = false;
    boolean isSavePaymentOption = false;
    String mStateName = "";
    String mStateID = "";
    Bitmap mBitmapFrontImage, mBitmapLastImage;

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_payment_n);
        ButterKnife.bind(this);
        txtHeaderTV.setText(R.string.edit_payment);
        // getIntentData();
        uploadFrontImageTV.setEnabled(true);
        uploadBackImageTV.setEnabled(true);
        backRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    /*
     * Widgets Click Listener
     * */
    @OnClick({R.id.btnContinueB, R.id.editDateBirthTV, R.id.uploadFrontImageTV, R.id.uploadBackImageTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnContinueB:
                performContinueClick();
                break;
            case R.id.editDateBirthTV:
                showDatePickerDialog(mActivity, editDateBirthTV);
                break;
            case R.id.uploadFrontImageTV:
                performFrontUploadImageClick();
                break;

            case R.id.uploadBackImageTV:
                performBackUploadImageClick();
                break;
        }
    }

    private void performBackUploadImageClick() {
        StrImageType = "back";
        if (checkPermission()) {
            openCameraGallery();
        } else {
            requestPermission();
        }
    }

    private void performFrontUploadImageClick() {
        StrImageType = "front";
        if (checkPermission()) {
            openCameraGallery();
        } else {
            requestPermission();
        }
    }


    /*********
     * Support for Marshmallows Version
     * GRANT PERMISSION FOR TAKEING IMAGE
     * 1) CAMERA PERMISSION
     * 2) WRITE_EXTERNAL_STORAGE PERMISSION
     * 3) READ_EXTERNAL_STORAGE PERMISSION
     **********/
    private boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
        int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
        int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
    }


    private void requestPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{writeExternalStorage, writeReadStorage, writeCamera}, 369);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 369:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    openCameraGallery();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            //val fileUri = data?.data
            if (data != null) {

                Uri mFileUri = data.getData();

                String path = data.getData().getPath();


                showIImage(mFileUri);

            } else if (resultCode == ImagePicker.RESULT_ERROR) {
                Toast.makeText(mActivity, ("Image Not Valid"), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(mActivity, "Task Cancelled", Toast.LENGTH_SHORT).show();
            }


        }
    }

    private void openCameraGallery() {
        ImagePicker.Companion.with(this)
                .crop()                    //Crop image(Optional), Check Customization for more option
//                .compress(400)            //Final image size will be less than 1 MB(Optional)
                .maxResultSize(400, 400)    //Final image resolution will be less than 1080 x 1080(Optional)
                .cropSquare()
                .start();
    }


    private void showIImage(Uri imageUri) {
        filenEW = getRealPathFromURI_API19(mActivity, imageUri);
        InputStream inputStream = null;
        try {
            inputStream = new FileInputStream(filenEW);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        selectedImage = BitmapFactory.decodeStream(inputStream);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        selectedImage.compress(Bitmap.CompressFormat.PNG, 100, out);
        Bitmap decoded = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
        if (decoded != null) {


            // disable all buttons
            uploadFrontImageTV.setEnabled(false);
            uploadBackImageTV.setEnabled(false);
            //final InputStream imageStream = getContentResolver().openInputStream(decoded);

            if (StrImageType.equals("front")) {
                mBitmapFrontImage = selectedImage;

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while (front_status < 100) {

                            front_status += 5;

                            try {
                                Thread.sleep(300);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {

                                    fileFrontImageTV.setText("Uploading " + String.valueOf(front_status) + ".00%");

                                    if (front_status == 100) {
                                        if (isChecked.equals("true")) {
                                            isChecked = "false";
                                            executeUploadDocumentApi("front", mBitmapFrontImage);
                                        }

                                    }
                                }
                            });
                        }
                    }
                }).start();

            } else if (StrImageType.equals("back")) {
                mBitmapLastImage = selectedImage;

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while (back_status < 100) {

                            back_status += 5;

                            try {
                                Thread.sleep(300);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {

                                    fileBackImageTV.setText("Uploading " + String.valueOf(back_status) + ".00%");

                                    if (back_status == 100) {
                                        if (isChecked2.equals("true")) {
                                            isChecked2 = "false";
                                            executeUploadDocumentApi("back", mBitmapLastImage);
                                        }
                                    }
                                }
                            });
                        }
                    }
                }).start();
            }


        }
    }


    /**
     * Start pick image activity with chooser.
     */
//    public void onSelectImageClick() {
//        CropImage.startPickImageActivity(this);
//    }
//
//    /**
//     * Start crop image activity for the given image.
//     */
//    private void startCropImageActivity(Uri imageUri) {
//        CropImage.activity(imageUri)
//                .setGuidelines(CropImageView.Guidelines.ON)
////                .setAspectRatio(200, 100)
//                .setMultiTouchEnabled(false)
//                .start(this);
//    }
//
//
//    @RequiresApi(api = Build.VERSION_CODES.M)
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        // handle result of pick image chooser
//        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
//            Uri imageUri = CropImage.getPickImageResultUri(this, data);
//
//            // For API >= 23 we need to check specifically that we have permissions to read external storage.
//            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
//                // request permissions and handle the result in onRequestPermissionsResult()
//
//            } else {
//                // no permissions required or already grunted, can start crop image activity
//                startCropImageActivity(imageUri);
//            }
//        }
//
//        // handle result of CropImageActivity
//        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
//            CropImage.ActivityResult result = CropImage.getActivityResult(data);
//            if (resultCode == RESULT_OK) {
//
//                try {
//
//                    // disable all buttons
//                    uploadFrontImageTV.setEnabled(false);
//                    uploadBackImageTV.setEnabled(false);
//
//                    final InputStream imageStream = getContentResolver().openInputStream(result.getUri());
//
//                    if (StrImageType.equals("front")) {
//                        mBitmapFrontImage = BitmapFactory.decodeStream(imageStream);
//
//                        new Thread(new Runnable() {
//                            @Override
//                            public void run() {
//                                while (front_status < 100) {
//
//                                    front_status += 5;
//
//                                    try {
//                                        Thread.sleep(300);
//                                    } catch (InterruptedException e) {
//                                        e.printStackTrace();
//                                    }
//
//                                    handler.post(new Runnable() {
//                                        @Override
//                                        public void run() {
//
//                                            fileFrontImageTV.setText("Uploading " + String.valueOf(front_status) + ".00%");
//
//                                            if (front_status == 100) {
//                                                executeUploadDocumentApi("front", mBitmapFrontImage);
//                                            }
//                                        }
//                                    });
//                                }
//                            }
//                        }).start();
//
//                    } else if (StrImageType.equals("back")) {
//                        mBitmapLastImage = BitmapFactory.decodeStream(imageStream);
//
//                        new Thread(new Runnable() {
//                            @Override
//                            public void run() {
//                                while (back_status < 100) {
//
//                                    back_status += 5;
//
//                                    try {
//                                        Thread.sleep(300);
//                                    } catch (InterruptedException e) {
//                                        e.printStackTrace();
//                                    }
//
//                                    handler.post(new Runnable() {
//                                        @Override
//                                        public void run() {
//
//                                            fileBackImageTV.setText("Uploading " + String.valueOf(back_status) + ".00%");
//
//                                            if (back_status == 100) {
//                                                executeUploadDocumentApi("back", mBitmapLastImage);
//                                            }
//                                        }
//                                    });
//                                }
//                            }
//                        }).start();
//                    }
//
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//
//            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
//                showToast(mActivity, "Cropping failed: " + result.getError());
//            }
//        }
//
//
//    }
    private void performOpenLinkClick() {
        Intent mIntent = new Intent(mActivity, OpenLinksActivity.class);
        mIntent.putExtra(Constants.LINK_TYPE, Constants.PRIVACY_POLICY);
        startActivity(mIntent);
    }

    private void performContinueClick() {
        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
        btnContinueB.startAnimation(myAnim);
        if (isValidate()) {
            if (isNetworkAvailable(mActivity)) {
                executeSubmitApi();
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error));
            }
        }
    }

    private void performSignInClick() {
        Intent intent = new Intent(mActivity, SignInActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    /*
     * Set up validations for fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editFirstNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_first_name));
            flag = false;
        } else if (editLastNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_last_name));
            flag = false;
        } else if (editDateBirthTV.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_dateof_birth));
            flag = false;
        } else if (editphoneNoET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_phone_number));
            flag = false;
        } else if (editstreetET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_street_address));
            flag = false;
        } else if (editAptNumerET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_apt_number));
            flag = false;
        } else if (editcityET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_city_name));
            flag = false;
        } else if (editStateET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_state_name));
            flag = false;
        } else if (editZipCodeET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_zip_code));
            flag = false;
        } else if (editIdentificationNumberET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_identification_number));
            flag = false;
        } else if (editAccountHolderET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_bank_account_holder_name));
            flag = false;
        } else if (editRoutingET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_routing_number));
            flag = false;
        } else if (editaccountNumberET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_account_number));
            flag = false;
        } else if (editaccountNumbConfirmET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_account_number_confirm));
            flag = false;
        } else if (StrFrontFileId.equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_add_front_image));
            flag = false;
        } else if (StrBackFileId.equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_add_back_image));
            flag = false;
        }

        return flag;
    }


    private void executeUploadDocumentApi(String imgType, Bitmap mBitmapFrontImage) {
//        MultipartBody.Part image = null;
//        File imageFile1 = null;
//        if (mBitmapFrontImage != null) {
//            Date date32 = new Date();
//            long time3 = date32.getTime();
//            imageFile1 = getBitmapFile(mBitmapFrontImage, "reduceimage1" + time3);
//            Log.e("Checkio", imageFile1.getPath());
//            RequestBody ppostImage = RequestBody.create(MediaType.parse("multipart/form-data"), imageFile1);
//            image = MultipartBody.Part.createFormData("image", imageFile1.getName(), ppostImage);
//        } else {
//            RequestBody ppostImage = RequestBody.create(MediaType.parse("multipart/form-data"), "");
//            image = MultipartBody.Part.createFormData("image", "", ppostImage);
//        }
        showProgressDialog(mActivity);
        MultipartBody.Part mMultipartBody1 = null;
        if (selectedImage != null) {
            File propertyImageFile = new File(filenEW);
            RequestBody requestFile1 = RequestBody.create(MediaType.parse("multipart/form-data"), propertyImageFile);
            mMultipartBody1 = MultipartBody.Part.createFormData("image", getAlphaNumericString() + ".jpg", requestFile1);
        }

        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);

        RequestBody imageTypee = RequestBody.create(MediaType.parse("multipart/form-data"), imgType);
        RequestBody providerIdd = RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(getProviderID()));

        Call<UploadDocumentModel> call1 = mApiInterface1.uploadDocument(imageTypee, providerIdd, mMultipartBody1);

        call1.enqueue(new Callback<UploadDocumentModel>() {
            @Override
            public void onResponse(Call<UploadDocumentModel> call, Response<UploadDocumentModel> response) {
                dismissProgressDialog();
                if (response.body() != null) {
                    UploadDocumentModel mModel = response.body();
                    if (StrImageType.equals("front")) {
                        StrFrontFileId = mModel.getFileId();
                        fileFrontImageTV.setText(StrFrontFileId);
                    } else if (StrImageType.equals("back")) {
                        StrBackFileId = mModel.getFileId();
                        fileBackImageTV.setText(StrBackFileId);
                    }
                    uploadFrontImageTV.setEnabled(true);
                    uploadBackImageTV.setEnabled(true);

                    back_status = 0;
                    front_status = 0;
                }

            }

            @Override
            public void onFailure(Call<UploadDocumentModel> call, Throwable t) {
                dismissProgressDialog();
            }
        });

    }

    private File getBitmapFile(Bitmap bmThumbnail_resized, String reduceimage1) {
        File file = new File(Environment.getExternalStorageDirectory() + File.separator + reduceimage1);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bmThumbnail_resized.compress(Bitmap.CompressFormat.JPEG, 100, bos);
        byte[] bitmapdata = bos.toByteArray();
        OutputStream os;
        try {
            file.createNewFile();
            FileOutputStream osw = new FileOutputStream(file);
            osw.write(bitmapdata);
            osw.flush();
            osw.close();
            return file;
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }
        return file;
    }


    private void executeSubmitApi() {
        if (editIdentificationNumberET.getText().toString().trim().length() == 4) {
            ssn_last4 = editIdentificationNumberET.getText().toString().trim();
        } else if (editIdentificationNumberET.getText().toString().trim().length() > 4) {
            ssn_last4 = editIdentificationNumberET.getText().toString().trim()
                    .substring(editIdentificationNumberET.getText().toString().trim().length() - 4);

            ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);

            RequestBody providerId = RequestBody.create(MediaType.parse("multipart/form-data"), getProviderID());
            RequestBody firstName = RequestBody.create(MediaType.parse("multipart/form-data"), editFirstNameET.getText().toString().trim());
            RequestBody lastName = RequestBody.create(MediaType.parse("multipart/form-data"), editLastNameET.getText().toString().trim());
            RequestBody city = RequestBody.create(MediaType.parse("multipart/form-data"), editcityET.getText().toString().trim());
            RequestBody country = RequestBody.create(MediaType.parse("multipart/form-data"), "US");
            RequestBody line1 = RequestBody.create(MediaType.parse("multipart/form-data"), editstreetET.getText().toString().trim());
            RequestBody line2 = RequestBody.create(MediaType.parse("multipart/form-data"), editAptNumerET.getText().toString().trim());
            RequestBody postalCode = RequestBody.create(MediaType.parse("multipart/form-data"), "" + editZipCodeET.getText().toString().trim());
            RequestBody state = RequestBody.create(MediaType.parse("multipart/form-data"), editStateET.getText().toString().trim());
            RequestBody dob = RequestBody.create(MediaType.parse("multipart/form-data"), editDateBirthTV.getText().toString().trim());
            RequestBody idNumber = RequestBody.create(MediaType.parse("multipart/form-data"), editIdentificationNumberET.getText().toString().trim());
            RequestBody phone = RequestBody.create(MediaType.parse("multipart/form-data"), editphoneNoET.getText().toString().trim());
            RequestBody ssnLast4 = RequestBody.create(MediaType.parse("multipart/form-data"), ssn_last4);
            RequestBody back = RequestBody.create(MediaType.parse("multipart/form-data"), StrBackFileId);
            RequestBody front = RequestBody.create(MediaType.parse("multipart/form-data"), StrFrontFileId);
            RequestBody acountHolderName = RequestBody.create(MediaType.parse("multipart/form-data"), editAccountHolderET.getText().toString().trim());
            RequestBody routingNumber = RequestBody.create(MediaType.parse("multipart/form-data"), editRoutingET.getText().toString());
            RequestBody accountNumber = RequestBody.create(MediaType.parse("multipart/form-data"), editaccountNumberET.getText().toString());

            Call<StripeModel> call1 = mApiInterface1.stripeAccount(providerId, firstName, lastName, city, country, line1, line2, postalCode, state, dob, idNumber, phone, ssnLast4, back, front, acountHolderName, routingNumber, accountNumber);

            call1.enqueue(new Callback<StripeModel>() {
                @Override
                public void onResponse(Call<StripeModel> call, Response<StripeModel> response) {
                    dismissProgressDialog();
                    StripeModel mModel = response.body();
                    if (response.body() != null) {
                        if (mModel.getStatus() == 1) {
                            showFinishAlertDialog(mActivity, mModel.getMessage());
                            uploadFrontImageTV.setEnabled(true);
                            uploadBackImageTV.setEnabled(true);
                            back_status = 0;
                            front_status = 0;
                        } else {
                            showAlertDialog(mActivity, mModel.getMessage());
                        }

                    }

                }

                @Override
                public void onFailure(Call<StripeModel> call, Throwable t) {
                    dismissProgressDialog();
                }
            });

        }
    }

    public void showFinishAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                finish();
            }
        });
        alertDialog.show();
    }

}