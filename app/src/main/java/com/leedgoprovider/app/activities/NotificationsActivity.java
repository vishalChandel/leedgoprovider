package com.leedgoprovider.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.leedgoprovider.app.R;
import com.leedgoprovider.app.adapters.Notificationadapter;
import com.leedgoprovider.app.interfaces.NotificationClickInterface;
import com.leedgoprovider.app.models.DataItem;
import com.leedgoprovider.app.models.InRequestModel;
import com.leedgoprovider.app.models.NotificationModel;
import com.leedgoprovider.app.models.ProviderCompleteTaskModel;
import com.leedgoprovider.app.models.RoomCreateModel;
import com.leedgoprovider.app.models.TransactionHistoryModel;
import com.leedgoprovider.app.retrofit.ApiClient;
import com.leedgoprovider.app.retrofit.ApiInterface;
import com.leedgoprovider.app.utils.Constants;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationsActivity extends BaseActivity {

    /**
     * Getting the Current Class Name
     */
    String TAG = NotificationsActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = NotificationsActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.backRL)
    RelativeLayout backRL;
    @BindView(R.id.txtHeaderTV)
    TextView txtHeaderTV;
    @BindView(R.id.no_notiifcations)
    TextView tx_no_notifications;
    @BindView(R.id.notification_recycler)
    RecyclerView recyclerView;
    List<NotificationModel.Data> mArrayList2 = new ArrayList<NotificationModel.Data>();

    String mNotId, mNotType;

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications2);
        ButterKnife.bind(this);
        setToolbarText(txtHeaderTV, getString(R.string.notification));
    }

    /*
     * Widgets Click Listener
     * */
    @OnClick({R.id.backRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backRL:
                onBackPressed();
                break;
        }
    }

    /*
     * Execute api update profile
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getProviderID());
        mMap.put("user_type", "provider");
        return mMap;
    }

    private void executeApi() {
        showProgressDialog(mActivity);
        Log.e("Dataq", "1");
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.executeNotificationList(mParam()).enqueue(new Callback<NotificationModel>() {
            @Override
            public void onResponse(Call<NotificationModel> call, Response<NotificationModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                Log.e("Dataq", "2" + response.body());
                NotificationModel mModel = response.body();

                if (mModel.getStatus() == 1) {
                    mArrayList2 = response.body().getData();
                    setAdapter();
                    tx_no_notifications.setVisibility(View.GONE);
                } else {
                    Log.e("Data", "error fetch");
                    recyclerView.setVisibility(View.GONE);
                    tx_no_notifications.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<NotificationModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("Dataq", "3");
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void setAdapter() {
        Notificationadapter mSearchLocationAdapter = new Notificationadapter(mActivity, mArrayList2, notificationClickInterface);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        recyclerView.setHasFixedSize(true);
        if (mArrayList2 != null && mArrayList2.size() > 0) {
            recyclerView.setItemViewCacheSize(mArrayList2.size());
        }
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(mSearchLocationAdapter);
    }

    String Sender_Id = "", Sender_Name = "", taskId = "", service_name = "", address = "", category_image = "",
            task_Id = "", endDate = "";

    NotificationClickInterface notificationClickInterface = new NotificationClickInterface() {
        @Override
        public void getNotification(NotificationModel.Data mModel) {
            if (mModel.getNotification_type().equals("chat")) {
                mNotId = mModel.getMessage_id();
                mNotType = "chat";
            } else {
                mNotId = mModel.getNotification_id();
                mNotType = "notification";
            }
            taskId = mModel.getTask_id();
            Sender_Id = mModel.getSender_id();
            Sender_Name = mModel.getSender_name();
            if (mModel.getNotification_type().equals("chat")) {
                executeNotApi();
                executeMessage();
            } else if (mModel.getNotification_type().equals("new_bid_request")) {
                executeNotApi();
                executeRequestsApi(mModel.getTask_id());
            } else if (mModel.getNotification_type().equals("send_payment")) {
                executeNotApi();
                executeTransactionApi(mModel.getTask_id());
            } else if (mModel.getNotification_type().equals("bid_approved")) {
                executeNotApi();
                Intent intent = new Intent(mActivity, RequestDetails.class);
                intent.putExtra("taskId", mModel.getTask_id());
                startActivity(intent);
            } else if (mModel.getNotification_type().equals("task_date_scheduled")) {
                executeNotApi();
                Intent intent = new Intent(mActivity, CompleteRequestActivity.class);
                intent.putExtra("taskId", mModel.getTask_id());
                startActivity(intent);
            } else if (mModel.getNotification_type().equals("task_complete")) {
                executeNotApi();
                executeCompleteApi(mModel.getTask_id());
            }
        }
    };

    /*
     * Execute api
     * */
    private Map<String, String> mMessageParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("client_id", Sender_Id);
        mMap.put("provider_id", getProviderID());
        Log.e("Room", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeMessage() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.createRoom(mMessageParam()).enqueue(new Callback<RoomCreateModel>() {
            @Override
            public void onResponse(Call<RoomCreateModel> call, Response<RoomCreateModel> response) {
                dismissProgressDialog();
                RoomCreateModel mModel = response.body();
                if (mModel.getStatus() == 1) {

                    Intent intent = new Intent(mActivity, ChatActivity.class);
                    intent.putExtra("room_id", mModel.getRoom_id());
                    intent.putExtra("name", Sender_Name);
                    startActivity(intent);

                } else {
                    showToast(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<RoomCreateModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(String.valueOf(mActivity), "**ERROR**" + t.getMessage());
            }
        });
    }

    /* Execute get all requests API to and filter out the required task id deatils */
    private Map<String, String> mRequestsParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("provider_id", getProviderID());
        return mMap;
    }

    private void executeRequestsApi(String Task_Id) {
        showProgressDialog(mActivity);

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.executeRequestsData(mRequestsParam()).enqueue(new Callback<InRequestModel>() {
            @Override
            public void onResponse(Call<InRequestModel> call, Response<InRequestModel> response) {
                dismissProgressDialog();

                InRequestModel mModel = response.body();

                if (mModel.getStatus() == 1) {

                    for (int i = 0; i < mModel.getData().size(); i++) {
                        if (mModel.getData().get(i).getTask_id().equals(Task_Id)) {

                            if (mModel.getData().get(i).getService_name() != null)
                                service_name = mModel.getData().get(i).getService_name();

                            if (mModel.getData().get(i).getAddress() != null)
                                address = mModel.getData().get(i).getAddress();

                            if (mModel.getData().get(i).getCategory_image() != null)
                                category_image = mModel.getData().get(i).getCategory_image();

                            if (mModel.getData().get(i).getTask_id() != null)
                                task_Id = mModel.getData().get(i).getTask_id();

                            if (mModel.getData().get(i).getBid_last_date() != null)
                                endDate = mModel.getData().get(i).getBid_last_date();

                            Intent intent = new Intent(mActivity, BidServiceActivity.class);
                            intent.putExtra("name", service_name);
                            intent.putExtra("address", address);
                            intent.putExtra("image", category_image);
                            intent.putExtra("taskId", task_Id);
                            intent.putExtra("endDaate", endDate);
                            startActivity(intent);

                            break;
                        }
                    }

                } else {
                    Log.e("Data", mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<InRequestModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("Dataq", "3");
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    /* Execute get all transactions API to and filter out the required transaction id deatils*/
    private Map<String, String> mTransactionParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("provider_id", getProviderID());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeTransactionApi(String Task_Id) {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getTransactionHistory(mTransactionParam()).enqueue(new Callback<TransactionHistoryModel>() {
            @Override
            public void onResponse(Call<TransactionHistoryModel> call, Response<TransactionHistoryModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                TransactionHistoryModel mModel = response.body();

                if (mModel.getStatus() == 1) {

                    TransactionHistoryModel.Datum mModell = new TransactionHistoryModel.Datum();
                    for (int i = 0; i < mModel.getData().size(); i++) {
                        if (mModel.getData().get(i).getTaskId().equals(Task_Id)) {
                            mModell = mModel.getData().get(i);
                        }
                    }

                    Intent intent = new Intent(mActivity, TransactionDetailActivity.class);
                    intent.putExtra("MyClass", (Serializable) mModell);
                    startActivity(intent);

                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<TransactionHistoryModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    /* Execute get complete tasks api*/
    private Map<String, String> mCompleteParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("provider_id", getProviderID());
        return mMap;
    }

    private void executeCompleteApi(String Task_Id) {
        showProgressDialog(mActivity);

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.providerCompleteTask(mCompleteParam()).enqueue(new Callback<ProviderCompleteTaskModel>() {
            @Override
            public void onResponse(Call<ProviderCompleteTaskModel> call, Response<ProviderCompleteTaskModel> response) {
                dismissProgressDialog();

                ProviderCompleteTaskModel mModel = response.body();
                String username = "", userEmail = "", userProfile = "", categoryName = "", categoryImage = "",
                        SubcategoryName = "", UniqueTaskID = "", paymentStatus = "", workLocation = "",
                        workDescription = "", CompletedDate = "", BidAmount = "";

                if (mModel.getStatus() == 1) {

                    for (int i = 0; i < mModel.getData().size(); i++) {
                        if (mModel.getData().get(i).getTaskId().equals(Task_Id)) {

                            if (mModel.getData().get(i).getUserDetail().getName() != null)
                                username = mModel.getData().get(i).getUserDetail().getName();

                            if (mModel.getData().get(i).getUserDetail().getEmail() != null)
                                userEmail = mModel.getData().get(i).getUserDetail().getEmail();

                            if (mModel.getData().get(i).getUserDetail().getProfilePic() != null)
                                userProfile = mModel.getData().get(i).getUserDetail().getProfilePic();

                            if (mModel.getData().get(i).getServiceName() != null)
                                categoryName = mModel.getData().get(i).getServiceName();

                            if (mModel.getData().get(i).getCategoryImage() != null)
                                categoryImage = mModel.getData().get(i).getCategoryImage();

                            if (mModel.getData().get(i).getSubCategoryName() != null)
                                SubcategoryName = mModel.getData().get(i).getSubCategoryName();

                            if (mModel.getData().get(i).getUniqueTaskId() != null)
                                UniqueTaskID = mModel.getData().get(i).getUniqueTaskId();

                            if (mModel.getData().get(i).getPaymentStatus() != null)
                                paymentStatus = mModel.getData().get(i).getPaymentStatus();

                            if (mModel.getData().get(i).getAddress() != null)
                                workLocation = mModel.getData().get(i).getAddress();

                            if (mModel.getData().get(i).getWorkDetails() != null)
                                workDescription = mModel.getData().get(i).getWorkDetails();

                            if (mModel.getData().get(i).getTaskCompleteDate() != null)
                                CompletedDate = timeChange(Long.parseLong(mModel.getData().get(i).getTaskCompleteDate()));

                            if (mModel.getData().get(i).getAssignedAmount() != null)
                                BidAmount = mModel.getData().get(i).getAssignedAmount();

                            Intent intent = new Intent(mActivity, CompletedRequestDetailsActivity.class);
                            intent.putExtra("username", username);
                            intent.putExtra("userEmail", userEmail);
                            intent.putExtra("userProfile", userProfile);
                            intent.putExtra("categoryName", categoryName);
                            intent.putExtra("categoryImage", categoryImage);
                            intent.putExtra("SubcategoryName", SubcategoryName);
                            intent.putExtra("taskId", UniqueTaskID);
                            intent.putExtra("paymentStatus", paymentStatus);
                            intent.putExtra("workLocation", workLocation);
                            intent.putExtra("workDescription", workDescription);
                            intent.putExtra("CompletedDate", CompletedDate);
                            intent.putExtra("BidAmount", BidAmount);
                            startActivity(intent);

                            break;
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<ProviderCompleteTaskModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("Dataq", "3");
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    String timeChange(long coment_time) {
        Date date = new java.util.Date(coment_time * 1000L);

        // the format of your date
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/MM/yyyy");
        // give a timezone reference for formatting (see comment at the bottom)
        sdf.setTimeZone(TimeZone.getDefault());
        String formattedDate = sdf.format(date);
        return formattedDate;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (getIntent() != null && getIntent().getStringExtra(Constants.NOTIFICATION_TYPE) != null
                && getIntent().getStringExtra(Constants.NOTIFICATION_TYPE).equals(Constants.PUSH)) {
            Intent mIntent = new Intent(mActivity, HomeActivity.class);
            mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(mIntent);
            finish();
        } else {
            finish();
        }
    }

    /*
     * Execute api update profile
     * */
    private Map<String, String> mNotParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getProviderID());
        mMap.put("user_type", "provider");
        mMap.put("notification_id", mNotId);
        mMap.put("notification_type", mNotType);
        Log.i(TAG, "mNotParam: " + getProviderID());
        return mMap;
    }

    private void executeNotApi() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.readNotificationRequest(mNotParam()).enqueue(new Callback<NotificationModel>() {
            @Override
            public void onResponse(Call<NotificationModel> call, Response<NotificationModel> response) {

            }

            @Override
            public void onFailure(Call<NotificationModel> call, Throwable t) {
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        executeApi();
    }
}
