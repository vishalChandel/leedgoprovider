package com.leedgoprovider.app.activities;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.leedgoprovider.app.LeedgoProviderApplication;
import com.leedgoprovider.app.R;
import com.leedgoprovider.app.StringChange.StringFormatter;
import com.leedgoprovider.app.adapters.PostChatAdapter;
import com.leedgoprovider.app.interfaces.ZoomImageInterface;
import com.leedgoprovider.app.models.ChatMessageModel;
import com.leedgoprovider.app.retrofit.ApiClient;
import com.leedgoprovider.app.retrofit.ApiInterface;
import com.leedgoprovider.app.utils.LeedgoProviderPrefrences;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.TimeZone;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

public class ChatActivity extends BaseActivity {
    /*
 Getting the Current Class Name
*/
    String TAG = ChatActivity.this.getClass().getSimpleName();

    /*
      Current Activity Instance
     */
    Activity mActivity = ChatActivity.this;

    @BindView(R.id.txtHeaderTV)
    TextView txtHeaderTV;
    @BindView(R.id.backRL)
    RelativeLayout backRl;
    @BindView(R.id.chat_input_msg)
    EditText ed_inputMsg;
    @BindView(R.id.chat_send_msg)
    TextView tx_chat_send_msg;
    @BindView(R.id.imgCloseIV)
    ImageView imgCloseIV;
    @BindView(R.id.imgChatIV)
    com.joooonho.SelectableRoundedImageView imgChatIV;
    @BindView(R.id.chatPostRV)
    RecyclerView chatPostRV;
    @BindView(R.id.imgLinkIV)
    ImageView imgLinkIV;
    @BindView(R.id.mSwipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.imageAttachRL)
    RelativeLayout imageAttachRL;
    @BindView(R.id.msgProgressBarPB)
    ProgressBar msgProgressBarPB;

    //Initialize
    boolean isSwipeRefresh = false;
    int pageNumber = 1;
    boolean strLastPage = false;

    String imageurl = "";
    String roomId = "";
    String name = "";
    String toServer;
    String toServerUnicodeEncoded = "";
    SharedPreferences prefv;
    private long mLastClickTab1 = 0;

    /*
     * Initialize Menifest Permissions:
     * & Camera Gallery Request @params
     * */
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    public String writeCamera = Manifest.permission.CAMERA;

    String mBase64Image;
    String mBase64PreviousImage;
    PostChatAdapter mPostAdapter;
    ArrayList<ChatMessageModel.All_messages> chatGroupModels = new ArrayList<>();
    ArrayList<ChatMessageModel.All_messages> tempArrayList = new ArrayList<>();

    Bitmap selectedImg = null;


    int pos;
    private Socket mSocket;
    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });
        }
    };
    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });
        }
    };
    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e(TAG, "Error connecting");
                }
            });
        }
    };
    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.e(TAG, "*****NEW Message Data****" + args[1].toString());
                        //JSONObject mJsonObject = new JSONObject(String.valueOf((args[1])));

                        JSONObject mJsonObject = new JSONObject(args[1].toString());
                        Log.e(TAG, "**Message Data**" + mJsonObject.toString());
                        ChatMessageModel.All_messages model = new Gson().fromJson(mJsonObject.toString(), ChatMessageModel.All_messages.class);

                        chatGroupModels.add(model);
                        try {
                            mPostAdapter.notifyDataSetChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        if (model != null) {
                            chatPostRV.scrollToPosition(chatGroupModels.size() - 1);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };
    private Emitter.Listener onUserJoined = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject mJsonData = null;
                    try {
                        mJsonData = new JSONObject(String.valueOf(args[1]));

                        chatPostRV.scrollToPosition(chatGroupModels.size() - 1);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    };
    private Emitter.Listener onUserLeft = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });
        }
    };
    private Emitter.Listener onTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    boolean isTyping = (boolean) args[1];

                }
            });
        }
    };
    private Emitter.Listener onStopTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });
        }
    };
    private Runnable onTypingTimeout = new Runnable() {
        @Override
        public void run() {

        }
    };

    ZoomImageInterface mZoomImageInterface = new ZoomImageInterface() {
        @Override
        public void onClick(String img) {
            showImagesDialog(mActivity, img);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
        getData();
        setData();
        setEditFocus();
        setUpSocketData();
        LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.SCROLL, null);
        prefv = getSharedPreferences("pref", Context.MODE_PRIVATE);
        // ed_inputMsg.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        tx_chat_send_msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
                tx_chat_send_msg.startAnimation(myAnim);

//                tx_chat_send_msg.setClickable(false);


                toServer = ed_inputMsg.getText().toString().trim();
                ed_inputMsg.setText("");

                //    if (isValidate()) {
                if (!toServer.equals("")) {
                    toServerUnicodeEncoded = StringEscapeUtils.escapeJava(toServer);
                    toServerUnicodeEncoded = StringEscapeUtils.unescapeJava(toServer);
                    imageAttachRL.setVisibility(View.GONE);

                    // showProgressDialog(mActivity);


                    Date currentTime = Calendar.getInstance().getTime();
                    SimpleDateFormat sdf = new java.text.SimpleDateFormat("hh:mm aa ");
                    // give a timezone reference for formatting (see comment at the bottom)
                    sdf.setTimeZone(TimeZone.getDefault());

                    String formattedDate = sdf.format(currentTime);

                    // }
                    // }
                    if (!isNetworkAvailable(mActivity)) {
                        Toast.makeText(mActivity, "No internet connection", Toast.LENGTH_SHORT).show();
                    } else {

                        Log.e("Chatt", toServerUnicodeEncoded);
                        fetchData();

                    }
                } else if (selectedImg != null && !selectedImg.equals("")) {
                    fetchData();
                }
            }

        });
        imgLinkIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkPermission()) {
                    onSelectImageClick();
                } else {
                    requestPermission();
                }
            }
        });


        imgCloseIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageAttachRL.setVisibility(GONE);
            }
        });
        backRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toServer = ed_inputMsg.getText().toString();
                String roomIdq = roomId;
                prefv.edit().putString(roomIdq, toServer).commit();
                finish();
            }
        });

        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                isSwipeRefresh = true;

                ++pageNumber;

                executeChatGroupListApi();
            }
        });
    }


    private boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
        int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
        int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(mActivity, new String[]{writeExternalStorage, writeReadStorage, writeCamera}, 369);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 369:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    onSelectImageClick();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }

                break;
        }
    }

    /**
     * Start pick image activity with chooser.
     */
    private void onSelectImageClick() {
        ImagePicker.with(this)
                .start();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            assert data != null;
            Uri uri = data.getData();
            InputStream imageStream = null;
            try {
                imageStream = getContentResolver().openInputStream(uri);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
            selectedImg = selectedImage;
            imageAttachRL.setVisibility(View.VISIBLE);
            imgChatIV.setImageBitmap(selectedImg);
        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show();
        }
    }

    private void fetchData() {
        imageAttachRL.setVisibility(GONE);
        MultipartBody.Part image = null;
        if (selectedImg != null) {
            Date date32 = new Date();
            long time3 = date32.getTime();
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), convertBitmapToByteArrayUncompressed(selectedImg));
            image = MultipartBody.Part.createFormData("image", getAlphaNumericString(), requestFile);
        }
        else {
            RequestBody ppostImage = RequestBody.create(MediaType.parse("multipart/form-data"), "");
            image = MultipartBody.Part.createFormData("image", "", ppostImage);
        }

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm a");
        String date = sdf.format(new Date());


        String data = ed_inputMsg.getText().toString();

        ChatMessageModel.All_messages mModel = new ChatMessageModel.All_messages();
        mModel.setSender_type("provider");
        Date date2 = new Date();
        long datel = date2.getTime();

        mModel.setMessage_time(String.valueOf(datel / 1000L));
//        if (selectedImg != null) {
//            mModel.setImage(imageFile1.getPath());
//        }

        mModel.setMessage(toServer);
        chatGroupModels.add(chatGroupModels.size(), mModel);
        try {
            mPostAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }

        ed_inputMsg.setText("");
        //if (chatGroupModels != null) {
        scrollToBottom();
        // }
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);

        RequestBody room_id = RequestBody.create(MediaType.parse("multipart/form-data"), roomId);
        RequestBody sender_type = RequestBody.create(MediaType.parse("multipart/form-data"), "provider");
        RequestBody message = RequestBody.create(MediaType.parse("multipart/form-data"),
                toServerUnicodeEncoded);

        Call<JsonObject> call1 = mApiInterface1.addUpdateChatMessage(room_id, sender_type, message, image);

        call1.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                tx_chat_send_msg.setClickable(true);
                dismissProgressDialog();
                toServer = "";
                toServerUnicodeEncoded = "";

                JSONObject mJsonObject = null;
                try {
                    mJsonObject = new JSONObject(response.body().toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {


                    JSONObject mObject = mJsonObject.getJSONObject("data");
                    Log.e("checkio", mJsonObject.toString());

                    mSocket.emit("newMessage", roomId, mObject, ed_inputMsg.getText().toString().trim());
                    if (chatGroupModels.size() < 2) {
                        executeChatGroupListApi();
                    }
                    if (chatGroupModels != null && chatGroupModels.size() > 0) {
                        chatGroupModels.remove(chatGroupModels.size() - 1);
                    }
                    ChatMessageModel.All_messages model = new ChatMessageModel.All_messages();

                    model.setId(mObject.getString("id"));
                    model.setSender_id(mObject.getString("sender_id"));
                    model.setReceiver_id(mObject.getString("receiver_id"));
                    model.setRoom_id(mObject.getString("room_id"));
                    model.setSender_type(mObject.getString("sender_type"));
                    model.setImage(mObject.getString("image"));
                    model.setMessage(mObject.getString("message"));
                    model.setMessage_time(mObject.getString("message_time"));
                    model.setMessage_seen(mObject.getString("message_seen"));
                    model.setReceiver_profile_pic(mObject.getString("receiver_profile_pic"));
                    model.setSender_profile_pic(mObject.getString("sender_profile_pic"));

                    chatGroupModels.add(model);

                    if (mPostAdapter != null) {
                        mPostAdapter.notifyDataSetChanged();
                        // ed_inputMsg.setText("");
                        imageAttachRL.setVisibility(View.GONE);
                        selectedImg = null;
                        scrollToBottom();
//                        tx_chat_send_msg.setEnabled(true);
                    }
                    //  }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dismissProgressDialog();
                showAlertDialog(mActivity, t.getMessage() + ":::" + roomId);

//                tx_chat_send_msg.setClickable(true);
            }
        });
    }

    private void getData() {
        if (getIntent() != null) {
            roomId = getIntent().getStringExtra("room_id");
            LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.ROOM_ID, roomId);
            name = getIntent().getStringExtra("name");

        }
    }


    private void setData() {
        if (name != null) {
            try {
                txtHeaderTV.setText(StringFormatter.capitalizeWord(name));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setUpSocketData() {
        LeedgoProviderApplication app = (LeedgoProviderApplication) getApplication();

        mSocket = app.getSocket();
        mSocket.emit("ConncetedChat", roomId);
        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on("newMessage", onNewMessage);
        mSocket.on("leaveChat", onUserLeft);
        mSocket.connect();

        getChat();
    }


    private void setEditFocus() {
        ed_inputMsg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                scrollToBottom();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    private File getBitmapFile(Bitmap bmThumbnail_resized, String reduceimage1) {
        File file = new File(Environment.getExternalStorageDirectory() + File.separator + reduceimage1);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bmThumbnail_resized.compress(Bitmap.CompressFormat.JPEG, 100, bos);
        byte[] bitmapdata = bos.toByteArray();
        OutputStream os;
        try {
            file.createNewFile();
            FileOutputStream osw = new FileOutputStream(file);
            osw.write(bitmapdata);
            osw.flush();
            osw.close();
            return file;
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }
        return file;
    }

    private void getChat() {
        if (!isNetworkAvailable(Objects.requireNonNull(mActivity))) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            if (chatGroupModels != null) {
                chatGroupModels.clear();
            }
            executeChatGroupListApi();
        }

    }


    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("room_id", roomId);
        mMap.put("page_no", String.valueOf(pageNumber));
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeChatGroupListApi() {
        tempArrayList.clear();
        if (!strLastPage) {
            if (msgProgressBarPB != null) {
                msgProgressBarPB.setVisibility(GONE);
            }
        } else {
            if (pageNumber == 1) {
                if (isSwipeRefresh) {
                } else {
                    //showProgressDialog(mActivity);
                }
            } else if (pageNumber > 1) {
                msgProgressBarPB.setVisibility(View.GONE);
            }
        }
        if (pageNumber == 1) {
            showProgressDialog(mActivity);
        }
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.chatGroupListRequest(mParam()).enqueue(new Callback<ChatMessageModel>() {
            @Override
            public void onResponse(Call<ChatMessageModel> call, Response<ChatMessageModel> response) {
                dismissProgressDialog();
                tempArrayList.clear();
                // Toast.makeText(mActivity, "hello"+pageNumber, Toast.LENGTH_SHORT).show();
                Log.e(TAG, "**RESPONSE**" + response.body());
                if (isSwipeRefresh) {
                    mSwipeRefreshLayout.setRefreshing(false);
                }
                try {
                    dismissProgressDialog();

                    ChatMessageModel mModel = response.body();
                    strLastPage = mModel.getNext_page();
                    JSONObject jsonObject = new JSONObject();

                    for (int i = 0; i <= mModel.getAll_messages().size(); i++) {
                        jsonObject.put("id", mModel.getAll_messages().get(i).getId());
                        jsonObject.put("sender_id", mModel.getAll_messages().get(i).getSender_id());
                        jsonObject.put("receiver_id", mModel.getAll_messages().get(i).getReceiver_id());
                        jsonObject.put("room_id", mModel.getAll_messages().get(i).getRoom_id());
                        jsonObject.put("sender_type", mModel.getAll_messages().get(i).getSender_type());
                        jsonObject.put("image", mModel.getAll_messages().get(i).getImage());
                        jsonObject.put("message", mModel.getAll_messages().get(i).getMessage());
                        jsonObject.put("message_time", mModel.getAll_messages().get(i).getMessage_time());
                        jsonObject.put("message_seen", mModel.getAll_messages().get(i).getMessage_seen());
                        jsonObject.put("receiver_profile_pic", mModel.getAll_messages().get(i).getReceiver_profile_pic());

                        jsonObject.put("sender_profile_pic", mModel.getAll_messages().get(i).getSender_profile_pic());
                        // chatGroupModels.add(mModel.getAll_messages().get(i));
                        if (pageNumber == 1) {
                            chatGroupModels.add(mModel.getAll_messages().get(i));
                            Log.e("Check", "30");
                        } else {
                            // chatGroupModels.clear();
                            Collections.reverse(chatGroupModels);
                            int position = chatGroupModels.size() - 1;
                            chatGroupModels.add(mModel.getAll_messages().get(i));

                            Collections.reverse(chatGroupModels);
                            chatPostRV.scrollToPosition(10);
                            mPostAdapter.notifyDataSetChanged();


                            // tempArrayList.add(mModel.getAll_messages().get(i));
                            Log.e("Check", "31");
                        }
                        Log.e("Check", "3" + mModel.getAll_messages().get(i).getMessage());
                    }


                    mSocket.emit("newMessage", roomId, jsonObject, ed_inputMsg.getText().toString());

                } catch (Exception e) {

                }


                // Toast.makeText(mActivity, "hello"+pageNumber, Toast.LENGTH_SHORT).show();
                if (pageNumber == 1) {
                    /*
                     * Set Adapter
                     * */
                    setPostAdapter();
                    Log.e("Check", "7");
                } else {
                    // setPostAdapter();

                    //  mPostAdapter.notifyDataSetChanged();

                    Log.e("Check", "8");
                }


                Log.e("", "**RESPONSE**" + response.body());


                //  setPostAdapter();
            }

            @Override
            public void onFailure(Call<ChatMessageModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void setPostAdapter() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.setStackFromEnd(true);
        chatPostRV.setLayoutManager(layoutManager);
        mPostAdapter = new PostChatAdapter(mActivity, chatGroupModels, mZoomImageInterface);
        chatPostRV.setAdapter(mPostAdapter);
        chatPostRV.setItemAnimator(new DefaultItemAnimator());

        //if (chatPostRV != null) {

        //  mPostAdapter = new PostChatAdapter(mActivity, chatGroupModels, dialogInterface);
        chatPostRV.setAdapter(mPostAdapter);
        scrollToBottom();
        mPostAdapter.notifyDataSetChanged();
        //  }
    }

    private void scrollToBottom() {
        chatPostRV.scrollToPosition(chatGroupModels.size() - 1);

    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            String message = prefv.getString(roomId, "");
            String fromServerUnicodeDecoded = StringEscapeUtils.unescapeJava(message);
            ed_inputMsg.setText(fromServerUnicodeDecoded);
            ed_inputMsg.setSelection(ed_inputMsg.getText().length());
            ed_inputMsg.requestFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        toServer = ed_inputMsg.getText().toString();
        String roomIdq = roomId;
        prefv.edit().putString(roomIdq, toServer).commit();
    }


    private void showImagesDialog(Context context, String img) {
        final Dialog alertDialog = new Dialog(context, R.style.Custom_Dialog);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_zoom_images);
        alertDialog.setCanceledOnTouchOutside(true);
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.setCancelable(true);
        alertDialog.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.WRAP_CONTENT);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));

        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

        wlp.gravity = Gravity.CENTER;
        wlp.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
        window.setAttributes(wlp);
        ImageView imageIV = alertDialog.findViewById(R.id.imageIV);
        RelativeLayout backRL = alertDialog.findViewById(R.id.backRL);
        Glide.with(context)
                .load(img)
                .into(imageIV);
        backRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
        alertDialog.show();
    }

    public void preventMultipleClick() {

        // Preventing multiple clicks, using threshold of 1 second
        if (SystemClock.elapsedRealtime() - mLastClickTab1 < 1500) {
            return;
        }
        mLastClickTab1 = SystemClock.elapsedRealtime();
    }
}