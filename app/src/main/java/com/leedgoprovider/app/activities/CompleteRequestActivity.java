package com.leedgoprovider.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.leedgoprovider.app.R;
import com.leedgoprovider.app.models.CheckModel;
import com.leedgoprovider.app.models.RoomCreateModel;
import com.leedgoprovider.app.models.TaskDetailsModel;
import com.leedgoprovider.app.retrofit.ApiClient;
import com.leedgoprovider.app.retrofit.ApiInterface;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompleteRequestActivity extends BaseActivity {
    Activity mActivity = CompleteRequestActivity.this;

    @BindView(R.id.txtHeaderTV)
    TextView txHeaderTv;
    @BindView(R.id.backRL)
    RelativeLayout backRL;
    @BindView(R.id.imgProfileCIV)
    CircleImageView circleImageView;
    @BindView(R.id.UpUsername)
    TextView txUpUsername;
    @BindView(R.id.UpServiceaddress)
    TextView txUpServiceaddress;
    @BindView(R.id.llyDirections)
    LinearLayout llyDirections;
    @BindView(R.id.llyMessage)
    LinearLayout llyMessage;
    @BindView(R.id.image_request)
    ImageView serviceImage;
    @BindView(R.id.servicename)
    TextView txservicename;
    @BindView(R.id.workDate)
    TextView txworkDate;
    @BindView(R.id.flexibleWeek)
    TextView txflexibleWeek;
    @BindView(R.id.workLocation)
    TextView txworkLocation;
    @BindView(R.id.btnSubmitB)
    TextView txbtnSubmitB;

    @BindView(R.id.historicalYes)
    ImageView historicalYes;
    @BindView(R.id.historicalNo)
    ImageView historicalNo;
    @BindView(R.id.insuranceYes)
    ImageView insuranceYes;
    @BindView(R.id.insuranceNo)
    ImageView insuranceNo;
    @BindView(R.id.ownerYes)
    ImageView ownerYes;
    @BindView(R.id.ownerNo)
    ImageView ownerNo;


    String taskCid, latitude, longitude, userid, reprentativeornot;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_request);
        ButterKnife.bind(this);

        txHeaderTv.setText("Complete Request");

        if (getIntent() != null) {
            taskCid = getIntent().getStringExtra("taskId");
        }
        executeDetailApi();
    }

    @OnClick({R.id.llyDirections, R.id.llyMessage, R.id.btnSubmitB, R.id.backRL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.llyDirections:
                performDirection();
                break;
            case R.id.llyMessage:
                performMessage();
                break;
            case R.id.btnSubmitB:
                perform();
                break;
            case R.id.backRL:
                finish();
                break;

        }
    }

    private void performMessage() {
        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
        llyMessage.startAnimation(myAnim);
        llyMessage.setClickable(false);
        executeMessage();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                llyMessage.setClickable(true);
            }
        }, 1500);

    }

    /*
     * Execute api
     * */
    private Map<String, String> mParamq() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("client_id", userid);
        mMap.put("provider_id", getProviderID());
        Log.e("Room", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeMessage() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.createRoom(mParamq()).enqueue(new Callback<RoomCreateModel>() {
            @Override
            public void onResponse(Call<RoomCreateModel> call, Response<RoomCreateModel> response) {

                RoomCreateModel mModel = response.body();
                if (mModel.getStatus() == 1) {

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            dismissProgressDialog();
                            Intent intent = new Intent(mActivity, ChatActivity.class);
                            intent.putExtra("room_id", mModel.getRoom_id());

                            intent.putExtra("name", reprentativeornot);
                            startActivity(intent);


                        }
                    }, 1500);
                } else {
                    showToast(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<RoomCreateModel> call, Throwable t) {

                Log.e(String.valueOf(mActivity), "**ERROR**" + t.getMessage());
            }
        });
    }


    private void performDirection() {
        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
        llyDirections.startAnimation(myAnim);
        llyDirections.setClickable(false);
        // String uri = String.format(Locale.ENGLISH, "geo:%f,%f", latitude, longitude);
        String geoUri = "http://maps.google.com/maps?q=loc:" + latitude + "," + longitude + " (" + "Client Location" + ")";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
        startActivity(intent);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                llyDirections.setClickable(true);
            }
        }, 1500);
    }

    private void perform() {
        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
        txbtnSubmitB.startAnimation(myAnim);
        UpdateApi();
    }

    /*
     * Execute api @param
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("task_id", taskCid);
        Log.e("INpro", "**PARAM**" + mMap.toString());
        return mMap;
    }

    /*
     * Execute Get Task Detail By Task Id Api
     * */
    private void executeDetailApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getRequesDetails(mParam()).enqueue(new Callback<TaskDetailsModel>() {
            @Override
            public void onResponse(Call<TaskDetailsModel> call, Response<TaskDetailsModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                TaskDetailsModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    Glide.with(mActivity).load(mModel.getData().getUserimage()).placeholder(R.drawable.ic_person).into(circleImageView);
                    txUpUsername.setText(mModel.getData().getUsername());
                    txUpServiceaddress.setText(mModel.getData().getAddress());
                    Glide.with(mActivity).load(mModel.getData().getCategory_image()).placeholder(R.drawable.ic_pp).into(serviceImage);
                    txservicename.setText(mModel.getData().getSubcategory_name());
                    txworkDate.setText(mModel.getData().getWork_date());
                    latitude = mModel.getData().getOrder_lat();
                    longitude = mModel.getData().getOrder_long();
                    userid = mModel.getData().getUser_id();
                    reprentativeornot = mModel.getData().getUsername();
                    if (txflexibleWeek != null) {
                        if (Integer.parseInt(mModel.getData().getFlexible_week()) > 1) {
                            txflexibleWeek.setText(mModel.getData().getFlexible_week() + " Weeks");
                        } else {
                            txflexibleWeek.setText(mModel.getData().getFlexible_week() + " Weeks");
                        }
                    }
                    txworkLocation.setText(mModel.getData().getAddress());

                    if (mModel.getData().getRequest_covered_by_insurance_claim() != null) {
                        if (mModel.getData().getRequest_covered_by_insurance_claim().trim().equals("no")) {
                            insuranceNo.setImageResource(R.drawable.ic_yes);
                            insuranceYes.setImageResource(R.drawable.ic_no);
                        } else {
                            insuranceNo.setImageResource(R.drawable.ic_no);
                            insuranceYes.setImageResource(R.drawable.ic_yes);
                        }
                    }

                    if (mModel.getData().getRequested_for_historical_structure() != null) {
                        if (mModel.getData().getRequested_for_historical_structure().trim().equals("no")) {
                            historicalNo.setImageResource(R.drawable.ic_yes);
                            historicalYes.setImageResource(R.drawable.ic_no);
                        } else {
                            historicalNo.setImageResource(R.drawable.ic_no);
                            historicalYes.setImageResource(R.drawable.ic_yes);
                        }
                    }

                    if (mModel.getData().getOwner_or_authorized_representative_of_owner() != null) {
                        if(mModel.getData().getOwner_or_authorized_representative_of_owner().trim().equals("representative")){
                            ownerNo.setImageResource(R.drawable.ic_yes);
                            ownerYes.setImageResource(R.drawable.ic_no);
                        }else {
                            ownerNo.setImageResource(R.drawable.ic_no);
                            ownerYes.setImageResource(R.drawable.ic_yes);
                        }
                    }
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<TaskDetailsModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private Map<String, String> mParam2() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("task_id", taskCid);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    /*
     * Execute Forgot Password Api
     * */
    private void UpdateApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getUpdateStatus(mParam2()).enqueue(new Callback<CheckModel>() {
            @Override
            public void onResponse(Call<CheckModel> call, Response<CheckModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                CheckModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showToast(mActivity, mModel.getMessage());
                    Intent mIntent = new Intent(mActivity, HomeActivity.class);
                    mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(mIntent);
                    finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<CheckModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

}