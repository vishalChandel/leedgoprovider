package com.leedgoprovider.app.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.leedgoprovider.app.R;
import com.leedgoprovider.app.models.DataItem;
import com.leedgoprovider.app.models.TransactionHistoryModel;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TransactionDetailActivity extends AppCompatActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = TransactionDetailActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = TransactionDetailActivity.this;

    @BindView(R.id.nameTV)
    TextView nameTV;
    @BindView(R.id.accountNumberTV)
    TextView accountNumberTV;
    @BindView(R.id.mobileNoTV)
    TextView mobileNoTV;
    @BindView(R.id.addressTV)
    TextView addressTV;
    @BindView(R.id.priceTV)
    TextView priceTV;
    @BindView(R.id.serviceNameTV)
    TextView serviceNameTV;
    @BindView(R.id.subCategoryTV)
    TextView subCategoryTV;
    @BindView(R.id.taskTV)
    TextView taskTV;
    @BindView(R.id.dateTV)
    TextView dateTV;
    @BindView(R.id.catIV)
    ImageView catIV;
    @BindView(R.id.txtHeaderTV)
    TextView txtHeaderTV;
    @BindView(R.id.backRL)
    RelativeLayout backRL;

    TransactionHistoryModel.Datum mModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_detail);
        ButterKnife.bind(this);
        txtHeaderTV.setText(getString(R.string.transaction_detail));
        if (getIntent() != null) {
            mModel = (TransactionHistoryModel.Datum) getIntent().getSerializableExtra("MyClass");
            serviceNameTV.setText(mModel.getServiceName());
            subCategoryTV.setText(mModel.getSubCategoryName());
            Float price = mModel.getAssignedAmount();
            priceTV.setText("" + price + " USD");
            taskTV.setText(mModel.getUniqueTaskId());
            mobileNoTV.setText(mModel.getUserDetail().getCellno());
            if(mModel.getGetProviderBankDetails()!=null){
                String lastFourDigits = mModel.getGetProviderBankDetails().getBankAccNumber().substring(mModel.getGetProviderBankDetails().getBankAccNumber().length() - 4);
                accountNumberTV.setText("XXXXXXXX" + lastFourDigits);
                nameTV.setText(mModel.getGetProviderBankDetails().getBankAccHolderName());
                String address = mModel.getGetProviderBankDetails().getLine1() + " " + mModel.getGetProviderBankDetails().getLine2() + " " + mModel.getGetProviderBankDetails().getCity() + " " + mModel.getGetProviderBankDetails().getState() + " " + mModel.getGetProviderBankDetails().getCountry();
                addressTV.setText(address);
            }

            if (mModel.getCategoryImage() != null && !mModel.getCategoryImage().equals("")) {
                Glide.with(mActivity)
                        .load(mModel.getCategoryImage())
                        .apply(new RequestOptions().placeholder(R.drawable.ic_pp).error(R.drawable.ic_pp))
                        .into(catIV);
            }
        }
        dateTV.setText(getDateCurrentTimeZone(Long.parseLong(mModel.getDateOfPayment())).replace("am", "AM").replace("pm", "PM"));
        backRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public String getDateCurrentTimeZone(long timestamp) {
        try {
            Calendar calendar = Calendar.getInstance();
            TimeZone tz = TimeZone.getDefault();
            calendar.setTimeInMillis(timestamp * 1000);
            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
            SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyy,HH:mm aa");

            Date currenTimeZone = (Date) calendar.getTime();
            return sdf.format(currenTimeZone);
        } catch (Exception e) {
        }
        return "";
    }

}