package com.leedgoprovider.app.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;

import com.leedgoprovider.app.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TicketSubmitted extends AppCompatActivity {
    @BindView(R.id.btnContinueB)
    Button btContinue;
    @BindView(R.id.txHeading)
    TextView txHeading;
    @BindView(R.id.txDescription)
    TextView txDesciption;
    String  serviceName="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_submitted);
        ButterKnife.bind(this);

        if (getIntent().getStringExtra("type").equals("offer"))
        {
            serviceName=getIntent().getStringExtra("serviceName");
            setViews();

        }
        else {
            setViews();
        }


        btContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Animation myAnim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bounce);
                btContinue.startAnimation(myAnim);


                Intent intent=new Intent(getApplicationContext(),HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        });

    }

    private void setViews1() {
        txHeading.setText("Registration Successfull");
        txDesciption.setText(R.string.registionSuccess);
    }

    private void setViews() {
        txHeading.setText("Offer Sent");
        txDesciption.setText(getString(R.string.offer)+serviceName+getString(R.string.offer2));
        btContinue.setText("Service List");
    }
}