package com.leedgoprovider.app.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.aigestudio.wheelpicker.WheelPicker;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.leedgoprovider.app.R;
import com.leedgoprovider.app.adapters.ServicesAdapter;
import com.leedgoprovider.app.interfaces.ServicesInterface;
import com.leedgoprovider.app.models.ServicesDataModel;
import com.leedgoprovider.app.models.ServicesModel;
import com.leedgoprovider.app.models.ServicesSubCatData;
import com.leedgoprovider.app.models.SignUpStepsModel;
import com.leedgoprovider.app.models.StateData;
import com.leedgoprovider.app.retrofit.ApiClient;
import com.leedgoprovider.app.retrofit.ApiInterface;
import com.leedgoprovider.app.utils.Constants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpStep3Activity extends BaseActivity {

    /**
     * Getting the Current Class Name
     */
    String TAG = SignUpStep3Activity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = SignUpStep3Activity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.mRecyclerViewRV)
    RecyclerView mRecyclerViewRV;
    @BindView(R.id.radioGroupRG)
    RadioGroup radioGroupRG;
    @BindView(R.id.yesRB)
    RadioButton yesRB;
    @BindView(R.id.noRB)
    RadioButton noRB;
    @BindView(R.id.mondayFridayLL)
    LinearLayout mondayFridayLL;
    @BindView(R.id.cbMonFriCB)
    CheckBox cbMonFriCB;
    @BindView(R.id.txtMonFriTV)
    TextView txtMonFriTV;
    @BindView(R.id.saturdayLL)
    LinearLayout saturdayLL;
    @BindView(R.id.cbSaturdayCB)
    CheckBox cbSaturdayCB;
    @BindView(R.id.txtSaturdayTV)
    TextView txtSaturdayTV;
    @BindView(R.id.sundayLL)
    LinearLayout sundayLL;
    @BindView(R.id.cbSundayCB)
    CheckBox cbSundayCB;
    @BindView(R.id.txtSundayTV)
    TextView txtSundayTV;
    @BindView(R.id.btnContinueB)
    Button btnContinueB;
    /*
     * Initialize...
     * */
    ArrayList<ServicesDataModel> mServicesArrayList = new ArrayList<>();
    ArrayList<ServicesSubCatData> mSelectedServicesAL = new ArrayList<>();
    SignUpStepsModel mSignUpStepsModel;
    ArrayList<StateData> mStatesArrayList = new ArrayList<>();
    boolean isSaturday = false;
    boolean isSunday = false;
    String mEmergency = "";


    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_step3);
        ButterKnife.bind(this);
        getIntentData();
        setUpRadioAndCheckButton();
        getServicesData();
    }


    private void getIntentData() {
        if (getIntent() != null) {
            mSignUpStepsModel = (SignUpStepsModel) getIntent().getSerializableExtra(Constants.MODEL);
            mStatesArrayList = (ArrayList<StateData>) getIntent().getSerializableExtra(Constants.LIST);
        }
    }

    private void getServicesData() {
        if (isNetworkAvailable(mActivity))
            executeServicesRequest();
        else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }

    private void executeServicesRequest() {
        mServicesArrayList.clear();
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getServicesRequest().enqueue(new Callback<ServicesModel>() {
            @Override
            public void onResponse(Call<ServicesModel> call, Response<ServicesModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                ServicesModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    mServicesArrayList.addAll(response.body().getData());
                    setAdapter();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ServicesModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });

    }

    ServicesInterface mServicesInterface = new ServicesInterface() {
        @Override
        public void getSelectedServices(ArrayList<ServicesSubCatData> mArrayList) {
            Log.e(TAG, "**Selected SubCategories**"+mArrayList.size());
            mSelectedServicesAL = mArrayList;
        }
    };

    private void setAdapter() {
        ServicesAdapter mSearchLocationAdapter = new ServicesAdapter(mActivity, mServicesArrayList,mServicesInterface);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        mRecyclerViewRV.setLayoutManager(mLayoutManager);
        mRecyclerViewRV.setAdapter(mSearchLocationAdapter);
    }

    private void setUpRadioAndCheckButton() {
        Typeface mTypeface = Typeface.createFromAsset(getAssets(), "AvenirRegular.otf");
        yesRB.setTypeface(mTypeface);
        noRB.setTypeface(mTypeface);
        cbMonFriCB.setTypeface(mTypeface);
        cbSaturdayCB.setTypeface(mTypeface);
        cbSundayCB.setTypeface(mTypeface);

        radioGroupRG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.yesRB:
                        mEmergency = "Yes";
                        break;
                    case R.id.noRB:
                        mEmergency = "No";
                        break;
                }
            }
        });

        cbMonFriCB.setEnabled(false);
        cbSaturdayCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                isSaturday = b;
            }
        });
        cbSundayCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                isSunday = b;
            }
        });
    }


    /*
     * Widgets Click Listener
     * */
    @OnClick({R.id.btnContinueB, R.id.txtSignInTV, R.id.txtSaturdayTV, R.id.txtSundayTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnContinueB:
                performContinueClick();
                break;
            case R.id.txtSignInTV:
                performSignInClick();
                break;
            case R.id.txtSaturdayTV:
                performSaturdayTimeClick();
                break;
            case R.id.txtSundayTV:
                performSundayTimeClick();
                break;

        }
    }

    private void performContinueClick() {
        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
       btnContinueB.startAnimation(myAnim);
        if (isValidate()) {
            mSignUpStepsModel.setServicesSubCatDataAL(mSelectedServicesAL);
            mSignUpStepsModel.setMondayFridayHours("9:00 AM - 5:00 PM");
            mSignUpStepsModel.setSaturdayHours(txtSaturdayTV.getText().toString().trim());
            mSignUpStepsModel.setSundayHours(txtSundayTV.getText().toString().trim());
            mSignUpStepsModel.setEmergencyService(mEmergency);

            Intent mIntent = new Intent(mActivity, SignUpStep4Activity.class);
            mIntent.putExtra(Constants.MODEL,mSignUpStepsModel);
            mIntent.putExtra(Constants.LIST,mStatesArrayList);
            startActivity(mIntent);
        }
    }

    private void performSignInClick() {
        Intent intent = new Intent(mActivity, SignInActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    private void performSaturdayTimeClick() {
        if (isSaturday == true)
            createBottomSheetDialog(mActivity, txtSaturdayTV);
        else
            showToast(mActivity, getString(R.string.please_checked_the_saturday));
    }

    private void performSundayTimeClick() {
        if (isSunday == true)
            createBottomSheetDialog(mActivity, txtSundayTV);
        else
            showToast(mActivity, getString(R.string.please_checked_the_sunday));
    }


    /*
     * Set up validations for fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (mSelectedServicesAL.size() == 0) {
            showAlertDialog(mActivity, getString(R.string.please_select_a_services));
            flag = false;
        }else if (isSaturday == true && txtSaturdayTV.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_select_saturday_business_hours));
            flag = false;
        }else if (isSunday == true && txtSundayTV.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_select_sunday_business_hours));
            flag = false;
        }
        return flag;
    }

    public void createBottomSheetDialog(Activity mActivity, TextView mTextView) {
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(this);
        @SuppressLint("InflateParams") View sheetView = mActivity.getLayoutInflater().inflate(R.layout.dialog_service_duration_timer, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.show();

        Typeface mTypeface = Typeface.createFromAsset(getAssets(), "AvenirRegular.otf");

        WheelPicker startHourWP = sheetView.findViewById(R.id.startHourWP);
        startHourWP.setTypeface(mTypeface);
        WheelPicker startMinuteWP = sheetView.findViewById(R.id.startMinuteWP);
        startMinuteWP.setTypeface(mTypeface);
        WheelPicker startAmPmWP = sheetView.findViewById(R.id.startAmPmWP);
        startAmPmWP.setTypeface(mTypeface);
        WheelPicker endHourWP = sheetView.findViewById(R.id.endHourWP);
        endHourWP.setTypeface(mTypeface);
        WheelPicker endMinuteWP = sheetView.findViewById(R.id.endMinuteWP);
        endMinuteWP.setTypeface(mTypeface);
        WheelPicker endAmPmWP = sheetView.findViewById(R.id.endAmPmWP);
        endAmPmWP.setTypeface(mTypeface);

        TextView txtCancelTV = sheetView.findViewById(R.id.txtCancelTV);
        TextView txtSaveTV = sheetView.findViewById(R.id.txtSaveTV);


        startHourWP.setAtmospheric(true);
        startHourWP.setCyclic(false);
        startHourWP.setCurved(true);
        startHourWP.setData(getHours());

        endHourWP.setAtmospheric(true);
        endHourWP.setCyclic(false);
        endHourWP.setCurved(true);
        endHourWP.setData(getHours());


        startMinuteWP.setAtmospheric(true);
        startMinuteWP.setCyclic(false);
        startMinuteWP.setCurved(true);
        startMinuteWP.setData(getMinutes());

        endMinuteWP.setAtmospheric(true);
        endMinuteWP.setCyclic(false);
        endMinuteWP.setCurved(true);
        endMinuteWP.setData(getMinutes());

        startAmPmWP.setAtmospheric(true);
        startAmPmWP.setCyclic(false);
        startAmPmWP.setCurved(true);
        startAmPmWP.setData(getAmPm());

        endAmPmWP.setAtmospheric(true);
        endAmPmWP.setCyclic(false);
        endAmPmWP.setCurved(true);
        endAmPmWP.setData(getAmPm());

        txtCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });

        //11:00 PM - 04:02 PM
        txtSaveTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strStartHour = getHours().get(startHourWP.getCurrentItemPosition());
                String strStartMinute = getMinutes().get(startMinuteWP.getCurrentItemPosition());
                String strStartAmPm = getAmPm().get(startAmPmWP.getCurrentItemPosition());

                String strEndHour = getHours().get(endHourWP.getCurrentItemPosition());
                String strEndMinute = getMinutes().get(endMinuteWP.getCurrentItemPosition());
                String strEndAmPm = getAmPm().get(endAmPmWP.getCurrentItemPosition());

                String time1 = strStartHour + ":" + strStartMinute + ":00" + " " + strStartAmPm;
                String time2 = strEndHour + ":" + strEndMinute + ":00" + " " + strEndAmPm;


                if (getTimeDiffrence(time1, time2) > 0 && getTimeDiffrence(time1, time2) < 24) {
                    mTextView.setText(strStartHour + ":" + strStartMinute + " " + strStartAmPm + " - " + strEndHour + ":" + strEndMinute + " " + strEndAmPm);
                } else {
                    mTextView.setText("");
                    showToast(mActivity, getString(R.string.selecct_invalid));
                }


                mBottomSheetDialog.dismiss();
            }
        });
    }

    private long getTimeDiffrence(String time1, String time2) {
        Log.e(TAG, "Time 1 :>>" + time1);
        Log.e(TAG, "Time 2 :>>" + time2);
        SimpleDateFormat format = new SimpleDateFormat("hh:mm:ss aaa");
        Date date1 = null;
        Date date2 = null;
        try {
            date1 = format.parse(time1);
            date2 = format.parse(time2);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long diff = date2.getTime() - date1.getTime();

        Log.e(TAG, "Diffrence : " + diff);

        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000) % 24;
        Log.e(TAG, "hours: " + diffHours);

        return diffHours;
    }


}