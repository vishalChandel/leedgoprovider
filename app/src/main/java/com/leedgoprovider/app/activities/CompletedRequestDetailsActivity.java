package com.leedgoprovider.app.activities;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.leedgoprovider.app.R;
import com.leedgoprovider.app.StringChange.StringFormatter;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class CompletedRequestDetailsActivity extends AppCompatActivity {

    Activity mActivity = CompletedRequestDetailsActivity.this;

    @BindView(R.id.txtHeaderTV)
    TextView txHeaderTv;

    @BindView(R.id.backRL)
    RelativeLayout backRL;

    @BindView(R.id.imgProfileCIV)
    CircleImageView imgProfileCIV;

    @BindView(R.id.UpUsername)
    TextView txUpUsername;

    @BindView(R.id.txtEmailTV)
    TextView txtEmailTV;

    @BindView(R.id.image_request)
    ImageView serviceImage;

    @BindView(R.id.servicename)
    TextView txservicename;

    @BindView(R.id.taskIDTV)
    TextView taskIDTV;

    @BindView(R.id.paymentStatusTV)
    TextView paymentStatusTV;

    @BindView(R.id.workLocation)
    TextView txworkLocation;

    @BindView(R.id.workDescription)
    TextView workDescription;

    @BindView(R.id.completedDate)
    TextView completedDateTV;

    @BindView(R.id.bidAmount)
    TextView bidAmountTV;

    @BindView(R.id.historicalYes)
    ImageView historicalYes;
    @BindView(R.id.historicalNo)
    ImageView historicalNo;
    @BindView(R.id.insuranceYes)
    ImageView insuranceYes;
    @BindView(R.id.insuranceNo)
    ImageView insuranceNo;
    @BindView(R.id.ownerYes)
    ImageView ownerYes;
    @BindView(R.id.ownerNo)
    ImageView ownerNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_completed_request_details);

        ButterKnife.bind(this);

        txHeaderTv.setText("Request Details");

        getIntentData();
        setActions();
    }

    private void setActions() {
        backRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void getIntentData() {
        if (getIntent() != null) {

            if (getIntent().getStringExtra("username") != null) {
                txUpUsername.setText(StringFormatter.capitalizeWord(getIntent().getStringExtra("username")));
            }

            if (getIntent().getStringExtra("userEmail") != null) {
                txtEmailTV.setText(getIntent().getStringExtra("userEmail"));
            }

            if (getIntent().getStringExtra("userProfile") != null) {
                Glide.with(mActivity).load(getIntent().getStringExtra("userProfile")).into(imgProfileCIV);
            }

            if (getIntent().getStringExtra("categoryName") != null) {
                txservicename.setText(getIntent().getStringExtra("categoryName"));
            }

            if (getIntent().getStringExtra("categoryImage") != null) {
                Glide.with(mActivity).load(getIntent().getStringExtra("categoryImage")).into(serviceImage);
            }

            if (getIntent().getStringExtra("taskId") != null) {
                taskIDTV.setText(getIntent().getStringExtra("taskId"));
            }

            if (getIntent().getStringExtra("paymentStatus") != null) {
                if (getIntent().getStringExtra("paymentStatus").equals("1")) {
                    paymentStatusTV.setText("PAID");
                    paymentStatusTV.setTextColor(getResources().getColor(R.color.colorPaymentDone));
                } else {
                    paymentStatusTV.setText("NOT PAID");
                    paymentStatusTV.setTextColor(getResources().getColor(R.color.colorPending));
                }
            }

            if (getIntent().getStringExtra("workLocation") != null) {
                txworkLocation.setText(getIntent().getStringExtra("workLocation"));
            }

            if (getIntent().getStringExtra("workDescription") != null) {
                workDescription.setText(getIntent().getStringExtra("workDescription"));
            }

            if (getIntent().getStringExtra("CompletedDate") != null) {
                completedDateTV.setText(getIntent().getStringExtra("CompletedDate"));
            }

            if (getIntent().getStringExtra("BidAmount") != null) {
                bidAmountTV.setText("$ " + getIntent().getStringExtra("BidAmount") + ".00");
            }

            if (getIntent().getStringExtra("insurance") != null) {
                if (getIntent().getStringExtra("insurance").trim().equals("no")) {
                    insuranceNo.setImageResource(R.drawable.ic_yes);
                    insuranceYes.setImageResource(R.drawable.ic_no);
                } else {
                    insuranceNo.setImageResource(R.drawable.ic_no);
                    insuranceYes.setImageResource(R.drawable.ic_yes);
                }
            }

            if (getIntent().getStringExtra("historical") != null) {
                if (getIntent().getStringExtra("historical").trim().equals("no")) {
                    historicalNo.setImageResource(R.drawable.ic_yes);
                    historicalYes.setImageResource(R.drawable.ic_no);
                } else {
                    historicalNo.setImageResource(R.drawable.ic_no);
                    historicalYes.setImageResource(R.drawable.ic_yes);
                }
            }

            if (getIntent().getStringExtra("owner") != null) {
                if(getIntent().getStringExtra("owner").trim().equals("representative")){
                    ownerNo.setImageResource(R.drawable.ic_yes);
                    ownerYes.setImageResource(R.drawable.ic_no);
                }else {
                    ownerNo.setImageResource(R.drawable.ic_no);
                    ownerYes.setImageResource(R.drawable.ic_yes);
                }
            }

        }
    }
}