package com.leedgoprovider.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.FragmentTransaction;

import com.bumptech.glide.Glide;
import com.leedgoprovider.app.R;
import com.leedgoprovider.app.fragments.FragmentUpcomingRequest;
import com.leedgoprovider.app.models.RoomCreateModel;
import com.leedgoprovider.app.models.TaskDetailsModel;
import com.leedgoprovider.app.retrofit.ApiClient;
import com.leedgoprovider.app.retrofit.ApiInterface;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestDetails extends BaseActivity {

    Activity mActivity = RequestDetails.this;

    @BindView(R.id.txtHeaderTV)
    TextView txHeaderTv;
    @BindView(R.id.backRL)
    RelativeLayout backRL;
    @BindView(R.id.imgProfileCIV)
    CircleImageView circleImageView;
    @BindView(R.id.UpUsername)
    TextView txUpUsername;
    @BindView(R.id.UpServiceaddress)
    TextView txUpServiceaddress;
    @BindView(R.id.llyDirections)
    LinearLayout llyDirections;
    @BindView(R.id.llyMessage)
    LinearLayout llyMessage;
    @BindView(R.id.image_request)
    ImageView serviceImage;
    @BindView(R.id.servicename)
    TextView txservicename;
    @BindView(R.id.workDate)
    TextView txworkDate;
    @BindView(R.id.flexibleWeek)
    TextView txflexibleWeek;
    @BindView(R.id.workLocation)
    TextView txworkLocation;
    @BindView(R.id.btnSubmitB)
    TextView txbtnSubmitB;
    @BindView(R.id.cancelService)
    TextView txcancelService;

    @BindView(R.id.historicalYes)
    ImageView historicalYes;
    @BindView(R.id.historicalNo)
    ImageView historicalNo;
    @BindView(R.id.insuranceYes)
    ImageView insuranceYes;
    @BindView(R.id.insuranceNo)
    ImageView insuranceNo;
    @BindView(R.id.ownerYes)
    ImageView ownerYes;
    @BindView(R.id.ownerNo)
    ImageView ownerNo;

    public static String servicename = "";

    String address, workdate, flexibleweek, serrviceimage, assigneduser, taskId, latitude, longitude, userid, reprentativeornot;
    int month = 0, year = 0, day = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_details);
        ButterKnife.bind(this);
        txHeaderTv.setText("Request Details");

        //fetching intent
        Intent intent = getIntent();
        taskId = intent.getStringExtra("taskId");
        txbtnSubmitB.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View v) {
                final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
                txbtnSubmitB.startAnimation(myAnim);
                try {
                    //   Toast.makeText(mActivity, "Mad"+workdate, Toast.LENGTH_SHORT).show();
                    Date currentDate;
                    String currentDateString = workdate;
                    SimpleDateFormat sd = new SimpleDateFormat("d-M-yyyy");
                    currentDate = sd.parse(currentDateString);
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(currentDate);
                    int year = cal.get(Calendar.YEAR);
                    int month = cal.get(Calendar.MONTH);
                    int day = cal.get(Calendar.DAY_OF_MONTH);
                    FragmentUpcomingRequest comment_f = new FragmentUpcomingRequest();
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(R.anim.in_from_bottom, R.anim.out_to_top, R.anim.in_from_top, R.anim.out_from_bottom);
                    Bundle args = new Bundle();
                    args.putString("day", String.valueOf(day));
                    args.putString("month", String.valueOf(month));
                    args.putString("year", String.valueOf(year));
                    args.putString("week", String.valueOf(flexibleweek));
                    args.putString("taskId", String.valueOf(taskId));
                    comment_f.setArguments(args);
                    transaction.addToBackStack(null);
                    transaction.replace(R.id.Fragment, comment_f).commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        txcancelService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        backRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        llyDirections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
                llyDirections.startAnimation(myAnim);
                llyDirections.setClickable(false);
                // String uri = String.format(Locale.ENGLISH, "geo:%f,%f", latitude, longitude);
                String geoUri = "http://maps.google.com/maps?q=loc:" + latitude + "," + longitude + " (" + "Client Location" + ")";
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
                startActivity(intent);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        llyDirections.setClickable(true);
                    }
                }, 1500);
            }
        });
        llyMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
                llyMessage.startAnimation(myAnim);
                llyMessage.setClickable(false);
                executeMessage();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        llyMessage.setClickable(true);
                    }
                }, 1500);
            }
        });

        executeDetailApi();
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("client_id", userid);
        mMap.put("provider_id", getProviderID());
        Log.e("Room", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeMessage() {
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.createRoom(mParam()).enqueue(new Callback<RoomCreateModel>() {
            @Override
            public void onResponse(Call<RoomCreateModel> call, Response<RoomCreateModel> response) {

                RoomCreateModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showProgressDialog(mActivity);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(mActivity, ChatActivity.class);
                            intent.putExtra("room_id", mModel.getRoom_id());
                            intent.putExtra("name", reprentativeornot);
                            startActivity(intent);

                        }
                    }, 1500);
                } else {
                    showToast(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<RoomCreateModel> call, Throwable t) {

                Log.e(String.valueOf(mActivity), "**ERROR**" + t.getMessage());
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        dismissProgressDialog();
    }

    /*
     * Execute api @param
     * */
    private Map<String, String> mDetailsParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("task_id", taskId);
        Log.e("INpro", "**PARAM**" + mMap.toString());
        return mMap;
    }

    /*
     * Execute Get Task Detail By Task Id Api
     * */
    private void executeDetailApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getRequesDetails(mDetailsParam()).enqueue(new Callback<TaskDetailsModel>() {
            @Override
            public void onResponse(Call<TaskDetailsModel> call, Response<TaskDetailsModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                TaskDetailsModel mModel = response.body();
                if (mModel.getStatus() == 1) {

                    Glide.with(mActivity).load(mModel.getData().getUserimage()).placeholder(R.drawable.ic_person).into(circleImageView);

                    try {
                        txUpUsername.setText(mModel.getData().getUsername());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    address = mModel.getData().getAddress();
                    serrviceimage = mModel.getData().getCategory_image();
                    servicename = mModel.getData().getCategory_name();
                    workdate = mModel.getData().getWork_date();
                    flexibleweek = mModel.getData().getFlexible_week();
                    userid = mModel.getData().getUser_id();
                    reprentativeornot = mModel.getData().getUsername();
                    assigneduser = mModel.getData().getAssign_user();
                    latitude = mModel.getData().getOrder_lat();
                    longitude = mModel.getData().getOrder_long();

                    Log.i("TAG123",mModel.getData().getOwner_or_authorized_representative_of_owner().trim());
                    Log.i("TAG123",mModel.getData().getRequested_for_historical_structure().trim());
                    Log.i("TAG123",mModel.getData().getRequest_covered_by_insurance_claim().trim());

                    if(mModel.getData().getRequested_for_historical_structure().trim().equals("no")){
                        historicalNo.setImageResource(R.drawable.ic_yes);
                        historicalYes.setImageResource(R.drawable.ic_no);
                    }else{
                        historicalNo.setImageResource(R.drawable.ic_no);
                        historicalYes.setImageResource(R.drawable.ic_yes);
                    }

                    if(mModel.getData().getRequest_covered_by_insurance_claim().trim().equals("no")){
                        insuranceNo.setImageResource(R.drawable.ic_yes);
                        insuranceYes.setImageResource(R.drawable.ic_no);
                    }else {
                        insuranceNo.setImageResource(R.drawable.ic_no);
                        insuranceYes.setImageResource(R.drawable.ic_yes);
                    }

                    if(mModel.getData().getOwner_or_authorized_representative_of_owner().trim().equals("representative")){
                        ownerNo.setImageResource(R.drawable.ic_yes);
                        ownerYes.setImageResource(R.drawable.ic_no);
                    }else {
                        ownerNo.setImageResource(R.drawable.ic_no);
                        ownerYes.setImageResource(R.drawable.ic_yes);
                    }
                    txUpServiceaddress.setText(mModel.getData().getAddress());
                    Glide.with(mActivity).load(mModel.getData().getCategory_image()).placeholder(R.drawable.ic_pp).into(serviceImage);
                    txservicename.setText(mModel.getData().getCategory_name());
                    txworkDate.setText(mModel.getData().getWork_date());
                    latitude = mModel.getData().getOrder_lat();
                    longitude = mModel.getData().getOrder_long();
                    userid = mModel.getData().getUser_id();
                    reprentativeornot = mModel.getData().getUsername();
                    if (txflexibleWeek != null) {
                        if (Integer.parseInt(mModel.getData().getFlexible_week()) > 1) {
                            txflexibleWeek.setText(mModel.getData().getFlexible_week() + " Weeks");
                        } else {
                            txflexibleWeek.setText(mModel.getData().getFlexible_week() + " Weeks");
                        }
                    }
                    txworkLocation.setText(mModel.getData().getAddress());

                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<TaskDetailsModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }
}