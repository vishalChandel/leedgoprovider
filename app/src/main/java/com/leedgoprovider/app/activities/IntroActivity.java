package com.leedgoprovider.app.activities;

import android.app.Activity;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.leedgoprovider.app.R;


public class IntroActivity extends AppCompatActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = IntroActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = IntroActivity.this;


    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
    }



}