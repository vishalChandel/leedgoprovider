package com.leedgoprovider.app.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


import com.leedgoprovider.app.R;
import com.leedgoprovider.app.utils.Constants;
import com.leedgoprovider.app.utils.LeedgoProviderPrefrences;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

public class BaseActivity extends AppCompatActivity {

    String TAG = BaseActivity.this.getClass().getSimpleName();

    private Dialog progressDialog;


    /*
     * Get Current User is  LoggedIN or Not
     * */
    public boolean IsLogin() {
        return LeedgoProviderPrefrences.readBoolean(BaseActivity.this, LeedgoProviderPrefrences.IS_LOGIN, false);
    }
    /*
     * Get Current User SignUp Compelete
     * */
    public boolean IsSignUpComplete() {
        return LeedgoProviderPrefrences.readBoolean(BaseActivity.this, LeedgoProviderPrefrences.IS_SIGNUP_COMPLETE, false);
    }
    /*
     * Get Remember Credentials
     * */
    public boolean IsRememberMe() {
        return LeedgoProviderPrefrences.readBoolean(BaseActivity.this, LeedgoProviderPrefrences.REMEMBER_CREDENTIALS, false);
    }
    /*
     * Get Remember Email
     * */
    public String getRemeberEmail() {
        return LeedgoProviderPrefrences.readString(BaseActivity.this, LeedgoProviderPrefrences.REMEMBER_EMAIL, "");
    }
    /*
     * Get Remember Password
     * */
    public String getRemeberPassword() {
        return LeedgoProviderPrefrences.readString(BaseActivity.this, LeedgoProviderPrefrences.REMEMBER_PASSWORD, "");
    }
    /*
     * Get Provider ID
     * */
    public String getProviderID() {
        return LeedgoProviderPrefrences.readString(BaseActivity.this, LeedgoProviderPrefrences.PROVIDER_ID, "");
    }
    /*
     * Get Provider Profile Picture
     * */
    public String getProviderPicture() {
        return LeedgoProviderPrefrences.readString(BaseActivity.this, LeedgoProviderPrefrences.PROVIDER_IMAGE, "");
    }
    /*
     * Get Provider Name
     * */
    public String getProviderName() {
        return LeedgoProviderPrefrences.readString(BaseActivity.this, LeedgoProviderPrefrences.PROVIDER_NAME, "");
    }
    /*
     * Get Provider Business EMail
     * */
    public String getProviderEmail() {
        return LeedgoProviderPrefrences.readString(BaseActivity.this, LeedgoProviderPrefrences.BUSINESS_EMAIL, "");
    }

    /*
     * Get Provider Notificaton
     * */
    public String getNotificationVal() {
        return LeedgoProviderPrefrences.readString(BaseActivity.this, LeedgoProviderPrefrences.ENABLE_PUSH, "");
    }

    /*
     * Get Provider Business Name
     * */
    public String getProviderBusinessName() {
        return LeedgoProviderPrefrences.readString(BaseActivity.this, LeedgoProviderPrefrences.PROVIDER_BUSINESS_NAME, "");
    }

    /*
     * Get Provider Business Hours
     * */
    public String getProviderBusinessHours() {
        return LeedgoProviderPrefrences.readString(BaseActivity.this, LeedgoProviderPrefrences.PROVIDER_BUSINESS_HOURS, "");
    }
    /*
     * Get Provider Website
     * */
    public String getProviderWebsite() {
        return LeedgoProviderPrefrences.readString(BaseActivity.this, LeedgoProviderPrefrences.PROVIDER_WEBSITE, "");
    }
    /*
     * Get Provider Experience
     * */
    public String getProviderExperience() {
        return LeedgoProviderPrefrences.readString(BaseActivity.this, LeedgoProviderPrefrences.PROVIDER_WORK_EXPERIENCE, "");
    }


    /*
    * Set Toolbar Text
    * */
    public void setToolbarText(TextView mTextView,String mString){
        mTextView.setText(mString);
    }



    /*
     * Finish the activity
     * */
    @Override
    public void finish() {
        super.finish();
       // overridePendingTransitionSlideDownExit();
    }

    public void hideKeyBoardFromView(Activity mActivity) {
        InputMethodManager inputMethodManager = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        View view = mActivity.getCurrentFocus();
        if (view == null) {
            view = new View(mActivity);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /*
     * To Start the New Activity
     * */
    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
       // overridePendingTransitionSlideUPEnter();
    }

    /**
     * Overrides the pending Activity transition by performing the "Enter" animation.
     */
    public void overridePendingTransitionSlideUPEnter() {
        overridePendingTransition(R.anim.bottom_up, 0);
    }

    /**
     * Overrides the pending Activity transition by performing the "Exit" animation.
     */
    public void overridePendingTransitionSlideDownExit() {
        overridePendingTransition(0, R.anim.bottom_down);
    }

    public byte[] convertBitmapToByteArrayUncompressed(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 60, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    /*
     * Show Progress Dialog
     * */
    public void showProgressDialog(Activity mActivity) {
        progressDialog = new Dialog(mActivity);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.setContentView(R.layout.dialog_progress);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);

        progressDialog.show();
    }

    /*
     * Hide Progress Dialog
     * */
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    /*
     * Hide Keyboard
     * */
    public boolean hideKeyBoad(Context context, View view) {
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
        return true;
    }

    /*
     * Validate Email Address
     * */
    public boolean isValidEmaillId(String email) {
        return Pattern.compile("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$").matcher(email).matches();
    }

    /*
     * Validate User Name
     * */
    public boolean isValidUserName(String email) {
        return Pattern.compile("/^(?=.{3,30}$)(?:[a-zA-Z\\d]+(?:(?:.|-|)[a-zA-Z\\d])*)+$/").matcher(email).matches();
    }


    /*
     * Check Internet Connections
     * */
    public boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /*
     * Error Toast Message
     * */
    public void showToast(Activity mActivity, String strMessage) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show();
    }

    /*
     *
     * Error Alert Dialog
     * */
    public void showAlertDialog(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }


    /*
     *
     * Convert Simple Date to Formated Date
     * */
    public String getConvertedDateDiffrentSymbol(String mString) {
        String[] mArray = mString.split("T");
        String formattedDate = "";
        try {
            DateFormat srcDf = new SimpleDateFormat("dd/MM/yyyy");

            // parse the date string into Date object
            Date date = srcDf.parse(mArray[0]);

            DateFormat destDf = new SimpleDateFormat("dd MMM, yyyy");

            // format the date into another format
            formattedDate = destDf.format(date);

            System.out.println("Converted date is : " + formattedDate);
        } catch (Exception e) {
            e.toString();
        }

        return formattedDate;
    }


    /*
     * Date Picker Dialog
     * */
    public void showDatePickerDialog(Activity mActivity, final TextView mTextView) {
        int mYear, mMonth, mDay, mHour, mMinute;
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(mActivity,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        int intMonth = monthOfYear + 1;
                        mTextView.setText(getFormatedString("" + dayOfMonth) + "/" + getFormatedString("" + intMonth) + "/" + year);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.getDatePicker().setMaxDate(new Date().getTime());
      //  datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        datePickerDialog.show();
    }

    /*
     * Date Picker Dialog
     * */
    public void showDatePickerDialogALL(Activity mActivity, final TextView mTextView) {
        int mYear, mMonth, mDay, mHour, mMinute;
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog datePickerDialog = new DatePickerDialog(mActivity,
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        int intMonth = monthOfYear + 1;
                        mTextView.setText(getFormatedString("" + dayOfMonth) + "/" + getFormatedString("" + intMonth) + "/" + year);

                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    public String getFormatedString(String strTemp) {
        String strActual = "";

        if (strTemp.length() == 1) {
            strActual = "0" + strTemp;
        } else if (strTemp.length() == 2) {
            strActual = strTemp;
        }

        return strActual;
    }


    /*
     * Time Picker
     * */
    public void showTimePicker(Activity mActivity, final TextView mTextView) {
        // Get Current Time
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        String format = "";

                        if (hourOfDay == 0) {

                            hourOfDay += 12;

                            format = "AM";
                        } else if (hourOfDay == 12) {

                            format = "PM";

                        } else if (hourOfDay > 12) {

                            hourOfDay -= 12;

                            format = "PM";

                        } else {

                            format = "AM";
                        }

                        String tempHour = "" + hourOfDay;
                        String tempMinute = "" + minute;
                        if (tempHour.length() == 1)
                            tempHour = "0" + hourOfDay;
                        if (tempMinute.length() == 1)
                            tempMinute = "0" + minute;

                        mTextView.setText(tempHour + ":" + tempMinute + " " + format);
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }


    /*
     *
     * Logout Dialog
     * */
    public void showLogoutDialog(final Activity mActivity) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_logout);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView btnCancelB = alertDialog.findViewById(R.id.btnCancelB);
        TextView btnOkB = alertDialog.findViewById(R.id.btnOkB);

        btnCancelB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        btnOkB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

                SharedPreferences preferences = LeedgoProviderPrefrences.getPreferences(mActivity);
                SharedPreferences.Editor editor = preferences.edit();
                editor.clear();
                editor.commit();
                editor.apply();

                Intent mIntent = new Intent(mActivity, SignInActivity.class);
                mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(mIntent);
                finish();
            }
        });
        alertDialog.show();
    }


    public void openLinkOnBrowser(String mUrl) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(mUrl));
        startActivity(i);
    }


    public int getMonthDaysCount(int year, int month) {
        int number_Of_DaysInMonth = 0;

        switch (month) {
            case 1:
                month = 1;
                number_Of_DaysInMonth = 31;
                break;
            case 2:
                month = 2;
                if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0))) {
                    number_Of_DaysInMonth = 29;
                } else {
                    number_Of_DaysInMonth = 28;
                }
                break;
            case 3:
                month = 3;
                number_Of_DaysInMonth = 31;
                break;
            case 4:
                month = 4;
                number_Of_DaysInMonth = 30;
                break;
            case 5:
                month = 5;
                number_Of_DaysInMonth = 31;
                break;
            case 6:
                month = 6;
                number_Of_DaysInMonth = 30;
                break;
            case 7:
                month = 7;
                number_Of_DaysInMonth = 31;
                break;
            case 8:
                month = 8;
                number_Of_DaysInMonth = 31;
                break;
            case 9:
                month = 9;
                number_Of_DaysInMonth = 30;
                break;
            case 10:
                month = 10;
                number_Of_DaysInMonth = 31;
                break;
            case 11:
                month = 11;
                number_Of_DaysInMonth = 30;
                break;
            case 12:
                month = 12;
                number_Of_DaysInMonth = 31;
        }

        return number_Of_DaysInMonth;
    }


    public void hideStatusBar() {

        //Hide the status bar on Android 4.0 and Lower
        if (Build.VERSION.SDK_INT < 16) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

        } else {
            View decorView = getWindow().getDecorView();
            // Hide the status bar.
            int visibility = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(visibility);

        }
    }

    /*
     * Get Hours List
     * */
    public List<String> getHours() {
        final List<String> mArrayList = new ArrayList<>();
        for (int i = 0; i < Constants.HOURS.length; i++) {
            mArrayList.add(Constants.HOURS[i]);
        }
        return mArrayList;
    }

    /*
     * Get Hours Text List
     * */
    public List<String> getHoursText() {
        final List<String> mArrayList = new ArrayList<>();
        for (int i = 0; i < Constants.HOURS_TEXT.length; i++) {
            mArrayList.add(Constants.HOURS_TEXT[i]);
        }
        return mArrayList;
    }

    /*
     * Get Hours Time Text List
     * */
    public List<String> getHoursTimeText() {
        final List<String> mArrayList = new ArrayList<>();
        for (int i = 0; i < Constants.HOURSTIME.length; i++) {
            mArrayList.add(Constants.HOURSTIME[i]);
        }
        return mArrayList;
    }

    /*
     * Get Minutes List
     * */
    public List<String> getMinutes() {
        final List<String> mArrayList = new ArrayList<>();
        for (int i = 0; i < Constants.MINUTESS.length; i++) {
            mArrayList.add(Constants.MINUTESS[i]);
        }
        return mArrayList;
    }

    /*
     * Get Minutes only 0 & 30  List
     * */
    public List<String> getMinutesInLimit() {
        final List<String> mArrayList = new ArrayList<>();
        for (int i = 0; i < Constants.MINUTESS_IN_LIMIT.length; i++) {
            mArrayList.add(Constants.MINUTESS_IN_LIMIT[i]);
        }
        return mArrayList;
    }

    /*
     * Get Minutes only 00 & 30  List
     * */
    public List<String> getMinutesLimit() {
        final List<String> mArrayList = new ArrayList<>();
        for (int i = 0; i < Constants.MINUTESS_LIMIT.length; i++) {
            mArrayList.add(Constants.MINUTESS_LIMIT[i]);
        }
        return mArrayList;
    }

    /*
     * Get Minutes Text
     * */
    public List<String> getMinutesText() {
        final List<String> mArrayList = new ArrayList<>();
        for (int i = 0; i < Constants.MINUTES_TEXT.length; i++) {
            mArrayList.add(Constants.MINUTES_TEXT[i]);
        }
        return mArrayList;
    }

    /*
     * Get AM_PM List
     * */
    public List<String> getAmPm() {
        final List<String> mArrayList = new ArrayList<>();
        for (int i = 0; i < Constants.AMPM.length; i++) {
            mArrayList.add(Constants.AMPM[i]);
        }
        return mArrayList;
    }

    /*
    * Get the Formatted Day
    * ["Sun 1","Mon 2","Tue 3","Wed 4","Thu 5","Fri 6","Sat 7"]
    * */
    public String getDayFormat(int mDay){
        if (mDay == 0){
            return "Mon";
        }else if (mDay == 1){
            return "Tue";
        }else if (mDay == 2){
            return "Wed";
        }else if (mDay == 3){
            return "Thu";
        }else if (mDay == 4){
            return "Fri";
        }else if (mDay == 5){
            return "Sat";
        }else if (mDay == 6){
            return "Sun";
        }else{
            return "";
        }
    }

    @SuppressLint("NewApi")
    public String getRealPathFromURI_API19(final Activity context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    private String getDataColumn(Context context, Uri uri, String selection,
                                 String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    private boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    private boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    private boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    private boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    //  generate dynamically string
    public String getAlphaNumericString() {
        int n = 20;
        // chose a Character random from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";

        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);
        for (int i = 0; i < n; i++)
        {
            // generate a random number between
            // 0 to AlphaNumericString variable length
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());

            // add Character one by one in end of sb
            sb.append(AlphaNumericString
                    .charAt(index));
        }
        return sb.toString();
    }

}
