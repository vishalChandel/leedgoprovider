package com.leedgoprovider.app.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.leedgoprovider.app.R;
import com.leedgoprovider.app.fragments.HomeFrament;
import com.leedgoprovider.app.models.ProviderAccountModel;
import com.leedgoprovider.app.models.profile.ProfileModel;
import com.leedgoprovider.app.retrofit.ApiClient;
import com.leedgoprovider.app.retrofit.ApiInterface;
import com.leedgoprovider.app.utils.LeedgoProviderPrefrences;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = HomeActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = HomeActivity.this;
    /*
     * Widgets
     * */
    @BindView(R.id.menuRL)
    RelativeLayout menuRL;
    @BindView(R.id.notificationRL)
    RelativeLayout notificationRL;
    @BindView(R.id.containerFL)
    FrameLayout containerFL;
    @BindView(R.id.tab1LL)
    LinearLayout tab1LL;
    @BindView(R.id.tab1IV)
    ImageView tab1IV;
    @BindView(R.id.tab2LL)
    LinearLayout tab2LL;
    @BindView(R.id.tab2IV)
    ImageView tab2IV;
    @BindView(R.id.tab3LL)
    LinearLayout tab3LL;
    @BindView(R.id.tab3IV)
    ImageView tab3IV;
    @BindView(R.id.tab4LL)
    LinearLayout tab4LL;
    @BindView(R.id.tab4IV)
    ImageView tab4IV;
    @BindView(R.id.imgNotificationBadgeIV)
    ImageView imgNotificationBadgeIV;

    /*
     * Initialize...
     * */
    String strDeviceToken = "";
    ProfileModel mProfileModel;
    /*
     * Activity Override method
     * #onActivityCreated
     * */

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        executeProviderAccountDEtail();

        switchFragment(mActivity, new HomeFrament(), false, null);

        Log.e("Test", "" + LeedgoProviderPrefrences.readString(HomeActivity.this, LeedgoProviderPrefrences.PROVIDER_ID, ""));
    }

    /*
     * Widgets Click Listener
     * */
    @OnClick({R.id.menuRL, R.id.notificationRL, R.id.tab1LL, R.id.tab2LL, R.id.tab3LL, R.id.tab4LL})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.menuRL:
                performMenuClick();
                break;
            case R.id.notificationRL:
                performNotificationClick();
                break;
            case R.id.tab1LL:
                //performTab1Click();
                break;
            case R.id.tab2LL:
                performTab2Click();
                break;
            case R.id.tab3LL:
                performTab3Click();
                break;
            case R.id.tab4LL:
                performTab4Click();
                break;
        }
    }

    private void performMenuClick() {
        startActivity(new Intent(mActivity, LeftMenuActivity.class));
    }

    private void performNotificationClick() {
        startActivity(new Intent(mActivity, NotificationsActivity.class));
    }

//    private void performTab1Click() {
//        tab1IV.setImageResource(R.drawable.ic_home_select);
//        tab2IV.setImageResource(R.drawable.ic_add_unselect);
//        tab3IV.setImageResource(R.drawable.ic_message_unselect);
//        tab4IV.setImageResource(R.drawable.ic_location_unselect);
//        switchFragment(mActivity, new HomeFrament(), false,null);
//    }

    private void performTab2Click() {
        tab1IV.setImageResource(R.drawable.ic_home_unselect);
        tab2IV.setImageResource(R.drawable.ic_add_select);
        tab3IV.setImageResource(R.drawable.ic_message_unselect);
        tab4IV.setImageResource(R.drawable.ic_location_unselect);
        switchActivity2(AddScreenActivity.class);
        finish();
    }


    private void performTab3Click() {
        tab1IV.setImageResource(R.drawable.ic_home_unselect);
        tab2IV.setImageResource(R.drawable.ic_add_unselect);
        tab3IV.setImageResource(R.drawable.ic_message_select);
        tab4IV.setImageResource(R.drawable.ic_location_unselect);
        switchActivity20(ChatMainActivity.class);
        finish();
    }


    private void performTab4Click() {
        tab1IV.setImageResource(R.drawable.ic_home_unselect);
        tab2IV.setImageResource(R.drawable.ic_add_unselect);
        tab3IV.setImageResource(R.drawable.ic_message_unselect);
        tab4IV.setImageResource(R.drawable.ic_location_select);
        switchActivity3(AddLocation.class);
        finish();
    }


    private void switchActivity3(Class<AddLocation> addLocationClass) {
        Intent intent = new Intent(mActivity, addLocationClass);
        startActivity(intent);
    }

    private void switchActivity20(Class<ChatMainActivity> chatMainActivityClass) {
        Intent intent = new Intent(mActivity, chatMainActivityClass);
        startActivity(intent);
    }


    /********
     *Replace Fragment In Activity
     **********/
    public void switchFragment(Activity activity, Fragment fragment, boolean addToStack, Bundle bundle) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        if (fragment != null) {
            // Replace current fragment by this new one
            ft.replace(R.id.containerFL, fragment);
            if (addToStack)
                ft.addToBackStack(null);
            if (bundle != null)
                fragment.setArguments(bundle);
            ft.commitAllowingStateLoss();
        }
    }

    /**
     * Activity Switch
     **/
    private void switchActivity2(Class<AddScreenActivity> editServiceDetailsClass) {
        Intent intent = new Intent(mActivity, editServiceDetailsClass);
        startActivity(intent);
    }

    private Map<String, String> mParam3() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("provider_id", getProviderID());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    /*
     * Execute Forgot Password Api
     * */
    private void executeProviderAccountDEtail() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getProviderDetail(mParam3()).enqueue(new Callback<ProviderAccountModel>() {
            @Override
            public void onResponse(Call<ProviderAccountModel> call, Response<ProviderAccountModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                ProviderAccountModel mModel = response.body();
                if (mModel.getStatus() == 1) {

                } else if (mModel.getStatus() == 3) {
                    if (mModel.getMessage() != null && !mModel.getMessage().equals("")) {
                        showNextAlertDialog(mActivity);

                    } else {
                        showNextAlertDialog(mActivity);
                    }


                } else if (mModel.getStatus() == 4) {
                    if (mModel.getMessage() != null && !mModel.getMessage().equals("")) {
                        showNextAlertDialog(mActivity);
                    } else {
                        showNextAlertDialog(mActivity);
                    }

                } else {
                    showNextAlertDialog(mActivity);
                }
            }

            @Override
            public void onFailure(Call<ProviderAccountModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    /*
     *
     * Error Alert Dialog
     * */
    public void showNextAlertDialog(Activity mActivity) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_next_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        Button btnCancel = alertDialog.findViewById(R.id.btnCancel);
        Button btnEditDetails = alertDialog.findViewById(R.id.btnEditDetails);
        //txtMessageTV.setText(strMessage);
        btnEditDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                startActivity(new Intent(mActivity, EditPaymentActivityN.class));
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();

            }
        });
        alertDialog.show();
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("provider_id", getProviderID());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeProfileDataApi() {

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getProviderDataRequest(mParam()).enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                mProfileModel = response.body();
                if (mProfileModel.getStatus() == 1) {
                    if (mProfileModel.getUnread_notification().equals("0")) {
                        imgNotificationBadgeIV.setVisibility(View.GONE);
                    } else {
                        imgNotificationBadgeIV.setVisibility(View.VISIBLE);
                    }

                } else {
                    showAlertDialog(mActivity, mProfileModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        executeProfileDataApi();
    }
}