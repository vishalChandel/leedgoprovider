package com.leedgoprovider.app.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.LoggingBehavior;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.leedgoprovider.app.R;
import com.leedgoprovider.app.models.CheckModel;
import com.leedgoprovider.app.models.SignInModel;
import com.leedgoprovider.app.models.SignUpModel;
import com.leedgoprovider.app.retrofit.ApiClient;
import com.leedgoprovider.app.retrofit.ApiInterface;
import com.leedgoprovider.app.utils.Constants;
import com.leedgoprovider.app.utils.LeedgoProviderPrefrences;

import org.json.JSONObject;

import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = SignUpActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = SignUpActivity.this;


    /*
     * Widgets
     * */
    @BindView(R.id.editFullNameET)
    EditText editFullNameET;
    @BindView(R.id.editEmailET)
    EditText editEmailET;
    @BindView(R.id.editPasswordET)
    EditText editPasswordET;
    @BindView(R.id.editConfirmPasswordET)
    EditText editConfirmPasswordET;
    @BindView(R.id.btnGetStartedB)
    Button btnGetStartedB;
    @BindView(R.id.btnGoogleB)
    Button btnGoogleB;
    @BindView(R.id.btnFacebookB)
    Button btnFacebookB;
    @BindView(R.id.txtSignInTV)
    TextView txtSignInTV;
    @BindView(R.id.txtTermsServicesTV)
    TextView txtTermsServicesTV;
    @BindView(R.id.txtPrivacyPolicyTV)
    TextView txtPrivacyPolicyTV;
    @BindView(R.id.txtDontSellTV)
    TextView txtDontSellTV;

    /*
     * Initialize...
     * */
    String strDeviceToken = "";
    /*
     * Initialize...
     * */

    CallbackManager mCallbackManager;
    GoogleSignInClient mGoogleSignInClient;
    String FacebookProfilePicture = "";
    FirebaseAuth firebaseAuth;
    private static final int RC_SIGN_IN = 1;

    String google_id,gooogle_name,google_email;

    private String fbEmail, fbLastName, fbFirstName, fbId, userName, fbSocialUserserName;
    String fb_Username = "";
    String facebook_id = "";
    String fb_email = "";
    private URL fbProfilePicture;

    Animation scaleUp, scaleDown;

    String mNotification_Status = "";

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        getDeviceToken();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder( GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString( R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

    }

    private void getDeviceToken() {
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            return;
                        }
                        // Get new Instance ID token
                        strDeviceToken = task.getResult().getToken();
                        Log.e(TAG, "**Push Token**" + strDeviceToken);
                    }
                });
    }


    /*
     * Widgets Click Listener
     * */
    @OnClick({R.id.btnGetStartedB, R.id.btnGoogleB, R.id.btnFacebookB, R.id.txtSignInTV,
            R.id.txtTermsServicesTV, R.id.txtPrivacyPolicyTV, R.id.txtDontSellTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnGetStartedB:
                performGetStartedClick();
                break;
            case R.id.btnGoogleB:
                performGoogleLoginClick();
                break;
            case R.id.btnFacebookB:
                performFacebookLoginClick();
                break;
            case R.id.txtSignInTV:
                performSignUpClick();
                break;
            case R.id.txtTermsServicesTV:
                performTermServicesClick();
                break;
            case R.id.txtPrivacyPolicyTV:
                performPrivacyPolicyClick();
                break;
            case R.id.txtDontSellTV:
                performDontSellClick();
                break;
        }
    }

    private void performGetStartedClick() {
        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
        btnGetStartedB.startAnimation(myAnim);
        if (strDeviceToken.equals("")) {
            getDeviceToken();
        }
        if (isValidate()) {
            if (isNetworkAvailable(mActivity))
                executeSignUpApi();
            else
                showToast(mActivity, getString(R.string.internet_connection_error));
        }
    }


    private void performGoogleLoginClick() {
        if (strDeviceToken.equals("")) {
            getDeviceToken();
        }
        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
        btnGoogleB.startAnimation(myAnim);
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
        Log.e("Check","1");




    }



    @Override
    public void onActivityResult(int requestCode, int resultCode,@Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(mCallbackManager!=null){
            Log.e("Check", "2");
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
        if (requestCode == RC_SIGN_IN) {
            GoogleSignIn.getClient(mActivity,
                    new GoogleSignInOptions.Builder( GoogleSignInOptions.DEFAULT_SIGN_IN).build()
            ).signOut();
            Log.e("Check","3");
            Task <GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {

                Log.e("Check","4");
                GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
                if (result.isSuccess()) {
// Google Sign In was successful, save Token and a state then authenticate with Firebase
                    GoogleSignInAccount account1 = result.getSignInAccount();
                    gooogle_name = account1.getDisplayName();
                    google_email = account1.getEmail();
                    google_id=account1.getId();
                    Uri photoUri = account1.getPhotoUrl();
                    String userimage = photoUri.toString();
                    Log.e("Check","40"+account1.getId()+" token:"+account1.getIdToken());
                    GoogleSignInAccount account = task.getResult( ApiException.class);
                    //  Toast.makeText(mActivity, "Successssghsvs ", Toast.LENGTH_SHORT).show();
                    // firebaseAuthWithGoogle(account);
                    executeGoogleSignInApi();
                }
                else {
                    Log.e("Check","41");
                }
            } catch (ApiException e) {

            }
        }
    }

    private void performFacebookLoginClick() {
        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
        btnFacebookB.startAnimation(myAnim);
        performFbLogin();
    }

    private void performSignUpClick() {
        startActivity(new Intent(mActivity, SignInActivity.class));
       finish();
    }

    private void performTermServicesClick() {
        Intent mIntent = new Intent(mActivity, OpenLinksActivity.class);
        mIntent.putExtra(Constants.LINK_TYPE, Constants.TERMS_SERVICES);
        startActivity(mIntent);
    }

    private void performPrivacyPolicyClick() {
        Intent mIntent = new Intent(mActivity, OpenLinksActivity.class);
        mIntent.putExtra(Constants.LINK_TYPE, Constants.PRIVACY_POLICY);
        startActivity(mIntent);
    }

    private void performDontSellClick() {
        Intent mIntent = new Intent(mActivity, OpenLinksActivity.class);
        mIntent.putExtra(Constants.LINK_TYPE, Constants.DONT_SELL_DATA);
        startActivity(mIntent);
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(mActivity, SignInActivity.class));
        finish();
    }


    /*
     * Set up validations for fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editFullNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_fullname));
            flag = false;
        } else if (editEmailET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_email_address));
            flag = false;
        } else if (!isValidEmaillId(editEmailET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_valid_email_address));
            flag = false;
        } else if (editPasswordET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password));
            flag = false;
        } else if (editConfirmPasswordET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_confirm_password));
            flag = false;
        } else if (!editPasswordET.getText().toString().trim().equals(editConfirmPasswordET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.password_should));
            flag = false;
        }
        return flag;
    }


    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("provider_name", editFullNameET.getText().toString().trim());
        mMap.put("business_email", editEmailET.getText().toString().trim());
        mMap.put("password", editPasswordET.getText().toString().trim());
        mMap.put("device_token", strDeviceToken);
        mMap.put("device_type", "android");
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeSignUpApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.signUpRequest(mParam()).enqueue(new Callback<SignUpModel>() {
            @Override
            public void onResponse(Call<SignUpModel> call, Response<SignUpModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                SignUpModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showToast(mActivity, mModel.getMessage());
                    LeedgoProviderPrefrences.writeBoolean(mActivity, LeedgoProviderPrefrences.IS_LOGIN, true);
                    LeedgoProviderPrefrences.writeBoolean(mActivity, LeedgoProviderPrefrences.IS_SIGNUP_COMPLETE, false);
                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.PROVIDER_ID, mModel.getData().getProviderId());
                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.PROVIDER_NAME, mModel.getData().getProviderName());
                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.BUSINESS_EMAIL, mModel.getData().getBusinessEmail());


                    Intent intent = new Intent(mActivity, SignUpStep1Activity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SignUpModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private Map<String, String> mParams2() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("business_email", google_email);
        mMap.put("provider_name", gooogle_name);
        mMap.put("google_id", google_id);
        mMap.put("device_token", strDeviceToken);
        mMap.put("device_type", "android");
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeGoogleSignInApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.signInGoogleRequest(mParams2()).enqueue(new Callback<SignInModel>() {
            @Override
            public void onResponse(Call<SignInModel> call, Response<SignInModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                SignInModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showToast(mActivity, "Signin Successfully");
                    LeedgoProviderPrefrences.writeBoolean(mActivity, LeedgoProviderPrefrences.IS_LOGIN, true);
                    LeedgoProviderPrefrences.writeBoolean(mActivity, LeedgoProviderPrefrences.IS_SIGNUP_COMPLETE, true);
                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.PROVIDER_ID, mModel.getData().getProviderId());
                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.PROVIDER_NAME, mModel.getData().getProviderName());
                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.BUSINESS_EMAIL, mModel.getData().getBusinessEmail());
                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.PROVIDER_IMAGE, mModel.getData().getProfileImage());
                    Intent intent = new Intent(mActivity, HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else if (mModel.getStatus() == 2) {

                    final Dialog alertDialog = new Dialog(mActivity);
                    // alertDialog.requestWindowFeature(Window.FEATURE_CUSTOM_TITLE);
                    alertDialog.setContentView(R.layout.dialog_alert);
                    alertDialog.setCanceledOnTouchOutside(false);
                    alertDialog.setCancelable(false);
                    alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                    // set the custom dialog components - text, image and button
                    TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
                    TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
                    txtMessageTV.setText("Your Details are not filled. Please fill the service details.");
                    btnDismiss.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                            LeedgoProviderPrefrences.writeBoolean(mActivity, LeedgoProviderPrefrences.IS_LOGIN, true);
                            LeedgoProviderPrefrences.writeBoolean(mActivity, LeedgoProviderPrefrences.IS_SIGNUP_COMPLETE, false);
                            LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.PROVIDER_ID, mModel.getProviderId());
                            LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.PROVIDER_NAME, mModel.getData().getProviderName());
                            LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.BUSINESS_EMAIL, mModel.getData().getBusinessEmail());
                            LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.PROVIDER_IMAGE, mModel.getData().getProfileImage());

                            Intent intent = new Intent(mActivity, SignUpStep1Activity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                    });
                    alertDialog.show();

                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SignInModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    //fb
    //fb Login
    private void performFbLogin() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            FacebookSdk.sdkInitialize(getApplicationContext());
            LoginManager.getInstance().logOut();
            AppEventsLogger.activateApp(getApplication());
            FacebookSdk.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
            mCallbackManager = CallbackManager.Factory.create();
            loginWithFacebook();
        }

    }


    private void loginWithFacebook() {

        if (isNetworkAvailable(mActivity)) {
            LoginManager.getInstance().logInWithReadPermissions(mActivity, Arrays.asList("public_profile", "email"));
            LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    Log.d("onSuccess: ", loginResult.getAccessToken().getToken());
                    Log.e("fook", "2");
                    getFacebookData(loginResult);
                }

                @Override
                public void onCancel() {
                    Log.e("fook", "3");
                    // Toast.makeText(mActivity, "Cancel", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(FacebookException error) {
                    Log.e("fook", "4");
                    Toast.makeText(mActivity, error.toString(), Toast.LENGTH_SHORT).show();
                    Log.e("error", "is" + error.toString());
                }

            });
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error));
        }

    }

    private void getFacebookData(LoginResult loginResult) {
        showProgressDialog(mActivity);
        GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                dismissProgressDialog();
                try {

                    if (object.has("id")) {
                        fbId = object.getString("id");
                        Log.e("LoginActivity", "id" + fbId);

                    }
                    //check permission first userName
                    if (object.has("first_name")) {
                        fbFirstName = object.getString("first_name");
                        Log.e("LoginActivity", "first_name" + fbFirstName);

                    }
                    //check permisson last userName
                    if (object.has("last_name")) {
                        fbLastName = object.getString("last_name");
                        Log.e("LoginActivity", "last_name" + fbLastName);
                    }
                    //check permisson email
                    if (object.has("email")) {
                        fbEmail = object.getString("email");
                        Log.e("LoginActivity", "email" + fbEmail);
                    }

                    fbSocialUserserName = fbFirstName + " " + fbLastName;

                    JSONObject jsonObject = new JSONObject(object.getString("picture"));
                    if (jsonObject != null) {
                        JSONObject dataObject = jsonObject.getJSONObject("data");
                        Log.e("Loginactivity", "json oject get picture" + dataObject);
                        fbProfilePicture = new URL("https://graph.facebook.com/" + fbId + "/picture?width=500&height=500");
                        Log.e("LoginActivity", "json object=>" + object.toString());
                    }

                    fb_Username = fbSocialUserserName;
                    fb_email = fbEmail;
                    facebook_id = fbId;

                    if (fbProfilePicture != null) {
                        FacebookProfilePicture = String.valueOf(fbProfilePicture);

                    } else {
                        FacebookProfilePicture = "";
                    }


                    executeLoginWithFbApi();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        Bundle bundle = new Bundle();
        Log.e("LoginActivity", "bundle set");
        bundle.putString("fields", "id, first_name, last_name,email,picture,gender,location");
        graphRequest.setParameters(bundle);
        graphRequest.executeAsync();

    }

    private Map<String, String> mParams3() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("business_email", fb_email);
        mMap.put("provider_name", fb_Username);
        mMap.put("fb_id", fbId);
        mMap.put("device_token", strDeviceToken);
        mMap.put("device_type", "android");
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeLoginWithFbApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.signInFbRequest(mParams3()).enqueue(new Callback<SignInModel>() {
            @Override
            public void onResponse(Call<SignInModel> call, Response<SignInModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                SignInModel mModel = response.body();
                if (mModel.getStatus() == 1) {

                    showToast(mActivity, "Signin Successfully");
                    LeedgoProviderPrefrences.writeBoolean(mActivity, LeedgoProviderPrefrences.IS_LOGIN, true);
                    LeedgoProviderPrefrences.writeBoolean(mActivity, LeedgoProviderPrefrences.IS_SIGNUP_COMPLETE, true);
                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.PROVIDER_ID, mModel.getData().getProviderId());
                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.PROVIDER_NAME, mModel.getData().getProviderName());
                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.BUSINESS_EMAIL, mModel.getData().getBusinessEmail());
                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.PROVIDER_IMAGE, mModel.getData().getProfileImage());

                    if (mModel.getData().getDisableNotification() != null && !mModel.getData().getDisableNotification().equals("")) {
                        mNotification_Status = mModel.getData().getDisableNotification();
                    }

                    executeNotificationApi();

                    Intent intent = new Intent(mActivity, HomeActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else if (mModel.getStatus() == 2) {
                    showToast(mActivity, mModel.getMessage());
                    LeedgoProviderPrefrences.writeBoolean(mActivity, LeedgoProviderPrefrences.IS_LOGIN, true);
                    LeedgoProviderPrefrences.writeBoolean(mActivity, LeedgoProviderPrefrences.IS_SIGNUP_COMPLETE, false);
                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.PROVIDER_ID, mModel.getProviderId());
                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.PROVIDER_NAME, mModel.getData().getProviderName());
                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.BUSINESS_EMAIL, mModel.getData().getBusinessEmail());
                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.PROVIDER_IMAGE, mModel.getData().getProfileImage());

                    Intent intent = new Intent(mActivity, SignUpStep1Activity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<SignInModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private Map<String, String> mParam2() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getProviderID());
        mMap.put("user_type", "provider");
        mMap.put("notification_status", mNotification_Status);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeNotificationApi() {
        //  showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getNotification(mParam2()).enqueue(new Callback<CheckModel>() {
            @Override
            public void onResponse(Call<CheckModel> call, Response<CheckModel> response) {

                CheckModel model;
                model = response.body();
                if (model.getStatus() == 1) {
                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.ENABLE_PUSH, mNotification_Status);
                }
            }

            @Override
            public void onFailure(Call<CheckModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    }