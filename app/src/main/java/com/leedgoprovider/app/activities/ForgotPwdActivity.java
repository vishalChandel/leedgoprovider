package com.leedgoprovider.app.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.leedgoprovider.app.R;
import com.leedgoprovider.app.models.ForgotPwdModel;
import com.leedgoprovider.app.models.SignUpModel;
import com.leedgoprovider.app.retrofit.ApiClient;
import com.leedgoprovider.app.retrofit.ApiInterface;
import com.leedgoprovider.app.utils.LeedgoProviderPrefrences;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPwdActivity extends BaseActivity {

    /**
     * Getting the Current Class Name
     */
    String TAG = ForgotPwdActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = ForgotPwdActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.backRL)
    RelativeLayout backRL;
    @BindView(R.id.txtHeaderTV)
    TextView txtHeaderTV;
    @BindView(R.id.editEmailET)
    EditText editEmailET;
    @BindView(R.id.btnSubmitB)
    Button btnSubmitB;


    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_pwd);
        ButterKnife.bind(this);
        setToolbarText(txtHeaderTV,getString(R.string.forgot_password_head));
    }


    /*
     * Widgets Click Listener
     * */
    @OnClick({R.id.backRL, R.id.btnSubmitB})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backRL:
                onBackPressed();
                break;
            case R.id.btnSubmitB:
                performSubmitClick();
                break;
        }
    }

    private void performSubmitClick() {
        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
        btnSubmitB.startAnimation(myAnim);
        if (isValidate()){
            if (isNetworkAvailable(mActivity))
                executeForgotApi();
            else
                showToast(mActivity,getString(R.string.internet_connection_error));
        }
    }



    /*
     * Set up validations for fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editEmailET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity,getString(R.string.please_enter_your_registered));
            flag = false;
        } else if (!isValidEmaillId(editEmailET.getText().toString().trim())) {
            showAlertDialog(mActivity,getString(R.string.please_enter_your_valid_email_address));
            flag = false;
        }
        return flag;
    }


    /*
     * Execute api @param
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("email", editEmailET.getText().toString().trim());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    /*
     * Execute Forgot Password Api
     * */
    private void executeForgotApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.forgotPasswordRequest(mParam()).enqueue(new Callback<ForgotPwdModel>() {
            @Override
            public void onResponse(Call<ForgotPwdModel> call, Response<ForgotPwdModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                ForgotPwdModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showToast(mActivity, mModel.getMessage());
                    finish();
                } else{
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ForgotPwdModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }
}