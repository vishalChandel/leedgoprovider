package com.leedgoprovider.app.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.leedgoprovider.app.R;

import com.leedgoprovider.app.models.ServiceAreaModel;
import com.leedgoprovider.app.models.SignUpStepsModel;
import com.leedgoprovider.app.models.StateData;
import com.leedgoprovider.app.utils.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpStep2Activity extends BaseActivity {

    /**
     * Getting the Current Class Name
     */
    String TAG = SignUpStep2Activity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = SignUpStep2Activity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.txtChooseCountryTV)
    TextView txtChooseCountryTV;
    @BindView(R.id.editBusinessNameET)
    EditText editBusinessNameET;
    @BindView(R.id.editAddress1ET)
    EditText editAddress1ET;
    @BindView(R.id.editAddress2ET)
    EditText editAddress2ET;
    @BindView(R.id.editCityNameET)
    EditText editCityNameET;
    @BindView(R.id.statesSpinnerSP)
    Spinner statesSpinnerSP;
    @BindView(R.id.editZipCodeET)
    EditText editZipCodeET;
    @BindView(R.id.txtServiceET)
    TextView txtServiceET;
    @BindView(R.id.btnAddB)
    Button btnAddB;
    @BindView(R.id.itemService1TV)
    TextView itemService1TV;
    @BindView(R.id.itemService2TV)
    TextView itemService2TV;
    @BindView(R.id.itemService3TV)
    TextView itemService3TV;
    @BindView(R.id.btnContinueB)
    Button btnContinueB;
    @BindView(R.id.txtSignInTV)
    TextView txtSignInTV;




    /*
     * Initialize...
     * */
    SignUpStepsModel mSignUpStepsModel;
    ArrayList<StateData> mStatesArrayList = new ArrayList<>();
    ArrayList<ServiceAreaModel> mServiceAreaAL = new ArrayList<ServiceAreaModel>();
    String mStateName = "";
    String mStateID = "";
    ServiceAreaModel mServiceAreaModel;

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_step2);
        ButterKnife.bind(this);
        getIntentData();
    }


    private void getIntentData() {
        if (getIntent() != null){
            mSignUpStepsModel = (SignUpStepsModel)getIntent().getSerializableExtra(Constants.MODEL);
            mStatesArrayList = (ArrayList<StateData>)getIntent().getSerializableExtra(Constants.LIST);
            editBusinessNameET.setText(mSignUpStepsModel.getBusinessName());
            editBusinessNameET.setSelection(mSignUpStepsModel.getBusinessName().length());
            chooseStateSpinner();
        }
    }


    /*
     * Widgets Click Listener
     * */
    @OnClick({R.id.btnContinueB, R.id.txtSignInTV, R.id.txtServiceET, R.id.btnAddB})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnContinueB:
                performContinueClick();
                break;
            case R.id.txtSignInTV:
                performSignInClick();
                break;
            case R.id.txtServiceET:
                performAddServiceClick();
                break;
            case R.id.btnAddB:
                performAddClick();
                break;

        }
    }

    private void performAddClick() {
        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
        btnAddB.startAnimation(myAnim);

        if (txtServiceET.getText().toString().length() > 0) {
            if (itemService1TV.getText().toString().trim().length() > 0 && itemService2TV.getText().toString().trim().length() > 0 && itemService3TV.getText().toString().trim().length() > 0 ) {
                showToast(mActivity, getString(R.string.you_can_add));
            } else {
                if (itemService1TV.getText().toString().trim().equals("")){
                    itemService1TV.setText(txtServiceET.getText().toString().trim());
                    mServiceAreaAL.add(mServiceAreaModel);
                    txtServiceET.setText("");
                }else if (itemService2TV.getText().toString().trim().equals("")) {
                    itemService2TV.setText(txtServiceET.getText().toString().trim());
                    mServiceAreaAL.add(mServiceAreaModel);
                    txtServiceET.setText("");
                }else if (itemService3TV.getText().toString().trim().equals("")) {
                    itemService3TV.setText(txtServiceET.getText().toString().trim());
                    mServiceAreaAL.add(mServiceAreaModel);
                    txtServiceET.setText("");
                }
            }
        }else{
            showToast(mActivity,getString(R.string.please_search_your));
        }

    }

    private void performAddServiceClick() {
        Intent mIntent = new Intent(mActivity,SearchLocationActivity.class);
        startActivityForResult(mIntent,Constants.REQUEST_CODE);
    }

    private void performContinueClick() {
        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
         btnContinueB.startAnimation(myAnim);

        if (isValidate()) {
            mSignUpStepsModel.setBusinessName(editBusinessNameET.getText().toString().trim());
            mSignUpStepsModel.setAddressLine1(editAddress1ET.getText().toString().trim());
            mSignUpStepsModel.setAddressLine2(editAddress2ET.getText().toString().trim());
            mSignUpStepsModel.setCity(editCityNameET.getText().toString().trim());
            mSignUpStepsModel.setState(mStateName);
            mSignUpStepsModel.setCountry(txtChooseCountryTV.getText().toString().trim());
            mSignUpStepsModel.setState_id(mStateID);
            mSignUpStepsModel.setZipcode(editZipCodeET.getText().toString().trim());

            mSignUpStepsModel.setAddServiceArea(mServiceAreaAL);

            Intent mIntent = new Intent(mActivity, SignUpStep3Activity.class);
            mIntent.putExtra(Constants.MODEL,mSignUpStepsModel);
            mIntent.putExtra(Constants.LIST,mStatesArrayList);
            startActivity(mIntent);
        }
    }

    private void performSignInClick() {
        Intent intent = new Intent(mActivity, SignInActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }


    /*
     * Set up validations for fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editBusinessNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_business_name));
            flag = false;
        }else if (editAddress1ET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_street_address));
            flag = false;
        }
        else if (editAddress2ET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_street_address2));
            flag = false;
        }
        else if (!editAddress2ET.getText().toString().trim().equals("")&&editAddress1ET.getText().toString().trim().length()<1) {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_suite_number));
            flag = false;
        }else if (editCityNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_city_name));
            flag = false;
        }else if (mStateName.equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_select_state_name));
            flag = false;
        }else if (editZipCodeET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_zip_code));
            flag = false;
        }else if (editZipCodeET.getText().toString().trim().length() < 5) {
            showAlertDialog(mActivity, getString(R.string.please_enter_zip_code_valid));
            flag = false;
        }else if (itemService1TV.getText().toString().trim().equals("") && itemService2TV.getText().toString().trim().equals("") && itemService3TV.getText().toString().trim().equals("") ){
            showAlertDialog(mActivity, getString(R.string.please_add_min_1_));
            flag = false;
        }
        return flag;
    }


    /*
     *  States Spinners.....
     * */
    private void chooseStateSpinner() {
        ArrayList<String> mArrayList = new ArrayList<>();
        for (int i = 0; i < mStatesArrayList.size(); i++) {
            mArrayList.add(mStatesArrayList.get(i).getAbbr());
        }

        Typeface font = Typeface.createFromAsset(mActivity.getAssets(), "AvenirMedium.otf");
        // Spinner click listener
        statesSpinnerSP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                // On selecting a spinner item
                Log.e(TAG, "onItemSelected: " + mArrayList.get(position));
                Log.e(TAG, "onItemSelected: " + mStatesArrayList.get(position).getAbbr());
                Log.e(TAG, "onItemSelected: " + mStatesArrayList.get(position).getId());
                mStateName = mStatesArrayList.get(position).getAbbr();
                mStateID = mStatesArrayList.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, mArrayList) {
            public View getView(int position, View convertView, android.view.ViewGroup parent) {
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(font);
                v.setTextColor(getResources().getColor(R.color.colorBlack));
                v.setTextSize(16);
                return v;
            }

            public View getDropDownView(final int position, View convertView, android.view.ViewGroup parent) {
                TextView v = (TextView) super.getView(position, convertView, parent);
                v.setTypeface(font);
                v.setTextColor(getResources().getColor(R.color.colorBlack));
                v.setTextSize(16);


                return v;
            }
        };

        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        statesSpinnerSP.setAdapter(adapter1);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null){
            if (requestCode == Constants.REQUEST_CODE){
                    mServiceAreaModel = (ServiceAreaModel)data.getSerializableExtra(Constants.MODEL);
                    txtServiceET.setText(mServiceAreaModel.getCity());
            }
        }
    }



}