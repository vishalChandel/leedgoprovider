package com.leedgoprovider.app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.leedgoprovider.app.LeedgoProviderApplication;
import com.leedgoprovider.app.R;
import com.leedgoprovider.app.adapters.PostChatSupportAdapter;
import com.leedgoprovider.app.models.AllSupportMessageModel;
import com.leedgoprovider.app.retrofit.ApiClient;
import com.leedgoprovider.app.retrofit.ApiInterface;
import com.leedgoprovider.app.utils.LeedgoProviderPrefrences;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatSuppotActivity extends BaseActivity {
    /*
  Getting the Current Class Name
 */
    String TAG = ChatSuppotActivity.this.getClass().getSimpleName();

    /*
      Current Activity Instance
     */
    Activity mActivity = ChatSuppotActivity.this;

    @BindView(R.id.txtHeaderTV)
    TextView txtHeaderTV;
    @BindView(R.id.chat_input_msg)
    EditText ed_inputMsg;
    @BindView(R.id.chat_send_msg)
    TextView tx_chat_send_msg;
    @BindView(R.id.backRL)
    RelativeLayout backRl;
    @BindView(R.id.chatPostRV)
    RecyclerView chatPostRV;
    @BindView(R.id.txServiceName)
    TextView txServiceName;
    @BindView(R.id.txServiceNameType)
    TextView txServiceNameType;
    @BindView(R.id.txticketNo)
    TextView txtiketNo;
    @BindView(R.id.imServiceImage)
    ImageView imServiceImage;
    String ticketId = "";
    String serviceImage,serviceName, serviceNameType;
    String toServer;
    String toServerUnicodeEncoded="";
    SharedPreferences prefv;
    private long mLastClickTab1 = 0;

    PostChatSupportAdapter mPostAdapter;
    ArrayList<AllSupportMessageModel> chatGroupModels = new ArrayList<>();
    ArrayList<AllSupportMessageModel> tempArrayList = new ArrayList<>();

    private Socket mSocket;
    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });
        }
    };
    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });
        }
    };
    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Log.e(TAG, "Error connecting");
                }
            });
        }
    };
    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.e(TAG, "*****NEW Message Data****" + args[1].toString());
                        //JSONObject mJsonObject = new JSONObject(String.valueOf((args[1])));

                        JSONObject mJsonObject = new JSONObject(args[1].toString());
                        Log.e(TAG, "**Message Data**" + mJsonObject.toString());
                        AllSupportMessageModel model = new Gson().fromJson(mJsonObject.toString(), AllSupportMessageModel.class);

                        chatGroupModels.add(model);
                        mPostAdapter.notifyDataSetChanged();
                        if (model != null) {
                            chatPostRV.scrollToPosition(chatGroupModels.size()-1);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };
    private Emitter.Listener onUserJoined = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject mJsonData = null;
                    try {
                        mJsonData = new JSONObject(String.valueOf(args[1]));

                        chatPostRV.scrollToPosition(chatGroupModels.size()-1);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });
        }
    };
    private Emitter.Listener onUserLeft = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });
        }
    };
    private Emitter.Listener onTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    boolean isTyping = (boolean) args[1];

                }
            });
        }
    };
    private Emitter.Listener onStopTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                }
            });
        }
    };
    private Runnable onTypingTimeout = new Runnable() {
        @Override
        public void run() {

        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_suppot);
        ButterKnife.bind(this);
        getData();
        setData();

        setEditFocus();
        setUpSocketData();
        prefv=getSharedPreferences("pref", Context.MODE_PRIVATE);

        LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.SCROLL, null);
        ed_inputMsg.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
        tx_chat_send_msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
                tx_chat_send_msg.startAnimation(myAnim);


                //  tx_chat_send_msg.setEnabled(false);


                toServer = ed_inputMsg.getText().toString();
                ed_inputMsg.setText("");

                //    if (isValidate()) {
                if (!toServer.equals("")) {
                    toServerUnicodeEncoded = toServer;
                    //  toServerUnicodeEncoded=encodeEmoji(toServer);
                    ///imageAttachRL.setVisibility(View.GONE);

                    // showProgressDialog(mActivity);


                    Date currentTime = Calendar.getInstance().getTime();
                    SimpleDateFormat sdf = new java.text.SimpleDateFormat(" hh:mm a ");
                    // give a timezone reference for formatting (see comment at the bottom)
                    sdf.setTimeZone(TimeZone.getDefault());

                    String formattedDate = sdf.format(currentTime);

                    // }
                    // }
                    if (!isNetworkAvailable(mActivity)) {
                        Toast.makeText(mActivity, "No internet connection", Toast.LENGTH_SHORT).show();
                    } else {

                        Log.e("Chatt", toServerUnicodeEncoded);
                        fetchData();

                    }
                }
            }

        });
        backRl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toServer = ed_inputMsg.getText().toString();
                String roomIdq=ticketId;
                prefv.edit().putString(roomIdq,toServer).commit();
                finish();
            }
        });

    }

    /*
     * Execute api
     * */
    private Map<String, String> mParamt() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("role", "provider");
        mMap.put("user_id",getProviderID());
        mMap.put("ticket_no",ticketId);
        mMap.put("message", toServerUnicodeEncoded);
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }


    private void fetchData() {
        Timestamp timestamp=new Timestamp(System.currentTimeMillis());
        Long data=timestamp.getTime()/1000L;

        AllSupportMessageModel mModel = new AllSupportMessageModel();
        mModel.setRole("client");
        mModel.setCreation_time(data.toString());


        mModel.setMessage(toServer);
        chatGroupModels.add(chatGroupModels.size(), mModel);


        ed_inputMsg.setText("");
        //if (chatGroupModels != null) {
        scrollToBottom();
        // }
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);

        mApiInterface1.chatSupportSendMessage(mParamt()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                dismissProgressDialog();

                // executeChatGroupListApi();

                Log.e("checkio",response.body().toString());
                //    tx_chat_send_msg.setClickable(true);
                // AddMessageModel mModel1 = response.body();
                //  assert mModel1 != null;
                //if(mModel1.getStatus()==1)
                //  {
                JSONObject mJsonObject = null;
                try {
                    mJsonObject = new JSONObject(response.body().toString());
//                    mPostAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                JSONObject jsonObject=new JSONObject();
                try {

                    jsonObject.put("id", mModel.getId());
                    jsonObject.put("user_id", mModel.getUser_id());
                    jsonObject.put("role", mModel.getRole());
                    jsonObject.put("ticket_no", mModel.getTicket_no());
                    jsonObject.put("message", mModel.getMessage());
                    jsonObject.put("creation_time", mModel.getCreation_time());
                    jsonObject.put("category_id", mModel.getCategory_id());
                    jsonObject.put("subcategory_id", mModel.getSubcategory_id());
                    jsonObject.put("category_name", mModel.getCategory_name());
                    jsonObject.put("icon_img", mModel.getIcon_img());
                    jsonObject.put("sub_category_name", mModel.getSub_category_name());

                }
                catch (Exception  e)
                {

                }

                mSocket.emit("newMessage", ticketId, jsonObject, ed_inputMsg.getText().toString());
                // AllSupportMessageModel model = new Gson().fromJson(mJsonObject.toString(), AllSupportMessageModel.class);

                // chatGroupModels.add(model);

                if (chatGroupModels != null) {
                    setPostAdapter();
                    chatPostRV.scrollToPosition(chatGroupModels.size()-1);
                    mPostAdapter.notifyDataSetChanged();
                }

                // mPostAdapter.notify();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dismissProgressDialog();
                showAlertDialog(mActivity,t.getMessage());



            }
        });

    }

    private void getData() {
        if (getIntent() != null) {
            ticketId = getIntent().getStringExtra("ticket_no");
            // ticketId="LG-TR-6524";
            serviceName=getIntent().getStringExtra("serviceName");
            serviceNameType=getIntent().getStringExtra("serviceNameType");
            try {
                if (!getIntent().getStringExtra("serviceImage").equals("")) {
                    Glide.with(mActivity)
                            .load(getIntent().getStringExtra("serviceImage"))
                            .apply(new RequestOptions().placeholder(R.drawable.ic_pp).error(R.drawable.ic_pp))
                            .into(imServiceImage);
                }
                else {
                    SharedPreferences preference=mActivity.getSharedPreferences("LeedGo", Context.MODE_PRIVATE);

                    Set<String> set=preference.getStringSet("myKey",null);
                    List<String> sample=new ArrayList<String>(set);
                    Glide.with(mActivity)
                            .load(sample.get(new Random().nextInt(sample.size())))
                            .apply(new RequestOptions().placeholder(R.drawable.ic_pp).error(R.drawable.ic_pp))
                            .into(imServiceImage);
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }



            LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.TICKET_ID, ticketId);
            txServiceName.setText(serviceName);
            txServiceNameType.setText(serviceNameType);
            txtiketNo.setText("TicketID: "+ticketId);

        }
    }

    private void setData() {
        //  if (name != null) {
        txtHeaderTV.setText("Support");


        // }
    }

    private void setUpSocketData() {
        LeedgoProviderApplication app = (LeedgoProviderApplication) getApplication();

        mSocket = app.getSocket();
        mSocket.emit("ConncetedChat", ticketId);
        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.on("newMessage", onNewMessage);
        mSocket.on("leaveChat", onUserLeft);
        mSocket.connect();

        getChat();
    }


    private void setEditFocus() {
        ed_inputMsg.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                scrollToBottom();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    private void getChat() {
        if (!isNetworkAvailable(Objects.requireNonNull(mActivity))) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        } else {
            if (chatGroupModels != null) {
                chatGroupModels.clear();
            }
            executeChatGroupListApi();
        }

    }



    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("ticket_no", ticketId);

        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }


    private void executeChatGroupListApi() {
        tempArrayList.clear();

        showProgressDialog(mActivity);
        //  }
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.chatGroupSupport(mParam()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                dismissProgressDialog();
                tempArrayList.clear();
                // Toast.makeText(mActivity, "hello"+pageNumber, Toast.LENGTH_SHORT).show();
                Log.e(TAG, "**RESPONSE**" + response.body());

                try {
                    dismissProgressDialog();
                    tempArrayList.clear();
                    JSONObject jsonObjectMM = new JSONObject(response.body().toString());
                    JSONArray mJsonArray = jsonObjectMM.getJSONArray("data");
                    Log.e(TAG, "obj: " + jsonObjectMM.getJSONArray("data"));

                    for (int i = 0; i < mJsonArray.length(); i++) {
                        JSONObject mDataObject = mJsonArray.getJSONObject(i);
                        AllSupportMessageModel mChatModel = new AllSupportMessageModel();
                        if (!mDataObject.isNull("id")) {
                            mChatModel.setId(mDataObject.getString("id"));
                            // strRoomID = mDataObject.getString("room_id");
                        }
                        if (!mDataObject.isNull("user_id"))
                            mChatModel.setUser_id(mDataObject.getString("user_id"));
                        if (!mDataObject.isNull("role"))
                            mChatModel.setRole(mDataObject.getString("role"));
                        if (!mDataObject.isNull("ticket_no"))
                            mChatModel.setTicket_no(mDataObject.getString("ticket_no"));
                        if (!mDataObject.isNull("message"))
                            mChatModel.setMessage(mDataObject.getString("message"));
                        if (!mDataObject.isNull("creation_time"))
                            mChatModel.setCreation_time(mDataObject.getString("creation_time"));
                        if (!mDataObject.isNull("category_id"))
                            mChatModel.setCategory_id(mDataObject.getString("category_id"));
                        if (!mDataObject.isNull("subcategory_id"))
                            mChatModel.setSubcategory_id(mDataObject.getString("subcategory_id"));
                        if (!mDataObject.isNull("category_name"))
                        {
                            mChatModel.setCategory_name(mDataObject.getString("category_name"));
                        }
                        if (!mDataObject.isNull("icon_img")) {
                            mChatModel.setIcon_img(mDataObject.getString("icon_img"));
                        }
                        if (!mDataObject.isNull("sub_caetgory_name")) {
                            mChatModel.setSub_category_name(mDataObject.getString("sub_category_name"));
                        }
                        //  chatGroupModels.add(mChatModel);

                        chatGroupModels.add(mChatModel);

                        // tempArrayList.add(mChatModel);
                        try {
//                                Glide.with(mActivity)
//                                        .load(chatGroupModels.get(0).getIcon_img())
//                                        .apply(new RequestOptions().placeholder(R.drawable.ic_pp).error(R.drawable.ic_pp))
//                                        .into(imServiceImage);
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }



                    }

                    if (chatGroupModels.size() > 0) {
//
                        Collections.reverse(chatGroupModels);
//                        chatGroupModels.addAll(tempArrayList);
//
//
//                        Collections.reverse(chatGroupModels);
//
                    }


                    setPostAdapter();


                } catch (Exception e) {
                    Log.e(TAG, "**ERROR**" + e.toString());
                }




                //  setPostAdapter();
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void setPostAdapter() {

        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.setStackFromEnd(true);
        chatPostRV.setLayoutManager(layoutManager);
        mPostAdapter = new PostChatSupportAdapter(mActivity, chatGroupModels);
        chatPostRV.setAdapter(mPostAdapter);
        chatPostRV.setItemAnimator(new DefaultItemAnimator());

        //if (chatPostRV != null) {

        //  mPostAdapter = new PostChatAdapter(mActivity, chatGroupModels, dialogInterface);
        chatPostRV.setAdapter(mPostAdapter);
        scrollToBottom();
        mPostAdapter.notifyDataSetChanged();
        //  }
    }
    private void scrollToBottom() {
        chatPostRV.scrollToPosition(chatGroupModels.size() - 1);

    }


    @Override
    protected void onResume() {
        super.onResume();
        try {
            String message=prefv.getString(ticketId,"");
            String fromServerUnicodeDecoded = StringEscapeUtils.unescapeJava(message);
            ed_inputMsg.setText(fromServerUnicodeDecoded);
            ed_inputMsg.setSelection(ed_inputMsg.getText().length());
            ed_inputMsg.requestFocus();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        toServer = ed_inputMsg.getText().toString();
        String roomIdq=ticketId;
        prefv.edit().putString(roomIdq,toServer).commit();
    }
    public static String encodeEmoji (String message) {
        try {
            return URLEncoder.encode(message,
                    "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return message;
        }
    }


    public static String decodeEmoji (String message) {
        String myString= null;
        try {
            return URLDecoder.decode(
                    message, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            return message;
        }
    }
    public void preventMultipleClick() {

        // Preventing multiple clicks, using threshold of 1 second
        if (SystemClock.elapsedRealtime() - mLastClickTab1 < 1500) {
            return;
        }
        mLastClickTab1 = SystemClock.elapsedRealtime();
    }

}