package com.leedgoprovider.app.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;

import com.aigestudio.wheelpicker.WheelPicker;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.leedgoprovider.app.R;
import com.leedgoprovider.app.models.profile.ProfileModel;
import com.leedgoprovider.app.models.profile.UpdateProfileModel;
import com.leedgoprovider.app.retrofit.ApiClient;
import com.leedgoprovider.app.retrofit.ApiInterface;
import com.leedgoprovider.app.utils.ImageUtil;
import com.leedgoprovider.app.utils.LeedgoProviderPrefrences;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends BaseActivity {
    /*
     * Initialize Menifest Permissions:
     * & Camera Gallery Request @params
     * */
    public String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
    public String writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
    public String writeCamera = Manifest.permission.CAMERA;
    /**
     * Getting the Current Class Name
     */
    String TAG = EditProfileActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = EditProfileActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.backRL)
    RelativeLayout backRL;
    @BindView(R.id.txtHeaderTV)
    TextView txtHeaderTV;
    @BindView(R.id.imgProfileCIV)
    CircleImageView imgProfileCIV;
    @BindView(R.id.txtChangePicTV)
    TextView txtChangePicTV;
    @BindView(R.id.editBusinessNameET)
    EditText editBusinessNameET;
    @BindView(R.id.editNameET)
    EditText editNameET;
    @BindView(R.id.txtEmailTV)
    TextView txtEmailTV;
    @BindView(R.id.txtWorkingExperienceTV)
    TextView txtWorkingExperienceTV;
    @BindView(R.id.txtWorkingHoursTV)
    TextView txtWorkingHoursTV;
    @BindView(R.id.editWebSiteET)
    EditText editWebSiteET;
    @BindView(R.id.txtEditPwdTV)
    TextView txtEditPwdTV;
    @BindView(R.id.btnSaveB)
    Button btnSaveB;


    /*
     * Initialize Objects...
     * */
    Bitmap mBitmap = null;
    ProfileModel mProfileModel;

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);
        setToolbarText(txtHeaderTV, getString(R.string.edit_profile_your));
        //setDataOnWidgets();
        getProfileData();
    }

    private void getProfileData() {
        if (isNetworkAvailable(mActivity))
            executeProfileDataApi();
        else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }


    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("provider_id", getProviderID());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeProfileDataApi() {

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getProviderDataRequest(mParam()).enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                dismissProgressDialog();
               Log.e(TAG, "**RESPONSE**" + response.body().toString());
                mProfileModel = response.body();
                if (mProfileModel.getStatus() == 1) {
                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.PROVIDER_ID, mProfileModel.getData().getProviderId());
                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.PROVIDER_NAME, mProfileModel.getData().getProviderName());
                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.BUSINESS_EMAIL, mProfileModel.getData().getBusinessEmail());
                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.PROVIDER_IMAGE, mProfileModel.getData().getProfileImage());
                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.PROVIDER_BUSINESS_NAME, mProfileModel.getData().getBusinessName());
                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.PROVIDER_WEBSITE, mProfileModel.getData().getWebsite());
                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.PROVIDER_WORK_EXPERIENCE, mProfileModel.getData().getWorkExperince());

                    String s1="";
                    String s2="";
                    String s3="";
                    for (int i=mProfileModel.getData().getBusinessHours().size()-1; i >= 0; i--) {
                     Log.e("Checkqwe",mProfileModel.getData().getBusinessHours().get(i).getDay());
                     //try {

                         if (mProfileModel.getData().getBusinessHours().get(i).getDay().contains("Monday")||mProfileModel.getData().getBusinessHours().get(i).getDay().contains("Mon"))
                         {
                             s1="Mon-Fri "+"(" +mProfileModel.getData().getBusinessHours().get(i).getStartTime()+" to "+mProfileModel.getData().getBusinessHours().get(i).getEndTime()+")";
                               // break;
                         }
                      else if (mProfileModel.getData().getBusinessHours().get(i).getDay().contains("Sat"))
                     {
                         s2="\nSaturday "+"(" +mProfileModel.getData().getBusinessHours().get(i).getStartTime()+" to "+mProfileModel.getData().getBusinessHours().get(i).getEndTime()+")";

                     }
                  else if (mProfileModel.getData().getBusinessHours().get(i).getDay().contains("Sun"))
                     {
                         s3="\nSunday "+"(" +mProfileModel.getData().getBusinessHours().get(i).getStartTime()+" to "+mProfileModel.getData().getBusinessHours().get(i).getEndTime()+")";

                     }


                    }
                    txtWorkingHoursTV.setText(s1+s2+s3);
                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.PROVIDER_BUSINESS_HOURS, txtWorkingHoursTV.getText().toString());

                    setDataOnWidgets();
                } else {
                    showAlertDialog(mActivity, mProfileModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void setDataOnWidgets() {
        if (getProviderPicture() != null && getProviderPicture().contains("http")) {
            Glide.with(getApplicationContext())
                    .asBitmap()
                    .load(getProviderPicture())
                    .into(new SimpleTarget<Bitmap>() {
                        @Override
                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                            if (resource != null) {
                                imgProfileCIV.setImageBitmap(resource);
                                mBitmap = resource;
                            }
                        }
                        @Override
                        public void onLoadCleared(@Nullable Drawable placeholder) {
                        }
                    });
        } else {
            imgProfileCIV.setImageResource(R.drawable.ic_person);
        }
        if (getProviderEmail() != null) {
            txtEmailTV.setText(getProviderEmail());
        }
        if (getProviderName() != null) {
            editNameET.setText(getProviderName());
            editNameET.setSelection(getProviderName().length());
        }
        if (getProviderBusinessName() != null) {
            editBusinessNameET.setText(getProviderBusinessName());
            editBusinessNameET.setSelection(getProviderBusinessName().length());
        }
        if (getProviderExperience() != null) {
            txtWorkingExperienceTV.setText(getProviderExperience());
        }
        if (getProviderWebsite() != null) {
            editWebSiteET.setText(getProviderWebsite());
            editWebSiteET.setSelection(getProviderWebsite().length());
        }


   }


    /*
     * Widgets Click Listener
     * */
    @RequiresApi(api = Build.VERSION_CODES.M)
    @OnClick({R.id.backRL, R.id.txtChangePicTV, R.id.txtEditPwdTV, R.id.txtWorkingExperienceTV, R.id.btnSaveB})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backRL:
                onBackPressed();
                break;
            case R.id.txtChangePicTV:
                performChangeProfileClick();
                break;
            case R.id.txtEditPwdTV:
                performEditPwdClick();
                break;
            case R.id.txtWorkingExperienceTV:
                performWorkingExpClick();
                break;
            case R.id.btnSaveB:
                performSaveClick();
                break;
        }
    }

    private void performWorkingExpClick() {
        createBottomSheetYearsDialog(mActivity, txtWorkingExperienceTV);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void performChangeProfileClick() {
        if (checkPermission()) {
            onSelectImageClick();
        } else {
            requestPermission();
        }
    }

    private void performEditPwdClick() {
        startActivity(new Intent(mActivity, ChangePasswordActivity.class));
    }


    /*
     * Set up validations for fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editBusinessNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_business_name));
            flag = false;
        } else if (editNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_name));
            flag = false;
        } else if (txtWorkingExperienceTV.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_select_your_working_experience));
            flag = false;
        } else if (editWebSiteET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_website));
            flag = false;
        }
        return flag;
    }


    private void performSaveClick() {
        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
        btnSaveB.startAnimation(myAnim);
        if (isValidate()) {
            if (isNetworkAvailable(mActivity))
                executeUpdateProfileData();
            else
                showToast(mActivity, getString(R.string.internet_connection_error));
        }
    }


    /*
     * Execute api update profile
     * */
    private Map<String, String> mUpdateProfileParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("provider_id", getProviderID());
        mMap.put("provider_name", editNameET.getText().toString().trim());
        mMap.put("business_name", editBusinessNameET.getText().toString().trim());

        mMap.put("work_experince", txtWorkingExperienceTV.getText().toString().trim());
        mMap.put("website", editWebSiteET.getText().toString().trim());

        mMap.put("profile_image", ImageUtil.convert(mBitmap));
        Log.e("Vihal", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeUpdateProfileData() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.updateProviderProfileRequest(mUpdateProfileParam()).enqueue(new Callback<ProfileModel>() {
            @Override
            public void onResponse(Call<ProfileModel> call, Response<ProfileModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                mProfileModel = response.body();
                if (mProfileModel.getStatus() == 1) {
                    showToast(mActivity,mProfileModel.getMessage());
//                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.PROVIDER_ID, mProfileModel.getData().getProviderId());
//                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.PROVIDER_NAME, mProfileModel.getData().getProviderName());
//                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.BUSINESS_EMAIL, mProfileModel.getData().getBusinessEmail());
//                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.PROVIDER_IMAGE, mProfileModel.getData().getProfileImage());
//                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.PROVIDER_BUSINESS_NAME, mProfileModel.getData().getBusinessName());
//                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.PROVIDER_WEBSITE, mProfileModel.getData().getWebsite());
//                    LeedgoProviderPrefrences.writeString(mActivity, LeedgoProviderPrefrences.PROVIDER_WORK_EXPERIENCE, mProfileModel.getData().getWorkExperince());
//                    setDataOnWidgets();
                    finish();

                } else {
                    showAlertDialog(mActivity, mProfileModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ProfileModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    /*
     * Update Profile Picture
     * */
    private boolean checkPermission() {
        int write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage);
        int read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage);
        int camera = ContextCompat.checkSelfPermission(mActivity, writeCamera);
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private void requestPermission() {
        requestPermissions(new String[]{writeExternalStorage, writeReadStorage, writeCamera}, 369);
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 369:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    onSelectImageClick();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Log.e(TAG, "**Permission Denied**");
                }

                break;
        }
    }

    /**
     * Start pick image activity with chooser.
     */
    private void onSelectImageClick() {
        CropImage.startPickImageActivity(mActivity);
    }

    /**
     * Start crop image activity for the given image.
     */
    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.OFF)
                .setAspectRatio(50, 50)
                .setRequestedSize(120, 120)
                .setMultiTouchEnabled(false)
                .start(mActivity);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(mActivity, data);

            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(mActivity, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()

            } else {
                // no permissions required or already grunted, can start crop image activity
                startCropImageActivity(imageUri);
            }
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                try {
                    Log.e(TAG, "onActivityResult: " + result.getUri());
                    final InputStream imageStream = getContentResolver().openInputStream(result.getUri());
                    final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    selectedImage.compress(Bitmap.CompressFormat.PNG, 40, out);
                    imgProfileCIV.setImageBitmap(selectedImage);
                    mBitmap = selectedImage;

                } catch (IOException e) {
                    e.printStackTrace();
                }

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                showToast(mActivity, "Cropping failed: " + result.getError());
            }
        }
    }


    public void createBottomSheetYearsDialog(Activity mActivity, TextView mTextView) {
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(this);
        @SuppressLint("InflateParams") View sheetView = mActivity.getLayoutInflater().inflate(R.layout.dialog_bs_years, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.show();

        Typeface mTypeface = Typeface.createFromAsset(getAssets(), "AvenirMedium.otf");

        WheelPicker yearsWP = sheetView.findViewById(R.id.yearsWP);
        yearsWP.setTypeface(mTypeface);

        TextView txtCancelTV = sheetView.findViewById(R.id.txtCancelTV);
        TextView txtSaveTV = sheetView.findViewById(R.id.txtSaveTV);


        ArrayList<String> years = new ArrayList<String>();
        int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = thisYear; i >= 1970; i--) {
            years.add(Integer.toString(i));
        }


        yearsWP.setAtmospheric(true);
        yearsWP.setCyclic(false);
        yearsWP.setCurved(true);
        yearsWP.setData(years);

        txtCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });

        txtSaveTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strStateName = years.get(yearsWP.getCurrentItemPosition());
                mTextView.setText(strStateName);
                mBottomSheetDialog.dismiss();
            }
        });
    }


}