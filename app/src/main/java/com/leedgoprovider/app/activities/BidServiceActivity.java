package com.leedgoprovider.app.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.leedgoprovider.app.R;
import com.leedgoprovider.app.models.BidModel;
import com.leedgoprovider.app.retrofit.ApiClient;
import com.leedgoprovider.app.retrofit.ApiInterface;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BidServiceActivity extends BaseActivity {
    Activity mActivity = BidServiceActivity.this;

    @BindView(R.id.mCountDownCD)
    com.leedgoprovider.app.views.CountdownChronometer mCountDownCD;
    @BindView(R.id.txtHeaderTV)
    TextView txHeader;
    @BindView(R.id.service_image)
    ImageView im_service;
    @BindView(R.id.service_name)
    TextView tx_servicename;
    @BindView(R.id.address)
    TextView tx_serviceaddress;
    @BindView(R.id.bidsubmit)
    TextView tx_submit;
    @BindView(R.id.amount)
    EditText tx_amount;
    @BindView(R.id.backRL)
    RelativeLayout backRL;


    Intent intent;
    String endDate;
    String address, name, image, taskId;
    int status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bid_service);
        ButterKnife.bind(this);


        intent = getIntent();
        if (intent != null) {
            endDate = getIntent().getStringExtra("endDaate");
            name = intent.getStringExtra("name");
            address = intent.getStringExtra("address");
            image = intent.getStringExtra("image");
            taskId = intent.getStringExtra("taskId");

        }
        txHeader.setText(name + " Service");
        tx_servicename.setText(name);
        tx_serviceaddress.setText(address);
        Glide.with(this).load(image).into(im_service);

        setCountDownTimmer();

        tx_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
                tx_submit.startAnimation(myAnim);


                tx_submit.setClickable(false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        tx_submit.setClickable(true);
                    }
                }, 1500);
                if (isValidate()) {
                    executeApi();
                }

            }
        });


        //}
        backRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /*
     * Set up validations for fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (tx_amount.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, "Please enter bid amount");
            flag = false;
        } else if (Integer.parseInt(tx_amount.getText().toString().trim()) < 1) {
            showAlertDialog(mActivity, "Please enter bid amount greater than 0");
            flag = false;
        }
        return flag;
    }

    /*
     * Execute api update profile
     * */
    private Map<String, String> mUpdateProfileParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("provider_id", getProviderID());
        mMap.put("task_id", taskId);
        mMap.put("amount", tx_amount.getText().toString().trim());
        return mMap;
    }

    private void executeApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.executeBid(mUpdateProfileParam()).enqueue(new Callback<BidModel>() {
            @Override
            public void onResponse(Call<BidModel> call, Response<BidModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "RESPONSE" + response.body().toString());

                BidModel mModel = response.body();

                if (mModel.getStatus() == 1) {
                    status = 1;
                    //showAlertDialog2(mActivity,mModel.getMessage());

                    Intent intent = new Intent(mActivity, TicketSubmitted.class);
                    intent.putExtra("type", "offer");
                    intent.putExtra("serviceName", name);
                    startActivity(intent);
                    finish();
                } else {
                    finish();
                    Toast.makeText(mActivity, mModel.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<BidModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    @Override
    public void onResume() {

        mCountDownCD.start();
        super.onResume();
        intent = getIntent();

    }

    @Override
    public void onPause() {
        mCountDownCD.stop();
        super.onPause();
    }

    private void setCountDownTimmer() {
        Typeface font = Typeface.createFromAsset(getAssets(), "AvenirBold.otf");
        mCountDownCD.setTypeface(font);
        //mCountDownCD.setFormat("D:HH:MM:SS");
        String value;
        String timeDifference = "";
        //date formatter as per the coder need
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        //parse the string date-ti
        // me to Date object
        Log.e("Date", "ff:::" + endDate);
        DateFormat srcDf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        // parse the date string into Date object
        Date enddDate = null;
        try {
            enddDate = srcDf.parse(endDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        //end date will be the current system time to calculate the lapse time difference
        //if needed, coder can add end date to whatever date
        Date currenTdate = new Date();
        System.out.println(sdf.format(currenTdate));
        String val = sdf.format(currenTdate);
        Date currentDatek = null;
        try {
            currentDatek = sdf.parse(val);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (enddDate != null && currentDatek != null) {
            long duration = enddDate.getTime() - currentDatek.getTime();
            mCountDownCD.setBase(System.currentTimeMillis() + TimeUnit.MILLISECONDS.toMillis(duration));
        }
    }

    /*
     *
     * Error Alert Dialog
     * */
    public void showAlertDialog2(Activity mActivity, String strMessage) {
        final Dialog alertDialog = new Dialog(mActivity);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(R.layout.dialog_alert);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        // set the custom dialog components - text, image and button
        TextView txtMessageTV = alertDialog.findViewById(R.id.txtMessageTV);
        TextView btnDismiss = alertDialog.findViewById(R.id.btnDismiss);
        txtMessageTV.setText(strMessage);
        btnDismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                if (status == 1) {
                    finish();
                }
            }
        });
        alertDialog.show();
    }

}