package com.leedgoprovider.app.activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.leedgoprovider.app.R;
import com.leedgoprovider.app.models.ChangePwdModel;
import com.leedgoprovider.app.models.SignInModel;
import com.leedgoprovider.app.retrofit.ApiClient;
import com.leedgoprovider.app.retrofit.ApiInterface;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends BaseActivity {

    /**
     * Getting the Current Class Name
     */
    String TAG = ChangePasswordActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = ChangePasswordActivity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.backRL)
    RelativeLayout backRL;
    @BindView(R.id.txtHeaderTV)
    TextView txtHeaderTV;
    @BindView(R.id.editOldPwdET)
    EditText editOldPwdET;
    @BindView(R.id.editNewPwdET)
    EditText editNewPwdET;
    @BindView(R.id.editConfirmNewPwdET)
    EditText editConfirmNewPwdET;
    @BindView(R.id.btnSaveB)
    Button btnSaveB;


    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        setToolbarText(txtHeaderTV, getString(R.string.change_password));
    }


    /*
     * Widgets Click Listener
     * */
    @OnClick({R.id.backRL, R.id.btnSaveB})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.backRL:
                onBackPressed();
                break;
            case R.id.btnSaveB:
                performSaveClick();
                break;
        }
    }

    private void performSaveClick() {
        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
        btnSaveB.startAnimation(myAnim);
        if (isValidate()) {
            if (isNetworkAvailable(mActivity))
                executeChangePasswordApi();
            else
                showToast(mActivity, getString(R.string.internet_connection_error));
        }
    }


    /*
     * Set up validations for fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editOldPwdET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_current_passwrd));
            flag = false;
        } else if (editNewPwdET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_new_password));
            flag = false;
        } else if (editConfirmNewPwdET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_confirm_your_new_password));
            flag = false;
        } else if (!editConfirmNewPwdET.getText().toString().trim().equals(editNewPwdET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.password_should));
            flag = false;
        }
        return flag;
    }


    /*
     * Execute api
     * */
    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("provider_id", getProviderID());
        mMap.put("current_password", editOldPwdET.getText().toString().trim());
        mMap.put("new_password", editNewPwdET.getText().toString().trim());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeChangePasswordApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.changePasswordRequest(mParam()).enqueue(new Callback<ChangePwdModel>() {
            @Override
            public void onResponse(Call<ChangePwdModel> call, Response<ChangePwdModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                ChangePwdModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    showToast(mActivity, mModel.getMessage());
                    finish();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<ChangePwdModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }
}