package com.leedgoprovider.app.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.leedgoprovider.app.R;
import com.leedgoprovider.app.adapters.ChatFAdapter;
import com.leedgoprovider.app.models.ChatModel;
import com.leedgoprovider.app.retrofit.ApiClient;
import com.leedgoprovider.app.retrofit.ApiInterface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatMainActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = ChatMainActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = ChatMainActivity.this;
    @BindView(R.id.tab1LL)
    LinearLayout tab1LL;
    @BindView(R.id.tab1IV)
    ImageView tab1IV;
    @BindView(R.id.tab2LL)
    LinearLayout tab2LL;
    @BindView(R.id.tab2IV)
    ImageView tab2IV;
    @BindView(R.id.tab3LL)
    LinearLayout tab3LL;
    @BindView(R.id.tab3IV)
    ImageView tab3IV;
    @BindView(R.id.tab4LL)
    LinearLayout tab4LL;
    @BindView(R.id.tab4IV)
    ImageView tab4IV;
    @BindView(R.id.txtHeaderTV)
    TextView txtHeaderTV;
    @BindView(R.id.chatf_recyclerr)
    RecyclerView recyclerView;
    @BindView(R.id.swiperefresh_chat)
    SwipeRefreshLayout swiperefresh;
    @BindView(R.id.p_bar_chat)
    ProgressBar p_bar;
    @BindView(R.id.lly_chatf_nothing)
    LinearLayout lly_chatf_nothing;
    @BindView(R.id.chatf_searchTab_text)
    EditText tx_searchtab;
    @BindView(R.id.text_no_result_chattab)
    TextView text_no_result;

    ChatModel mGetDetailsModel;
    List<ChatModel.Data> mArrayList = new ArrayList<>();
    ChatFAdapter chatFAdapter;
    private long mLastClickTab1 = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_main);
        ButterKnife.bind(this);
        txtHeaderTV.setText("Chat");
        swiperefresh.setProgressViewOffset(false, 0, 200);
        swiperefresh.setColorSchemeResources(R.color.colorBlack);
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getDetails();
            }
        });


        getDetails();

        tx_searchtab.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                if (mArrayList.size()>0) {
                    chatFAdapter.getFilter().filter(string);
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        executeDetailsApi();
    }

    private void getDetails() {
        executeDetailsApi();
    }

    /*
     * Execute api
     * */
    private Map<String, String> mParams() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("user_id", getProviderID());
        mMap.put("user_type","provider");
        Log.e("", "**PARAM**" + mMap.toString());
        return mMap;
    }

    private void executeDetailsApi() {
        ApiInterface mApiInterface1 = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface1.getAllChats(mParams()).enqueue(new Callback<ChatModel>() {
            @Override
            public void onResponse(Call<ChatModel> call, Response<ChatModel> response) {
                dismissProgressDialog();
                swiperefresh.setRefreshing(false);
                Log.e("", "**RESPONSE**" + response.body());
                mGetDetailsModel = response.body();

                if (mGetDetailsModel.getStatus() == 1) {
                    lly_chatf_nothing.setVisibility(View.GONE);
                    mArrayList =response.body().getData();
                    setChatAdapter();
                } else {
                    lly_chatf_nothing.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onFailure(Call<ChatModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("", "**ERROR**" + t.getMessage());
            }
        });
    }

    private void setChatAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        chatFAdapter = new ChatFAdapter(this, (ArrayList<ChatModel.Data>) mArrayList,text_no_result);
        recyclerView.setAdapter(chatFAdapter);
        chatFAdapter.notifyDataSetChanged();
    }

    /*
     * Widgets Click Listener
     * */
    @OnClick({ R.id.tab1LL, R.id.tab2LL, R.id.tab3LL, R.id.tab4LL})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.tab1LL:
                performTab1Click();
                break;
            case R.id.tab2LL:
                performTab2Click();
                break;
            case R.id.tab3LL:
                performTab3Click();
                break;
            case R.id.tab4LL:
                performTab4Click();
                break;
        }}


    private void performTab1Click() {
        tab1IV.setImageResource(R.drawable.ic_home_select);
        tab2IV.setImageResource(R.drawable.ic_add_unselect);
        tab3IV.setImageResource(R.drawable.ic_message_unselect);
        tab4IV.setImageResource(R.drawable.ic_location_unselect);
        switchActivity1(HomeActivity.class);
        finish();
    }

    private void performTab2Click() {
        tab1IV.setImageResource(R.drawable.ic_home_unselect);
        tab2IV.setImageResource(R.drawable.ic_add_select);
        tab3IV.setImageResource(R.drawable.ic_message_unselect);
        tab4IV.setImageResource(R.drawable.ic_location_unselect);
        switchActivity2(AddScreenActivity.class);
        finish();
    }

    private void switchActivity2(Class<AddScreenActivity> addScreenActivityClass) {
        Intent intent=new Intent(mActivity,addScreenActivityClass);
        startActivity(intent);
    }

    private void performTab3Click() {
        tab1IV.setImageResource(R.drawable.ic_home_unselect);
        tab2IV.setImageResource(R.drawable.ic_add_unselect);
        tab3IV.setImageResource(R.drawable.ic_message_select);
        tab4IV.setImageResource(R.drawable.ic_location_unselect);
    }

    private void performTab4Click() {
        tab1IV.setImageResource(R.drawable.ic_home_unselect);
        tab2IV.setImageResource(R.drawable.ic_add_unselect);
        tab3IV.setImageResource(R.drawable.ic_message_unselect);
        tab4IV.setImageResource(R.drawable.ic_location_select);
        switchActivity20(AddLocation.class);
        finish();
    }

    private void switchActivity20(Class<AddLocation> addLocationClass) {
        Intent intent=new Intent(mActivity,addLocationClass);
        startActivity(intent);
    }

    /**Activity Switch
     * **/
    private void switchActivity1(Class<HomeActivity> homeActivityClass) {
        Intent intent=new Intent(mActivity,homeActivityClass);
        startActivity(intent);
    }

    public void preventMultipleClick() {

        // Preventing multiple clicks, using threshold of 1 second
        if (SystemClock.elapsedRealtime() - mLastClickTab1 < 1500) {
            return;
        }
        mLastClickTab1 = SystemClock.elapsedRealtime();
    }
}