package com.leedgoprovider.app.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.aigestudio.wheelpicker.WheelPicker;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.leedgoprovider.app.R;
import com.leedgoprovider.app.models.SignUpStepsModel;
import com.leedgoprovider.app.models.StateData;
import com.leedgoprovider.app.models.StatesModel;
import com.leedgoprovider.app.retrofit.ApiClient;
import com.leedgoprovider.app.retrofit.ApiInterface;
import com.leedgoprovider.app.utils.Constants;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpStep1Activity extends BaseActivity {

    /**
     * Getting the Current Class Name
     */
    String TAG = SignUpStep1Activity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = SignUpStep1Activity.this;

    /*
     * Widgets
     * */
    @BindView(R.id.editBusinessNameET)
    EditText editBusinessNameET;
    @BindView(R.id.editBusinessEmailET)
    EditText editBusinessEmailET;
    @BindView(R.id.editBusinessPhoneET)
    EditText editBusinessPhoneET;
    @BindView(R.id.editBusinessLicenceET)
    EditText editBusinessLicenceET;
    @BindView(R.id.txtLicenceStateTV)
    TextView txtLicenceStateTV;
    @BindView(R.id.txtBusinessSinceTV)
    TextView txtBusinessSinceTV;
    @BindView(R.id.editWebSiteET)
    EditText editWebSiteET;
    @BindView(R.id.btnContinueB)
    Button btnContinueB;
    @BindView(R.id.txtSignInTV)
    TextView txtSignInTV;


    /*
     * Initialize...
     * */
    ArrayList<StateData> mStatesArrayList = new ArrayList<>();
    ArrayList<String> mStatesNamesList = new ArrayList<>();


    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_step1);
        ButterKnife.bind(this);
        getStatesData();
    }

    private void getStatesData() {
        editBusinessNameET.setText(getProviderName());
        editBusinessEmailET.setText(getProviderEmail());
        if (isNetworkAvailable(mActivity))
            executeStatesRequest();
        else
            showToast(mActivity, getString(R.string.internet_connection_error));
    }

    private void executeStatesRequest() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.statesRequest().enqueue(new Callback<StatesModel>() {
            @Override
            public void onResponse(Call<StatesModel> call, Response<StatesModel> response) {
                dismissProgressDialog();
                StatesModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    mStatesArrayList.addAll(mModel.getData());
                    for (int i = 0; i < mStatesArrayList.size(); i++) {
                        mStatesNamesList.add(mStatesArrayList.get(i).getName());
                    }
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatesModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }


    /*
     * Widgets Click Listener
     * */
    @OnClick({R.id.btnContinueB, R.id.txtSignInTV, R.id.txtBusinessSinceTV, R.id.txtLicenceStateTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnContinueB:
                performContinueClick();
                break;
            case R.id.txtSignInTV:
                performSignInClick();
                break;
            case R.id.txtBusinessSinceTV:
                performWorkExperieceClick();
                break;
            case R.id.txtLicenceStateTV:
                performStateClick();
                break;

        }
    }

    private void performStateClick() {
        createBottomSheetStateDialog(mActivity, txtLicenceStateTV);
    }

    private void performWorkExperieceClick() {
        createBottomSheetYearsDialog(mActivity, txtBusinessSinceTV);
    }

    private void performContinueClick() {
        final Animation myAnim = AnimationUtils.loadAnimation(mActivity, R.anim.bounce);
        btnContinueB.startAnimation(myAnim);
        if (isValidate()) {
            SignUpStepsModel mModel = new SignUpStepsModel();
            mModel.setBusinessName(editBusinessNameET.getText().toString().trim());
            mModel.setBusinessEmail(editBusinessEmailET.getText().toString().trim());
            mModel.setBusinessPhone(editBusinessPhoneET.getText().toString().trim());
            mModel.setBusinessLicenceNo(editBusinessLicenceET.getText().toString().trim());
            mModel.setBusinessLicenceState(txtLicenceStateTV.getText().toString().trim());
            mModel.setBusinessSince(txtBusinessSinceTV.getText().toString().trim());
            mModel.setBusinessWebSite(editWebSiteET.getText().toString().trim());

            Intent mIntent = new Intent(mActivity, SignUpStep2Activity.class);
            mIntent.putExtra(Constants.MODEL, mModel);
            mIntent.putExtra(Constants.LIST, mStatesArrayList);
            startActivity(mIntent);
        }
    }

    private void performSignInClick() {
        Intent intent = new Intent(mActivity, SignInActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    /*
     * Set up validations for fields
     * */
    private boolean isValidate() {
        boolean flag = true;
        if (editBusinessNameET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_business_name));
            flag = false;
        } else if (editBusinessEmailET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_business_email_address));
            flag = false;
        } else if (!isValidEmaillId(editBusinessEmailET.getText().toString().trim())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_valid_business_email_address));
            flag = false;
        } else if (editBusinessPhoneET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_business_phone_number));
            flag = false;
        } else if (editBusinessPhoneET.getText().toString().trim().length() != 10) {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_valid_business_phone_number));
            flag = false;
        } else if (editBusinessLicenceET.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_business_licence_number));
            flag = false;
        } else if (txtLicenceStateTV.getText().toString().trim().equals("")) {
            showAlertDialog(mActivity, getString(R.string.please_select_your_licence_state));
            flag = false;
        } else if ((editWebSiteET.getText().toString().trim().length() > 1) && !editWebSiteET.getText().toString().trim().contains("www.")) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_website_url));
            flag = false;
        }
        return flag;
    }


    public void createBottomSheetStateDialog(Activity mActivity, TextView mTextView) {
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(this);
        @SuppressLint("InflateParams") View sheetView = mActivity.getLayoutInflater().inflate(R.layout.dialog_bs_states, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.show();

        Typeface mTypeface = Typeface.createFromAsset(getAssets(), "AvenirMedium.otf");

        WheelPicker startHourWP = sheetView.findViewById(R.id.statesWP);
        startHourWP.setTypeface(mTypeface);

        TextView txtCancelTV = sheetView.findViewById(R.id.txtCancelTV);
        TextView txtSaveTV = sheetView.findViewById(R.id.txtSaveTV);


        startHourWP.setAtmospheric(true);
        startHourWP.setCyclic(false);
        startHourWP.setCurved(true);
        startHourWP.setData(mStatesNamesList);

        txtCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });

        txtSaveTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strStateName = mStatesNamesList.get(startHourWP.getCurrentItemPosition());
                mTextView.setText(strStateName);
                mBottomSheetDialog.dismiss();
            }
        });
    }


    public void createBottomSheetYearsDialog(Activity mActivity, TextView mTextView) {
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(this);
        @SuppressLint("InflateParams") View sheetView = mActivity.getLayoutInflater().inflate(R.layout.dialog_bs_years, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.show();

        Typeface mTypeface = Typeface.createFromAsset(getAssets(), "AvenirMedium.otf");

        WheelPicker yearsWP = sheetView.findViewById(R.id.yearsWP);
        yearsWP.setTypeface(mTypeface);

        TextView txtCancelTV = sheetView.findViewById(R.id.txtCancelTV);
        TextView txtSaveTV = sheetView.findViewById(R.id.txtSaveTV);


        ArrayList<String> years = new ArrayList<String>();
        int thisYear = Calendar.getInstance().get(Calendar.YEAR);
        for (int i = thisYear; i >= 1970; i--) {
            years.add(Integer.toString(i));
        }


        yearsWP.setAtmospheric(true);
        yearsWP.setCyclic(false);
        yearsWP.setCurved(true);
        yearsWP.setData(years);

        txtCancelTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBottomSheetDialog.dismiss();
            }
        });

        txtSaveTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String strStateName = years.get(yearsWP.getCurrentItemPosition());
                mTextView.setText(strStateName);
                mBottomSheetDialog.dismiss();
            }
        });
    }
}