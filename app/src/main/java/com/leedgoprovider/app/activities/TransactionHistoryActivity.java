package com.leedgoprovider.app.activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.leedgoprovider.app.R;
import com.leedgoprovider.app.adapters.TransactionListAdapter;
import com.leedgoprovider.app.models.TransactionHistoryModel;
import com.leedgoprovider.app.retrofit.ApiClient;
import com.leedgoprovider.app.retrofit.ApiInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TransactionHistoryActivity extends BaseActivity {
    /**
     * Getting the Current Class Name
     */
    String TAG = TransactionHistoryActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = TransactionHistoryActivity.this;
    @BindView(R.id.txtHeaderTV)
    TextView txtHeaderTV;
    @BindView(R.id.backRL)
    RelativeLayout backRL;
    @BindView(R.id.transctionRV)
    RecyclerView transctionRV;

    //Initialize
    ArrayList<TransactionHistoryModel.Datum> mDataAL = new ArrayList<TransactionHistoryModel.Datum>();
    ArrayList<TransactionHistoryModel> mFullDataAL = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_history);
        ButterKnife.bind(this);
        txtHeaderTV.setText(R.string.transaction_history);
        if (isNetworkAvailable(mActivity)) {
            executeTransactionApi();
        } else {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error));
        }
        backRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private Map<String, String> mParam3() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("provider_id", getProviderID());
        Log.e(TAG, "**PARAM**" + mMap.toString());
        return mMap;
    }

    /*
     * Execute Forgot Password Api
     * */
    private void executeTransactionApi() {
        showProgressDialog(mActivity);
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.getTransactionHistory(mParam3()).enqueue(new Callback<TransactionHistoryModel>() {
            @Override
            public void onResponse(Call<TransactionHistoryModel> call, Response<TransactionHistoryModel> response) {
                dismissProgressDialog();
                Log.e(TAG, "**RESPONSE**" + response.body().toString());
                TransactionHistoryModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    mDataAL.addAll(mModel.getData());
                    setAdapter();
                } else {
                    showAlertDialog(mActivity, mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<TransactionHistoryModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void setAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        transctionRV.setLayoutManager(layoutManager);
        TransactionListAdapter mAdapter = new TransactionListAdapter(mActivity, mDataAL);
        transctionRV.setAdapter(mAdapter);
    }
}