package com.leedgoprovider.app.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import com.leedgoprovider.app.R;
import com.leedgoprovider.app.utils.Constants;

import java.security.MessageDigest;

public class SplashActivity extends BaseActivity {

    /**
     * Splash Close Time
     */
    public final int SPLASH_TIME_OUT = 1500;

    /**
     * Getting the Current Class Name
     */
    String TAG = SplashActivity.this.getClass().getSimpleName();

    /**
     * Current Activity Instance
     */
    Activity mActivity = SplashActivity.this;

    boolean isNotification = false;
    String notification_type = "";

    /*
     * Activity Override method
     * #onActivityCreated
     * */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        hideStatusBar();

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        setNotifications();
    }

    private void setNotifications() {
        if (getIntent().getExtras() != null) {

            notification_type = String.valueOf(getIntent().getExtras().get("type"));

            if (notification_type != null) {
                isNotification = true;

                if (notification_type.equalsIgnoreCase("chat") ||
                        notification_type.equalsIgnoreCase("order") ||
                        notification_type.equalsIgnoreCase("new_bid_request") ||
                        notification_type.equalsIgnoreCase("bid_submitted") ||
                        notification_type.equalsIgnoreCase("send_payment") ||
                        notification_type.equalsIgnoreCase("bid_approved") ||
                        notification_type.equalsIgnoreCase("task_complete") ||
                        notification_type.equalsIgnoreCase("task_date_scheduled")) {
                    Intent mIntent = new Intent(this, NotificationsActivity.class);
                    mIntent.putExtra(Constants.NOTIFICATION_TYPE, Constants.PUSH);
                    startActivity(mIntent);
                    finish();
                } else {
                    setUpSplash();
                }
            }

            if (!isNotification) {
                setUpSplash();
            }
        } else {
            setUpSplash();
        }
    }

    private void setUpSplash() {
        printHashKey(mActivity);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (IsLogin() && IsSignUpComplete()) {
                    Intent mIntent = new Intent(mActivity, HomeActivity.class);
                    startActivity(mIntent);
                    finish();
                } else if (!IsLogin() && !IsSignUpComplete()) {
                    Intent mIntent = new Intent(mActivity, SignInActivity.class);
                    startActivity(mIntent);
                    finish();
                } else if (IsLogin() && !IsSignUpComplete()) {
                    startActivity(new Intent(mActivity, SignUpStep1Activity.class));
                    finish();
                }
            }
        }, SPLASH_TIME_OUT);
    }

    public static void printHashKey(Context pContext) {
        try {
            PackageInfo info = pContext.getPackageManager().getPackageInfo(pContext.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i("HashKey",  hashKey);
            }
        } catch (Exception e) {
            Log.e("TAG", "printHashKey()", e);
        }
    }
}