package com.leedgoprovider.app;

import android.app.Application;

import com.google.firebase.FirebaseApp;
import com.leedgoprovider.app.utils.Constants;

import java.net.URISyntaxException;


import io.socket.client.IO;
import io.socket.client.Socket;

public class LeedgoProviderApplication extends Application {
    /**
     * Getting the Current Class Name
     */
    public static final String TAG = LeedgoProviderApplication.class.getSimpleName();
    /**
     * Initialize the Applications Instance
     */
    private static LeedgoProviderApplication mInstance;
    /*
     * Socket
     * */
    private Socket mSocket;

    {
        try {
            mSocket = IO.socket(Constants.SocketURL);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public static synchronized LeedgoProviderApplication getInstance() {
        return mInstance;
    }

    public Socket getSocket() {
        return mSocket;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        FirebaseApp.initializeApp(this);
    }
}
