package com.leedgoprovider.app.fragments;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.leedgoprovider.app.R;
import com.leedgoprovider.app.adapters.CompleteAdapter;
import com.leedgoprovider.app.models.ProviderCompleteTaskModel;
import com.leedgoprovider.app.retrofit.ApiClient;
import com.leedgoprovider.app.retrofit.ApiInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CompletedFragment extends BaseFragment {
    /**
     * Getting the Current Class Name
     */
    String TAG = CompletedFragment.this.getClass().getSimpleName();
    /*
     * Initialize  Un binder for butter knife;
     * */
    private Unbinder unbinder;
    /*
     * Widgets
     * */
    @BindView(R.id.completeListRV)
    RecyclerView completeListRV;
    @BindView(R.id.dummyTV)
    TextView dummyTV;


    CompleteAdapter adapter;
    ArrayList<ProviderCompleteTaskModel.Datum> modelArrayList = new ArrayList<ProviderCompleteTaskModel.Datum>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_completed, container, false);
        unbinder = ButterKnife.bind(this, view);
        if (isNetworkAvailable(getActivity())) {
            if(modelArrayList!=null){
                modelArrayList.clear();
            }
            executeApi();
        } else {
            showToast(getActivity(), getString(R.string.internet_connection_error));
        }
        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    private Map<String, String> mUpdateProfileParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("provider_id", getProviderID());
        return mMap;
    }

    private void executeApi() {
        showProgressDialog(getActivity());

        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.providerCompleteTask(mUpdateProfileParam()).enqueue(new Callback<ProviderCompleteTaskModel>() {
            @Override
            public void onResponse(Call<ProviderCompleteTaskModel> call, Response<ProviderCompleteTaskModel> response) {
                dismissProgressDialog();

                ProviderCompleteTaskModel mModel = response.body();

                if (mModel.getStatus() == 1) {
                    modelArrayList.addAll(mModel.getData());
                }
                if (modelArrayList.size() == 0) {
                    dummyTV.setVisibility(View.VISIBLE);
                    setAdapter();
                } else {
                    dummyTV.setVisibility(View.GONE);
                    setAdapter();
                }

            }

            @Override
            public void onFailure(Call<ProviderCompleteTaskModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("Dataq", "3");
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    public void setAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        completeListRV.setLayoutManager(layoutManager);
        adapter = new CompleteAdapter(getActivity(), modelArrayList);
        completeListRV.setAdapter(adapter);
    }

}