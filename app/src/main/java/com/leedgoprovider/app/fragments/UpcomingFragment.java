package com.leedgoprovider.app.fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.leedgoprovider.app.R;
import com.leedgoprovider.app.adapters.UpcomingAdapter;
import com.leedgoprovider.app.models.UpcomingTaskModel;
import com.leedgoprovider.app.retrofit.ApiClient;
import com.leedgoprovider.app.retrofit.ApiInterface;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


@RequiresApi(api = Build.VERSION_CODES.O)
public class UpcomingFragment extends BaseFragment implements OnDateSelectedListener {
    //Views
    @BindView(R.id.calendarView2)
    MaterialCalendarView calendarView;
    @BindView(R.id.upcoming_recyclerview)
    RecyclerView recyclerView;

    @BindView(R.id.upcoming_nodata)
    TextView tx_nodata;

    String currentDate;
    Context context;
    int limit = 3;
    ArrayList<UpcomingTaskModel.Data> mArrayList = new ArrayList<UpcomingTaskModel.Data>();
    boolean check = true;
    Unbinder unbinder;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_upcoming, container, false);
        unbinder = ButterKnife.bind(this, mView);
        context = getContext();

        Date date = new Date();
        calendarView.setCurrentDate(date);
        calendarView.setSelectedDate(date);

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String currentDateandTime = sdf.format(new Date());
        currentDate = currentDateandTime;

//        calendarView.state().edit()
//                .setMinimumDate(date)
//                .setMaximumDate(CalendarDay.from(2021, 5, 12))
//                .setCalendarDisplayMode(CalendarMode.WEEKS)
//                //.setSaveCurrentPosition(true)
//                .commit();

        try {
            calendarView.setOnDateChangedListener(this);
            calendarView.setTopbarVisible(true);

        } catch (Exception e) {
            e.printStackTrace();
        }
        executeApi();

        return mView;
    }

    //api execute
    private Map<String, String> mUpdateProfileParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("provider_id", getProviderID());
        mMap.put("upcoming_date", currentDate);
        return mMap;
    }

    private void executeApi() {
        showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.executeUpcomingData(mUpdateProfileParam()).enqueue(new Callback<UpcomingTaskModel>() {
            @Override
            public void onResponse(Call<UpcomingTaskModel> call, Response<UpcomingTaskModel> response) {
                dismissProgressDialog();

                UpcomingTaskModel mModel = response.body();

                if (mModel.getStatus() == 1) {
                    mArrayList = (ArrayList<UpcomingTaskModel.Data>) response.body().getData();
                    tx_nodata.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    setAdapter();

                } else {
                    recyclerView.setVisibility(View.GONE);
                    tx_nodata.setVisibility(View.VISIBLE);

                    Log.e("Data", "error fetch");
                }
            }

            @Override
            public void onFailure(Call<UpcomingTaskModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("Dataq", "3");
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void setAdapter() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        UpcomingAdapter adapter = new UpcomingAdapter(getActivity(), mArrayList, limit);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date1, boolean selected) {

        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Date date = date1.getDate();
        currentDate = String.format(format.format(date));

        executeApi();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}