package com.leedgoprovider.app.fragments;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.NonNull;

import com.leedgoprovider.app.R;
import com.leedgoprovider.app.activities.HomeActivity;
import com.leedgoprovider.app.models.RequestModel;
import com.leedgoprovider.app.retrofit.ApiClient;
import com.leedgoprovider.app.retrofit.ApiInterface;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentUpcomingRequest extends BaseFragment {
    @BindView(R.id.selecttime)
    TextView tx_selecttime;
    @BindView(R.id.cancelService)
    TextView tx_cancelservice;
    @BindView(R.id.btnSubmitB)
    TextView tx_btnSubmitB;
    int year, month, day, week, hour, minute;
    String taskId;
    long millis;
    Long timestamp;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_upcoming_request, container, false);
        Bundle bundle = getArguments();
        if (bundle != null) {
            year = Integer.parseInt(bundle.getString("year"));
            month = Integer.parseInt(bundle.getString("month"));
            day = Integer.parseInt(bundle.getString("day"));
            week = Integer.parseInt(bundle.getString("week"));
            taskId = bundle.getString("taskId");
        }
        ButterKnife.bind(this, view);
        int dayCount = (week * 7) - 1;

        CalendarView calendar = (CalendarView) view.findViewById(R.id.calendarViewT);
        Calendar febFirst = Calendar.getInstance();
        GregorianCalendar cal = new GregorianCalendar(year, month, day);
        millis = cal.getTimeInMillis();

        febFirst.set(year, month, day, 0, 0, 0);

        calendar.setMinDate(febFirst.getTimeInMillis());

        Calendar febLast = Calendar.getInstance();

        febLast.set(year, month, day + dayCount, 0, 0, 0);

        calendar.setMaxDate(febLast.getTimeInMillis());
        calendar.setDate(febFirst.getTimeInMillis());

        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                GregorianCalendar cal = new GregorianCalendar(year, month, dayOfMonth);
                millis = cal.getTimeInMillis();
                Log.e("Check", "" + millis);
            }
        });

        tx_selecttime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hourr = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minutee = mcurrentTime.get(Calendar.MINUTE);
                int am = mcurrentTime.get(Calendar.AM_PM);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String am_pm;
                        if (selectedHour == 0) {
                            selectedHour += 12;
                            am_pm = "am";
                        } else if (selectedHour == 12) {
                            am_pm = "pm";
                        } else if (selectedHour > 12) {
                            selectedHour -= 12;
                            am_pm = "pm";
                        } else {
                            am_pm = "am";
                        }


                        if (selectedMinute < 10) {
                            tx_selecttime.setText(selectedHour + ":0" + selectedMinute + " " + am_pm);

                        } else {
                            tx_selecttime.setText(selectedHour + ":" + selectedMinute + " " + am_pm);

                        }


                        hour = selectedHour;
                        minute = selectedMinute;


                    }

                }, hourr, minutee, false);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });
        tx_cancelservice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
        tx_btnSubmitB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int n_year = year;
                int n_month = month;
                int n_day = day;
                int n_hour = hour;
                int n_minute = minute;


                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.YEAR, n_year);
                calendar.set(Calendar.MONTH, n_month);
                calendar.set(Calendar.DAY_OF_MONTH, n_day);
                calendar.set(Calendar.HOUR_OF_DAY, n_hour);
                calendar.set(Calendar.MINUTE, n_minute);
                calendar.set(Calendar.SECOND, 0);
                timestamp = (calendar.getTimeInMillis()) / 1000L;
                if (isValidate()) {
                    Log.e("Check", tx_selecttime.getText().toString());
                    if (isNetworkAvailable(getContext())) {
                        executeApi();
                    } else {
                        showAlertDialog(getActivity(), "Internet connection error");
                    }
                }


            }

        });
        return view;
    }

    private boolean isValidate() {
        boolean flag = true;
        if (tx_selecttime.getText().toString().equals("")) {
            showAlertDialog(getActivity(), getString(R.string.select_a_time));
            flag = false;
        }
        return flag;
    }

    private Map<String, String> mParam() {
        Map<String, String> mMap = new HashMap<>();
        //int i=timestamp.intValue();
        mMap.put("provider_work_date", String.valueOf(Math.abs(timestamp)));
        mMap.put("task_id", taskId);
        mMap.put("provider_id", getProviderID());
        return mMap;
    }

    private void executeApi() {
        Log.e("Check", mParam().toString());
        showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.executeUpdateTaskDate(mParam()).enqueue(new Callback<RequestModel>() {
            @Override
            public void onResponse(Call<RequestModel> call, Response<RequestModel> response) {
                dismissProgressDialog();

                RequestModel mModel = response.body();

                if (mModel.getStatus() == 1) {

//                    Toast.makeText(getContext(), ""+mModel.getMessage(), Toast.LENGTH_SHORT).show();
//                    getActivity().onBackPressed();
                    showToast(getActivity(), "Work is assigned");
                    Intent mIntent = new Intent(getContext(), HomeActivity.class);
                    mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    mIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(mIntent);
                    getActivity().onBackPressed();
                } else {
                    showAlertDialog(getActivity(), mModel.getMessage());
                }
            }

            @Override
            public void onFailure(Call<RequestModel> call, Throwable t) {
                dismissProgressDialog();
                Log.e("Dataq", "3");
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }
}