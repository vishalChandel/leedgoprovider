package com.leedgoprovider.app.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.leedgoprovider.app.R;
import com.leedgoprovider.app.adapters.AddScreenAdapter;
import com.leedgoprovider.app.adapters.InProgressAdapter;
import com.leedgoprovider.app.adapters.InProgressAdapterTest;
import com.leedgoprovider.app.models.AddModel;
import com.leedgoprovider.app.models.AddScreenCheckModel;
import com.leedgoprovider.app.models.InprogressModel;
import com.leedgoprovider.app.retrofit.ApiClient;
import com.leedgoprovider.app.retrofit.ApiInterface;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class InProgressFragment extends BaseFragment {
    Context context=getContext();

    @BindView(R.id.inprogress_recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.chatPostRV)
    StickyListHeadersListView chatPostRV;
    @BindView(R.id.inprogress_nodata)
    TextView tx_nodata;

    Unbinder unbinder;
    InProgressAdapter adapter;
    InProgressAdapterTest adaptern;

    ArrayList<InprogressModel> modelArrayList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView =inflater.inflate(R.layout.fragment_in_progress, container, false);
        unbinder=ButterKnife.bind(this,mView);
        recyclerView.setVisibility(View.GONE);
        //tx_nodata=mView.findViewById(R.id.inprogress_nodata);

        exexuteApi();
        return mView;
    }



    private Map<String, String> mUpdateProfileParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("provider_id",getProviderID());
        return mMap;
    }
    private void exexuteApi() {
        showProgressDialog(getActivity());
        modelArrayList.clear();

        Log.e("Check",mUpdateProfileParam().toString());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.executeProgressData(mUpdateProfileParam()).enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                dismissProgressDialog();
               tx_nodata.setVisibility(View.VISIBLE);
                String key="";
                Log.e(TAG, "RESPONSE" + response.body().toString());
                Log.e("Check",response.body().toString());
                try {
                    JSONObject jsonObjectMM = new JSONObject(response.body().toString());

                    int val=jsonObjectMM.getInt("status");
                    Log.e(TAG, "RESPONSE" +  val);
                    JSONObject jsonObject = jsonObjectMM.getJSONObject("data");
                    if (val==1)
                    {
                        Log.e("RESPONSE","Status 1"+jsonObjectMM.toString());
                        chatPostRV.setVisibility(View.VISIBLE);
                        tx_nodata.setVisibility(View.GONE);

                    }
                    else {
                        Log.e("RESPONSE","Status 0");
                        chatPostRV.setVisibility(View.GONE);
                   tx_nodata.setVisibility(View.VISIBLE);
//
                    }

//                    }
                    Log.e(TAG, "obj: " + jsonObjectMM.getJSONObject("data"));
                    Iterator<String> keys = jsonObject.keys();
                    while (keys.hasNext()) {
                        key = keys.next();
                        Log.e(TAG, "setJsonModel: " + key);
//                        txtDateTV.setText(key);
                        if (jsonObject.get(key) instanceof JSONArray) {
                            for (int i = 0; i < ((JSONArray) jsonObject.get(key)).length(); i++) {
                                JSONObject mObject = ((JSONArray) jsonObject.get(key)).getJSONObject(i);
                                InprogressModel model = new InprogressModel();
                                model.setId(mObject.getString("id"));
                                model.setUser_id(mObject.getString("user_id"));
                                model.setCategory_id(mObject.getString("category_id"));
                                model.setSubcategory_id(mObject.getString("subcategory_id"));
                                model.setOrder_lat(mObject.getString("order_lat"));
                                model.setOrder_long(mObject.getString("order_long"));
                                model.setWork_details(mObject.getString("work_details"));
                                model.setOrder_status(mObject.getString("order_status"));
                                model.setSubcategory_id(mObject.getString("start_address"));
                                model.setAddress(mObject.getString("address"));
                                model.setWork_date(mObject.getString("work_date"));
                                model.setZip_code(mObject.getString("zip_code"));
                                model.setAssign_user(mObject.getString("assign_user"));
                                model.setFlexible_week(mObject.getString("flexible_week"));
                                model.setState(mObject.getString("state"));
                                model.setAssigned_amount(mObject.getString("assigned_amount"));
                                model.setAssigned_date(mObject.getString("assigned_date"));
                                model.setProvider_work_date(mObject.getString("provider_work_date"));
                                model.setOwner_or_authorized_representative_of_owner(mObject.getString("owner_or_authorized_representative_of_owner"));
                                model.setService_name(mObject.getString("service_name"));
                                model.setProvider_work_date1(mObject.getString("provider_work_date1"));

                                modelArrayList.add(model);
                            }
                        }

                    }

                } catch (Exception e) {
                    Log.e(TAG, "setJsonModel: " + e.toString());
                }

               setAdapter(key);
                }




            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                dismissProgressDialog();
                Log.e(TAG, "**ERROR**" + t.getMessage());
            }
        });
    }

    private void setAdapter(String key) {


        if (chatPostRV != null) {
            adaptern = new InProgressAdapterTest(getActivity(), modelArrayList,key);
            chatPostRV.setAdapter(adaptern);

        }
    }

    private void getData() {


    }

    @Override
    public void onStart() {
        super.onStart();

       // Toast.makeText(context, "trt", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        exexuteApi();
       // Toast.makeText(context, "reult", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {
        super.onResume();

       // Toast.makeText(context, "reum", Toast.LENGTH_SHORT).show();
    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}