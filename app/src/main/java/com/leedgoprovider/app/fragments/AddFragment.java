package com.leedgoprovider.app.fragments;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.leedgoprovider.app.R;

import butterknife.BindView;
import butterknife.Unbinder;

public class AddFragment extends Fragment {
    /**
     * Getting the Current Class Name
     */
    String TAG = AddFragment.this.getClass().getSimpleName();
    /*
     * Initialize  Un binder for butter knife;
     * */
    private Unbinder unbinder;
    /*
     * Widgets
     * */
    @BindView(R.id.mTabLayoutTL)
    com.leedgoprovider.app.views.CustomTabLayout mTabLayoutTL;
    @BindView(R.id.mViewPagerVP)
    ViewPager mViewPagerVP;
}
