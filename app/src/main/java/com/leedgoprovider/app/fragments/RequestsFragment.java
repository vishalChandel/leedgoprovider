
package com.leedgoprovider.app.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.leedgoprovider.app.R;
import com.leedgoprovider.app.adapters.RequestAdapter;
import com.leedgoprovider.app.models.InRequestModel;
import com.leedgoprovider.app.retrofit.ApiClient;
import com.leedgoprovider.app.retrofit.ApiInterface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class RequestsFragment extends BaseFragment {

    Context context = getContext();

    @BindView(R.id.tx_head)
    TextView tx_head;
    @BindView(R.id.tx_head_services)
    TextView tx_head_services;
    @BindView(R.id.allFillTV)
    TextView allFillTV;
    @BindView(R.id.servingTV)
    TextView servingTV;
    @BindView(R.id.tabLL)
    LinearLayout tabLL;
    @BindView(R.id.requests_recyclerview)
    RecyclerView recyclerView;
    @BindView(R.id.requests_nodata)
    TextView txnodata;
    @BindView(R.id.swipeRefreshServiceRequests)
    SwipeRefreshLayout swiperefresh;

    ArrayList<InRequestModel.Data> mArrayList = new ArrayList<InRequestModel.Data>();
    RequestAdapter adapter;
    Unbinder unbinder;
    Boolean isCurrent = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_requests, container, false);
        unbinder = ButterKnife.bind(this, mView);
        tabSelection(0);
        performClicks();
        return mView;
    }

    @SuppressLint("NewApi")
    void performClicks() {

        allFillTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isCurrent = true;
                tabSelection(0);
                executeApi();
            }
        });

        servingTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isCurrent = false;
                tabSelection(1);
                executeExpiredApi();
            }
        });
    }

    void tabSelection(int pos) {
        allFillTV.setBackgroundResource(R.drawable.bg_tab_unselect);
        servingTV.setBackgroundResource(R.drawable.bg_tab_unselect);

        tabLL.setBackgroundResource(R.drawable.bg_tab_grey);
        if (pos == 0) {
            allFillTV.setBackgroundResource(R.drawable.bg_tabs_bt);
        } else if (pos == 1) {
            servingTV.setBackgroundResource(R.drawable.bg_tabs_bt);
        }
    }


    private Map<String, String> mUpdateProfileParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("provider_id", getProviderID());
        return mMap;
    }

    private void executeApi() {
        showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.executeRequestsData(mUpdateProfileParam()).enqueue(new Callback<InRequestModel>() {
            @Override
            public void onResponse(Call<InRequestModel> call, Response<InRequestModel> response) {
                Log.e("ApiResponseStatus", "Success");
                dismissProgressDialog();
                swiperefresh.setRefreshing(false);
                InRequestModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    recyclerView.setVisibility(View.VISIBLE);
                    mArrayList = (ArrayList<InRequestModel.Data>) response.body().getData();
                    tx_head.setText("Welcome " + getProviderName() + "!");
                    tx_head_services.setText("You have " + mArrayList.size() + " service requests to bid on.");
                    setAdapter();
                    txnodata.setVisibility(View.GONE);
                } else {
                    tx_head.setText("Welcome " + getProviderName() + "!");
                    recyclerView.setVisibility(View.GONE);
                    tx_head_services.setText("You have no service requests to bid on.");
                }
            }

            @Override
            public void onFailure(Call<InRequestModel> call, Throwable t) {
                dismissProgressDialog();
                swiperefresh.setRefreshing(false);
                Log.e("ApiResponseStatus", "Failure");
            }
        });
    }

    private Map<String, String> mExpiredParam() {
        Map<String, String> mMap = new HashMap<>();
        mMap.put("provider_id", getProviderID());
        mMap.put("past", "1");
        return mMap;
    }

    private void executeExpiredApi() {
        showProgressDialog(getActivity());
        ApiInterface mApiInterface = ApiClient.getApiClient().create(ApiInterface.class);
        mApiInterface.executeRequestsData(mExpiredParam()).enqueue(new Callback<InRequestModel>() {
            @Override
            public void onResponse(Call<InRequestModel> call, Response<InRequestModel> response) {
                Log.e("ApiResponseStatus", "Success");
                dismissProgressDialog();
                swiperefresh.setRefreshing(false);
                InRequestModel mModel = response.body();
                if (mModel.getStatus() == 1) {
                    recyclerView.setVisibility(View.VISIBLE);
                    mArrayList = (ArrayList<InRequestModel.Data>) response.body().getData();
                    tx_head.setText("Welcome " + getProviderName() + "!");
                    tx_head_services.setText("You have " + mArrayList.size() + " service requests to bid on.");
                    setAdapter();
                    txnodata.setVisibility(View.GONE);
                } else {
                    recyclerView.setVisibility(View.GONE);
                    tx_head.setText("Welcome " + getProviderName() + "!");
                    tx_head_services.setText("You have no service requests to bid on.");
                }
            }

            @Override
            public void onFailure(Call<InRequestModel> call, Throwable t) {
                dismissProgressDialog();
                swiperefresh.setRefreshing(false);
                Log.e("ApiResponseStatus", "Failure");
            }
        });
    }

    public void setAdapter() {
        swiperefresh.setVisibility(View.VISIBLE);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RequestAdapter(getActivity(), mArrayList);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
//                swiperefresh.setVisibility(View.INVISIBLE);
                if (isCurrent) {
                    executeApi();
                } else {
                    executeExpiredApi();
                }
            }
        });
        executeApi();
    }

}