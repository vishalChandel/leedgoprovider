package com.leedgoprovider.app.fragments;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.leedgoprovider.app.R;
import com.leedgoprovider.app.activities.HomeActivity;
import com.leedgoprovider.app.adapters.ViewPagerAdapter;
import com.leedgoprovider.app.views.CustomTabLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class HomeFrament extends Fragment {
    /**
     * Getting the Current Class Name
     */
    String TAG = HomeFrament.this.getClass().getSimpleName();
    /*
     * Initialize  Un binder for butter knife;
     * */
    private Unbinder unbinder;
    /*
     * Widgets
     * */
    @BindView(R.id.mTabLayoutTL)
    com.leedgoprovider.app.views.CustomTabLayout mTabLayoutTL;
    @BindView(R.id.mViewPagerVP)
    ViewPager mViewPagerVP;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View mView = inflater.inflate(R.layout.fragment_home_frament, container, false);
        unbinder = ButterKnife.bind(this, mView);
        setUpViewPagerAdapter();
        return mView;
    }

    private void setUpViewPagerAdapter() {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getFragmentManager());
        mViewPagerVP.setAdapter(viewPagerAdapter);
        mTabLayoutTL.setupWithViewPager(mViewPagerVP);
//        for (int i = 0; i < mTabLayoutTL.getTabCount(); i++) {
//            Typeface mTypeface = Typeface.createFromAsset(getActivity().getAssets(), "AvenirBold.otf");
//            TextView itemTabTV = (TextView)LayoutInflater.from(getActivity()).inflate(R.layout.item_tabs_layout,null);
//
//            itemTabTV.setTypeface(mTypeface);
//            itemTabTV.setTextColor(getActivity().getResources().getColor(R.color.colorWhite));
//            mTabLayoutTL.getTabAt(i).setCustomView(itemTabTV);
//        }
    }

    /*
     * Widgets Click Listener
     * */
    /*@OnClick({R.id.userCurrentLocaRL, R.id.locationRL, R.id.txtSeeAllTV})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.userCurrentLocaRL:
                performRadiusClick();
                break;
            case R.id.locationRL:
                performLocationClick();
                break;
            case R.id.txtSeeAllTV:
                performSeeAllClick();
                break;
        }
    }*/

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}