package com.leedgoprovider.app.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class LeedgoProviderPrefrences {

    /***************
     * Define Shared Preferences Keys & MODE
     ****************/
    public static final String PREF_NAME = "App_PREF";
    public static final int MODE = Context.MODE_PRIVATE;

    /*
     * Keys
     * */
    public static final String IS_LOGIN = "is_login";
    public static final String IS_SIGNUP_COMPLETE = "is_signup_complete";
    public static final String PROVIDER_ID = "provider_id";
    public static final String PROVIDER_NAME = "provider_name";
    public static final String PROVIDER_IMAGE = "provider_image";
    public static final String PROVIDER_BUSINESS_NAME = "business_name";
    public static final String PROVIDER_WEBSITE = "website";
    public static final String PROVIDER_WORK_EXPERIENCE = "experience";
    public static final String SCROLL = "scroll";
    public static final String ROOM_ID = "room_id";

    public static final String BUSINESS_EMAIL = "business_email";
    public static final String REMEMBER_CREDENTIALS = "remember_credentials";
    public static final String REMEMBER_EMAIL = "remember_email";
    public static final String REMEMBER_PASSWORD = "remeber_password";
    public static final String TICKET_ID ="ticket_no" ;
    public static final String PHONENUMBER ="cell_no" ;
    public static final String ENABLE_PUSH ="enable_push";
    public static final String PROVIDER_BUSINESS_HOURS ="business_hours" ;
    public static final String DEVICE_TOKEN = "DEVICE_TOKEN";


    /*
     * Write the Boolean Value
     * */
    public static void writeBoolean(Context context, String key, boolean value) {
        getEditor(context).putBoolean(key, value).apply();
    }

    /*
     * Read the Boolean Value
     * */
    public static boolean readBoolean(Context context, String key, boolean defValue) {
        return getPreferences(context).getBoolean(key, defValue);
    }

    /*
     * Write the Integer Value
     * */
    public static void writeInteger(Context context, String key, int value) {
        getEditor(context).putInt(key, value).apply();

    }

    /*
     * Read the Interger Value
     * */
    public static int readInteger(Context context, String key, int defValue) {
        return getPreferences(context).getInt(key, defValue);
    }

    /*
     * Write the String Value
     * */
    public static void writeString(Context context, String key, String value) {
        getEditor(context).putString(key, value).apply();

    }

    /*
     * Read the String Value
     * */
    public static String readString(Context context, String key, String defValue) {
        return getPreferences(context).getString(key, defValue);
    }

    /*
     * Write the Float Value
     * */
    public static void writeFloat(Context context, String key, float value) {
        getEditor(context).putFloat(key, value).apply();
    }

    /*
     * Read the Float Value
     * */
    public static float readFloat(Context context, String key, float defValue) {
        return getPreferences(context).getFloat(key, defValue);
    }

    /*
     * Write the Long Value
     * */
    public static void writeLong(Context context, String key, long value) {
        getEditor(context).putLong(key, value).apply();
    }

    /*
     * Read the Long Value
     * */
    public static long readLong(Context context, String key, long defValue) {
        return getPreferences(context).getLong(key, defValue);
    }

    /*
     * Return the SharedPreferences
     * with
     * @Prefreces Name & Prefreces = MODE
     * */
    public static SharedPreferences getPreferences(Context context) {
        return context.getSharedPreferences(PREF_NAME, MODE);
    }

    /*
     * Return the SharedPreferences Editor
     * */
    public static SharedPreferences.Editor getEditor(Context context) {
        return getPreferences(context).edit();
    }

}
