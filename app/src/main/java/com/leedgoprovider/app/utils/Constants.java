package com.leedgoprovider.app.utils;

public class Constants {

    //Staging Server Url
   // public static final String BASE_URL = "https://www.dharmani.com/solidittechnologies/solidwebservice/";

    //Production Server Url
    public static final String BASE_URL = "https://dev.leedgo.com/solidwebservice/";


    //Google Places Apis
    public static final String GOOGLE_PLACES_SEARCH = "https://maps.googleapis.com/maps/api/place/autocomplete/json?";
    public static final String GOOGLE_PLACES_LAT_LONG= "https://maps.googleapis.com/maps/api/place/details/json?place_id=";
    //Working Socket URL
    public static final String SocketURL = "http://jaohar-uk.herokuapp.com:80";

    /*
     * Constants @params
     * */
    public static int REQUEST_CODE = 123;
    public static String MODEL = "model";
    public static String ID = "id";
    public static String LIST = "list";
    public static String TEXT = "text";

    public static String LINK_TYPE = "link_type";
    public static String PRIVACY_POLICY = "privacy_policy";
    public static String TERMS_SERVICES = "terms_condtions";
    public static String DONT_SELL_DATA = "dont_sell_my_data";
    public static String ABOUT_US = "about_us";
    public static String NOTIFICATION_TYPE = "notification_type";
    public static String PUSH = "push";

    /*
    * App Links
    * */
    public static final String PRIVACY_POLICY_LINK = "";
    public static final String TERMS_SERVICES_LINK = "";



    /*
     * Static Array:
     *
     * */
    public static String HOURS[] = {"0","01","02","03","04","05","06","07","08","09","10","11","12"};
    public static String HOURSTIME[] = {"01","02","03","04","05","06","07","08","09","10","11","12"};
    public static String HOURS_TEXT[] = {"Hours"};
    public static String MINUTESS[] = {"00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42","43","44","45","46","47","48","49","50","51","52","53","54","55","56","57","58","59","60"};
    public static String MINUTESS_IN_LIMIT[] = {"0","30"};
    public static String MINUTESS_LIMIT[] = {"00","30"};
    public static String MINUTES_TEXT[] = {"Minutes"};
    public static String AMPM[] = {"AM","PM"};


    public static boolean Servicesub = false;

}
