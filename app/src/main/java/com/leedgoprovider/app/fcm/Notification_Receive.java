package com.leedgoprovider.app.fcm;


import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.text.Spanned;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.leedgoprovider.app.R;
import com.leedgoprovider.app.activities.NotificationsActivity;
import com.leedgoprovider.app.utils.Constants;

/**
 * Setting up FirebaseMessagingService
 */
public class Notification_Receive extends FirebaseMessagingService {
    Spanned mssg;
    String title = "LeedGo", message = "";
    String val = "";
    Intent intent;
    private static int NOTIFICATION_ID = 6578;

    String TAG = Notification_Receive.this.getClass().getSimpleName();

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

//        if (remoteMessage.getData().size() > 0) {
//            title = remoteMessage.getData().get("title");
//            val = remoteMessage.getData().get("body");
//            mssg = Html.fromHtml(val);
//            sendNotification();
//        }

        Log.e(TAG, "From: " + remoteMessage.getFrom());
        Log.e(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());

        if (remoteMessage.getNotification() != null) {
            title = remoteMessage.getNotification().getTitle();
            message = remoteMessage.getNotification().getBody();
        }

        intent = new Intent(this, NotificationsActivity.class);
        intent.putExtra(Constants.NOTIFICATION_TYPE, Constants.PUSH);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */,
                intent,
                PendingIntent.FLAG_ONE_SHOT);
        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.mipmap.ic_notification_icon_white)
                        .setColor(getResources().getColor(R.color.colorAccent))
                        .setContentTitle(title)
                        .setContentText(message)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent)
                        .setPriority(NotificationCompat.PRIORITY_HIGH).setDefaults(NotificationCompat.DEFAULT_ALL);
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (notificationManager != null) {

                NotificationChannel channel = new NotificationChannel(channelId, getString(R.string.default_notification_channel_name),
                        NotificationManager.IMPORTANCE_DEFAULT);
                channel.enableVibration(true);
                notificationManager.createNotificationChannel(channel);
            }
        }
        if (notificationManager != null) {
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
            } else {
                notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
            }
        }
    }

//    @Override
//    public void onMessageReceived(RemoteMessage remoteMessage) {
//
//        if (remoteMessage.getData().size() > 0) {
//            title=remoteMessage.getData().get("title");
//
//
//            val=remoteMessage.getData().get("body");
//            mssg = Html.fromHtml(val);
//            sendNotification();
//        }
//    }

    private void sendNotification() {
        intent = new Intent(this, NotificationsActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);
        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder;

        if (val.equals("")) {
            notificationBuilder =
                    new NotificationCompat.Builder(this, channelId)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle(title)
                            .setAutoCancel(true)
                            .setSound(defaultSoundUri)
                            .setContentIntent(pendingIntent);

        } else {
            notificationBuilder =
                    new NotificationCompat.Builder(this, channelId)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle(title)
                            .setStyle(new NotificationCompat.BigTextStyle().bigText(val))
                            .setContentText(val)
                            .setAutoCancel(true)
                            .setSound(defaultSoundUri)
                            .setContentIntent(pendingIntent);
        }

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (notificationManager != null) {

                NotificationChannel channel = new NotificationChannel(channelId, getString(R.string.default_notification_channel_name),
                        NotificationManager.IMPORTANCE_DEFAULT);
                channel.enableVibration(true);
                channel.setShowBadge(true);
                notificationManager.createNotificationChannel(channel);
            }
        }

        if (notificationManager != null) {
            //if (newNotifyType!=null &&!newNotifyType.equals(notification_type))
            // {
            NOTIFICATION_ID++;
            //  }
            notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build());
        }
    }

    void createNotification(RemoteMessage remoteMessage) {
        Intent intent = new Intent(this, NotificationsActivity.class);
        intent.putExtra("text", remoteMessage.getData().get("message_data_text"));
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        String channelId = getString(R.string.default_notification_channel_id);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(remoteMessage.getData().get("title"))
                .setContentText(remoteMessage.getData().get("body"))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, notificationBuilder.build());

    }

    public void onNewToken(String s) {
        super.onNewToken(s);

        Log.e("newToken", s);

        //Add your token in your sharepreferences.
        getSharedPreferences("_", MODE_PRIVATE).edit().putString("fcm_token", s).apply();
    }
}