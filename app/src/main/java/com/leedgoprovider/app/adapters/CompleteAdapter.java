package com.leedgoprovider.app.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.leedgoprovider.app.R;
import com.leedgoprovider.app.activities.CompletedRequestDetailsActivity;
import com.leedgoprovider.app.models.ProviderCompleteTaskModel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CompleteAdapter extends RecyclerView.Adapter<CompleteAdapter.ViewHolder> {
    Activity context;
    boolean check = true;
    ArrayList<ProviderCompleteTaskModel.Datum> mArrayList;

    public CompleteAdapter(Activity context, ArrayList<ProviderCompleteTaskModel.Datum> mArrayList) {
        this.context = context;
        this.mArrayList = mArrayList;
    }

    @NonNull
    @Override
    public CompleteAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.completescreen_item1, parent, false);
        return new CompleteAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CompleteAdapter.ViewHolder holder, int position) {
        ProviderCompleteTaskModel.Datum mModel = mArrayList.get(position);

        holder.tx_servicename.setText(mModel.getServiceName());
        holder.tx_address.setText(mModel.getAddress());

        if (mModel.getPaymentStatus().equals("1")) {
            holder.statusTV.setText("PAID");
            holder.statusTV.setTextColor(context.getResources().getColor(R.color.colorPaymentDone));
        } else {
            holder.statusTV.setText("NOT PAID");
            holder.statusTV.setTextColor(context.getResources().getColor(R.color.colorPending));
        }

        try {
            Calendar calendar = Calendar.getInstance();
            TimeZone tz = TimeZone.getDefault();
            calendar.setTimeInMillis(Long.parseLong(mModel.getTaskCompleteDate()) * 1000);
            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy");
            Date currenTimeZone = (Date) calendar.getTime();

            holder.tx_servicebiddate.setText("Completed on: " + sdf.format(currenTimeZone));

        } catch (Exception e) {
        }

        Glide.with(context).load(mModel.getCategoryImage()).into(holder.imageView);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, CompletedRequestDetailsActivity.class);
                intent.putExtra("username", mModel.getUserDetail().getName());
                intent.putExtra("userEmail", mModel.getUserDetail().getEmail());
                intent.putExtra("userProfile", mModel.getUserDetail().getProfilePic());
                intent.putExtra("categoryName", mModel.getServiceName());
                intent.putExtra("categoryImage", mModel.getCategoryImage());
                intent.putExtra("SubcategoryName", mModel.getSubCategoryName());
                intent.putExtra("taskId", mModel.getUniqueTaskId());
                intent.putExtra("paymentStatus", mModel.getPaymentStatus());
                intent.putExtra("workLocation", mModel.getAddress());
                intent.putExtra("workDescription", mModel.getWorkDetails());
                intent.putExtra("CompletedDate", timeChange(Long.parseLong(mModel.getTaskCompleteDate())));
                intent.putExtra("BidAmount", mModel.getAssignedAmount());
                intent.putExtra("insurance", mModel.getRequestCoveredByInsuranceClaim());
                intent.putExtra("historical", mModel.getRequestedForHistoricalStructure());
                intent.putExtra("owner", mModel.getOwnerOrAuthorizedRepresentativeOfOwner());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.service_name)
        TextView tx_servicename;
        @BindView(R.id.address)
        TextView tx_address;
        @BindView(R.id.service_biddate)
        TextView tx_servicebiddate;
        @BindView(R.id.service_image)
        ImageView imageView;
        @BindView(R.id.statusTV)
        TextView statusTV;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    String timeChange(long coment_time) {
        Date date = new java.util.Date(coment_time * 1000L);

        // the format of your date
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd/MM/yyyy");
        // give a timezone reference for formatting (see comment at the bottom)
        sdf.setTimeZone(TimeZone.getDefault());
        String formattedDate = sdf.format(date);
        return formattedDate;
    }
}

