package com.leedgoprovider.app.adapters;

import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.leedgoprovider.app.fragments.CompletedFragment;
import com.leedgoprovider.app.fragments.InProgressFragment;
import com.leedgoprovider.app.fragments.RequestsFragment;
import com.leedgoprovider.app.fragments.UpcomingFragment;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        if (position == 0) {
            fragment = new RequestsFragment();
        } else if (position == 1) {
            fragment = new InProgressFragment();
        } else if (position == 2) {
            fragment = new UpcomingFragment();
        } else if (position == 3) {
            fragment = new CompletedFragment();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;
        if (position == 0) {
            title = "Request";
        } else if (position == 1) {
            title = "In-Progress";
        } else if (position == 2) {
            title = "Upcoming";
        } else if (position == 3) {
            title = "Completed";
        }
        return title;
    }
}