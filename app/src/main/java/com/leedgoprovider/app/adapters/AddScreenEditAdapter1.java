package com.leedgoprovider.app.adapters;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.leedgoprovider.app.R;
import com.leedgoprovider.app.StringChange.StringFormatter;
import com.leedgoprovider.app.interfaces.ServicesSubCatInterface;
import com.leedgoprovider.app.models.AddScreenCheckModel;
import com.leedgoprovider.app.models.ServicesDataModel;
import com.leedgoprovider.app.models.ServicesSubCatData;

import java.util.ArrayList;

import static com.leedgoprovider.app.utils.Constants.Servicesub;

public class AddScreenEditAdapter1  extends RecyclerView.Adapter<AddScreenEditAdapter1.ViewHolder>  {
    /*
     * Initialize LayoutInflater
     * Initialize Context mContext
     * */
    private ArrayList<ServicesSubCatData> mArrayList;
    Activity mActivity;
    ServicesSubCatInterface mServicesSubCatInterface;
    ArrayList<AddScreenCheckModel.Service_detail> mArrayList2=new ArrayList<>();
    private ArrayList<ServicesDataModel> mArrayListN;
    CheckBox checkBoxx;
    String valueCheck;
    public AddScreenEditAdapter1(Activity mActivity, ArrayList<ServicesSubCatData> subCat, ArrayList<ServicesDataModel> mArrayList, ArrayList<AddScreenCheckModel.Service_detail> mArrayList2, ServicesSubCatInterface mServicesSubCatInterface, CheckBox checkBoxx, String valueK) {
        this.mActivity = mActivity;
        this.mArrayList = subCat;
        this.mArrayListN=mArrayList;
        this.mArrayList2=mArrayList2;
        this.mServicesSubCatInterface = mServicesSubCatInterface;
        this.checkBoxx=checkBoxx;
        this.valueCheck=valueK;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_screen_item2, parent, false);
        return new AddScreenEditAdapter1.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final ServicesSubCatData mModel = mArrayList.get(position);
        holder.tx_heading.setText(StringFormatter.capitalizeWord(mModel.getName()));
//        try {
//            Toast.makeText(mActivity, "if"+valueCheck , Toast.LENGTH_SHORT).show();
//            if (mModel.getScatId() == Integer.parseInt(valueCheck)) {
////               if (Servicesub)
////                {
////                    Toast.makeText(mActivity, "if" , Toast.LENGTH_SHORT).show();
////
////                    holder.checkBox.setChecked(true);
////
////                }
////                else {
////                   Toast.makeText(mActivity, "else" , Toast.LENGTH_SHORT).show();
////                    holder.checkBox.setChecked(false);
////                }
//            }
//        }
//        catch (Exception e)
//        {
//            e.printStackTrace();
//        }
        try {
            for (int i=0;i<=mArrayList2.size();i++) {
                if (mModel.getId() == Integer.parseInt(mArrayList2.get(i).getSub_category_id())) {
                    holder.checkBox.setChecked(true);

                               mServicesSubCatInterface.getSubCategoryItem(mModel,true);


                }

            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    checkBoxx.setChecked(true);
                    mServicesSubCatInterface.getSubCategoryItem(mModel,true);


                }else{
                    mServicesSubCatInterface.getSubCategoryItem(mModel,false);

                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tx_heading;
        ImageView im_mark;
        CheckBox checkBox;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tx_heading=itemView.findViewById(R.id.service_subheading);
            im_mark=itemView.findViewById(R.id.service_submarking);
            checkBox=itemView.findViewById(R.id.itemCheckBoxCB);
        }
    }
}
