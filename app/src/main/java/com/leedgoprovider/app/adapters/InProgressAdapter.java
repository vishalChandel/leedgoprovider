package com.leedgoprovider.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.leedgoprovider.app.R;
import com.leedgoprovider.app.models.AddModel;
import com.leedgoprovider.app.models.InprogressModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InProgressAdapter  extends RecyclerView.Adapter<InProgressAdapter.ViewHolder> {
    Context context;
    ArrayList<InprogressModel> modelArrayList=new ArrayList<>();
    String key;


    public InProgressAdapter(Context context, ArrayList<InprogressModel> modelArrayList, String key) {
        this.context=context;
        this.modelArrayList=modelArrayList;
        this.key=key;
    }

    @NonNull
    @Override
    public InProgressAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.inprogress_item, parent, false);
        return new InProgressAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InProgressAdapter.ViewHolder holder, int position) {
        InprogressModel mModel = modelArrayList.get(position);
        try {
            holder.tx_time.setText("Due " + key);
            for (int i = 0; i <= modelArrayList.size(); i++)
            {
                if (key.equals(modelArrayList.get(i).getWork_date()))
                {
                    holder.recyclerView.setVisibility(View.VISIBLE);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(context);
                    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    holder.recyclerView.setLayoutManager(layoutManager);
                    InProgressAdapter1 otherAdapter = new InProgressAdapter1(mModel,modelArrayList, context);
                    holder.recyclerView.setAdapter(otherAdapter);
                }
                else
                {
                    holder.recyclerView.setVisibility(View.GONE);
                }
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.inprogress_time)
        TextView tx_time;
        @BindView(R.id.in_progress_recycler1)
        RecyclerView recyclerView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

        }
    }
}
