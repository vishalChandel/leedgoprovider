package com.leedgoprovider.app.adapters;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.leedgoprovider.app.R;
import com.leedgoprovider.app.activities.AddLocation;
import com.leedgoprovider.app.models.AddLocationModel;

import java.util.ArrayList;
import java.util.List;

public class AddLocationAdapter extends RecyclerView.Adapter<AddLocationAdapter.ViewHolder> {
    Activity activity;
    List<AddLocationModel.Data> mArrayList=new ArrayList<>();
    OnItemClickListener itemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int positon, AddLocationModel.Data item, View view);
    }
    public AddLocationAdapter(Activity mActivity, List<AddLocationModel.Data> mArrayList,OnItemClickListener onItemClickListener) {
        this.activity=mActivity;
        this.mArrayList=mArrayList;
        this.itemClickListener=onItemClickListener;
    }
    @NonNull
    @Override
    public AddLocationAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.location_item, parent, false);
        return new AddLocationAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AddLocationAdapter.ViewHolder holder, int position) {
        AddLocationModel.Data model=mArrayList.get(position);

        Log.e("Dataj",model.getCity());
        holder.tx_locationame.setText(model.getCity());
        try {
            holder.bind(position,model,itemClickListener);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tx_locationame;
        ImageView im_delete,im_edit;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tx_locationame=itemView.findViewById(R.id.tx_locationname);
            im_delete=itemView.findViewById(R.id.im_location_delete);
            im_edit=itemView.findViewById(R.id.im_location_edit);
        }

        public void bind(int position, AddLocationModel.Data model, OnItemClickListener itemClickListener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onItemClick(position,model,v);
                }
            });


            im_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    itemClickListener.onItemClick(position,model,v);
                }
            });
            im_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    itemClickListener.onItemClick(position,model,v);
                }
            });
        }
    }
}
