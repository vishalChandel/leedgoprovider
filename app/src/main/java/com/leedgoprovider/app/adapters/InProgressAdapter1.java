package com.leedgoprovider.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.leedgoprovider.app.R;
import com.leedgoprovider.app.models.InprogressModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class InProgressAdapter1 extends RecyclerView.Adapter<InProgressAdapter1.ViewHolder> {

    Context context;
    ArrayList<InprogressModel> modelArrayList=new ArrayList<>();
    InprogressModel mModel;
    public InProgressAdapter1(InprogressModel mModel, ArrayList<InprogressModel> modelArrayList, Context context) {
        this.mModel=mModel;

        this.modelArrayList=modelArrayList;
        this.context=context;
    }

    @NonNull
    @Override
    public InProgressAdapter1.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.inprogress_item1, parent, false);
        return new InProgressAdapter1.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InProgressAdapter1.ViewHolder holder, int position) {
        mModel=modelArrayList.get(position);
        holder.tx_servicename.setText(mModel.getService_name());
        holder.tx_address.setText(mModel.getAddress());
        holder.imageView.setImageResource(R.drawable.green_check);

        try {
            if (mModel.getOrder_status().equals("1")) {
                holder.imageView.setImageResource(R.drawable.clock_gray);
            }
            else {
                holder.imageView.setImageResource(R.drawable.yellow_time);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return modelArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tx_servicename)
        TextView tx_servicename;
        @BindView(R.id.tx_address)
        TextView tx_address;
        @BindView(R.id.imageview)
        ImageView imageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);

        }
    }
}
