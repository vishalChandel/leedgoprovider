package com.leedgoprovider.app.adapters;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.leedgoprovider.app.R;
import com.leedgoprovider.app.activities.BidServiceActivity;
import com.leedgoprovider.app.models.InRequestModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RequestAdapter extends RecyclerView.Adapter<RequestAdapter.ViewHolder> {
    Activity context;
    ArrayList<InRequestModel.Data> mArrayList = new ArrayList<>();
    boolean check = true;

    public RequestAdapter(Activity context, ArrayList<InRequestModel.Data> mArrayList) {
        this.context = context;
        this.mArrayList = mArrayList;
    }

    @NonNull
    @Override
    public RequestAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.requestscreen_item1, parent, false);
        return new RequestAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RequestAdapter.ViewHolder holder, int position) {
        InRequestModel.Data mModel = mArrayList.get(position);
        try {
            holder.tx_servicename.setText(mModel.getService_name());
            holder.tx_address.setText(mModel.getAddress());
            String dateStr = mModel.getBid_last_date();

            DateFormat destDf = new SimpleDateFormat("dd/MM/yyyy");


            DateFormat srcDf = new SimpleDateFormat("dd-MM-yyyy hh:mm a");
            // parse the date string into Date object
            Date date = srcDf.parse(dateStr);

            // format the date into another format
            dateStr = destDf.format(date);
            //String date=mModel.getBid_last_date();
            holder.tx_servicebiddate.setText("Submit bid on " + dateStr);
            holder.tx_datetime.setText(mModel.getWork_date());
            if (Integer.parseInt(mModel.getFlexible_week()) > 1) {
                holder.tx_flexibilitytime.setText(mModel.getFlexible_week() + " Weeks");
            } else {
                holder.tx_flexibilitytime.setText(mModel.getFlexible_week() + " Week");
            }


            holder.tx_projectdetails.setText(mModel.getWork_details());
            holder.tx_servicename.setText(mModel.getService_name());
            holder.tx_servicedetails.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (check) {
                        holder.lly_detailscreen.setVisibility(View.VISIBLE);
                        check = false;
                    } else {
                        holder.lly_detailscreen.setVisibility(View.GONE);
                        check = true;
                    }

                }
            });


            Glide.with(context)
                    .load(mModel.getCategory_image())
                    .into(holder.imageView);

            if (!mModel.getImage1().equals("")) {
                Glide.with(context).load(mModel.getImage1()).into(holder.image1);
                holder.tx_nophotos.setVisibility(View.GONE);
            } else {
                holder.card1.setVisibility(View.GONE);
                holder.tx_nophotos.setVisibility(View.VISIBLE);
            }

            if (!mModel.getImage2().equals("")) {
                Glide.with(context).load(mModel.getImage2()).into(holder.image2);
                holder.tx_nophotos.setVisibility(View.GONE);
            } else {
                holder.card2.setVisibility(View.GONE);
                holder.tx_nophotos.setVisibility(View.GONE);
            }
            if (!mModel.getImage3().equals("")) {
                Glide.with(context).load(mModel.getImage3()).into(holder.image3);
                holder.tx_nophotos.setVisibility(View.GONE);
            } else {
                holder.card3.setVisibility(View.GONE);
                holder.tx_nophotos.setVisibility(View.GONE);
            }
            if(mModel.getImage1().equals("") && mModel.getImage2().equals("") && mModel.getImage3().equals("") ){
                holder.tx_nophotos.setVisibility(View.VISIBLE);
            }
            holder.tx_servicebid.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, BidServiceActivity.class);
                    intent.putExtra("name", mModel.getService_name());
                    intent.putExtra("address", mModel.getAddress());
                    intent.putExtra("image", mModel.getCategory_image());
                    intent.putExtra("taskId", mModel.getTask_id());
                    intent.putExtra("endDaate", mModel.getBid_last_date());
                    context.startActivity(intent);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.service_name)
        TextView tx_servicename;
        @BindView(R.id.address)
        TextView tx_address;
        @BindView(R.id.service_biddate)
        TextView tx_servicebiddate;
        @BindView(R.id.service_datetime)
        TextView tx_datetime;
        @BindView(R.id.service_flexibilitytime)
        TextView tx_flexibilitytime;
        @BindView(R.id.service_projrctdetails)
        TextView tx_projectdetails;
        @BindView(R.id.service_details)
        TextView tx_servicedetails;
        @BindView(R.id.detailsscreen)
        LinearLayout lly_detailscreen;
        @BindView(R.id.service_bid)
        TextView tx_servicebid;
        @BindView(R.id.nophotos)
        TextView tx_nophotos;
        @BindView(R.id.service_image)
        ImageView imageView;
        @BindView(R.id.service_image1)
        ImageView image1;
        @BindView(R.id.service_image2)
        ImageView image2;
        @BindView(R.id.service_image3)
        ImageView image3;
        @BindView(R.id.card1)
        CardView card1;
        @BindView(R.id.card2)
        CardView card2;
        @BindView(R.id.card3)
        CardView card3;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
