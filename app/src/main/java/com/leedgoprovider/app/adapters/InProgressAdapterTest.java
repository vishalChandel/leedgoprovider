package com.leedgoprovider.app.adapters;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.leedgoprovider.app.R;
import com.leedgoprovider.app.activities.CompleteRequestActivity;
import com.leedgoprovider.app.activities.RequestDetails;
import com.leedgoprovider.app.models.InprogressModel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by android-da on 6/7/18.
 */


public class InProgressAdapterTest extends BaseAdapter implements StickyListHeadersAdapter
{
    Context context;
    ArrayList<InprogressModel> modelArrayList=new ArrayList<>();
    String key;
    private LayoutInflater inflater;
        String finalDate="";


    public InProgressAdapterTest(Context context, ArrayList<InprogressModel> modelArrayList, String key) {
        this.context=context;
        this.modelArrayList=modelArrayList;
        this.key=key;
        inflater = LayoutInflater.from(context);
    }



    @Override
    public int getCount() {
        return modelArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return modelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
//        ChatGroupModel mModel = mChatgroupList.get(position);
        InprogressModel mModel = modelArrayList.get(position);
        InProgressAdapterTest.ViewHolder holder;

        if (convertView == null) {
            holder = new InProgressAdapterTest.ViewHolder();
            convertView = inflater.inflate(R.layout.inprogress_item1, parent, false);
            holder.tx_servicename = convertView.findViewById(R.id.tx_servicename);
            holder.tx_address = convertView.findViewById(R.id.tx_address);
            holder.imageView = convertView.findViewById(R.id.imageview);

            convertView.setTag(holder);

        } else {
            holder = (InProgressAdapterTest.ViewHolder) convertView.getTag();
        }
      //  try {
            holder.tx_servicename.setText(mModel.getService_name());
            holder.tx_address.setText(mModel.getAddress());
            holder.imageView.setImageResource(R.drawable.green_check);


                if (mModel.getOrder_status().equals("0")) {
                    holder.imageView.setImageResource(R.drawable.clock_gray);
                }
                else {
                    holder.imageView.setImageResource(R.drawable.yellow_time);
                }

           holder.tx_address.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                 //  Toast.makeText(context, "a!!!"+mModel.getId(), Toast.LENGTH_SHORT).show();
                   Intent intent=new Intent(context, CompleteRequestActivity.class);
                   intent.putExtra("taskId",mModel.getId());
                   context.startActivity(intent);
               }
           });

//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        return convertView;
    }


    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        InProgressAdapterTest.HeaderViewHolder holder;
        if (convertView == null) {
            holder = new InProgressAdapterTest.HeaderViewHolder();
            convertView = inflater.inflate(R.layout.inprogress_item, parent, false);
            holder.tx_time = convertView.findViewById(R.id.inprogress_time);

            convertView.setTag(holder);
        } else {
            holder = (InProgressAdapterTest.HeaderViewHolder) convertView.getTag();
        }
        //set header text as first char in name
        if (modelArrayList.get(position).getProvider_work_date1() != null && modelArrayList.get(position).getProvider_work_date1().length() > 0) {
            String headerText = "" + modelArrayList.get(position).getProvider_work_date1();
            String dateStr = modelArrayList.get(position).getProvider_work_date1();
try {
    DateFormat destDf = new SimpleDateFormat("d MMMM yyyy");


    DateFormat srcDf = new SimpleDateFormat("dd-MM-yyyy");
    // parse the date string into Date object
    Date date = srcDf.parse(dateStr);

    // format the date into another format
    dateStr = destDf.format(date);

    holder.tx_time.setText("Due "+dateStr);
}
catch (Exception e)
{
    e.printStackTrace();
}

        }
        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
       finalDate= timeChange(Long.parseLong(modelArrayList.get(position).getProvider_work_date()));
        //return the first character of the country as ID because this is what headers are based upon
        if (modelArrayList.size() > 0)
            return finalDate.subSequence(0, 2).charAt(0);
        else
            return 0;
    }

    class HeaderViewHolder {
        TextView tx_time;
    }

    class ViewHolder {
        TextView tx_servicename;
        TextView tx_address;
        ImageView imageView;
    }

    String timeChange(long coment_time)
    {
        Date date = new java.util.Date(coment_time * 1000L);

        SimpleDateFormat sdf2 = new java.text.SimpleDateFormat("MMM dd, yyyy ");
        sdf2.setTimeZone(TimeZone.getDefault());
        String  formattedDate2 = sdf2.format(date);
        finalDate=formattedDate2;
        return  finalDate;
    }


}
