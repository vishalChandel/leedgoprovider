package com.leedgoprovider.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.leedgoprovider.app.R;
import com.leedgoprovider.app.activities.TransactionDetailActivity;
import com.leedgoprovider.app.models.DataItem;
import com.leedgoprovider.app.models.TransactionHistoryModel;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TransactionListAdapter extends RecyclerView.Adapter<TransactionListAdapter.ViewHolder> {
    ArrayList<TransactionHistoryModel.Datum> mDataAL = new ArrayList<>();
    Context context;

    public TransactionListAdapter(Activity mActivity, ArrayList<TransactionHistoryModel.Datum> mDataAL) {
        this.mDataAL = mDataAL;
        this.context = mActivity;
    }

    @NonNull
    @Override
    public TransactionListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.transaction_list_item, parent, false);
        return new TransactionListAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull TransactionListAdapter.ViewHolder holder, int position) {
        TransactionHistoryModel.Datum mModel = mDataAL.get(position);
        holder.textStatus.setText(mModel.getServiceName());
        holder.textMain.setText(mModel.getSubCategoryName());
        holder.timeTV.setText(getDateCurrentTimeZone(Long.parseLong(mModel.getDateOfPayment())).replace("am", "AM").replace("pm", "PM"));
        holder.priceTV.setText("+$" + mModel.getAssignedAmount());
        if (mModel.getCategoryImage() != null && !mModel.getCategoryImage().equals("")) {

            Glide.with(context)
                    .load(mModel.getCategoryImage())
                    .apply(new RequestOptions().placeholder(R.drawable.ic_pp).error(R.drawable.ic_pp))
                    .into(holder.imageView);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, TransactionDetailActivity.class);
                intent.putExtra("MyClass", (Serializable) mModel);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDataAL.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.textMain)
        TextView textMain;
        @BindView(R.id.texySubmain)
        TextView textSubMain;
        @BindView(R.id.textDate)
        TextView textDate;
        @BindView(R.id.textStatus)
        TextView textStatus;
        @BindView(R.id.imageview)
        ImageView imageView;
        @BindView(R.id.timeTV)
        TextView timeTV;
        @BindView(R.id.priceTV)
        TextView priceTV;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textStatus = itemView.findViewById(R.id.textStatus);
            ButterKnife.bind(this, itemView);
        }
    }


    public String getDateCurrentTimeZone(long timestamp) {
        try {
            Calendar calendar = Calendar.getInstance();
            TimeZone tz = TimeZone.getDefault();
            calendar.setTimeInMillis(timestamp * 1000);
            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyy");
            Date currenTimeZone = (Date) calendar.getTime();
            return sdf.format(currenTimeZone);
        } catch (Exception e) {
        }
        return "";
    }
}
