package com.leedgoprovider.app.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.leedgoprovider.app.R;
import com.leedgoprovider.app.interfaces.LocationInterface;
import com.leedgoprovider.app.models.locations.PredictionsItem;

import java.util.ArrayList;


public class SearchLocationAdapter extends RecyclerView.Adapter<SearchLocationAdapter.ViewHolder> {

    LocationInterface mLocationInterface;
    /*
     * Initialize LayoutInflater
     * Initialize Context mContext
     * */
    private ArrayList<PredictionsItem> mArrayList;
    private Activity mActivity;

    public SearchLocationAdapter(Activity mActivity, ArrayList<PredictionsItem> mArrayList, LocationInterface mLocationInterface) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
        this.mLocationInterface = mLocationInterface;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search_locations, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final PredictionsItem mModel = mArrayList.get(position);

        holder.itemCityTV.setText(mModel.getStructuredFormatting().getMainText());
        holder.itemAddressTV.setText(mModel.getDescription());

        holder.bind(mModel);

        if (mArrayList.size() - 1 == position) {
            holder.mView.setVisibility(View.GONE);
        } else {
            holder.mView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        if (mArrayList.size() > 0)
            return mArrayList.size();
        else
            return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView itemCityTV, itemAddressTV;
        public View mView;

        public ViewHolder(View itemView) {
            super(itemView);
            itemCityTV = itemView.findViewById(R.id.itemCityTV);
            itemAddressTV = itemView.findViewById(R.id.itemAddressTV);
            mView = itemView.findViewById(R.id.mView);
        }

        public void bind(PredictionsItem mModel) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mLocationInterface.getSelectedLocation(mModel);

                }
            });
        }
    }
}
