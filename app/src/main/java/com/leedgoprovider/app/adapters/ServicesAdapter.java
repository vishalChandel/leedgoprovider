package com.leedgoprovider.app.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.leedgoprovider.app.R;
import com.leedgoprovider.app.interfaces.LocationInterface;
import com.leedgoprovider.app.interfaces.ServicesInterface;
import com.leedgoprovider.app.interfaces.ServicesSubCatInterface;
import com.leedgoprovider.app.models.ServicesDataModel;
import com.leedgoprovider.app.models.ServicesSubCatData;
import com.leedgoprovider.app.models.locations.PredictionsItem;

import java.util.ArrayList;


public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.ViewHolder> {
    /*
     * Initialize LayoutInflater
     * Initialize Context mContext
     * */
    private ArrayList<ServicesSubCatData> mSelectedServicesArrayList = new ArrayList<>();
    private ArrayList<ServicesDataModel> mArrayList;
    private Activity mActivity;
    ServicesInterface mServicesInterface;

    public ServicesAdapter(Activity mActivity, ArrayList<ServicesDataModel> mArrayList,ServicesInterface mServicesInterface) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
        this.mServicesInterface = mServicesInterface;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_services, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ServicesDataModel mModel = mArrayList.get(position);
        holder.txtItemNameTV.setText(mModel.getName());

        if (mModel.getImage() != null) {
            Glide.with(mActivity)
                    .load(mModel.getImage())
                    .apply(new RequestOptions().placeholder(R.drawable.ic_pp).error(R.drawable.ic_pp))
                    .into(holder.imgItemIV);
        } else {
            holder.imgItemIV.setImageResource(R.drawable.ic_pp);
        }

        //Set SubCategories
        ServicesSubCateAdapter mSearchLocationAdapter = new ServicesSubCateAdapter(mActivity, mModel.getSubCat(),mServicesSubCatInterface);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        holder.mRecyclerViewRV.setLayoutManager(mLayoutManager);
        holder.mRecyclerViewRV.setAdapter(mSearchLocationAdapter);

        holder.rootLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.mRecyclerViewRV.getVisibility() == View.VISIBLE){
                    holder.imgDropDownIV.setImageResource(R.drawable.ic_arrow_right);
                    holder.mRecyclerViewRV.setVisibility(View.GONE);
                }else{
                    holder.imgDropDownIV.setImageResource(R.drawable.ic_arrow_bottom);
                    holder.mRecyclerViewRV.setVisibility(View.VISIBLE);
                }
            }
        });

        mServicesInterface.getSelectedServices(mSelectedServicesArrayList);

    }

    @Override
    public int getItemCount() {
        if (mArrayList.size() > 0)
            return mArrayList.size();
        else
            return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgItemIV,imgDropDownIV;
        public TextView txtItemNameTV;
        public RecyclerView mRecyclerViewRV;
        public LinearLayout rootLL;

        public ViewHolder(View itemView) {
            super(itemView);
            imgItemIV = itemView.findViewById(R.id.imgItemIV);
            txtItemNameTV = itemView.findViewById(R.id.txtItemNameTV);
            imgDropDownIV = itemView.findViewById(R.id.imgDropDownIV);
            mRecyclerViewRV = itemView.findViewById(R.id.mRecyclerViewRV);
            rootLL = itemView.findViewById(R.id.rootLL);
        }
    }

    ServicesSubCatInterface mServicesSubCatInterface = new ServicesSubCatInterface() {
        @Override
        public void getSubCategoryItem(ServicesSubCatData mModel,boolean b) {
            if (b){
                mSelectedServicesArrayList.add(mModel);
                mServicesInterface.getSelectedServices(mSelectedServicesArrayList);
            }else{
                mSelectedServicesArrayList.remove(mModel);
                mServicesInterface.getSelectedServices(mSelectedServicesArrayList);
            }

        }
    };

}
