package com.leedgoprovider.app.adapters;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.leedgoprovider.app.R;
import com.leedgoprovider.app.activities.RequestDetails;
import com.leedgoprovider.app.models.UpcomingTaskModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class UpcomingAdapter extends RecyclerView.Adapter<UpcomingAdapter.ViewHolder> {
    Activity context;
    ArrayList<UpcomingTaskModel.Data> mArrayList = new ArrayList<>();
    int limit;

    public UpcomingAdapter(Activity context, ArrayList<UpcomingTaskModel.Data> mArrayList, int limit) {
        this.context = context;
        this.mArrayList = mArrayList;
        this.limit = limit;
    }

    @NonNull
    @Override
    public UpcomingAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.upcoming_item, parent, false);
        return new UpcomingAdapter.ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull UpcomingAdapter.ViewHolder holder, int position) {
        UpcomingTaskModel.Data mModel = mArrayList.get(position);
        holder.tx_address.setText(mModel.getAddress());
        holder.tx_servicename.setText(mModel.getService_name());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, RequestDetails.class);
//                intent.putExtra("servicename", mModel.getService_name());
//                intent.putExtra("username", mModel.getAssign_user());
//                intent.putExtra("useraddress", mModel.getAddress());
//                intent.putExtra("categoryImage", mModel.getCategory_image());
//                intent.putExtra("flexibleWeek", mModel.getFlexible_week());
//                intent.putExtra("userid", mModel.getUser_id());
//                intent.putExtra("reprentativeornot", mModel.getClient_name());
//                intent.putExtra("workDate", mModel.getWork_date());
                intent.putExtra("taskId", mModel.getId());
//                intent.putExtra("latitude", mModel.getOrder_lat());
//                intent.putExtra("longitude", mModel.getOrder_long());
                context.startActivity(intent);
            }
        });

        try {
            Glide.with(context).load(mModel.getCategory_image()).into(holder.imageView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.UpServiceaddress)
        TextView tx_address;
        @BindView(R.id.img)
        ImageView imageView;
        @BindView(R.id.UpServicename)
        TextView tx_servicename;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
