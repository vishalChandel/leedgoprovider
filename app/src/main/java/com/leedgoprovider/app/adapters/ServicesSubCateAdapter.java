package com.leedgoprovider.app.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.leedgoprovider.app.R;
import com.leedgoprovider.app.StringChange.StringFormatter;
import com.leedgoprovider.app.interfaces.ServicesSubCatInterface;
import com.leedgoprovider.app.models.ServicesSubCatData;

import java.util.ArrayList;


public class ServicesSubCateAdapter extends RecyclerView.Adapter<ServicesSubCateAdapter.ViewHolder> {
    /*
     * Initialize LayoutInflater
     * Initialize Context mContext
     * */
    private ArrayList<ServicesSubCatData> mArrayList;
    private Activity mActivity;
    ServicesSubCatInterface mServicesSubCatInterface;
    boolean check=true;

    public ServicesSubCateAdapter(Activity mActivity, ArrayList<ServicesSubCatData> mArrayList,ServicesSubCatInterface mServicesSubCatInterface) {
        this.mActivity = mActivity;
        this.mArrayList = mArrayList;
        this.mServicesSubCatInterface = mServicesSubCatInterface;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sub_services, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ServicesSubCatData mModel = mArrayList.get(position);
        holder.itemSubServiceTV.setText(StringFormatter.capitalizeWord(mModel.getName()));
        holder.itemCheckBoxCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    mServicesSubCatInterface.getSubCategoryItem(mModel,true);
                }else{
                    mServicesSubCatInterface.getSubCategoryItem(mModel,false);
                }
            }
        });






    }

    @Override
    public int getItemCount() {
        if (mArrayList.size() > 0)
            return mArrayList.size();
        else
            return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public CheckBox itemCheckBoxCB;
        public TextView itemSubServiceTV;

        public ViewHolder(View itemView) {
            super(itemView);
            itemSubServiceTV = itemView.findViewById(R.id.itemSubServiceTV);
            itemCheckBoxCB = itemView.findViewById(R.id.itemCheckBoxCB);
        }
    }
}
