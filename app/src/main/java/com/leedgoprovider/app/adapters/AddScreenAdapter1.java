package com.leedgoprovider.app.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.leedgoprovider.app.R;
import com.leedgoprovider.app.StringChange.StringFormatter;
import com.leedgoprovider.app.models.AddModel;
import com.leedgoprovider.app.models.AddScreenCheckModel;

import java.util.ArrayList;
import java.util.List;

public class AddScreenAdapter1  extends RecyclerView.Adapter<AddScreenAdapter1.ViewHolder> {
    ArrayList<AddModel.Sub_cat> mArrayList;
    Context context;
    ArrayList<AddScreenCheckModel.Service_detail> mArrayList2=new ArrayList<>();


    public AddScreenAdapter1(ArrayList<AddModel.Sub_cat> sub_cat, ArrayList<AddScreenCheckModel.Service_detail> mArrayList2, Context context) {
        this.mArrayList=sub_cat;
        this.context=context;
        this.mArrayList2=mArrayList2;
    }

    @NonNull
    @Override
    public AddScreenAdapter1.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_screen_item2, parent, false);
        return new AddScreenAdapter1.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AddScreenAdapter1.ViewHolder holder, int position) {
        AddModel.Sub_cat mModel=mArrayList.get(position);
        holder.tx_heading.setText(StringFormatter.capitalizeWord(mModel.getName()));

        try {
            for (int i=0;i<=mArrayList2.size();i++) {
                if (mModel.getId() == Integer.parseInt(mArrayList2.get(i).getSub_category_id())) {
                    holder.checkBox.setChecked(true);
                    holder.checkBox.setClickable(false);
                }
                else {
                    holder.checkBox.setClickable(false);
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tx_heading;
        ImageView im_mark;
        CheckBox checkBox;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tx_heading=itemView.findViewById(R.id.service_subheading);
            im_mark=itemView.findViewById(R.id.service_submarking);
            checkBox=itemView.findViewById(R.id.itemCheckBoxCB);
        }
    }
}
