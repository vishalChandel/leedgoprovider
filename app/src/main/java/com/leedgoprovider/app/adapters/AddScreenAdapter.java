package com.leedgoprovider.app.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.leedgoprovider.app.R;
import com.leedgoprovider.app.models.AddModel;
import com.leedgoprovider.app.models.AddScreenCheckModel;

import java.util.ArrayList;
import java.util.List;

public class AddScreenAdapter extends RecyclerView.Adapter<AddScreenAdapter.ViewHolder> {
    Context context;
    ArrayList<AddModel.Data> mArrayList = new ArrayList<>();
    ArrayList<AddScreenCheckModel.Service_detail> mArrayList2 = new ArrayList<>();
    AddScreenCheckModel model;

    public AddScreenAdapter(Context context, ArrayList<AddModel.Data> mArrayList, ArrayList<AddScreenCheckModel.Service_detail> mArrayList2, AddScreenCheckModel model) {
        this.context = context;
        this.mArrayList = mArrayList;
        this.mArrayList2 = mArrayList2;
        this.model = model;
    }


    @NonNull
    @Override
    public AddScreenAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_screen_item1, parent, false);
        return new AddScreenAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AddScreenAdapter.ViewHolder holder, int position) {
        AddModel.Data mModel = mArrayList.get(position);

        // Log.e("value","0: "+mArrayList2.get(position).getCategory_id());
        holder.tx_heading.setText(mModel.getName());


        for (int i=0;i<mArrayList2.size();i++) {
            if (mModel.getId() == Integer.parseInt(mArrayList2.get(i).getCategory_id())) {
                holder.checkBox.setChecked(true);
                holder.checkBox.setClickable(false);
            }
            else {
                holder.checkBox.setClickable(false);
            }
        }
//                    if (mModel.getId() == Integer.parseInt(mArrayList2.get(i).getCategory_id())) {
//
//                        holder.checkBox.setChecked(true);
//                        holder.checkBox.setClickable(false);
//                    }
//                    else {
//                        holder.checkBox.setClickable(false);
//                    }
//                    model=mArrayList2.get(position);
//
//
//            }
        //

        AddScreenAdapter1 horizontalAdapter = new AddScreenAdapter1((ArrayList<AddModel.Sub_cat>) mModel.getSub_cat(), mArrayList2, context);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        holder.recyclerView.setLayoutManager(layoutManager);
        holder.recyclerView.setAdapter(horizontalAdapter);

    }

    @Override
    public int getItemCount() {
        return mArrayList == null ? 0 : mArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tx_heading;
        ImageView im_mark;
        RecyclerView recyclerView;
        CheckBox checkBox;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tx_heading = itemView.findViewById(R.id.service_heading);
            im_mark = itemView.findViewById(R.id.service_marking);
            recyclerView = itemView.findViewById(R.id.recycler_category);
            checkBox = itemView.findViewById(R.id.itemCheckBoxCB);
        }
    }
}
