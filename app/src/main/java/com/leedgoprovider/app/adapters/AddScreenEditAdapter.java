package com.leedgoprovider.app.adapters;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.leedgoprovider.app.R;
import com.leedgoprovider.app.interfaces.ServicesInterface;
import com.leedgoprovider.app.interfaces.ServicesSubCatInterface;
import com.leedgoprovider.app.models.AddScreenCheckModel;
import com.leedgoprovider.app.models.ServicesDataModel;
import com.leedgoprovider.app.models.ServicesSubCatData;

import java.util.ArrayList;

import static com.leedgoprovider.app.utils.Constants.Servicesub;

public class AddScreenEditAdapter extends RecyclerView.Adapter<AddScreenEditAdapter.ViewHolder> {
    /*
     * Initialize LayoutInflater
     * Initialize Context mContext
     * */
    private ArrayList<ServicesSubCatData> mSelectedServicesArrayList = new ArrayList<>();
    private ArrayList<ServicesDataModel> mArrayList;
    private Activity mActivity;
    ServicesInterface mServicesInterface;
    ArrayList<AddScreenCheckModel.Service_detail> mArrayList2=new ArrayList<>();
    AddScreenCheckModel model;
    String valueK;
    AddScreenEditAdapter1 mSearchLocationAdapter;

    public AddScreenEditAdapter(Activity mActivity, ArrayList<ServicesDataModel> mServicesArrayList, ArrayList<AddScreenCheckModel.Service_detail> mArrayList2, AddScreenCheckModel model, ServicesInterface mServicesInterface) {
        this.mActivity = mActivity;
        this.mArrayList = mServicesArrayList;
        this.mArrayList2=mArrayList2;
        this.model=model;
        this.mServicesInterface = mServicesInterface;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_screen_item1, parent, false);
        return new AddScreenEditAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final ServicesDataModel mModel = mArrayList.get(position);
        holder.tx_heading.setText(mModel.getName());
        holder.checkBox.setClickable(false);
        if (model!=null) {
            for (int i = 0; i <= mArrayList.size(); i++) {
                try {

                    if (mModel.getId() == Integer.parseInt(mArrayList2.get(i).getCategory_id())) {
                       //  valueK=mArrayList2.get(i).getCategory_id();
                        holder.checkBox.setChecked(true);

                    }
                    model = mArrayList2.get(position);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

//
//        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//
//                if (isChecked){
//                    for (int i = 0; i <= mArrayList.size(); i++) {
//                        try {
//
//                            if (mModel.getId() == Integer.parseInt(mArrayList2.get(i).getCategory_id())) {
//                                valueK=mArrayList2.get(i).getCategory_id();
//                                Servicesub=true;
//                                holder.checkBox.setChecked(true);
//                            }
//                            model = mArrayList2.get(position);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                   mSearchLocationAdapter.notifyDataSetChanged();
//                }else{
//                    for (int i = 0; i <= mArrayList.size(); i++) {
//                        try {
//
//                            if (mModel.getId() == Integer.parseInt(mArrayList2.get(i).getCategory_id())) {
//                                valueK=mArrayList2.get(i).getCategory_id();
//                                Servicesub=false;
//                                holder.checkBox.setChecked(false);
//                          Log.e("Test",valueK);
//                            }
//                            else {
//                                valueK="";
//                            }
//                            model = mArrayList2.get(position);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                    mSearchLocationAdapter.notifyDataSetChanged();
//                    Log.e("Test","2  "+valueK);
//                }
//
//            }
//        });
        Log.e("Test","3  "+valueK);
        //Set SubCategories
         mSearchLocationAdapter = new  AddScreenEditAdapter1(mActivity, mModel.getSubCat(),mArrayList,mArrayList2,mServicesSubCatInterface,holder.checkBox,valueK);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(mActivity);
        holder.recyclerView.setLayoutManager(mLayoutManager);
        holder.recyclerView.setAdapter(mSearchLocationAdapter);
        mServicesInterface.getSelectedServices(mSelectedServicesArrayList);
    }

    @Override
    public int getItemCount() {
        return mArrayList== null ? 0 : mArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tx_heading;
        ImageView im_mark;
        RecyclerView recyclerView;
        CheckBox checkBox;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tx_heading=itemView.findViewById(R.id.service_heading);
            im_mark=itemView.findViewById(R.id.service_marking);
            recyclerView=itemView.findViewById(R.id.recycler_category);
            checkBox=itemView.findViewById(R.id.itemCheckBoxCB);
        }
    }
    ServicesSubCatInterface mServicesSubCatInterface = new ServicesSubCatInterface() {
        @Override
        public void getSubCategoryItem(ServicesSubCatData mModel,boolean b) {
            if (b){
                mSelectedServicesArrayList.add(mModel);
                mServicesInterface.getSelectedServices(mSelectedServicesArrayList);
            }else{
                mSelectedServicesArrayList.remove(mModel);
                mServicesInterface.getSelectedServices(mSelectedServicesArrayList);
            }

        }
    };

}
