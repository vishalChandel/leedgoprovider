package com.leedgoprovider.app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.leedgoprovider.app.R;
import com.leedgoprovider.app.interfaces.ZoomImageInterface;
import com.leedgoprovider.app.models.ChatMessageModel;
import com.leedgoprovider.app.utils.LeedgoProviderPrefrences;

import org.apache.commons.lang3.StringEscapeUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;

public class PostChatAdapter extends RecyclerView.Adapter<PostChatAdapter.ViewHolder> {
    private Context context;
    ArrayList<ChatMessageModel.All_messages> mChatgroupList;
    private LayoutInflater inflater;
    Date date_format;
    ZoomImageInterface mZoomImageInterface;

    String formattedDate;
    public PostChatAdapter(Context context, ArrayList<ChatMessageModel.All_messages> mChatgroupModel,ZoomImageInterface mZoomImageInterface){
        this.context = context;
        this.mChatgroupList = mChatgroupModel;
        this.mZoomImageInterface = mZoomImageInterface;


    }
    @NonNull
    @Override
    public PostChatAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_post_chat_list, parent, false);
        return new PostChatAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PostChatAdapter.ViewHolder holder, int position) {
        ChatMessageModel.All_messages mModel = mChatgroupList.get(position);
        if (mModel.getSender_type().equals("client")) {
            holder.lly_leftcomplete.setVisibility(View.VISIBLE);
            holder.lly_rightcomplete.setVisibility(View.GONE);
            holder.llyViewLeft.setVisibility(View.VISIBLE);
            holder.llyViewRight.setVisibility(View.GONE);
            long unix_seconds = Long.parseLong(mModel.getMessage_time());
//convert seconds to milliseconds

            Date date = new Date(unix_seconds * 1000L);

            DateFormat originalFormat = new SimpleDateFormat("hh:mm aaa");

            String formattedDate2 = originalFormat.format(date);

            String str = formattedDate2.replace("am", "AM").replace("pm","PM");
            holder.tx_leftTime.setText(str);

            String serverResponse =mModel.getMessage();
            if (mModel.getMessage().equals(""))
            {
                holder.lly_leftmessage.setVisibility(View.GONE);
            }
            else
            {
                holder.lly_leftmessage.setVisibility(View.VISIBLE);

            }
            String fromServerUnicodeDecoded = StringEscapeUtils.unescapeJava(serverResponse);
            holder.tx_leftmessage.setText(fromServerUnicodeDecoded);

            RequestOptions optionsIcon = new RequestOptions()
                    .placeholder(R.drawable.ic_person)
                    .error(R.drawable.ic_person)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)
                    .dontAnimate()
                    .dontTransform();
           // if (mModel.getSender_profile_pic() != null) {
                Glide.with(context)
                        .load(mModel.getSender_profile_pic())
                        .apply(optionsIcon)
                        .into(holder.leftProfileImage);

          //  }
            RequestOptions optionsIcon2 = new RequestOptions()
                    .placeholder(R.drawable.ic_pp)
                    .error(R.drawable.ic_pp)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)
                    .dontAnimate()
                    .dontTransform();

            if (mModel.getImage()!=null&& mModel.getImage().length()>4) {
                Glide.with(context)
                        .load(mModel.getImage())
                        .apply(optionsIcon2)
                        .into(holder.leftImage);
                holder.leftImage.setVisibility(View.VISIBLE);
            }
            else
            {
                holder.leftImage.setVisibility(View.GONE);
            }
            holder.tx_leftmessage.setText(mModel.getMessage());





        }
        else {
            holder.lly_rightcomplete.setVisibility(View.VISIBLE);
            holder.lly_leftcomplete.setVisibility(View.GONE);
            holder.llyViewRight.setVisibility(View.VISIBLE);
            holder.llyViewLeft.setVisibility(View.GONE);
            long unix_seconds = Long.parseLong(mModel.getMessage_time());
//convert seconds to milliseconds

            Date date = new Date(unix_seconds * 1000L);

            DateFormat originalFormat = new SimpleDateFormat("hh:mm aaa");

            String formattedDate2 = originalFormat.format(date);
            String str = formattedDate2.replace("am", "AM").replace("pm","PM");
            holder.tx_rightTime.setText(str);
            String serverResponse =mModel.getMessage();
            if (mModel.getMessage().equals(""))
            {
                holder.lly_rightmessage.setVisibility(View.GONE);
            }
            else
            {
                holder.lly_rightmessage.setVisibility(View.VISIBLE);

            }
            String fromServerUnicodeDecoded = StringEscapeUtils.unescapeJava(serverResponse);
            holder.tx_rightmessage.setText(fromServerUnicodeDecoded);

            RequestOptions optionsIcon = new RequestOptions()
                    .placeholder(R.drawable.ic_person)
                    .error(R.drawable.ic_person)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)
                    .dontAnimate()
                    .dontTransform();
            String image= LeedgoProviderPrefrences.readString(context,LeedgoProviderPrefrences.PROVIDER_IMAGE,null);

            Glide.with(context)
                    .load(image)
                    .apply(optionsIcon)
                    .into(holder.rightprofileImage);

            RequestOptions optionsIcon2 = new RequestOptions()
                    .placeholder(R.drawable.ic_pp)
                    .error(R.drawable.ic_pp)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH)
                    .dontAnimate()
                    .dontTransform();

            if ( mModel.getImage()!=null && mModel.getImage().length()>4) {

                Glide.with(context)
                        .load(mModel.getImage())
                        .apply(optionsIcon2)
                        .into(holder.rightImage);
                holder.rightImage.setVisibility(View.VISIBLE);
            }
            else
            {
                holder.rightImage.setVisibility(View.GONE);
            }
            holder.tx_rightmessage.setText(mModel.getMessage());

            if (holder.rightImage.getVisibility() == View.VISIBLE) {
                holder.rightImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mZoomImageInterface.onClick(mModel.getImage());
                    }
                });
            }
            if (holder.leftImage.getVisibility() == View.VISIBLE) {
                holder.leftImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mZoomImageInterface.onClick(mModel.getImage());
                    }
                });
            }

        }


    }

    @Override
    public int getItemCount() {
        return mChatgroupList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView leftImage,rightImage;
        CircleImageView leftProfileImage,rightprofileImage;

        TextView tx_rightTime,tx_leftTime;
        TextView tx_rightmessage, tx_leftmessage;
        LinearLayout lly_leftmessage,lly_rightmessage;
        LinearLayout lly_leftcomplete,lly_rightcomplete;
        LinearLayout llyViewLeft, llyViewRight;

        public ViewHolder(@NonNull View convertView) {
            super(convertView);
            leftImage=convertView.findViewById(R.id.leftImage);
            rightImage=convertView.findViewById(R.id.rightImage);
            leftProfileImage=convertView.findViewById(R.id.chati_left_photo);
            rightprofileImage=convertView.findViewById(R.id.chati_right_photo);
            tx_leftTime=convertView.findViewById(R.id.chati_left_time);
            tx_rightTime=convertView.findViewById(R.id.chati_right_time);
            tx_leftmessage=convertView.findViewById(R.id.chat_left_msg_text_view);
            tx_rightmessage=convertView.findViewById(R.id.chat_right_msg_text_view);
            lly_rightmessage=convertView.findViewById(R.id.chat_right_msg_layout);
            lly_leftmessage=convertView.findViewById(R.id.chat_left_msg_layout);
            lly_leftcomplete=convertView.findViewById(R.id.leftlayoutcomplete);
            lly_rightcomplete=convertView.findViewById(R.id.rightlayoutcomplete);
            llyViewLeft=convertView.findViewById(R.id.viewLeft);
            llyViewRight=convertView.findViewById(R.id.viewRight);
        }
    }
    private void timeChange(String message_time) throws ParseException {
        Date date1=new SimpleDateFormat("dd-MM-yyyy HH:mm aa").parse(message_time);
        DateFormat originalFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm aa");

        DateFormat targetFormat = new SimpleDateFormat("h:mm a");

        //formattedDate = targetFormat.format(date1);
        formattedDate=message_time;
    }


}
