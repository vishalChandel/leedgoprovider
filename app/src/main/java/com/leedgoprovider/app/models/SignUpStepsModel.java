package com.leedgoprovider.app.models;

import java.io.Serializable;
import java.util.ArrayList;

public class SignUpStepsModel implements Serializable {
    String businessName = "";
    String businessEmail = "";
    String businessPhone = "";
    String businessLicenceNo = "";
    String businessLicenceState = "";
    String businessSince = "";
    String businessWebSite = "";
    String country = "";
    String addressLine1 = "";
    String addressLine2 = "";
    String city = "";
    String state = "";
    String state_id = "";
    String zipcode = "";
    ArrayList<ServiceAreaModel> addServiceArea = new ArrayList<ServiceAreaModel>();
    ArrayList<ServicesSubCatData> ServicesSubCatDataAL = new ArrayList<ServicesSubCatData>();
    String mondayFridayHours = "";
    String saturdayHours = "";
    String sundayHours = "";
    String emergencyService = "";

    public ArrayList<ServicesSubCatData> getServicesSubCatDataAL() {
        return ServicesSubCatDataAL;
    }

    public void setServicesSubCatDataAL(ArrayList<ServicesSubCatData> servicesSubCatDataAL) {
        ServicesSubCatDataAL = servicesSubCatDataAL;
    }

    public String getMondayFridayHours() {
        return mondayFridayHours;
    }

    public void setMondayFridayHours(String mondayFridayHours) {
        this.mondayFridayHours = mondayFridayHours;
    }

    public String getSaturdayHours() {
        return saturdayHours;
    }

    public void setSaturdayHours(String saturdayHours) {
        this.saturdayHours = saturdayHours;
    }

    public String getSundayHours() {
        return sundayHours;
    }

    public void setSundayHours(String sundayHours) {
        this.sundayHours = sundayHours;
    }

    public String getEmergencyService() {
        return emergencyService;
    }

    public void setEmergencyService(String emergencyService) {
        this.emergencyService = emergencyService;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessEmail() {
        return businessEmail;
    }

    public void setBusinessEmail(String businessEmail) {
        this.businessEmail = businessEmail;
    }

    public String getBusinessPhone() {
        return businessPhone;
    }

    public void setBusinessPhone(String businessPhone) {
        this.businessPhone = businessPhone;
    }

    public String getBusinessLicenceNo() {
        return businessLicenceNo;
    }

    public void setBusinessLicenceNo(String businessLicenceNo) {
        this.businessLicenceNo = businessLicenceNo;
    }

    public String getBusinessLicenceState() {
        return businessLicenceState;
    }

    public void setBusinessLicenceState(String businessLicenceState) {
        this.businessLicenceState = businessLicenceState;
    }

    public String getBusinessSince() {
        return businessSince;
    }

    public void setBusinessSince(String businessSince) {
        this.businessSince = businessSince;
    }

    public String getBusinessWebSite() {
        return businessWebSite;
    }

    public void setBusinessWebSite(String businessWebSite) {
        this.businessWebSite = businessWebSite;
    }


    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getState_id() {
        return state_id;
    }

    public void setState_id(String state_id) {
        this.state_id = state_id;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public ArrayList<ServiceAreaModel> getAddServiceArea() {
        return addServiceArea;
    }

    public void setAddServiceArea(ArrayList<ServiceAreaModel> addServiceArea) {
        this.addServiceArea = addServiceArea;
    }
}
