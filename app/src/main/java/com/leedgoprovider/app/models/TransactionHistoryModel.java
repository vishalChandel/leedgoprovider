package com.leedgoprovider.app.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TransactionHistoryModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Datum> getData() {
        return (ArrayList<Datum>) data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public static class Datum implements Serializable {
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("unique_task_id")
        @Expose
        private String uniqueTaskId;
        @SerializedName("user_id")
        @Expose
        private String userId;
        @SerializedName("category_id")
        @Expose
        private String categoryId;
        @SerializedName("subcategory_id")
        @Expose
        private String subcategoryId;
        @SerializedName("business_id")
        @Expose
        private String businessId;
        @SerializedName("order_lat")
        @Expose
        private String orderLat;
        @SerializedName("order_long")
        @Expose
        private String orderLong;
        @SerializedName("work_details")
        @Expose
        private String workDetails;
        @SerializedName("order_status")
        @Expose
        private String orderStatus;
        @SerializedName("start_address")
        @Expose
        private Object startAddress;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("end_address")
        @Expose
        private Object endAddress;
        @SerializedName("work_date")
        @Expose
        private String workDate;
        @SerializedName("work_time")
        @Expose
        private Object workTime;
        @SerializedName("task_size")
        @Expose
        private Object taskSize;
        @SerializedName("what_bring")
        @Expose
        private Object whatBring;
        @SerializedName("job_title")
        @Expose
        private Object jobTitle;
        @SerializedName("zip_code")
        @Expose
        private String zipCode;
        @SerializedName("assign_user")
        @Expose
        private String assignUser;
        @SerializedName("payment_status")
        @Expose
        private String paymentStatus;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private Object updatedAt;
        @SerializedName("flexible_week")
        @Expose
        private String flexibleWeek;
        @SerializedName("photos")
        @Expose
        private Object photos;
        @SerializedName("promo_code")
        @Expose
        private String promoCode;
        @SerializedName("state")
        @Expose
        private String state;
        @SerializedName("balance")
        @Expose
        private Object balance;
        @SerializedName("image1")
        @Expose
        private String image1;
        @SerializedName("image2")
        @Expose
        private String image2;
        @SerializedName("image3")
        @Expose
        private String image3;
        @SerializedName("image1_server")
        @Expose
        private String image1Server;
        @SerializedName("image2_server")
        @Expose
        private String image2Server;
        @SerializedName("image3_server")
        @Expose
        private String image3Server;
        @SerializedName("assigned_amount")
        @Expose
        private Float assignedAmount;
        @SerializedName("assigned_date")
        @Expose
        private Object assignedDate;
        @SerializedName("provider_work_date")
        @Expose
        private String providerWorkDate;
        @SerializedName("requested_for_historical_structure")
        @Expose
        private String requestedForHistoricalStructure;
        @SerializedName("request_covered_by_insurance_claim")
        @Expose
        private String requestCoveredByInsuranceClaim;
        @SerializedName("owner_or_authorized_representative_of_owner")
        @Expose
        private String ownerOrAuthorizedRepresentativeOfOwner;
        @SerializedName("task_complete_date")
        @Expose
        private String taskCompleteDate;
        @SerializedName("cancel_reason")
        @Expose
        private Object cancelReason;
        @SerializedName("service_name")
        @Expose
        private String serviceName;
        @SerializedName("category_image")
        @Expose
        private String categoryImage;
        @SerializedName("sub_category_name")
        @Expose
        private String subCategoryName;
        @SerializedName("service_rating")
        @Expose
        private String serviceRating;
        @SerializedName("task_id")
        @Expose
        private String taskId;
        @SerializedName("date_of_payment")
        @Expose
        private String dateOfPayment;
        @SerializedName("get_provider_bank_details")
        @Expose
        private GetProviderBankDetails getProviderBankDetails;
        @SerializedName("user_detail")
        @Expose
        private UserDetail userDetail;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUniqueTaskId() {
            return uniqueTaskId;
        }

        public void setUniqueTaskId(String uniqueTaskId) {
            this.uniqueTaskId = uniqueTaskId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(String categoryId) {
            this.categoryId = categoryId;
        }

        public String getSubcategoryId() {
            return subcategoryId;
        }

        public void setSubcategoryId(String subcategoryId) {
            this.subcategoryId = subcategoryId;
        }

        public String getBusinessId() {
            return businessId;
        }

        public void setBusinessId(String businessId) {
            this.businessId = businessId;
        }

        public String getOrderLat() {
            return orderLat;
        }

        public void setOrderLat(String orderLat) {
            this.orderLat = orderLat;
        }

        public String getOrderLong() {
            return orderLong;
        }

        public void setOrderLong(String orderLong) {
            this.orderLong = orderLong;
        }

        public String getWorkDetails() {
            return workDetails;
        }

        public void setWorkDetails(String workDetails) {
            this.workDetails = workDetails;
        }

        public String getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(String orderStatus) {
            this.orderStatus = orderStatus;
        }

        public Object getStartAddress() {
            return startAddress;
        }

        public void setStartAddress(Object startAddress) {
            this.startAddress = startAddress;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public Object getEndAddress() {
            return endAddress;
        }

        public void setEndAddress(Object endAddress) {
            this.endAddress = endAddress;
        }

        public String getWorkDate() {
            return workDate;
        }

        public void setWorkDate(String workDate) {
            this.workDate = workDate;
        }

        public Object getWorkTime() {
            return workTime;
        }

        public void setWorkTime(Object workTime) {
            this.workTime = workTime;
        }

        public Object getTaskSize() {
            return taskSize;
        }

        public void setTaskSize(Object taskSize) {
            this.taskSize = taskSize;
        }

        public Object getWhatBring() {
            return whatBring;
        }

        public void setWhatBring(Object whatBring) {
            this.whatBring = whatBring;
        }

        public Object getJobTitle() {
            return jobTitle;
        }

        public void setJobTitle(Object jobTitle) {
            this.jobTitle = jobTitle;
        }

        public String getZipCode() {
            return zipCode;
        }

        public void setZipCode(String zipCode) {
            this.zipCode = zipCode;
        }

        public String getAssignUser() {
            return assignUser;
        }

        public void setAssignUser(String assignUser) {
            this.assignUser = assignUser;
        }

        public String getPaymentStatus() {
            return paymentStatus;
        }

        public void setPaymentStatus(String paymentStatus) {
            this.paymentStatus = paymentStatus;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public Object getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(Object updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getFlexibleWeek() {
            return flexibleWeek;
        }

        public void setFlexibleWeek(String flexibleWeek) {
            this.flexibleWeek = flexibleWeek;
        }

        public Object getPhotos() {
            return photos;
        }

        public void setPhotos(Object photos) {
            this.photos = photos;
        }

        public String getPromoCode() {
            return promoCode;
        }

        public void setPromoCode(String promoCode) {
            this.promoCode = promoCode;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public Object getBalance() {
            return balance;
        }

        public void setBalance(Object balance) {
            this.balance = balance;
        }

        public String getImage1() {
            return image1;
        }

        public void setImage1(String image1) {
            this.image1 = image1;
        }

        public String getImage2() {
            return image2;
        }

        public void setImage2(String image2) {
            this.image2 = image2;
        }

        public String getImage3() {
            return image3;
        }

        public void setImage3(String image3) {
            this.image3 = image3;
        }

        public String getImage1Server() {
            return image1Server;
        }

        public void setImage1Server(String image1Server) {
            this.image1Server = image1Server;
        }

        public String getImage2Server() {
            return image2Server;
        }

        public void setImage2Server(String image2Server) {
            this.image2Server = image2Server;
        }

        public String getImage3Server() {
            return image3Server;
        }

        public void setImage3Server(String image3Server) {
            this.image3Server = image3Server;
        }

        public Float getAssignedAmount() {
            return assignedAmount;
        }

        public void setAssignedAmount(Float assignedAmount) {
            this.assignedAmount = assignedAmount;
        }

        public Object getAssignedDate() {
            return assignedDate;
        }

        public void setAssignedDate(Object assignedDate) {
            this.assignedDate = assignedDate;
        }

        public String getProviderWorkDate() {
            return providerWorkDate;
        }

        public void setProviderWorkDate(String providerWorkDate) {
            this.providerWorkDate = providerWorkDate;
        }

        public String getRequestedForHistoricalStructure() {
            return requestedForHistoricalStructure;
        }

        public void setRequestedForHistoricalStructure(String requestedForHistoricalStructure) {
            this.requestedForHistoricalStructure = requestedForHistoricalStructure;
        }

        public String getRequestCoveredByInsuranceClaim() {
            return requestCoveredByInsuranceClaim;
        }

        public void setRequestCoveredByInsuranceClaim(String requestCoveredByInsuranceClaim) {
            this.requestCoveredByInsuranceClaim = requestCoveredByInsuranceClaim;
        }

        public String getOwnerOrAuthorizedRepresentativeOfOwner() {
            return ownerOrAuthorizedRepresentativeOfOwner;
        }

        public void setOwnerOrAuthorizedRepresentativeOfOwner(String ownerOrAuthorizedRepresentativeOfOwner) {
            this.ownerOrAuthorizedRepresentativeOfOwner = ownerOrAuthorizedRepresentativeOfOwner;
        }

        public String getTaskCompleteDate() {
            return taskCompleteDate;
        }

        public void setTaskCompleteDate(String taskCompleteDate) {
            this.taskCompleteDate = taskCompleteDate;
        }

        public Object getCancelReason() {
            return cancelReason;
        }

        public void setCancelReason(Object cancelReason) {
            this.cancelReason = cancelReason;
        }

        public String getServiceName() {
            return serviceName;
        }

        public void setServiceName(String serviceName) {
            this.serviceName = serviceName;
        }

        public String getCategoryImage() {
            return categoryImage;
        }

        public void setCategoryImage(String categoryImage) {
            this.categoryImage = categoryImage;
        }

        public String getSubCategoryName() {
            return subCategoryName;
        }

        public void setSubCategoryName(String subCategoryName) {
            this.subCategoryName = subCategoryName;
        }

        public String getServiceRating() {
            return serviceRating;
        }

        public void setServiceRating(String serviceRating) {
            this.serviceRating = serviceRating;
        }

        public String getTaskId() {
            return taskId;
        }

        public void setTaskId(String taskId) {
            this.taskId = taskId;
        }

        public String getDateOfPayment() {
            return dateOfPayment;
        }

        public void setDateOfPayment(String dateOfPayment) {
            this.dateOfPayment = dateOfPayment;
        }

        public GetProviderBankDetails getGetProviderBankDetails() {
            return getProviderBankDetails;
        }

        public void setGetProviderBankDetails(GetProviderBankDetails getProviderBankDetails) {
            this.getProviderBankDetails = getProviderBankDetails;
        }

        public UserDetail getUserDetail() {
            return userDetail;
        }

        public void setUserDetail(UserDetail userDetail) {
            this.userDetail = userDetail;
        }

    }
}
