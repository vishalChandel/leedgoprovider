package com.leedgoprovider.app.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DataItem implements Serializable {

    @SerializedName("order_lat")
    private String orderLat;

    @SerializedName("assigned_date")
    private Object assignedDate;

    @SerializedName("promo_code")
    private String promoCode;

    @SerializedName("photos")
    private Object photos;

    @SerializedName("zip_code")
    private String zipCode;

    @SerializedName("subcategory_id")
    private String subcategoryId;

    @SerializedName("order_status")
    private String orderStatus;

    @SerializedName("request_covered_by_insurance_claim")
    private String requestCoveredByInsuranceClaim;

    @SerializedName("balance")
    private Object balance;

    @SerializedName("date_of_payment")
    private String dateOfPayment;

    @SerializedName("flexible_week")
    private String flexibleWeek;

    @SerializedName("sub_category_name")
    private String subCategoryName;

    @SerializedName("id")
    private String id;

    @SerializedName("what_bring")
    private Object whatBring;

    @SerializedName("state")
    private String state;

    @SerializedName("assign_user")
    private String assignUser;

    @SerializedName("job_title")
    private Object jobTitle;

    @SerializedName("service_name")
    private String serviceName;

    @SerializedName("service_rating")
    private String serviceRating;

    @SerializedName("task_size")
    private Object taskSize;

    @SerializedName("get_provider_bank_details")
    private GetProviderBankDetails getProviderBankDetails;

    @SerializedName("user_id")
    private String userId;

    @SerializedName("task_complete_date")
    private String taskCompleteDate;

    @SerializedName("end_address")
    private Object endAddress;

    @SerializedName("work_time")
    private Object workTime;

    @SerializedName("assigned_amount")
    private int assignedAmount;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("image2_server")
    private String image2Server;

    @SerializedName("image3_server")
    private String image3Server;

    @SerializedName("task_id")
    private String taskId;

    @SerializedName("image1_server")
    private String image1Server;

    @SerializedName("category_image")
    private String categoryImage;

    @SerializedName("category_id")
    private String categoryId;

    @SerializedName("updated_at")
    private Object updatedAt;

    @SerializedName("unique_task_id")
    private String uniqueTaskId;

    @SerializedName("order_long")
    private String orderLong;

    @SerializedName("work_details")
    private String workDetails;

    @SerializedName("image3")
    private String image3;

    @SerializedName("address")
    private String address;

    @SerializedName("work_date")
    private String workDate;

    @SerializedName("user_detail")
    private UserDetail userDetail;

    @SerializedName("payment_status")
    private String paymentStatus;

    @SerializedName("requested_for_historical_structure")
    private String requestedForHistoricalStructure;

    @SerializedName("provider_work_date")
    private String providerWorkDate;

    @SerializedName("image1")
    private String image1;

    @SerializedName("image2")
    private String image2;

    @SerializedName("start_address")
    private Object startAddress;

    @SerializedName("owner_or_authorized_representative_of_owner")
    private String ownerOrAuthorizedRepresentativeOfOwner;

    @SerializedName("business_id")
    private String businessId;

    public String getOrderLat() {
        return orderLat;
    }

    public Object getAssignedDate() {
        return assignedDate;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public Object getPhotos() {
        return photos;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getSubcategoryId() {
        return subcategoryId;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public String getRequestCoveredByInsuranceClaim() {
        return requestCoveredByInsuranceClaim;
    }

    public Object getBalance() {
        return balance;
    }

    public String getDateOfPayment() {
        return dateOfPayment;
    }

    public String getFlexibleWeek() {
        return flexibleWeek;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public String getId() {
        return id;
    }

    public Object getWhatBring() {
        return whatBring;
    }

    public String getState() {
        return state;
    }

    public String getAssignUser() {
        return assignUser;
    }

    public Object getJobTitle() {
        return jobTitle;
    }

    public String getServiceName() {
        return serviceName;
    }

    public String getServiceRating() {
        return serviceRating;
    }

    public Object getTaskSize() {
        return taskSize;
    }

    public GetProviderBankDetails getGetProviderBankDetails() {
        return getProviderBankDetails;
    }

    public String getUserId() {
        return userId;
    }

    public String getTaskCompleteDate() {
        return taskCompleteDate;
    }

    public Object getEndAddress() {
        return endAddress;
    }

    public Object getWorkTime() {
        return workTime;
    }

    public int getAssignedAmount() {
        return assignedAmount;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getImage2Server() {
        return image2Server;
    }

    public String getImage3Server() {
        return image3Server;
    }

    public String getTaskId() {
        return taskId;
    }

    public String getImage1Server() {
        return image1Server;
    }

    public String getCategoryImage() {
        return categoryImage;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public String getUniqueTaskId() {
        return uniqueTaskId;
    }

    public String getOrderLong() {
        return orderLong;
    }

    public String getWorkDetails() {
        return workDetails;
    }

    public String getImage3() {
        return image3;
    }

    public String getAddress() {
        return address;
    }

    public String getWorkDate() {
        return workDate;
    }

    public UserDetail getUserDetail() {
        return userDetail;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public String getRequestedForHistoricalStructure() {
        return requestedForHistoricalStructure;
    }

    public String getProviderWorkDate() {
        return providerWorkDate;
    }

    public String getImage1() {
        return image1;
    }

    public String getImage2() {
        return image2;
    }

    public Object getStartAddress() {
        return startAddress;
    }

    public String getOwnerOrAuthorizedRepresentativeOfOwner() {
        return ownerOrAuthorizedRepresentativeOfOwner;
    }

    public String getBusinessId() {
        return businessId;
    }
}