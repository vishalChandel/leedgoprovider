package com.leedgoprovider.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddScreenCheckModel {
    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private Data data;

    public void setStatus(int status){
        this.status = status;
    }
    public int getStatus(){
        return this.status;
    }
    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return this.message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }



    public class Data extends AddScreenCheckModel {
        private String id;

        private String provider_id;

        private String business_name;

        private String business_phone;

        private String licence_number;

        private String licence_state;

        private String work_experince;

        private String website;

        private String country;

        private String address1;

        private String address2;

        private String city;

        private String state;

        private String zip;

        private String service_area1;

        private String service_area2;

        private String service_area3;

        private String service_area4;

        private String service_area5;

        private String emergency_service;

        private List<Business_hours> business_hours;

        private List<Service_detail> service_detail;

        private Bank_details bank_details;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getProvider_id() {
            return provider_id;
        }

        public void setProvider_id(String provider_id) {
            this.provider_id = provider_id;
        }

        public String getBusiness_name() {
            return business_name;
        }

        public void setBusiness_name(String business_name) {
            this.business_name = business_name;
        }

        public String getBusiness_phone() {
            return business_phone;
        }

        public void setBusiness_phone(String business_phone) {
            this.business_phone = business_phone;
        }

        public String getLicence_number() {
            return licence_number;
        }

        public void setLicence_number(String licence_number) {
            this.licence_number = licence_number;
        }

        public String getLicence_state() {
            return licence_state;
        }

        public void setLicence_state(String licence_state) {
            this.licence_state = licence_state;
        }

        public String getWork_experince() {
            return work_experince;
        }

        public void setWork_experince(String work_experince) {
            this.work_experince = work_experince;
        }

        public String getWebsite() {
            return website;
        }

        public void setWebsite(String website) {
            this.website = website;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getAddress1() {
            return address1;
        }

        public void setAddress1(String address1) {
            this.address1 = address1;
        }

        public String getAddress2() {
            return address2;
        }

        public void setAddress2(String address2) {
            this.address2 = address2;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getZip() {
            return zip;
        }

        public void setZip(String zip) {
            this.zip = zip;
        }

        public String getService_area1() {
            return service_area1;
        }

        public void setService_area1(String service_area1) {
            this.service_area1 = service_area1;
        }

        public String getService_area2() {
            return service_area2;
        }

        public void setService_area2(String service_area2) {
            this.service_area2 = service_area2;
        }

        public String getService_area3() {
            return service_area3;
        }

        public void setService_area3(String service_area3) {
            this.service_area3 = service_area3;
        }

        public String getService_area4() {
            return service_area4;
        }

        public void setService_area4(String service_area4) {
            this.service_area4 = service_area4;
        }

        public String getService_area5() {
            return service_area5;
        }

        public void setService_area5(String service_area5) {
            this.service_area5 = service_area5;
        }

        public String getEmergency_service() {
            return emergency_service;
        }

        public void setEmergency_service(String emergency_service) {
            this.emergency_service = emergency_service;
        }

        public List<Business_hours> getBusiness_hours() {
            return business_hours;
        }

        public void setBusiness_hours(List<Business_hours> business_hours) {
            this.business_hours = business_hours;
        }

        public List<Service_detail> getService_detail() {
            return service_detail;
        }

        public void setService_detail(List<Service_detail> service_detail) {
            this.service_detail = service_detail;
        }

        public Bank_details getBank_details() {
            return bank_details;
        }

        public void setBank_details(Bank_details bank_details) {
            this.bank_details = bank_details;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "id='" + id + '\'' +
                    ", provider_id='" + provider_id + '\'' +
                    ", business_name='" + business_name + '\'' +
                    ", business_phone='" + business_phone + '\'' +
                    ", licence_number='" + licence_number + '\'' +
                    ", licence_state='" + licence_state + '\'' +
                    ", work_experince='" + work_experince + '\'' +
                    ", website='" + website + '\'' +
                    ", country='" + country + '\'' +
                    ", address1='" + address1 + '\'' +
                    ", address2='" + address2 + '\'' +
                    ", city='" + city + '\'' +
                    ", state='" + state + '\'' +
                    ", zip='" + zip + '\'' +
                    ", service_area1='" + service_area1 + '\'' +
                    ", service_area2='" + service_area2 + '\'' +
                    ", service_area3='" + service_area3 + '\'' +
                    ", service_area4='" + service_area4 + '\'' +
                    ", service_area5='" + service_area5 + '\'' +
                    ", emergency_service='" + emergency_service + '\'' +
                    ", business_hours=" + business_hours +
                    ", service_detail=" + service_detail +
                    ", bank_details=" + bank_details +
                    '}';
        }
    }
    public class Bank_details
    {
        private String bank_id;

        private String provider_id;

        private String bank_name;

        private String routing_number;

        private String account_number;

        private String social_security_number;

        private String state_of_bank_account_issue;

        private String billing_country;

        private String billing_state;

        private String billing_address1;

        private String billing_address2;

        private String billing_city;

        private String billing_zip;

        public void setBank_id(String bank_id){
            this.bank_id = bank_id;
        }
        public String getBank_id(){
            return this.bank_id;
        }
        public void setProvider_id(String provider_id){
            this.provider_id = provider_id;
        }
        public String getProvider_id(){
            return this.provider_id;
        }
        public void setBank_name(String bank_name){
            this.bank_name = bank_name;
        }
        public String getBank_name(){
            return this.bank_name;
        }
        public void setRouting_number(String routing_number){
            this.routing_number = routing_number;
        }
        public String getRouting_number(){
            return this.routing_number;
        }
        public void setAccount_number(String account_number){
            this.account_number = account_number;
        }
        public String getAccount_number(){
            return this.account_number;
        }
        public void setSocial_security_number(String social_security_number){
            this.social_security_number = social_security_number;
        }
        public String getSocial_security_number(){
            return this.social_security_number;
        }
        public void setState_of_bank_account_issue(String state_of_bank_account_issue){
            this.state_of_bank_account_issue = state_of_bank_account_issue;
        }
        public String getState_of_bank_account_issue(){
            return this.state_of_bank_account_issue;
        }
        public void setBilling_country(String billing_country){
            this.billing_country = billing_country;
        }
        public String getBilling_country(){
            return this.billing_country;
        }
        public void setBilling_state(String billing_state){
            this.billing_state = billing_state;
        }
        public String getBilling_state(){
            return this.billing_state;
        }
        public void setBilling_address1(String billing_address1){
            this.billing_address1 = billing_address1;
        }
        public String getBilling_address1(){
            return this.billing_address1;
        }
        public void setBilling_address2(String billing_address2){
            this.billing_address2 = billing_address2;
        }
        public String getBilling_address2(){
            return this.billing_address2;
        }
        public void setBilling_city(String billing_city){
            this.billing_city = billing_city;
        }
        public String getBilling_city(){
            return this.billing_city;
        }
        public void setBilling_zip(String billing_zip){
            this.billing_zip = billing_zip;
        }
        public String getBilling_zip(){
            return this.billing_zip;
        }
    }
    public class Service_detail extends AddScreenCheckModel {
        private String id;

        private String provider_id;

        private String category_id;

        private String sub_category_id;

        public void setId(String id){
            this.id = id;
        }
        public String getId(){
            return this.id;
        }
        public void setProvider_id(String provider_id){
            this.provider_id = provider_id;
        }
        public String getProvider_id(){
            return this.provider_id;
        }
        public void setCategory_id(String category_id){
            this.category_id = category_id;
        }
        public String getCategory_id(){
            return this.category_id;
        }
        public void setSub_category_id(String sub_category_id){
            this.sub_category_id = sub_category_id;
        }
        public String getSub_category_id(){
            return this.sub_category_id;
        }

        @Override
        public String toString() {
            return "Service_detail{" +
                    "id='" + id + '\'' +
                    ", provider_id='" + provider_id + '\'' +
                    ", category_id='" + category_id + '\'' +
                    ", sub_category_id='" + sub_category_id + '\'' +
                    '}';
        }
    }

    public class Business_hours
    {
        private String id;

        private String provider_id;

        private String day;

        private String start_time;

        private String end_time;

        public void setId(String id){
            this.id = id;
        }
        public String getId(){
            return this.id;
        }
        public void setProvider_id(String provider_id){
            this.provider_id = provider_id;
        }
        public String getProvider_id(){
            return this.provider_id;
        }
        public void setDay(String day){
            this.day = day;
        }
        public String getDay(){
            return this.day;
        }
        public void setStart_time(String start_time){
            this.start_time = start_time;
        }
        public String getStart_time(){
            return this.start_time;
        }
        public void setEnd_time(String end_time){
            this.end_time = end_time;
        }
        public String getEnd_time(){
            return this.end_time;
        }
    }
}
