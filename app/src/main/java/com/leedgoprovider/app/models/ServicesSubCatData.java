package com.leedgoprovider.app.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ServicesSubCatData implements Serializable {

	@SerializedName("name")
	private String name;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private int id;

	@SerializedName("scat_id")
	private int scatId;

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setScatId(int scatId){
		this.scatId = scatId;
	}

	public int getScatId(){
		return scatId;
	}

	@Override
 	public String toString(){
		return
			"SubCatItem{" +
			"name = '" + name + '\'' +
			",created_at = '" + createdAt + '\'' +
			",id = '" + id + '\'' +
			",scat_id = '" + scatId + '\'' +
			"}";
		}
}