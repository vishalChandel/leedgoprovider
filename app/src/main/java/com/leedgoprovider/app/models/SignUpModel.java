package com.leedgoprovider.app.models;

import com.google.gson.annotations.SerializedName;

public class SignUpModel{

	@SerializedName("data")
	private SignUpData data;

	@SerializedName("provider_id")
	private String providerId;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setData(SignUpData data){
		this.data = data;
	}

	public SignUpData getData(){
		return data;
	}

	public void setProviderId(String providerId){
		this.providerId = providerId;
	}

	public String getProviderId(){
		return providerId;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return
			"SignUpModel{" +
			"data = '" + data + '\'' +
			",provider_id = '" + providerId + '\'' +
			",message = '" + message + '\'' +
			",status = '" + status + '\'' +
			"}";
		}
}