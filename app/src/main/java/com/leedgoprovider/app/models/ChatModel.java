package com.leedgoprovider.app.models;

import java.util.List;

public class ChatModel {
    private int status;

    private String message;

    private List<Data> data;

    public void setStatus(int status){
        this.status = status;
    }
    public int getStatus(){
        return this.status;
    }
    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return this.message;
    }
    public void setData(List<Data> data){
        this.data = data;
    }
    public List<Data> getData(){
        return this.data;
    }
    public class Data
    {
        private String id;

        private String provider_id;

        private String client_id;

        private String room_id;

        private String creation_time;

        private String profile_image;

        private String latest_message;

        private String sender_type;

        private String name;

        public void setId(String id){
            this.id = id;
        }
        public String getId(){
            return this.id;
        }
        public void setProvider_id(String provider_id){
            this.provider_id = provider_id;
        }
        public String getProvider_id(){
            return this.provider_id;
        }
        public void setClient_id(String client_id){
            this.client_id = client_id;
        }
        public String getClient_id(){
            return this.client_id;
        }
        public void setRoom_id(String room_id){
            this.room_id = room_id;
        }
        public String getRoom_id(){
            return this.room_id;
        }

        public String getCreation_time() {
            return creation_time;
        }

        public void setCreation_time(String creation_time) {
            this.creation_time = creation_time;
        }

        public void setProfile_image(String profile_image){
            this.profile_image = profile_image;
        }
        public String getProfile_image(){
            return this.profile_image;
        }
        public void setLatest_message(String latest_message){
            this.latest_message = latest_message;
        }
        public String getLatest_message(){
            return this.latest_message;
        }
        public void setSender_type(String sender_type){
            this.sender_type = sender_type;
        }
        public String getSender_type(){
            return this.sender_type;
        }
        public void setName(String name){
            this.name = name;
        }
        public String getName(){
            return this.name;
        }
    }
}
