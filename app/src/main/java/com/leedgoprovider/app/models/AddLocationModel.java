package com.leedgoprovider.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddLocationModel {
    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private List<Data> data;

    public void setStatus(int status){
        this.status = status;
    }
    public int getStatus(){
        return this.status;
    }
    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return this.message;
    }
    public void setData(List<Data> data){
        this.data = data;
    }
    public List<Data> getData(){
        return this.data;
    }
    public class Data
    {
        private String provider_location_id;

        private String provider_id;

        private String location_name;

        private String state;

        private String city;

        private String latitude;

        private String longitude;

        private String primary_value;

        public void setProvider_location_id(String provider_location_id){
            this.provider_location_id = provider_location_id;
        }
        public String getProvider_location_id(){
            return this.provider_location_id;
        }
        public void setProvider_id(String provider_id){
            this.provider_id = provider_id;
        }
        public String getProvider_id(){
            return this.provider_id;
        }
        public void setLocation_name(String location_name){
            this.location_name = location_name;
        }
        public String getLocation_name(){
            return this.location_name;
        }
        public void setState(String state){
            this.state = state;
        }
        public String getState(){
            return this.state;
        }
        public void setCity(String city){
            this.city = city;
        }
        public String getCity(){
            return this.city;
        }
        public void setLatitude(String latitude){
            this.latitude = latitude;
        }
        public String getLatitude(){
            return this.latitude;
        }
        public void setLongitude(String longitude){
            this.longitude = longitude;
        }
        public String getLongitude(){
            return this.longitude;
        }
        public void setPrimary_value(String primary_value){
            this.primary_value = primary_value;
        }
        public String getPrimary_value(){
            return this.primary_value;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "provider_location_id='" + provider_location_id + '\'' +
                    ", provider_id='" + provider_id + '\'' +
                    ", location_name='" + location_name + '\'' +
                    ", state='" + state + '\'' +
                    ", city='" + city + '\'' +
                    ", latitude='" + latitude + '\'' +
                    ", longitude='" + longitude + '\'' +
                    ", primary_value='" + primary_value + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "AddLocationModel{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
