package com.leedgoprovider.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationModel {
    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private List<Data> data;

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return this.status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public List<Data> getData() {
        return this.data;
    }

    public class Data {
        private String notification_id;

        private String message_id;

        private String title;

        private String message;

        private String notification_type;

        private String sender_id;

        private String sender_name;

        private String user_id;

        private String user_type;

        private String task_id;

        private String task_status;

        private String server_creation_date;

        private String viewed_status;

        private String tab_seen;

        private String creation_date;

        private String creation_date_timestamp;

        public void setNotification_id(String notification_id) {
            this.notification_id = notification_id;
        }

        public String getNotification_id() {
            return this.notification_id;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getTitle() {
            return this.title;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getMessage() {
            return this.message;
        }

        public void setNotification_type(String notification_type) {
            this.notification_type = notification_type;
        }

        public String getNotification_type() {
            return this.notification_type;
        }

        public void setSender_id(String sender_id) {
            this.sender_id = sender_id;
        }

        public String getSender_id() {
            return this.sender_id;
        }

        public void setSender_name(String sender_name) {
            this.sender_name = sender_name;
        }

        public String getSender_name() {
            return this.sender_name;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getUser_id() {
            return this.user_id;
        }

        public void setUser_type(String user_type) {
            this.user_type = user_type;
        }

        public String getUser_type() {
            return this.user_type;
        }

        public void setTask_id(String task_id) {
            this.task_id = task_id;
        }

        public String getTask_id() {
            return this.task_id;
        }

        public void setTask_status(String task_status) {
            this.task_status = task_status;
        }

        public String getTask_status() {
            return this.task_status;
        }

        public String getServer_creation_date() {
            return server_creation_date;
        }

        public void setServer_creation_date(String server_creation_date) {
            this.server_creation_date = server_creation_date;
        }

        public void setViewed_status(String viewed_status) {
            this.viewed_status = viewed_status;
        }

        public String getViewed_status() {
            return this.viewed_status;
        }

        public void setTab_seen(String tab_seen) {
            this.tab_seen = tab_seen;
        }

        public String getTab_seen() {
            return this.tab_seen;
        }

        public void setCreation_date(String creation_date) {
            this.creation_date = creation_date;
        }

        public String getCreation_date() {
            return this.creation_date;
        }

        public void setCreation_date_timestamp(String creation_date_timestamp) {
            this.creation_date_timestamp = creation_date_timestamp;
        }

        public String getCreation_date_timestamp() {
            return this.creation_date_timestamp;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "notification_id='" + notification_id + '\'' +
                    ", title='" + title + '\'' +
                    ", message='" + message + '\'' +
                    ", notification_type='" + notification_type + '\'' +
                    ", user_id='" + user_id + '\'' +
                    ", user_type='" + user_type + '\'' +
                    ", task_id='" + task_id + '\'' +
                    ", task_status='" + task_status + '\'' +
                    ", server_creation_date='" + server_creation_date + '\'' +
                    ", viewed_status='" + viewed_status + '\'' +
                    ", tab_seen='" + tab_seen + '\'' +
                    ", creation_date='" + creation_date + '\'' +
                    ", creation_date_timestamp='" + creation_date_timestamp + '\'' +
                    '}';
        }

        public String getMessage_id() {
            return message_id;
        }

        public void setMessage_id(String message_id) {
            this.message_id = message_id;
        }
    }

}
