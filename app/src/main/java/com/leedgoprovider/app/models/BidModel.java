package com.leedgoprovider.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BidModel {
    @SerializedName("status")
    @Expose
    int status;
    @SerializedName("message")
    @Expose
    String message;
    @SerializedName("bid_id")
    @Expose
    String bid_id;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getBid_id() {
        return bid_id;
    }

    public void setBid_id(String bid_id) {
        this.bid_id = bid_id;
    }
}
