package com.leedgoprovider.app.models.locationlatlong;

import com.google.gson.annotations.SerializedName;

public class Result {

    @SerializedName("geometry")
    private Geometry geometry;

    @SerializedName("place_id")
    private String placeId;

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    @Override
    public String toString() {
        return
                "Result{" +
                        ",geometry = '" + geometry + '\'' +
                        ",place_id = '" + placeId + '\'' +
                        "}";
    }
}