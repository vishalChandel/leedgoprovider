package com.leedgoprovider.app.models.profile;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class ProfileData {

	@SerializedName("creation_time")
	private String creationTime;

	@SerializedName("business_hours")
	private List<ProfileBusinessHours> businessHours;

	@SerializedName("business_name")
	private String businessName;

	@SerializedName("google_id")
	private String googleId;

	@SerializedName("website")
	private String website;

	@SerializedName("address")
	private String address;

	@SerializedName("disable_notification")
	private String disableNotification;

	@SerializedName("latitude")
	private String latitude;

	@SerializedName("device_type")
	private String deviceType;

	@SerializedName("apple_id")
	private Object appleId;

	@SerializedName("facebook_id")
	private String facebookId;

	@SerializedName("work_experince")
	private String workExperince;

	@SerializedName("password")
	private String password;

	@SerializedName("profile_image")
	private String profileImage;

	@SerializedName("disable")
	private String disable;

	@SerializedName("device_token")
	private String deviceToken;

	@SerializedName("provider_id")
	private String providerId;

	@SerializedName("business_email")
	private String businessEmail;

	@SerializedName("provider_name")
	private String providerName;

	@SerializedName("longitude")
	private String longitude;

	public void setCreationTime(String creationTime){
		this.creationTime = creationTime;
	}

	public String getCreationTime(){
		return creationTime;
	}

	public void setBusinessHours(List<ProfileBusinessHours> businessHours){
		this.businessHours = businessHours;
	}

	public List<ProfileBusinessHours> getBusinessHours(){
		return businessHours;
	}

	public void setBusinessName(String businessName){
		this.businessName = businessName;
	}

	public String getBusinessName(){
		return businessName;
	}

	public void setGoogleId(String googleId){
		this.googleId = googleId;
	}

	public String getGoogleId(){
		return googleId;
	}

	public void setWebsite(String website){
		this.website = website;
	}

	public String getWebsite(){
		return website;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setDisableNotification(String disableNotification){
		this.disableNotification = disableNotification;
	}

	public String getDisableNotification(){
		return disableNotification;
	}

	public void setLatitude(String latitude){
		this.latitude = latitude;
	}

	public String getLatitude(){
		return latitude;
	}

	public void setDeviceType(String deviceType){
		this.deviceType = deviceType;
	}

	public String getDeviceType(){
		return deviceType;
	}

	public void setAppleId(Object appleId){
		this.appleId = appleId;
	}

	public Object getAppleId(){
		return appleId;
	}

	public void setFacebookId(String facebookId){
		this.facebookId = facebookId;
	}

	public String getFacebookId(){
		return facebookId;
	}

	public void setWorkExperince(String workExperince){
		this.workExperince = workExperince;
	}

	public String getWorkExperince(){
		return workExperince;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setProfileImage(String profileImage){
		this.profileImage = profileImage;
	}

	public String getProfileImage(){
		return profileImage;
	}

	public void setDisable(String disable){
		this.disable = disable;
	}

	public String getDisable(){
		return disable;
	}

	public void setDeviceToken(String deviceToken){
		this.deviceToken = deviceToken;
	}

	public String getDeviceToken(){
		return deviceToken;
	}

	public void setProviderId(String providerId){
		this.providerId = providerId;
	}

	public String getProviderId(){
		return providerId;
	}

	public void setBusinessEmail(String businessEmail){
		this.businessEmail = businessEmail;
	}

	public String getBusinessEmail(){
		return businessEmail;
	}

	public void setProviderName(String providerName){
		this.providerName = providerName;
	}

	public String getProviderName(){
		return providerName;
	}

	public void setLongitude(String longitude){
		this.longitude = longitude;
	}

	public String getLongitude(){
		return longitude;
	}

	@Override
 	public String toString(){
		return
			"Data{" +
			"creation_time = '" + creationTime + '\'' +
			",business_hours = '" + businessHours + '\'' +
			",business_name = '" + businessName + '\'' +
			",google_id = '" + googleId + '\'' +
			",website = '" + website + '\'' +
			",address = '" + address + '\'' +
			",disable_notification = '" + disableNotification + '\'' +
			",latitude = '" + latitude + '\'' +
			",device_type = '" + deviceType + '\'' +
			",apple_id = '" + appleId + '\'' +
			",facebook_id = '" + facebookId + '\'' +
			",work_experince = '" + workExperince + '\'' +
			",password = '" + password + '\'' +
			",profile_image = '" + profileImage + '\'' +
			",disable = '" + disable + '\'' +
			",device_token = '" + deviceToken + '\'' +
			",provider_id = '" + providerId + '\'' +
			",business_email = '" + businessEmail + '\'' +
			",provider_name = '" + providerName + '\'' +
			",longitude = '" + longitude + '\'' +
			"}";
		}
}