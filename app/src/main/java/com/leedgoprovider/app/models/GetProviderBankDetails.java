package com.leedgoprovider.app.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GetProviderBankDetails implements Serializable {

    @SerializedName("creation_time")
    private String creationTime;

    @SerializedName("country")
    private String country;

    @SerializedName("firstname")
    private String firstname;

    @SerializedName("id_number")
    private String idNumber;

    @SerializedName("city")
    private String city;

    @SerializedName("stripe_account_id")
    private String stripeAccountId;

    @SerializedName("verified")
    private String verified;

    @SerializedName("bank_acc_holder_name")
    private String bankAccHolderName;

    @SerializedName("bank_routing_number")
    private String bankRoutingNumber;

    @SerializedName("back_document")
    private String backDocument;

    @SerializedName("enabled")
    private String enabled;

    @SerializedName("lastname")
    private String lastname;

    @SerializedName("stripe_bank_id")
    private String stripeBankId;

    @SerializedName("bank_acc_number")
    private String bankAccNumber;

    @SerializedName("phone")
    private String phone;

    @SerializedName("dob")
    private String dob;

    @SerializedName("ssn_last_4")
    private String ssnLast4;

    @SerializedName("provider_id")
    private String providerId;

    @SerializedName("state")
    private String state;

    @SerializedName("postal_code")
    private String postalCode;

    @SerializedName("line2")
    private String line2;

    @SerializedName("front_document")
    private String frontDocument;

    @SerializedName("line1")
    private String line1;

    @SerializedName("account_detail_id")
    private String accountDetailId;

    public String getCreationTime(){
        return creationTime;
    }

    public String getCountry(){
        return country;
    }

    public String getFirstname(){
        return firstname;
    }

    public String getIdNumber(){
        return idNumber;
    }

    public String getCity(){
        return city;
    }

    public String getStripeAccountId(){
        return stripeAccountId;
    }

    public String getVerified(){
        return verified;
    }

    public String getBankAccHolderName(){
        return bankAccHolderName;
    }

    public String getBankRoutingNumber(){
        return bankRoutingNumber;
    }

    public String getBackDocument(){
        return backDocument;
    }

    public String getEnabled(){
        return enabled;
    }

    public String getLastname(){
        return lastname;
    }

    public String getStripeBankId(){
        return stripeBankId;
    }

    public String getBankAccNumber(){
        return bankAccNumber;
    }

    public String getPhone(){
        return phone;
    }

    public String getDob(){
        return dob;
    }

    public String getSsnLast4(){
        return ssnLast4;
    }

    public String getProviderId(){
        return providerId;
    }

    public String getState(){
        return state;
    }

    public String getPostalCode(){
        return postalCode;
    }

    public String getLine2(){
        return line2;
    }

    public String getFrontDocument(){
        return frontDocument;
    }

    public String getLine1(){
        return line1;
    }

    public String getAccountDetailId(){
        return accountDetailId;
    }
}
