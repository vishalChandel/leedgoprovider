package com.leedgoprovider.app.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserDetail implements Serializable {

	@SerializedName("updated_on")
	private Object updatedOn;

	@SerializedName("google_id")
	private Object googleId;

	@SerializedName("role")
	private Object role;

	@SerializedName("gender")
	private Object gender;

	@SerializedName("latitude")
	private Object latitude;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("device_type")
	private String deviceType;

	@SerializedName("lastvisit")
	private Object lastvisit;

	@SerializedName("password")
	private String password;

	@SerializedName("cellno")
	private String cellno;

	@SerializedName("category_id")
	private String categoryId;

	@SerializedName("id")
	private String id;

	@SerializedName("email")
	private String email;

	@SerializedName("longitude")
	private Object longitude;

	@SerializedName("profile_pic_server")
	private Object profilePicServer;

	@SerializedName("authKey")
	private String authKey;

	@SerializedName("address")
	private Object address;

	@SerializedName("push_status")
	private Object pushStatus;

	@SerializedName("disable_notification")
	private Object disableNotification;

	@SerializedName("profile_pic")
	private Object profilePic;

	@SerializedName("usertype")
	private String usertype;

	@SerializedName("apple_id")
	private Object appleId;

	@SerializedName("facebook_id")
	private Object facebookId;

	@SerializedName("device_token")
	private String deviceToken;

	@SerializedName("name")
	private String name;

	@SerializedName("user_location")
	private Object userLocation;

	@SerializedName("customer_id")
	private String customerId;

	@SerializedName("superuser")
	private Object superuser;

	@SerializedName("username")
	private Object username;

	@SerializedName("status")
	private Object status;

	public Object getUpdatedOn(){
		return updatedOn;
	}

	public Object getGoogleId(){
		return googleId;
	}

	public Object getRole(){
		return role;
	}

	public Object getGender(){
		return gender;
	}

	public Object getLatitude(){
		return latitude;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public String getDeviceType(){
		return deviceType;
	}

	public Object getLastvisit(){
		return lastvisit;
	}

	public String getPassword(){
		return password;
	}

	public String getCellno(){
		return cellno;
	}

	public String getCategoryId(){
		return categoryId;
	}

	public String getId(){
		return id;
	}

	public String getEmail(){
		return email;
	}

	public Object getLongitude(){
		return longitude;
	}

	public Object getProfilePicServer(){
		return profilePicServer;
	}

	public String getAuthKey(){
		return authKey;
	}

	public Object getAddress(){
		return address;
	}

	public Object getPushStatus(){
		return pushStatus;
	}

	public Object getDisableNotification(){
		return disableNotification;
	}

	public Object getProfilePic(){
		return profilePic;
	}

	public String getUsertype(){
		return usertype;
	}

	public Object getAppleId(){
		return appleId;
	}

	public Object getFacebookId(){
		return facebookId;
	}

	public String getDeviceToken(){
		return deviceToken;
	}

	public String getName(){
		return name;
	}

	public Object getUserLocation(){
		return userLocation;
	}

	public String getCustomerId(){
		return customerId;
	}

	public Object getSuperuser(){
		return superuser;
	}

	public Object getUsername(){
		return username;
	}

	public Object getStatus(){
		return status;
	}
}