package com.leedgoprovider.app.models;

import java.io.Serializable;
import java.util.ArrayList;

import com.google.gson.annotations.SerializedName;

public class ServicesDataModel implements Serializable {

	@SerializedName("image")
	private String image;

	@SerializedName("sub_cat")
	private ArrayList<ServicesSubCatData> subCat;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	@SerializedName("scat_id")
	private int scatId;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setSubCat(ArrayList<ServicesSubCatData> subCat){
		this.subCat = subCat;
	}

	public ArrayList<ServicesSubCatData> getSubCat(){
		return subCat;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setScatId(int scatId){
		this.scatId = scatId;
	}

	public int getScatId(){
		return scatId;
	}

	@Override
 	public String toString(){
		return
			"DataItem{" +
			"image = '" + image + '\'' +
			",sub_cat = '" + subCat + '\'' +
			",name = '" + name + '\'' +
			",id = '" + id + '\'' +
			",scat_id = '" + scatId + '\'' +
			"}";
		}
}