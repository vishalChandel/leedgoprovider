package com.leedgoprovider.app.models.profile;

import com.google.gson.annotations.SerializedName;

public class ProfileModel{

	@SerializedName("data")
	private ProfileData data;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public String getUnread_notification() {
		return unread_notification;
	}

	public void setUnread_notification(String unread_notification) {
		this.unread_notification = unread_notification;
	}

	@SerializedName("unread_notification")
	private String unread_notification;

	public void setData(ProfileData data){
		this.data = data;
	}

	public ProfileData getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return
			"ProfileModel{" +
			"data = '" + data + '\'' +
			",message = '" + message + '\'' +
			",status = '" + status + '\'' +
			"}";
		}
}