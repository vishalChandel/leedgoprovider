package com.leedgoprovider.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AddModel {
        @SerializedName("status")
        @Expose
        private int status;

       @SerializedName("message")
        @Expose
        private String message;

        @SerializedName("data")
        @Expose
        private List<Data> data;

        public void setStatus(int status){
            this.status = status;
        }
        public int getStatus(){
            return this.status;
        }
        public void setMessage(String message){
            this.message = message;
        }
        public String getMessage(){
            return this.message;
        }
        public void setData(List<Data> data){
            this.data = data;
        }
        public List<Data> getData(){
            return this.data;
        }
    public class Data
    {
        private int id;

        private int scat_id;

        private String name;

        private String image;

        private List<Sub_cat> sub_cat;

        public void setId(int id){
            this.id = id;
        }
        public int getId(){
            return this.id;
        }
        public void setScat_id(int scat_id){
            this.scat_id = scat_id;
        }
        public int getScat_id(){
            return this.scat_id;
        }
        public void setName(String name){
            this.name = name;
        }
        public String getName(){
            return this.name;
        }
        public void setImage(String image){
            this.image = image;
        }
        public String getImage(){
            return this.image;
        }
        public void setSub_cat(List<Sub_cat> sub_cat){
            this.sub_cat = sub_cat;
        }
        public List<Sub_cat> getSub_cat(){
            return this.sub_cat;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "id=" + id +
                    ", scat_id=" + scat_id +
                    ", name='" + name + '\'' +
                    ", image='" + image + '\'' +
                    ", sub_cat=" + sub_cat +
                    '}';
        }
    }
    public class Sub_cat
    {
        private int id;

        private int scat_id;

        private String name;

        private String created_at;

        public void setId(int id){
            this.id = id;
        }
        public int getId(){
            return this.id;
        }
        public void setScat_id(int scat_id){
            this.scat_id = scat_id;
        }
        public int getScat_id(){
            return this.scat_id;
        }
        public void setName(String name){
            this.name = name;
        }
        public String getName(){
            return this.name;
        }
        public void setCreated_at(String created_at){
            this.created_at = created_at;
        }
        public String getCreated_at(){
            return this.created_at;
        }
    }
}
