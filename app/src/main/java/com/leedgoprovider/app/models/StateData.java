package com.leedgoprovider.app.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class StateData implements Serializable {

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private String id;

	@SerializedName("abbr")
	private String abbr;

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setAbbr(String abbr){
		this.abbr = abbr;
	}

	public String getAbbr(){
		return abbr;
	}

	@Override
 	public String toString(){
		return
			"DataItem{" +
			"name = '" + name + '\'' +
			",id = '" + id + '\'' +
			",abbr = '" + abbr + '\'' +
			"}";
		}
}