package com.leedgoprovider.app.models;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class StatesModel{

	@SerializedName("data")
	private List<StateData> data;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setData(List<StateData> data){
		this.data = data;
	}

	public List<StateData> getData(){
		return data;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return
			"StatesModel{" +
			"data = '" + data + '\'' +
			",message = '" + message + '\'' +
			",status = '" + status + '\'' +
			"}";
		}
}