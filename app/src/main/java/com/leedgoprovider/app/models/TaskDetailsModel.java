package com.leedgoprovider.app.models;

public class TaskDetailsModel {
    private int status;

    private String message;

    private Data data;

    public void setStatus(int status) {
        this.status = status;
    }

    public int getStatus() {
        return this.status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return this.message;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Data getData() {
        return this.data;
    }

    public class Data {
        private String id;

        private String unique_task_id;

        private String user_id;

        private String category_id;

        private String subcategory_id;

        private String business_id;

        private String order_lat;

        private String order_long;

        private String work_details;

        private String order_status;

        private String start_address;

        private String address;

        private String end_address;

        private String work_date;

        private String work_time;

        private String task_size;

        private String what_bring;

        private String job_title;

        private String zip_code;

        private String assign_user;

        private String payment_status;

        private String created_at;

        private String updated_at;

        private String flexible_week;

        private String photos;

        private String promo_code;

        private String state;

        private String balance;

        private String image1;

        private String image2;

        private String image3;

        private String image1_server;

        private String image2_server;

        private String image3_server;

        private String assigned_amount;

        private String assigned_date;

        private String provider_work_date;

        private String requested_for_historical_structure;

        private String request_covered_by_insurance_claim;

        private String owner_or_authorized_representative_of_owner;

        private String task_complete_date;

        private String username;

        private String userimage;

        private String user_cellno;

        private String category_name;

        private String category_image;

        private String subcategory_name;

        public void setId(String id) {
            this.id = id;
        }

        public String getId() {
            return this.id;
        }

        public void setUnique_task_id(String unique_task_id) {
            this.unique_task_id = unique_task_id;
        }

        public String getUnique_task_id() {
            return this.unique_task_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getUser_id() {
            return this.user_id;
        }

        public void setCategory_id(String category_id) {
            this.category_id = category_id;
        }

        public String getCategory_id() {
            return this.category_id;
        }

        public void setSubcategory_id(String subcategory_id) {
            this.subcategory_id = subcategory_id;
        }

        public String getSubcategory_id() {
            return this.subcategory_id;
        }

        public void setBusiness_id(String business_id) {
            this.business_id = business_id;
        }

        public String getBusiness_id() {
            return this.business_id;
        }

        public void setOrder_lat(String order_lat) {
            this.order_lat = order_lat;
        }

        public String getOrder_lat() {
            return this.order_lat;
        }

        public void setOrder_long(String order_long) {
            this.order_long = order_long;
        }

        public String getOrder_long() {
            return this.order_long;
        }

        public void setWork_details(String work_details) {
            this.work_details = work_details;
        }

        public String getWork_details() {
            return this.work_details;
        }

        public void setOrder_status(String order_status) {
            this.order_status = order_status;
        }

        public String getOrder_status() {
            return this.order_status;
        }

        public void setStart_address(String start_address) {
            this.start_address = start_address;
        }

        public String getStart_address() {
            return this.start_address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getAddress() {
            return this.address;
        }

        public void setEnd_address(String end_address) {
            this.end_address = end_address;
        }

        public String getEnd_address() {
            return this.end_address;
        }

        public void setWork_date(String work_date) {
            this.work_date = work_date;
        }

        public String getWork_date() {
            return this.work_date;
        }

        public void setWork_time(String work_time) {
            this.work_time = work_time;
        }

        public String getWork_time() {
            return this.work_time;
        }

        public void setTask_size(String task_size) {
            this.task_size = task_size;
        }

        public String getTask_size() {
            return this.task_size;
        }

        public void setWhat_bring(String what_bring) {
            this.what_bring = what_bring;
        }

        public String getWhat_bring() {
            return this.what_bring;
        }

        public void setJob_title(String job_title) {
            this.job_title = job_title;
        }

        public String getJob_title() {
            return this.job_title;
        }

        public void setZip_code(String zip_code) {
            this.zip_code = zip_code;
        }

        public String getZip_code() {
            return this.zip_code;
        }

        public void setAssign_user(String assign_user) {
            this.assign_user = assign_user;
        }

        public String getAssign_user() {
            return this.assign_user;
        }

        public void setPayment_status(String payment_status) {
            this.payment_status = payment_status;
        }

        public String getPayment_status() {
            return this.payment_status;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getUpdated_at() {
            return this.updated_at;
        }

        public void setFlexible_week(String flexible_week) {
            this.flexible_week = flexible_week;
        }

        public String getFlexible_week() {
            return this.flexible_week;
        }

        public void setPhotos(String photos) {
            this.photos = photos;
        }

        public String getPhotos() {
            return this.photos;
        }

        public void setPromo_code(String promo_code) {
            this.promo_code = promo_code;
        }

        public String getPromo_code() {
            return this.promo_code;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getState() {
            return this.state;
        }

        public void setBalance(String balance) {
            this.balance = balance;
        }

        public String getBalance() {
            return this.balance;
        }

        public void setImage1(String image1) {
            this.image1 = image1;
        }

        public String getImage1() {
            return this.image1;
        }

        public void setImage2(String image2) {
            this.image2 = image2;
        }

        public String getImage2() {
            return this.image2;
        }

        public void setImage3(String image3) {
            this.image3 = image3;
        }

        public String getImage3() {
            return this.image3;
        }

        public void setImage1_server(String image1_server) {
            this.image1_server = image1_server;
        }

        public String getImage1_server() {
            return this.image1_server;
        }

        public void setImage2_server(String image2_server) {
            this.image2_server = image2_server;
        }

        public String getImage2_server() {
            return this.image2_server;
        }

        public void setImage3_server(String image3_server) {
            this.image3_server = image3_server;
        }

        public String getImage3_server() {
            return this.image3_server;
        }

        public void setAssigned_amount(String assigned_amount) {
            this.assigned_amount = assigned_amount;
        }

        public String getAssigned_amount() {
            return this.assigned_amount;
        }

        public void setAssigned_date(String assigned_date) {
            this.assigned_date = assigned_date;
        }

        public String getAssigned_date() {
            return this.assigned_date;
        }

        public void setProvider_work_date(String provider_work_date) {
            this.provider_work_date = provider_work_date;
        }

        public String getProvider_work_date() {
            return this.provider_work_date;
        }

        public void setRequested_for_historical_structure(String requested_for_historical_structure) {
            this.requested_for_historical_structure = requested_for_historical_structure;
        }

        public String getRequested_for_historical_structure() {
            return this.requested_for_historical_structure;
        }

        public void setRequest_covered_by_insurance_claim(String request_covered_by_insurance_claim) {
            this.request_covered_by_insurance_claim = request_covered_by_insurance_claim;
        }

        public String getRequest_covered_by_insurance_claim() {
            return this.request_covered_by_insurance_claim;
        }

        public void setOwner_or_authorized_representative_of_owner(String owner_or_authorized_representative_of_owner) {
            this.owner_or_authorized_representative_of_owner = owner_or_authorized_representative_of_owner;
        }

        public String getOwner_or_authorized_representative_of_owner() {
            return this.owner_or_authorized_representative_of_owner;
        }

        public void setTask_complete_date(String task_complete_date) {
            this.task_complete_date = task_complete_date;
        }

        public String getTask_complete_date() {
            return this.task_complete_date;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getUsername() {
            return this.username;
        }

        public void setUserimage(String userimage) {
            this.userimage = userimage;
        }

        public String getUserimage() {
            return this.userimage;
        }

        public void setUser_cellno(String user_cellno) {
            this.user_cellno = user_cellno;
        }

        public String getUser_cellno() {
            return this.user_cellno;
        }

        public void setCategory_name(String category_name) {
            this.category_name = category_name;
        }

        public String getCategory_name() {
            return this.category_name;
        }

        public void setCategory_image(String category_image) {
            this.category_image = category_image;
        }

        public String getCategory_image() {
            return this.category_image;
        }

        public void setSubcategory_name(String subcategory_name) {
            this.subcategory_name = subcategory_name;
        }

        public String getSubcategory_name() {
            return this.subcategory_name;
        }
    }

}
