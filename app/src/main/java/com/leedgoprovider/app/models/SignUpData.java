package com.leedgoprovider.app.models;

import com.google.gson.annotations.SerializedName;

public class SignUpData {

	@SerializedName("creation_time")
	private String creationTime;

	@SerializedName("google_id")
	private String googleId;

	@SerializedName("address")
	private String address;

	@SerializedName("disable_notification")
	private String disableNotification;

	@SerializedName("latitude")
	private String latitude;

	@SerializedName("device_type")
	private String deviceType;

	@SerializedName("facebook_id")
	private String facebookId;

	@SerializedName("password")
	private String password;

	@SerializedName("profile_image")
	private String profileImage;

	@SerializedName("disable")
	private String disable;

	@SerializedName("device_token")
	private String deviceToken;

	@SerializedName("provider_id")
	private String providerId;

	@SerializedName("business_email")
	private String businessEmail;

	@SerializedName("provider_name")
	private String providerName;

	@SerializedName("longitude")
	private String longitude;

	public void setCreationTime(String creationTime){
		this.creationTime = creationTime;
	}

	public String getCreationTime(){
		return creationTime;
	}

	public void setGoogleId(String googleId){
		this.googleId = googleId;
	}

	public String getGoogleId(){
		return googleId;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setDisableNotification(String disableNotification){
		this.disableNotification = disableNotification;
	}

	public String getDisableNotification(){
		return disableNotification;
	}

	public void setLatitude(String latitude){
		this.latitude = latitude;
	}

	public String getLatitude(){
		return latitude;
	}

	public void setDeviceType(String deviceType){
		this.deviceType = deviceType;
	}

	public String getDeviceType(){
		return deviceType;
	}

	public void setFacebookId(String facebookId){
		this.facebookId = facebookId;
	}

	public String getFacebookId(){
		return facebookId;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getPassword(){
		return password;
	}

	public void setProfileImage(String profileImage){
		this.profileImage = profileImage;
	}

	public String getProfileImage(){
		return profileImage;
	}

	public void setDisable(String disable){
		this.disable = disable;
	}

	public String getDisable(){
		return disable;
	}

	public void setDeviceToken(String deviceToken){
		this.deviceToken = deviceToken;
	}

	public String getDeviceToken(){
		return deviceToken;
	}

	public void setProviderId(String providerId){
		this.providerId = providerId;
	}

	public String getProviderId(){
		return providerId;
	}

	public void setBusinessEmail(String businessEmail){
		this.businessEmail = businessEmail;
	}

	public String getBusinessEmail(){
		return businessEmail;
	}

	public void setProviderName(String providerName){
		this.providerName = providerName;
	}

	public String getProviderName(){
		return providerName;
	}

	public void setLongitude(String longitude){
		this.longitude = longitude;
	}

	public String getLongitude(){
		return longitude;
	}

	@Override
 	public String toString(){
		return
			"Data{" +
			"creation_time = '" + creationTime + '\'' +
			",google_id = '" + googleId + '\'' +
			",address = '" + address + '\'' +
			",disable_notification = '" + disableNotification + '\'' +
			",latitude = '" + latitude + '\'' +
			",device_type = '" + deviceType + '\'' +
			",facebook_id = '" + facebookId + '\'' +
			",password = '" + password + '\'' +
			",profile_image = '" + profileImage + '\'' +
			",disable = '" + disable + '\'' +
			",device_token = '" + deviceToken + '\'' +
			",provider_id = '" + providerId + '\'' +
			",business_email = '" + businessEmail + '\'' +
			",provider_name = '" + providerName + '\'' +
			",longitude = '" + longitude + '\'' +
			"}";
		}
}