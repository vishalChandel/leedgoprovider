package com.leedgoprovider.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class InRequestModel {
    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("message")
    @Expose
    private String message;


    @SerializedName("data")
    @Expose
    private List<Data> data;

    public void setStatus(int status){
        this.status = status;
    }
    public int getStatus(){
        return this.status;
    }
    public void setMessage(String message){
        this.message = message;
    }
    public String getMessage(){
        return this.message;
    }
    public void setData(List<Data> data){
        this.data = data;
    }
    public List<Data> getData(){
        return this.data;
    }
    public class Data
    {
        private String service_name;

        private String task_id;

        private int distance;

        private String flexible_week;

        private String created_at;

        private String request_id;

        private String address;

        private String work_date;

        private String work_time;

        private String work_details;

        private String image1;

        private String image2;

        private String image3;

        private String category_image;

        private String isBidSubmitted;

        private String bid_last_date;

        public void setService_name(String service_name){
            this.service_name = service_name;
        }
        public String getService_name(){
            return this.service_name;
        }
        public void setTask_id(String task_id){
            this.task_id = task_id;
        }
        public String getTask_id(){
            return this.task_id;
        }
        public void setDistance(int distance){
            this.distance = distance;
        }
        public int getDistance(){
            return this.distance;
        }
        public void setFlexible_week(String flexible_week){
            this.flexible_week = flexible_week;
        }
        public String getFlexible_week(){
            return this.flexible_week;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public void setRequest_id(String request_id){
            this.request_id = request_id;
        }
        public String getRequest_id(){
            return this.request_id;
        }
        public void setAddress(String address){
            this.address = address;
        }
        public String getAddress(){
            return this.address;
        }
        public void setWork_date(String work_date){
            this.work_date = work_date;
        }
        public String getWork_date(){
            return this.work_date;
        }
        public void setWork_time(String work_time){
            this.work_time = work_time;
        }
        public String getWork_time(){
            return this.work_time;
        }
        public void setWork_details(String work_details){
            this.work_details = work_details;
        }
        public String getWork_details(){
            return this.work_details;
        }
        public void setImage1(String image1){
            this.image1 = image1;
        }
        public String getImage1(){
            return this.image1;
        }
        public void setImage2(String image2){
            this.image2 = image2;
        }
        public String getImage2(){
            return this.image2;
        }
        public void setImage3(String image3){
            this.image3 = image3;
        }
        public String getImage3(){
            return this.image3;
        }
        public void setCategory_image(String category_image){
            this.category_image = category_image;
        }
        public String getCategory_image(){
            return this.category_image;
        }
        public void setIsBidSubmitted(String isBidSubmitted){
            this.isBidSubmitted = isBidSubmitted;
        }
        public String getIsBidSubmitted(){
            return this.isBidSubmitted;
        }
        public void setBid_last_date(String bid_last_date){
            this.bid_last_date = bid_last_date;
        }
        public String getBid_last_date(){
            return this.bid_last_date;
        }
    }

}
