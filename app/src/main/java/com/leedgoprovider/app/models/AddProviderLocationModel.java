package com.leedgoprovider.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AddProviderLocationModel {
    @SerializedName("status")
    @Expose
    int status;

    @SerializedName("message")
    @Expose
    String message;

    @SerializedName("provider_location_id")
    @Expose
    String  provider_location_id;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getProvider_location_id() {
        return provider_location_id;
    }

    public void setProvider_location_id(String provider_location_id) {
        this.provider_location_id = provider_location_id;
    }
}
