package com.leedgoprovider.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProviderAccountDetailModel implements Serializable {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data implements Serializable {

        @SerializedName("account_detail_id")
        @Expose
        private String accountDetailId;
        @SerializedName("firstname")
        @Expose
        private String firstname;
        @SerializedName("lastname")
        @Expose
        private String lastname;
        @SerializedName("dob")
        @Expose
        private String dob;
        @SerializedName("line1")
        @Expose
        private String line1;
        @SerializedName("line2")
        @Expose
        private String line2;
        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("state")
        @Expose
        private String state;
        @SerializedName("country")
        @Expose
        private String country;
        @SerializedName("postal_code")
        @Expose
        private String postalCode;
        @SerializedName("id_number")
        @Expose
        private String idNumber;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("ssn_last_4")
        @Expose
        private String ssnLast4;
        @SerializedName("front_document")
        @Expose
        private String frontDocument;
        @SerializedName("back_document")
        @Expose
        private String backDocument;
        @SerializedName("bank_acc_holder_name")
        @Expose
        private String bankAccHolderName;
        @SerializedName("bank_acc_number")
        @Expose
        private String bankAccNumber;
        @SerializedName("bank_routing_number")
        @Expose
        private String bankRoutingNumber;
        @SerializedName("verified")
        @Expose
        private String verified;
        @SerializedName("enabled")
        @Expose
        private String enabled;
        @SerializedName("provider_id")
        @Expose
        private String providerId;
        @SerializedName("stripe_account_id")
        @Expose
        private String stripeAccountId;
        @SerializedName("stripe_bank_id")
        @Expose
        private String stripeBankId;
        @SerializedName("creation_time")
        @Expose
        private String creationTime;

        public String getAccountDetailId() {
            return accountDetailId;
        }

        public void setAccountDetailId(String accountDetailId) {
            this.accountDetailId = accountDetailId;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getLine1() {
            return line1;
        }

        public void setLine1(String line1) {
            this.line1 = line1;
        }

        public String getLine2() {
            return line2;
        }

        public void setLine2(String line2) {
            this.line2 = line2;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getPostalCode() {
            return postalCode;
        }

        public void setPostalCode(String postalCode) {
            this.postalCode = postalCode;
        }

        public String getIdNumber() {
            return idNumber;
        }

        public void setIdNumber(String idNumber) {
            this.idNumber = idNumber;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getSsnLast4() {
            return ssnLast4;
        }

        public void setSsnLast4(String ssnLast4) {
            this.ssnLast4 = ssnLast4;
        }

        public String getFrontDocument() {
            return frontDocument;
        }

        public void setFrontDocument(String frontDocument) {
            this.frontDocument = frontDocument;
        }

        public String getBackDocument() {
            return backDocument;
        }

        public void setBackDocument(String backDocument) {
            this.backDocument = backDocument;
        }

        public String getBankAccHolderName() {
            return bankAccHolderName;
        }

        public void setBankAccHolderName(String bankAccHolderName) {
            this.bankAccHolderName = bankAccHolderName;
        }

        public String getBankAccNumber() {
            return bankAccNumber;
        }

        public void setBankAccNumber(String bankAccNumber) {
            this.bankAccNumber = bankAccNumber;
        }

        public String getBankRoutingNumber() {
            return bankRoutingNumber;
        }

        public void setBankRoutingNumber(String bankRoutingNumber) {
            this.bankRoutingNumber = bankRoutingNumber;
        }

        public String getVerified() {
            return verified;
        }

        public void setVerified(String verified) {
            this.verified = verified;
        }

        public String getEnabled() {
            return enabled;
        }

        public void setEnabled(String enabled) {
            this.enabled = enabled;
        }

        public String getProviderId() {
            return providerId;
        }

        public void setProviderId(String providerId) {
            this.providerId = providerId;
        }

        public String getStripeAccountId() {
            return stripeAccountId;
        }

        public void setStripeAccountId(String stripeAccountId) {
            this.stripeAccountId = stripeAccountId;
        }

        public String getStripeBankId() {
            return stripeBankId;
        }

        public void setStripeBankId(String stripeBankId) {
            this.stripeBankId = stripeBankId;
        }

        public String getCreationTime() {
            return creationTime;
        }

        public void setCreationTime(String creationTime) {
            this.creationTime = creationTime;
        }


    }
}