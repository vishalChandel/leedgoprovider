package com.leedgoprovider.app.models;

import com.google.gson.annotations.SerializedName;

public class ForgotPwdModel{

	@SerializedName("success")
	private boolean success;

	@SerializedName("provider_id")
	private String providerId;

	@SerializedName("message")
	private String message;

	@SerializedName("status")
	private int status;

	public void setSuccess(boolean success){
		this.success = success;
	}

	public boolean isSuccess(){
		return success;
	}

	public void setProviderId(String providerId){
		this.providerId = providerId;
	}

	public String getProviderId(){
		return providerId;
	}

	public void setMessage(String message){
		this.message = message;
	}

	public String getMessage(){
		return message;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ForgotPwdModel{" + 
			"success = '" + success + '\'' + 
			",provider_id = '" + providerId + '\'' + 
			",message = '" + message + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}