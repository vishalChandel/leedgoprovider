package com.leedgoprovider.app.models.profile;

import com.google.gson.annotations.SerializedName;

public class ProfileBusinessHours {

	@SerializedName("start_time")
	private String startTime;

	@SerializedName("end_time")
	private String endTime;

	@SerializedName("provider_id")
	private String providerId;

	@SerializedName("id")
	private String id;

	@SerializedName("day")
	private String day;

	public void setStartTime(String startTime){
		this.startTime = startTime;
	}

	public String getStartTime(){
		return startTime;
	}

	public void setEndTime(String endTime){
		this.endTime = endTime;
	}

	public String getEndTime(){
		return endTime;
	}

	public void setProviderId(String providerId){
		this.providerId = providerId;
	}

	public String getProviderId(){
		return providerId;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setDay(String day){
		this.day = day;
	}

	public String getDay(){
		return day;
	}

	@Override
 	public String toString(){
		return
			"BusinessHoursItem{" +
			"start_time = '" + startTime + '\'' +
			",end_time = '" + endTime + '\'' +
			",provider_id = '" + providerId + '\'' +
			",id = '" + id + '\'' +
			",day = '" + day + '\'' +
			"}";
		}
}