package com.leedgoprovider.app.fonts;

import android.content.Context;
import android.graphics.Typeface;


public class AvenirMedium {
    /*
     * Initialize the Typeface
     * */
    public static Typeface fontTypeface;
    /*
     * Custom 3rd parth Font family
     * */
    public String path = "AvenirMedium.otf";
    /*
     * Initialize Context
     * */
    public Context mContext;
    /*
     * Default Constructor
     * */
    public AvenirMedium() {
    }

    /*
     * Constructor with Context
     * */
    public AvenirMedium(Context context) {
        mContext = context;
    }

    /*
     * Getting the Font Familty from the Assets Folder
     * */
    public Typeface getFont() {
        if (fontTypeface == null)
            fontTypeface = Typeface.createFromAsset(mContext.getAssets(), path);
        return fontTypeface;
    }

}