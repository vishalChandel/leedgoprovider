package com.leedgoprovider.app.fonts;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Button;

/**
 * Created by Dharmani Apps
 */


/*
 * Button with Custom font family
 * */


public class ButtonMedium extends Button {


    /*
     * Getting Current Class Name
     * */

    private String mTag = ButtonMedium.this.getClass().getSimpleName();


    /*
     * Constructor with
     * #Context
     * */

    public ButtonMedium(Context context) {
        super(context);
        applyCustomFont(context);
    }



    /*
     * Constructor with
     * #Context
     * #AttributeSet
     * */

    public ButtonMedium(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyCustomFont(context);
    }


    /*
     * Constructor with
     * #Context
     * #AttributeSet
     * #int defStyleAttr
     * */

    public ButtonMedium(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyCustomFont(context);
    }



    /*
     * Constructor with
     * #Context
     * #AttributeSet
     * #int defStyleAttr
     * #int defStyleAttr
     * */

    public ButtonMedium(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        applyCustomFont(context);
    }


    /*
     * Apply font.
     * */

    public void applyCustomFont(Context context) {
        try {
            this.setTypeface(new AvenirMedium(context).getFont());
        } catch (Exception e) {
            Log.e(mTag,e.toString());
        }
    }
}
