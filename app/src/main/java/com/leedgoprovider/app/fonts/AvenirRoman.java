package com.leedgoprovider.app.fonts;

import android.content.Context;
import android.graphics.Typeface;

class AvenirRoman {
    /*
     * Initialize the Typeface
     * */
    public static Typeface fontTypeface;
    /*
     * Custom 3rd parth Font family
     * */
    public String path = "AvenirRegular.otf";
    /*
     * Initialize Context
     * */
    public Context mContext;
    /*
     * Default Constructor
     * */
    public AvenirRoman() {
    }

    /*
     * Constructor with Context
     * */
    public AvenirRoman(Context context) {
        mContext = context;
    }

    /*
     * Getting the Font Familty from the Assets Folder
     * */
    public Typeface getFont() {
        if (fontTypeface == null)
            fontTypeface = Typeface.createFromAsset(mContext.getAssets(), path);
        return fontTypeface;
    }

}
